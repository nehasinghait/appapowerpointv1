﻿using AppaDocx.Common;
using AppaDocx.Helpers;
using AppaPowerpoint.WatermarkHelpers;
using Microsoft.Office.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Xml.Serialization;
using Office = Microsoft.Office.Core;

// TODO:  Follow these steps to enable the Ribbon (XML) item:

// 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new rbnShowUI();
//  }

// 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
//    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
//    move your code from the event handlers to the callback methods and modify the code to work with the
//    Ribbon extensibility (RibbonX) programming model.

// 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

// For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.


namespace AppaPowerpoint.Ribbon
{
    [ComVisible(true)]
    public class rbnShowUI : Office.IRibbonExtensibility
    {
        private Office.IRibbonUI ribbon;

        public rbnShowUI()
        {

        }



        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("AppaPowerpoint.Ribbon.rbnShowUI.xml");
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, visit https://go.microsoft.com/fwlink/?LinkID=271226

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;
        }

        //satish added
        #region commented code
        // private AppaPowerpointUserControl myUserControl1;
        //private Microsoft.Office.Tools.CustomTaskPane myCustomTaskPane; 
        #endregion
        public void OnActionCallback(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.create_Taskpane(Globals.ThisAddIn.Application.ActivePresentation);
            #region Comemnted code
            //AppaPowerpointUI myWpfPage = new AppaPowerpointUI();
            //myUserControl1 = new MyUserControl(myWpfPage);
            // myCustomTaskPane.Visible = false;
            // Globals.ThisAddIn. create_Taskpane(Globals.ThisAddIn.Application.ActiveDocument); 
            #endregion
        }

        public void OnClickReuseButton(Office.IRibbonControl control)
        {
            //test
            ReuseSlidePaneOption reuseSlidePaneOption = new ReuseSlidePaneOption();
            reuseSlidePaneOption.Show();
        }

        public void OnClickSetPropertyButton(Office.IRibbonControl control)
        {
            PropertyManager propertyManager = new PropertyManager();

            Microsoft.Office.Interop.PowerPoint.Application application = Globals.ThisAddIn.Application;
            Microsoft.Office.Interop.PowerPoint.Presentation presentation = application.ActivePresentation;

            if (!propertyManager.IsPresentaionSaved(presentation))
            {
                MD_MsgBox.MaterialDesign_MessageBoxOK("Please Save Presentaion First to Set the Properties");
                return;
            }

            if (!propertyManager.HaveSlides(presentation))
            {
                MD_MsgBox.MaterialDesign_MessageBoxOK("Current Presentation does not have any Slides.");
                return;
            }

            //2.for each slide set the properties
            if (propertyManager.SetSlideProperties(presentation))
            {
                MD_MsgBox.MaterialDesign_MessageBoxOK("Property is now set for all slide.");
            }
            else
            {
                MD_MsgBox.MaterialDesign_MessageBoxOK("Something went wrong while setting properties.");
            }

            //3. try to get Property
            List<SlideProperties> SlidePropertiesList = propertyManager.GetSlideProperties(presentation);
            MD_MsgBox.MaterialDesign_MessageBoxShow(Newtonsoft.Json.JsonConvert.SerializeObject(SlidePropertiesList), 1);

            //test
            //PropertyManager obj = new PropertyManager();
            //obj.CreatePropertyCollection(Newtonsoft.Json.JsonConvert.SerializeObject(SlidePropertiesList));

        }

        private static readonly System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
        public void OnClickCheckSlideUpdateButton(Office.IRibbonControl control)
        {
            try
            {
                //var tes = Globals.ThisAddIn.Application.ActivePresentation.Slides[1].CustomLayout.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 200, 200, 600, 100);

                //tes.TextFrame.TextRange.Text = "updated..";
                //tes.Rotation = -45;
                //tes.TextFrame.TextRange.Font.Color.RGB = Color.LightGray.ToArgb();
                //tes.TextFrame.TextRange.Font.Size = 48;
                //return;

                PropertyManager propertyManager = new PropertyManager();
                List<SlideProperties> SlidePropertiesList = propertyManager.GetSlideProperties(Globals.ThisAddIn.Application.ActivePresentation);
                string LocalSlideProperties = JsonConvert.SerializeObject(SlidePropertiesList);
                if (SlidePropertiesList.Count == 0)
                {
                    MD_MsgBox.MaterialDesign_MessageBoxOK("Reused slides not have properties. cant check for updates on server.");
                }

                #region commented code
                ////test code
                ////HttpWebRequest request = WebRequest.Create("http://localhost:59012/AppaPowerpointWebService.asmx/HelloWorld?a=abc") as HttpWebRequest;
                //HttpWebRequest request = WebRequest.Create("http://localhost:59012/AppaPowerpointWebService.asmx/GetUpdatedTimestampColllection?ClientSlidePropertiesJson=" + LocalSlideProperties) as HttpWebRequest;

                //request.KeepAlive = true;
                //request.Timeout = 15000;  //Decrease timeout
                //request.Proxy = null;
                //request.Method = "POST";
                ////
                //byte[] bytes = Encoding.UTF8.GetBytes(LocalSlideProperties);
                //request.ContentLength = bytes.Length;
                //System.IO.Stream os = request.GetRequestStream();
                //os.Write(bytes, 0, bytes.Length); //Push it out there
                //os.Close();
                //request.ContentType = "application/x-www-form-urlencoded";// "application/json";
                //WebResponse response = request.GetResponse();
                //Stream objStream = response.GetResponseStream();
                //StreamReader streamReader = new StreamReader(objStream);
                //string resp = streamReader.ReadToEnd();
                //MD_MsgBox.MaterialDesign_MessageBoxShow(resp, 1);
                ////end
                /// // 
                #endregion

                #region commentedd code 
                // #region Commented code for web call
                // var url = "http://localhost:59012/AppaPowerpointWebService.asmx/GetUpdatedTimestampColllection"; //"http://192.168.1.163/PPTPOC/AppaPowerpointWebService.asmx/GetUpdatedTimestampColllection"; //
                ///// Uri myUri = new Uri(url, UriKind.Absolute);
                // var client = new WebClient();
                // var method = "POST"; // If your endpoint expects a GET then do it.
                // var parameters = new System.Collections.Specialized.NameValueCollection();

                // parameters.Add("ClientSlidePropertiesJson", LocalSlideProperties);

                // //client.OpenRead(myUri);

                // var response_data = client.UploadValues(url, method, parameters);
                // var responseString = UnicodeEncoding.UTF8.GetString(response_data);
                // #endregion
                //


                //through web req
                //WebRequest request = WebRequest.Create("http://localhost:59012/AppaPowerpointWebService.asmx/GetUpdatedTimestampColllection");
                //request.Method = "POST";
                //byte[] byteArray = Encoding.UTF8.GetBytes(LocalSlideProperties);
                //request.ContentType = "application/octet-stream";//"application/json";
                //request.ContentLength = byteArray.Length;
                ////
                //request.Timeout = 1500000000;  //Decrease timeout
                //request.Proxy = null;


                //Stream dataStream = request.GetRequestStream();
                //dataStream.Write(byteArray, 0, byteArray.Length);
                //dataStream.Close();
                //WebResponse response = request.GetResponse();
                //MessageBox.Show(((HttpWebResponse)response).StatusDescription);
                //dataStream = response.GetResponseStream();
                //StreamReader reader = new StreamReader(dataStream);
                //string responseFromServer = reader.ReadToEnd();

                //var request = (HttpWebRequest)WebRequest.Create("http://localhost:59012/AppaPowerpointWebService.asmx/GetUpdatedTimestampColllection");
                //var postData = "ClientSlidePropertiesJson=" + LocalSlideProperties;
                //var data = Encoding.ASCII.GetBytes(postData);
                //request.Method = "POST";
                //request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentLength = data.Length;
                //request.Timeout = 1500000000;
                //request.Proxy = null;
                //using (var stream = request.GetRequestStream())
                //{
                //    stream.Write(data, 0, data.Length);
                //}
                //var response = (HttpWebResponse)request.GetResponse();// GetResponse();
                //var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd(); 
                #endregion

                //*******check slide image compare

                //1. converting current ppt slide into image
                SlideImageForLocal slideImageForLocal = new SlideImageForLocal();
                var SlideIdAndPathList = slideImageForLocal.ConvertPresentationSlidesToImage(Globals.ThisAddIn.Application.ActivePresentation);
                
                //2. getting stream dictionary of local slide images
                Dictionary<string, byte[]> FileNameAndStream = slideImageForLocal.GetByteStreamOfSlideImages(SlideIdAndPathList); //we need list of PropertyPath_slideID, stream [local ]

                int index = 0;
                foreach (SlideProperties item in SlidePropertiesList)
                {
                    #region SLIDE IMAGE STREAM COMPARISON
                    ////http://localhost/AppaPPT/AppaPowerpointWebService.asmx?op=HelloWorld

                    ////var request = (HttpWebRequest)WebRequest.Create("http://192.168.1.163/AppaPPT/AppaPowerpointWebService.asmx/GetSlideImageStream"); //get checksum and compair that checksum
                    //var request = (HttpWebRequest)WebRequest.Create("http://localhost:59012/AppaPowerpointWebService.asmx/GetSlideImageStream");
                    //var postData = "FilePathAndSlideId=" + item.PPTPath + "^" + item.SlideID;

                    //var data = Encoding.ASCII.GetBytes(postData);
                    //request.Method = "POST";
                    //request.ContentType = "application/x-www-form-urlencoded";
                    //request.ContentLength = data.Length;

                    //using (var stream = request.GetRequestStream())
                    //{
                    //    stream.Write(data, 0, data.Length);
                    //}
                    //var response = (HttpWebResponse)request.GetResponse();// GetResponse();
                    //var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    //List<ImageResponse> ImgDetailFromServer = JsonConvert.DeserializeObject<List<ImageResponse>>(responseString);

                    //var ServerSlideImageStream = ImgDetailFromServer[0].SlideImageStreams;
                    //var LocalSlideImageStream = FileNameAndStream[ImgDetailFromServer[0].PPTPath + "^" + ImgDetailFromServer[0].SlideId + ".png"];
                    #endregion

                    #region SLIDE IMAGE CHECKSUM COMPARISON
                    var request = (HttpWebRequest)WebRequest.Create("http://localhost:59012/AppaPowerpointWebService.asmx/GetSlideImageChecksum");//for localhost
                    //var request = (HttpWebRequest)WebRequest.Create("http://192.168.1.163/AppaPPT/AppaPowerpointWebService.asmx/GetSlideImageChecksum"); //for IIS server. Neha's pc
                     
                    var postData = "FilePathAndSlideId=" + item.PPTPath + "^" + item.SlideID;

                    var data = Encoding.ASCII.GetBytes(postData);
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = data.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();// GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    List<ImageResponse> ListImageResponse = JsonConvert.DeserializeObject<List<ImageResponse>>(responseString);
                    if (ListImageResponse.Count == 0)
                    {
                        string slideNo = Globals.ThisAddIn.Application.ActivePresentation.Slides.FindBySlideID(item.LocalSlideId).SlideNumber.ToString();
                        MD_MsgBox.MaterialDesign_MessageBoxOK("Web service returns blank response. Reused slide no:"+ slideNo + " not found on server. "+
                            "or The Slide might be a copy of previously reused slide, with changes.");
                        return;
                    }
                    #endregion

                    bool IsSame = false;
                    if (item.SlideImageChecksum == ListImageResponse[0].SlideImageChecksum)
                    {
                        IsSame = true;
                    }
                    //comparison

                    //IsSame = SlideImageForLocal.CompareMemoryStreams(LocalSlideImageStream, ServerSlideImageStream);
                    if (!IsSame)
                    {
                        // MD_MsgBox.MaterialDesign_MessageBoxOK("Slide index no =" + index.ToString() + " is Updated On server.");
                        //watermark

                        var test = Globals.ThisAddIn.Application.ActivePresentation.Slides.FindBySlideID(item.LocalSlideId).Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 250, 600, 100);

                        test.TextFrame.TextRange.Text = "Slide Updated on Server...";
                        test.Rotation = -50;
                        test.TextFrame.TextRange.Font.Color.RGB = Color.LightGray.ToArgb();
                        test.TextFrame.TextRange.Font.Size = 48;
                        test.Shadow.Visible = MsoTriState.msoTrue;
                        test.Fill.BackColor.RGB = Color.LightGreen.ToArgb();
                        test.Fill.Transparency = 0.8f;

                        //watermark end
                    }
                    else
                    {
                        string slideNo = Globals.ThisAddIn.Application.ActivePresentation.Slides.FindBySlideID(item.LocalSlideId).SlideNumber.ToString();
                        MD_MsgBox.MaterialDesign_MessageBoxOK("Slide no =" + slideNo + " is Same On server.");
                    }


                    #region commented code
                    //XmlSerializer serializer = new XmlSerializer(typeof(ImageResponse[]));
                    //StreamReader reader = new StreamReader("D:\\xmlDoc.txt"); //new StreamReader(responseString);
                    //var test = (ImageResponse)serializer.Deserialize(reader);
                    //reader.Close(); 
                    #endregion

                    index++;


                }
            }
            catch (WebException webex)
            {
                WebResponse errResp = webex.Response;
                using (Stream respStream = errResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respStream);
                    string text = reader.ReadToEnd();
                    MessageBox.Show(text);
                }
            }
        }

        //
        public void OnClickGetSlideToImages(Office.IRibbonControl control)
        {
            try
            {
                //WebClient client = new WebClient();
                //string reply = client.DownloadString("http://192.168.1.163/PPTPOC/WebService.asmx/HelloWorld");////("http://192.168.1.176/Test/WebService.asmx/HelloWorld");//192.168.1.163 // 192.168.1.176
                //return;
                PropertyManager propertyManager = new PropertyManager();
                propertyManager.ConvertPresentationSlidesToImage(Globals.ThisAddIn.Application.ActivePresentation);
                MD_MsgBox.MaterialDesign_MessageBoxOK("Process Done.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void OnClickPublishSlides(Office.IRibbonControl control)
        {
            try
            {
                Microsoft.Office.Interop.PowerPoint.Application application = Globals.ThisAddIn.Application;
                Microsoft.Office.Interop.PowerPoint.Presentation presentation = application.ActivePresentation;

                SlideImageForServer objSlideImg = new SlideImageForServer();
                objSlideImg.PublishSlides(presentation);
                objSlideImg.RemoveSlideImages();
                presentation.Save();

                MD_MsgBox.MaterialDesign_MessageBoxOK("Details inserted in DB.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //
        public void OnClickUpdatePropertyButton(Office.IRibbonControl control)
        {



        }

        /// <summary>
        /// this method show appatura ribbon if user is logged in
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool GetVisible(Office.IRibbonControl control)
        {

            return true;
        }

        public Bitmap imageSuper_GetImage(Microsoft.Office.Core.IRibbonControl control)
        {
            Bitmap objBitmap = null;
            try
            {
                #region commented code
                //if (AppaDocx.Common.GlobalsData.dir == null)
                //{
                //    AppaDocx.Common.GlobalsData.dir = AppDomain.CurrentDomain.BaseDirectory;
                //}
                //else if (AppaDocx.Common.GlobalsData.dir == "")
                //{
                //    AppaDocx.Common.GlobalsData.dir = AppDomain.CurrentDomain.BaseDirectory;
                //}
                //else if (!AppaDocx.Common.GlobalsData.dir.Contains("TemplatesRequires"))
                //{
                //    AppaDocx.Common.GlobalsData.dir += "\\TemplatesRequires";
                //} 
                #endregion

                objBitmap = (Bitmap)Image.FromFile(AppaDocx.Common.GlobalsData.rootDir + "//Images//ui.png", true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to get the Icon Image for task pane UI: " + ex.Message);
            }
            return (objBitmap);
        }

        public Bitmap imageReuseSlidePane_GetImage(Microsoft.Office.Core.IRibbonControl control)
        {
            Bitmap objBitmap = null;
            try
            {
                objBitmap = (Bitmap)Image.FromFile(AppaDocx.Common.GlobalsData.rootDir + "//Images//reuse.png", true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to get the Icon Image for task pane UI: " + ex.Message);
            }
            return (objBitmap);
        }

        public Bitmap imageSetProperties_GetImage(Microsoft.Office.Core.IRibbonControl control)
        {
            Bitmap objBitmap = null;
            try
            {
                objBitmap = (Bitmap)Image.FromFile(AppaDocx.Common.GlobalsData.rootDir + "//Images//SetProperty.png", true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to get the Icon Image for task pane UI: " + ex.Message);
            }
            return (objBitmap);
        }

        public Bitmap imageUpdateProperties_GetImage(Microsoft.Office.Core.IRibbonControl control)
        {
            Bitmap objBitmap = null;
            try
            {
                objBitmap = (Bitmap)Image.FromFile(AppaDocx.Common.GlobalsData.rootDir + "//Images//UpdateProperty.jpg", true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to get the Icon Image for task pane UI: " + ex.Message);
            }
            return (objBitmap);
        }

        public Bitmap imageSlideUpdate_GetImage(Microsoft.Office.Core.IRibbonControl control)
        {
            Bitmap objBitmap = null;
            try
            {
                objBitmap = (Bitmap)Image.FromFile(AppaDocx.Common.GlobalsData.rootDir + "//Images//watermarkUpdate.png", true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to get the Icon Image for task pane UI: " + ex.Message);
            }
            return (objBitmap);
        }

        public Bitmap Convert_To_Image_GetImage(Microsoft.Office.Core.IRibbonControl control)
        {
            Bitmap objBitmap = null;
            try
            {
                objBitmap = (Bitmap)Image.FromFile(AppaDocx.Common.GlobalsData.rootDir + "//Images//SlideToImg.png", true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to get the Icon Image for task pane UI: " + ex.Message);
            }
            return (objBitmap);
        }

        public Bitmap PublishSlideIcon(Microsoft.Office.Core.IRibbonControl control)
        {
            Bitmap objBitmap = null;
            try
            {
                objBitmap = (Bitmap)Image.FromFile(AppaDocx.Common.GlobalsData.rootDir + "//Images//publishSlides.png", true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to get the Icon Image for task pane UI: " + ex.Message);
            }
            return (objBitmap);
        }
        #endregion

        #region Helpers
        private static string GetResourceText(string resourceName)
        {

            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion


    }
}
