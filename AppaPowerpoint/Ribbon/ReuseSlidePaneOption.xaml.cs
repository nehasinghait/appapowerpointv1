﻿using AppaDocx.Common;
using AppPowerpoint.StorageConnectors;
using OneDriveApiBrowser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppaPowerpoint.Ribbon
{
    /// <summary>
    /// Interaction logic for ReuseSlidePaneOption.xaml
    /// </summary>
    public partial class ReuseSlidePaneOption : Window
    {
        public ReuseSlidePaneOption()
        {
            InitializeComponent();
        }



        #region commentedCode

        //private void cboStorageConnector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //   // this.Close();
        //    #region Code for Storage Connectors
        //    if (cboStorageConnector.SelectedIndex > 0)
        //    {
        //        ComboBoxItem cboselectedItem = (ComboBoxItem)cboStorageConnector.SelectedItem;

        //        if (cboselectedItem.Content.ToString() == "Google Drive")
        //        {
        //            //LoginGoogleDrive obj = new LoginGoogleDrive();
        //            //obj.Show();
        //            //obj.Navigate("https://drive.google.com/");
        //            var service = AppPowerpoint.StorageConnectors.GoogleDriveAccess.AuthenticateOauth("C:\\Users\\Srijan\\Desktop\\appaPowerpoint\\AppaPowerPoint\\AppaPowerpoint\\AppaPowerpoint\\AppaPowerpoint\\StorageConnectors\\credentials.json", "AppaPowerPointUser3");

        //            //code for fetch the file details from the drive.
        //            var files = AppPowerpoint.StorageConnectors.GoogleDriveAccess.ListFiles(service,
        //                      // new FilesListOptionalParms() { Q = "mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'" });//satish comment and add this below line
        //                      //new FilesListOptionalParms() { Q = "mimeType = 'application/vnd.google-apps.folder'" }); //for get the folder
        //                      new FilesListOptionalParms() { Q = "mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'" }); //for presentaion PPT

        //            List<Google.Apis.Drive.v3.Data.File> DriveFiles = new List<Google.Apis.Drive.v3.Data.File>();
        //            foreach (var item in files.Files)
        //            {
        //                DriveFiles.Add(item);
        //            }
        //            FileList objFileList = new FileList();
        //            objFileList.PopulateFileButtons(service, DriveFiles);
        //            objFileList.Show();

        //            //////Code for download the file
        //            //AppPowerpoint.StorageConnectors.GoogleDriveAccess.DownloadFile(service, item, "D:\\" + item.Name);
        //            //Google.Apis.Drive.v3.Data.File file = service.Files.Get(item.Id).Execute();


        //            //// //var bytes = service.HttpClient.GetByteArrayAsync(item.); 



        //            //Process.Start("https://drive.google.com/");


        //            #region Code for download file from google drive
        //            //foreach (var item in files.Files)
        //            //{
        //            //    // download each file
        //            //    //Download(service, item, string.Format(@"C:\FilesFromGoogleDrive\{0}", item.Name));
        //            //    AppPowerpoint.StorageConnectors.GoogleDriveAccess.DownloadFile(service, item, "E:\\Download\\" + item.Name);

        //            //    //Google.Apis.Drive.v3.Data.File file = service.Files.Get(item.Id).Execute();
        //            //    //var bytes = service.HttpClient.GetByteArrayAsync(item.);
        //            //}

        //            //foreach (var item in files.Files)
        //            //{
        //            //    var fileId = item.Id;
        //            //    var request = service.Files.Export(fileId, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        //            //    var stream = new System.IO.MemoryStream();
        //            //    // Add a handler which will be notified on progress changes.
        //            //    // It will notify on each chunk download and when the
        //            //    // download is completed or failed.
        //            //    request.MediaDownloader.ProgressChanged +=
        //            //            (IDownloadProgress progress) =>
        //            //            {
        //            //                switch (progress.Status)
        //            //                {
        //            //                    case DownloadStatus.Downloading:
        //            //                        {
        //            //                            Console.WriteLine(progress.BytesDownloaded);
        //            //                            break;
        //            //                        }
        //            //                    case DownloadStatus.Completed:
        //            //                        {
        //            //                            Console.WriteLine("Download complete.");
        //            //                            break;
        //            //                        }
        //            //                    case DownloadStatus.Failed:
        //            //                        {
        //            //                            Console.WriteLine("Download failed.");
        //            //                            break;
        //            //                        }
        //            //                }
        //            //            };
        //            //    request.Download(stream);

        //            //} 
        //            #endregion

        //        }
        //        else if (cboselectedItem.Content.ToString() == "DropBox")
        //        {
        //            MainWindow objMainWindow = new MainWindow();
        //            objMainWindow.Authenticate();

        //        }
        //        else if (cboselectedItem.Content.ToString() == "Microsoft OneDrive")
        //        {
        //            FormBrowser form = new FormBrowser();
        //            form.SignIn();







        //        }
        //    }
        //    #endregion
        //}

        #endregion
        private void btnSharedServer_Selected(object sender, RoutedEventArgs e)
        {

            Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
            ppApp.CommandBars.ExecuteMso("SlidesReuseSlides");
            Microsoft.Office.Interop.PowerPoint.Pane activePane = ppApp.ActiveWindow.ActivePane;
            activePane.Activate();
            // this.Close();

            CloseCurrentWindow(this);
        }

        private void CloseCurrentWindow(ReuseSlidePaneOption reuseSlidePaneOption)
        {
            this.Visibility = Visibility.Hidden;
            this.Hide();
        }

        private void btnDropBox_Selected(object sender, RoutedEventArgs e)
        {
            MainWindow objMainWindow = new MainWindow();
            objMainWindow.Authenticate();
            CloseCurrentWindow(this);
        }

        private void btnGoogleDrive_Selected(object sender, RoutedEventArgs e)
        {
            //LoginGoogleDrive obj = new LoginGoogleDrive();
            //obj.Show();
            //obj.Navigate("https://drive.google.com/");
            var service = AppPowerpoint.StorageConnectors.GoogleDriveAccess.AuthenticateOauth(GlobalsData.rootDir + "\\" + "StorageConnectors\\credentials.json", "AppaPowerPointUser3");

            //code for fetch the file details from the drive.
            var files = AppPowerpoint.StorageConnectors.GoogleDriveAccess.ListFiles(service,
                      // new FilesListOptionalParms() { Q = "mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'" });//satish comment and add this below line
                      //new FilesListOptionalParms() { Q = "mimeType = 'application/vnd.google-apps.folder'" }); //for get the folder
                      new FilesListOptionalParms() { Q = "mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'" }); //for presentaion PPT

            List<Google.Apis.Drive.v3.Data.File> DriveFiles = new List<Google.Apis.Drive.v3.Data.File>();
            foreach (var item in files.Files)
            {
                DriveFiles.Add(item);
            }
            FileList objFileList = new FileList();
            objFileList.PopulateFileButtons(service, DriveFiles);
            objFileList.Show();

            //////Code for download the file
            //AppPowerpoint.StorageConnectors.GoogleDriveAccess.DownloadFile(service, item, "D:\\" + item.Name);
            //Google.Apis.Drive.v3.Data.File file = service.Files.Get(item.Id).Execute();


            //// //var bytes = service.HttpClient.GetByteArrayAsync(item.); 



            //Process.Start("https://drive.google.com/");


            #region Code for download file from google drive
            //foreach (var item in files.Files)
            //{
            //    // download each file
            //    //Download(service, item, string.Format(@"C:\FilesFromGoogleDrive\{0}", item.Name));
            //    AppPowerpoint.StorageConnectors.GoogleDriveAccess.DownloadFile(service, item, "E:\\Download\\" + item.Name);

            //    //Google.Apis.Drive.v3.Data.File file = service.Files.Get(item.Id).Execute();
            //    //var bytes = service.HttpClient.GetByteArrayAsync(item.);
            //}

            //foreach (var item in files.Files)
            //{
            //    var fileId = item.Id;
            //    var request = service.Files.Export(fileId, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            //    var stream = new System.IO.MemoryStream();
            //    // Add a handler which will be notified on progress changes.
            //    // It will notify on each chunk download and when the
            //    // download is completed or failed.
            //    request.MediaDownloader.ProgressChanged +=
            //            (IDownloadProgress progress) =>
            //            {
            //                switch (progress.Status)
            //                {
            //                    case DownloadStatus.Downloading:
            //                        {
            //                            Console.WriteLine(progress.BytesDownloaded);
            //                            break;
            //                        }
            //                    case DownloadStatus.Completed:
            //                        {
            //                            Console.WriteLine("Download complete.");
            //                            break;
            //                        }
            //                    case DownloadStatus.Failed:
            //                        {
            //                            Console.WriteLine("Download failed.");
            //                            break;
            //                        }
            //                }
            //            };
            //    request.Download(stream);

            //} 
            #endregion


            CloseCurrentWindow(this);

        }

        private void btnOneDrive_Selected(object sender, RoutedEventArgs e)
        {
            FormBrowser form = new FormBrowser();
            form.SignIn();

            CloseCurrentWindow(this);
        }

        private void btnCloseWindow_Click(object sender, RoutedEventArgs e)
        {
            CloseCurrentWindow(this);
        } 

        private void btnMoveWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
