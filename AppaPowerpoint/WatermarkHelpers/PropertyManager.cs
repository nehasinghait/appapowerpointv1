﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.PowerPoint;
using AppaDocx.Helpers;
using Microsoft.Office.Core;
using AppaDocx.Common;
using System.Timers;
using System.IO;
using System.Security.Cryptography;

namespace AppaPowerpoint.WatermarkHelpers
{
    public class PropertyManager
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AppaPowerpointUI));

        public PropertyManager()
        {

        }

        internal bool HaveSlides(Presentation presentation)
        {

            if (presentation.Slides.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal bool SetSlideProperties(Presentation presentation)
        {
            try
            {
                List<int> slideIndexList = new List<int>();
                List<int> slideIDList = new List<int>();    //Added by Neha

                foreach (Slide slide in presentation.Slides)
                {
                    if(slideIDList.Contains(slide.SlideID)) //Added by Neha
                    {
                        UpdateSlideTags(presentation, slide);
                        
                        if (slide.Tags.Count > 0)
                        {
                            slideIndexList.Add(slide.SlideNumber);
                            continue;
                        }
                    }
                    else
                    {
                        slideIDList.Add(slide.SlideID);
                    }


                    if (slide.Tags.Count > 0)
                    {
                        slideIndexList.Add(slide.SlideNumber);
                        continue;
                    }

                    UpdateSlideTags(presentation, slide);

                }

                presentation.Save();

                if (slideIndexList.Count > 0)
                {
                    string msg = "Properties are already set for some slides. Properties can't be modified once set. Following are slide no which have properties already set.\n ";
                    string slideNos = string.Empty;
                    foreach (int item in slideIndexList)
                    {
                        if (slideNos == string.Empty)
                        {
                            slideNos = item.ToString();
                        }
                        else
                        {
                            slideNos += "," + item.ToString();
                        }

                    }
                    MD_MsgBox.MaterialDesign_MessageBoxOK(msg + slideNos);
                }

                return true;
            }
            catch (Exception ex)
            {
                log.Error("AppaPowerPoints", ex);
                return false;
            }


        }

        private void UpdateSlideTags(Presentation presentation, Slide slide)
        {
            string SlidePropertyJSON = Newtonsoft.Json.JsonConvert.SerializeObject(PropertyBuilder(slide, presentation.FullName, presentation.Name));
            slide.Tags.Add("SlideProperties", SlidePropertyJSON);
            MD_MsgBox.MaterialDesign_MessageBoxOK(SlidePropertyJSON);
        }

        internal List<SlideProperties> GetSlideProperties(Presentation presentation)
        {
            List<SlideProperties> SlidePropertyList = new List<SlideProperties>();

            foreach (Slide slide in presentation.Slides)
            {

                if (slide.Tags.Count > 0 && slide.Tags.Name(1).ToUpper() == "SlideProperties".ToUpper())
                {
                    string SlidePropertyJSON = slide.Tags.Value(1);
                    if (!string.IsNullOrEmpty(SlidePropertyJSON))
                    {
                        SlideProperties slideProperties = Newtonsoft.Json.JsonConvert.DeserializeObject<SlideProperties>(SlidePropertyJSON);
                        slideProperties.LocalSlideId = slide.SlideID;
                        slideProperties.SlideID = slide.SlideID;
                        SlidePropertyList.Add(slideProperties);
                    }

                }
            }

            return SlidePropertyList;

        }

        internal SlideProperties PropertyBuilder(Slide slide, string FullFilePath, string FileName)
        {
            SlideProperties slideProperties = new SlideProperties()
            {
                PPTName = FileName,
                PPTPath = FullFilePath,
                SlideID = slide.SlideID,
                LastTimeStamp = GetCurrentTimeStamp()
                
            };

            return slideProperties;
        }
        internal static bool UpdateTimestampOnSlide(Slide slide)
        {
            SlideProperties properties = Newtonsoft.Json.JsonConvert.DeserializeObject<SlideProperties>(slide.Tags["SlideProperties"]);
            properties.LastTimeStamp = GetCurrentTimeStamp();
            string SlidePropertyJSON = Newtonsoft.Json.JsonConvert.SerializeObject(properties);
            slide.Tags.Delete("SlideProperties");
            slide.Tags.Add("SlideProperties", SlidePropertyJSON);
            Globals.ThisAddIn.Application.ActivePresentation.Saved = MsoTriState.msoTrue; //Globals.ThisAddIn.Application.ActivePresentation.Save();
            System.Diagnostics.Debug.WriteLine(SlidePropertyJSON);
            return true;
        }

        internal static string GetCurrentTimeStamp()
        {
            string TimeStamp = System.DateTime.Now.Year.ToString() + "_" + System.DateTime.Now.Month.ToString() + "_" + System.DateTime.Now.Day.ToString()
                              + "_" + System.DateTime.Now.Hour.ToString() + "_" + System.DateTime.Now.Minute.ToString() + "_" + System.DateTime.Now.Second.ToString() + "_"
                              + System.DateTime.Now.Millisecond.ToString();
            return TimeStamp;
        }

        internal bool IsPresentaionSaved(Presentation presentation)
        {
            if (string.IsNullOrEmpty(presentation.Path))
            {
                return false;
            }
            return true;
        }

        internal List<Slide> WriteSlidesOnBufferPPT(string sourcePresentaionPath)
        {
            string BufferPPTLocation = GlobalsData.rootDir + "\\" + "WatermarkHelpers\\BufferPresentaion\\AfterOpen";//\\BufferPPTForTimeStamp.pptx"; //will come from config file

            //new try
            string destinationPath = System.IO.Path.Combine(BufferPPTLocation, "BufferPPTForTimeStamp.zip");
            System.IO.File.Copy(sourcePresentaionPath, destinationPath, true);
            UnzipFile(destinationPath, BufferPPTLocation);
            //go to BufferPresentaion\ppt\slides and get the slide 1,2,3..n xml. 
            //get Presentaion
            var app = new Microsoft.Office.Interop.PowerPoint.Application();
            Presentation presentaion = app.Presentations.Open(destinationPath, MsoTriState.msoTrue, MsoTriState.msoTrue, MsoTriState.msoFalse);
            List<Slide> SlideList = new List<Slide>();
            foreach (Slide item in presentaion.Slides)
            {
                SlideList.Add(item);
            }

            //

            //insert slides
            // presentaion.Slides.InsertFromFile(sourcePresentaionPath, 0, 1, -1);

            ////new code here
            //int slideCountBeforeWrite = presentaion.Slides.Count;

            //foreach (Slide item in Globals.ThisAddIn.Application.ActivePresentation.Slides)
            //{
            //    item.Copy(); //pptPresentation.Slides[slideId].Copy();
            //    presentaion.Windows[0].Activate(); //Ensures that the destination ppt is activaed for writting 
            //    presentaion.Windows[0].View.GotoSlide(presentaion.Slides.Count); // copy the slide to the last position of the powerpoint 
            //    Globals.ThisAddIn.Application.CommandBars.ExecuteMso("PasteSourceFormatting");
            //}




            //if (slideCountBeforeWrite == presentaion.Slides.Count - 1) //check whether slide count increase or not
            //{

            //}

            ////


            //make a list
            // List<Slide> SlideList = new List<Slide>();
            //foreach (Slide item in presentaion.Slides)
            //{
            //    SlideList.Add(item);
            //}
            //return
            return SlideList;
        }

        internal void UnzipFile(string sourceFilePath, string destinationDirectory)
        {
            if (!System.IO.Directory.Exists(destinationDirectory))
            {
                System.IO.Directory.CreateDirectory(destinationDirectory);
            }
            System.IO.Compression.ZipFile.ExtractToDirectory(sourceFilePath, destinationDirectory);
        }

        public static System.Timers.Timer aTimer;
        internal static void ExecuteTrackerForSlideModify()
        {
            return;

            aTimer = new System.Timers.Timer(1500);
            aTimer.Elapsed += new ElapsedEventHandler(TrackModification);
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        internal static void GetModifiedSlideAndUpdateTimeStamp()
        {
            //
            if (Globals.ThisAddIn.Application.ActiveWindow.View.Type == PpViewType.ppViewNormal)
            {
                Globals.ThisAddIn.Application.ActiveWindow.Panes[2].Activate();
                Slide ActiveSlide = Globals.ThisAddIn.Application.ActiveWindow.View.Slide as Slide;
                if (ActiveSlide.Tags.Count != 0)// ActiveSlide.Tags["SlideProperties"] != null
                {
                    UpdateTimestampOnSlide(ActiveSlide);
                }
            }

        }
        private static void TrackModification(object source, ElapsedEventArgs e)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Presentations.Count == 0)
                {
                    return;
                }

                if (string.IsNullOrEmpty(Globals.ThisAddIn.Application.ActivePresentation.Path))
                {
                    //MD_MsgBox.MaterialDesign_MessageBoxOK("Modify tracker is only work for saved presentaion. Please save Presentaion first !");
                    return;
                }
                if (Globals.ThisAddIn.Application.ActivePresentation.Saved == MsoTriState.msoFalse)
                {
                    GetModifiedSlideAndUpdateTimeStamp();
                }
            }
            catch (Exception) { }


        }

        #region CODE FOR WEB SERVICE
        //prepare slide property collection
        //1. get path from input parameter and browse file 
        //2. get the slide from that file and get properties form that slide


        // string LocalSlideProperties = "[{"PPTName":"myExp.pptx","PPTPath":"C:\\Users\\B.Satish\\Desktop\\myExp.pptx","SlideID":256,"LastTimeStamp":"2019_2_13_18_28_58_953"},{"PPTName":"myExp.pptx","PPTPath":"C:\\Users\\B.Satish\\Desktop\\myExp.pptx","SlideID":257,"LastTimeStamp":"2019_2_13_18_30_1_372"},{"PPTName":"myExp.pptx","PPTPath":"C:\\Users\\B.Satish\\Desktop\\myExp.pptx","SlideID":258,"LastTimeStamp":"2019_2_13_18_30_0_577"}]"
        public void CreatePropertyCollection(string ClientSlidesJsonString)
        {
            List<SlideProperties> LatestSlidePropertyCollection = new List<SlideProperties>();
            List<SlideProperties> ClientSlidePropertiesList = new List<SlideProperties>();
            if (!string.IsNullOrEmpty(ClientSlidesJsonString))
            {
                ClientSlidePropertiesList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SlideProperties>>(ClientSlidesJsonString);
            }

            foreach (SlideProperties slideProperty in ClientSlidePropertiesList)
            {
                if (!string.IsNullOrEmpty(slideProperty.PPTPath))
                {
                    Presentation presentation = GetFileByPath(slideProperty.PPTPath);
                    if (presentation != null)
                    {
                        SlideProperties LatestSP = GetSlidePropertyBySlideId(presentation, slideProperty.SlideID);
                        LatestSlidePropertyCollection.Add(LatestSP);
                        presentation.Close();
                    }

                }
            }
        }
        private Presentation GetFileByPath(string path)
        {
            Presentation presentation = null;

            if (File.Exists(path))
            {
                if ((Path.GetExtension(path).ToUpper() == ".PPTX") || (Path.GetExtension(path).ToUpper() == ".PPT"))
                {
                    Application application = new Application();
                    presentation = application.Presentations.Open(path);
                }

            }
            return presentation;
        }
        private SlideProperties GetSlidePropertyBySlideId(Presentation presentation, int slideId)
        {
            SlideProperties slideProperty = new SlideProperties();
            Slide slide = presentation.Slides.FindBySlideID(slideId);
            if (slide.Tags.Count > 0 && slide.Tags.Name(1).ToUpper() == "SlideProperties".ToUpper())
            {
                string SlidePropertyJSON = slide.Tags.Value(1);
                if (!string.IsNullOrEmpty(SlidePropertyJSON))
                {
                    slideProperty = Newtonsoft.Json.JsonConvert.DeserializeObject<SlideProperties>(SlidePropertyJSON);
                }

            }
            return slideProperty;
        }
        #endregion

        #region CODE FOR CREATE IMAGE OF SLIDE

        public void ConvertPresentationSlidesToImage(Microsoft.Office.Interop.PowerPoint.Presentation presentation)
        {
            foreach (Slide slide in presentation.Slides)
            {
                string path = GlobalsData.rootDir + "\\WatermarkHelpers\\SlideImage";
                slide.Export(path + slide.SlideID.ToString() +"_"+ slide.SlideNumber.ToString()+".png", "png", 320, 240);
            }
            
        }
        
        #endregion

    }
}
