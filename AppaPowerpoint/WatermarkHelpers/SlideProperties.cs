﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppaPowerpoint.WatermarkHelpers
{
   public class SlideProperties
    {
        public string PPTName { get; set; }
        public string PPTPath { get; set; }
        public int SlideID { get; set; }
        public string LastTimeStamp { get; set; }

        public int LocalSlideId { get; set; }

        public string SlideImageChecksum { get; set; }
    }
}
