﻿using AppaDocx.Common;
using AppaDocx.Helpers;
using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppaPowerpoint.WatermarkHelpers
{
    public class SlideImageForLocal
    {
        public static bool CompareMemoryStreams(byte[] LocalSlideByteArray, byte[] ServerSlideByteArray)
        {
            MemoryStream msLocal = new MemoryStream(LocalSlideByteArray);
            MemoryStream msServer = new MemoryStream(ServerSlideByteArray);
            return msLocal.ToArray().SequenceEqual(msServer.ToArray());
        }

        public List<string[]> ConvertPresentationSlidesToImage(Microsoft.Office.Interop.PowerPoint.Presentation presentation)
        {
            List<string[]> ImageNameAndSlidePath = new List<string[]>();
            List<int> listSlideNo = new List<int>();        //Added by Neha
            foreach (Slide slide in presentation.Slides)
            {
                if (listSlideNo.Contains(slide.SlideID))
                {
                    PropertyManager obj = new PropertyManager();
                    obj.SetSlideProperties(presentation);
                }
                else
                    listSlideNo.Add(slide.SlideID);

                string path = GlobalsData.rootDir + "\\WatermarkHelpers\\SlideImage\\Local\\";

                //(Validation)convert slide to image. only for those slide which are reused from server. 
                //i.e. slide have properties that slide have to get converted to images.
                //geting slide property for path.

                if (slide.Tags.Count > 0 && slide.Tags.Name(1).ToUpper() == "SlideProperties".ToUpper())
                {
                    string SlidePropertyJSON = slide.Tags.Value(1);
                    if (!string.IsNullOrEmpty(SlidePropertyJSON))
                    {
                        SlideProperties slideProperties = Newtonsoft.Json.JsonConvert.DeserializeObject<SlideProperties>(SlidePropertyJSON);
                        slide.Export(path + slideProperties.SlideID.ToString() + ".png", "png", 320, 240);
                        string[] nameAndPath = new string[2];
                        nameAndPath[0] = slideProperties.SlideID.ToString() + ".png";
                        nameAndPath[1] = slideProperties.PPTPath;
                        ImageNameAndSlidePath.Add(nameAndPath);
                    }
                    else
                    {
                        MD_MsgBox.MaterialDesign_MessageBoxOK("properties does not found for the slide. please set properties.");
                    }

                }
            }
            return ImageNameAndSlidePath;

        }

        public Dictionary<string, byte[]> GetByteStreamOfSlideImages(List<string[]> SlideIdAndPathList)
        {
            List<string> slideIDList = new List<string>();      //Added by Neha

            Dictionary<string, byte[]> StreamDict = new Dictionary<string, byte[]>();
            string path = GlobalsData.rootDir + "\\WatermarkHelpers\\SlideImage\\Local\\";
            foreach (string[] item in SlideIdAndPathList)
            {
                if(slideIDList.Contains(item[0]))
                {
                    continue;
                }
                slideIDList.Add(item[0]);
                StreamDict.Add(item[1] + "^" + item[0], File.ReadAllBytes(path + item[0]));
            }

            return StreamDict;
        }
        
    }
}
