﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.PowerPoint;

namespace AppaPowerpoint.WatermarkHelpers
{
    public static class SlideCollector
    {
        
        public static List<Slide> SlidesFileOpened { get; set; }
        public static List<Slide> SlidesBeforeSave { get; set; }
    }
}
