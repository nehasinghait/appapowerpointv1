﻿using AppaDocx.Common;
using AppaDocx.Helpers;
using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AppaPowerpoint.WatermarkHelpers
{
    public class SlideImageForServer
    {
        #region IMAGE STREAM COMPARISON SENARIO

        //1.on publish slides event convert all slide into images and store it folder
        public List<string[]> ConvertPresentationSlidesToImage(Microsoft.Office.Interop.PowerPoint.Presentation presentation)
        {
            List<string[]> SlideImageNames = new List<string[]>();
            foreach (Slide slide in presentation.Slides)
            {
                string path = GlobalsData.rootDir + "\\WatermarkHelpers\\SlideImage\\Server\\";
                slide.Export(path + slide.SlideID.ToString() + ".png", "png", 320, 240);
                string[] ImageNameAndSlidePath = new string[2];
                ImageNameAndSlidePath[0] = slide.SlideID.ToString() + ".png";
                //geting slide property for path
                if (slide.Tags.Count > 0 && slide.Tags.Name(1).ToUpper() == "SlideProperties".ToUpper())
                {
                    string SlidePropertyJSON = slide.Tags.Value(1);
                    if (!string.IsNullOrEmpty(SlidePropertyJSON))
                    {
                        SlideProperties slideProperties = Newtonsoft.Json.JsonConvert.DeserializeObject<SlideProperties>(SlidePropertyJSON);
                        ImageNameAndSlidePath[1] = slideProperties.PPTPath;
                    }
                    else
                    {
                        MD_MsgBox.MaterialDesign_MessageBoxOK("properties were not found for the slide. please set properties.");
                    }

                }

                SlideImageNames.Add(ImageNameAndSlidePath);
            }
            return SlideImageNames;

        }
        //2.access image files and convert in streams 
        public Dictionary<string, byte[]> GetByteStreamOfSlideImages(List<string[]> fileNames)
        {
            Dictionary<string, byte[]> StreamDict = new Dictionary<string, byte[]>();
            string path = GlobalsData.rootDir + "\\WatermarkHelpers\\SlideImage\\Server\\";
            foreach (string[] item in fileNames)
            {
                StreamDict.Add(item[1] + "^" + item[0], File.ReadAllBytes(path + item[0]));
            }
            //
            //DirectoryInfo directory = new DirectoryInfo(path);
            //FileInfo[] Files = directory.GetFiles("*.png");
            //foreach (FileInfo file in Files)
            //{
            //}
            //
            return StreamDict;
        }
        //3.create db connection and and insert stream in to db table
        public Dictionary<int, string> InsertSlideImageStreamToDB(Dictionary<string, byte[]> StreamDict)
        {
            Dictionary<int, string> SlideIdChecksum = new Dictionary<int, string>();
            //create connection
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\AppaPPTCode\WebServiceApp12MAR\WebServiceApp\WebServiceApp\powerpointDB.mdf;Integrated Security=True");
            try
            {

                con.Open();
                foreach (var keyValuePair in StreamDict)
                {
                    string[] values = keyValuePair.Key.Split('^');
                    string slideIdStr = System.IO.Path.GetFileNameWithoutExtension(values[1]);
                    int slideId = Convert.ToInt32(slideIdStr);
                    string pptPath = values[0];

                    //string SlideChecksum = getMD5ChecksumOfObject(keyValuePair.Value);
                    string SlideChecksum = getMD5ChecksumOfObject(GlobalsData.rootDir + "\\WatermarkHelpers\\SlideImage\\Server\\"+values[1]);
                    
                    SqlCommand cmd = new SqlCommand("insert into SlideImageDetails(NU_SLIDE_ID,VC_PPT_FILE_PATH,VC_SLIDE_IMAGE_PATH,VC_SLIDE_IMAGE_STREAM) values(@SlideId,@PPT_Path,@SlideImagePath,@ImageStream)");//,,VC_SLIDE_IMAGE_STREAM * ,"+ keyValuePair.Value + "
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@SlideId", slideId);
                    cmd.Parameters.AddWithValue("@PPT_Path", pptPath);
                    cmd.Parameters.AddWithValue("@SlideImagePath", SlideChecksum);
                    cmd.Parameters.AddWithValue("@ImageStream", keyValuePair.Value);
                    int result = cmd.ExecuteNonQuery();
                    if (result > 0)
                    {
                        SlideIdChecksum.Add(slideId, SlideChecksum);
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                throw;
            }

            //con open
            //code for insertion
            //con close
            return SlideIdChecksum;
        }

        public string getMD5ChecksumOfObject(string filepath)//byte[] byteArray
        {
            //Stream stream1 = new MemoryStream(byteArray);
            //using (var md5 = MD5.Create())
            //{
            //    MemoryStream stream = new MemoryStream();
            //    IFormatter formatter = new BinaryFormatter();
            //    formatter.Serialize(stream, stream1);//byteArray
            //    // return Encoding.Default.GetString(md5.ComputeHash(stream));
            //    return Convert.ToBase64String(md5.ComputeHash(stream));

            //}
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filepath))
                {
                    var s = md5.ComputeHash(stream);
                    return Convert.ToBase64String(s);
                }

            }
        }

        public bool PublishSlides(Microsoft.Office.Interop.PowerPoint.Presentation presentation)
        {
            List<string[]> FileNames = ConvertPresentationSlidesToImage(presentation);
            Dictionary<string, byte[]> StreamDict = GetByteStreamOfSlideImages(FileNames);
            Dictionary<int, string> SlideIdChecksum = InsertSlideImageStreamToDB(StreamDict);
            foreach (var item in SlideIdChecksum)
            {
                Slide slide = presentation.Slides.FindBySlideID(item.Key);

                if (slide.Tags.Count > 0 && slide.Tags.Name(1).ToUpper() == "SlideProperties".ToUpper())
                {
                    string SlidePropertyJSON = slide.Tags.Value(1);
                    if (!string.IsNullOrEmpty(SlidePropertyJSON))
                    {
                        SlideProperties slideProperties = Newtonsoft.Json.JsonConvert.DeserializeObject<SlideProperties>(SlidePropertyJSON);
                        slideProperties.SlideImageChecksum = item.Value.ToString();
                        slide.Tags.Delete("SlideProperties".ToUpper());
                        string TagJson = Newtonsoft.Json.JsonConvert.SerializeObject(slideProperties);
                        slide.Tags.Add("SlideProperties".ToUpper(), TagJson);
                    }
                }

            }
            return true;
            #endregion

        }

        public void RemoveSlideImages()
        {
            string SlideImagePath = GlobalsData.rootDir + "\\WatermarkHelpers\\SlideImage\\Server\\";
            var ImageList = System.IO.Directory.GetFiles(SlideImagePath,"*.png");
            foreach (var item in ImageList)
            {
                System.IO.File.Delete(item);
            }
        }
    }

    public class ImageResponse
    {
        //public byte[] VC_SLIDE_IMAGE_STREAM { get; set; }
        //public int NU_SLIDE_ID { get; set; }
        //public int VC_PPT_FILE_PATH { get; set; }

        public byte[] SlideImageStreams { get; set; }
        public string SlideId { get; set; }
        public string PPTPath { get; set; }
        public string SlideImageChecksum { get; set; }
    }
    public class ImageRequest
    {
        public string PPTPath { get; set; }
        public string SlideId { get; set; }
    }
}

