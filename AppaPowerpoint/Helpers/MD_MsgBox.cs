﻿using AppaDocx.Application.Helpers;
using AppaDocx.Common.Helpers;
using AppaDocx.Common.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.PerformanceData;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AppaDocx.Common;
using AppaDocx.Common.Enums;
//using Microsoft.Office.Interop.Word;
using System.Windows.Input;
using AppaDocx.UserInterfaces;
using System.Text;

namespace AppaDocx.Helpers
{
    public static class MD_MsgBox
    {
        public static bool? MaterialDesign_MessageBoxShow(string strData, int optType, string caption = "AppaDocx", List<PendingDocument> data = null)
        {
            bool? blnResult = false;
            try
            {
                CustomDialog objCustomDialog = new CustomDialog();
                switch (optType)
                {
                    case 1:
                        objCustomDialog.Title = caption;
                        objCustomDialog.txtMessage.Text = strData.TrimStart();
                        objCustomDialog.ShowActivated = true;
                        objCustomDialog.btnDialogCancel.Content = "OK";
                        objCustomDialog.btnDialogOk.Visibility = Visibility.Collapsed;
                        objCustomDialog.btnDialogOk.Content = "No";
                        blnResult = objCustomDialog.ShowDialog();
                        break;
                    case 2:
                        objCustomDialog.Title = caption;
                        objCustomDialog.txtMessage.Text = strData.TrimStart();
                        objCustomDialog.ShowActivated = true;
                        objCustomDialog.btnDialogCancel.Content = "No";
                        objCustomDialog.btnDialogOk.Content = "Yes";
                        objCustomDialog.btnDialogOk.Visibility = Visibility.Visible;
                        blnResult = objCustomDialog.ShowDialog();
                        break;
                    case 3:
                        objCustomDialog.Title = caption;
                        objCustomDialog.ShowInTaskbar = true;
                        objCustomDialog.ResizeMode = ResizeMode.NoResize;
                        objCustomDialog.ShowActivated = true;
                        strData = PrepareData(data);
                        objCustomDialog.txtMessage.Text = strData.TrimStart();
                        objCustomDialog.btnDialogOk.Visibility = Visibility.Hidden;
                        objCustomDialog.Width = 650;
                        objCustomDialog.Height = 340;
                        objCustomDialog.txtMessage.Width = 610;
                        objCustomDialog.txtMessage.Height = 240;
                        blnResult = objCustomDialog.ShowDialog();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.ToString());
            }
            return (blnResult);
        }
        private static string PrepareData(List<PendingDocument> data)
        {
            StringBuilder strBldrData = new StringBuilder();
            foreach (PendingDocument item in data)
            {
                strBldrData.Append("\r\n" + item.Title);
            }
            return (strBldrData.ToString());
        }

        //messagebox Ok only
        public static bool? MaterialDesign_MessageBoxOK(string strData)
        {
            bool? blnResult = false;
            try
            {
                CustomDialog objCustomDialog = new CustomDialog();  
                objCustomDialog.btnDialogCancel.Visibility = Visibility.Collapsed;
                objCustomDialog.Title = "Appa Powerpoint";
                objCustomDialog.txtMessage.Text = strData.TrimStart();
                objCustomDialog.ShowActivated = true;
                bool? result = objCustomDialog.ShowDialog();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.ToString());
            }
            return (blnResult);
        }
    }

    public class PendingDocument
    {
        public string Title { get; set; }
        public string Status { get; set; }
    }
}