﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Windows;
using System.Xml;
using AppaDocx.Common;
using AppaDocx.Common.Enums;
using AppaDocx.Common.Helpers;
using AppaDocx.Common.Models;
using AppaDocx.Application.Helpers;
using AppaDocx.WebService;

using Newtonsoft.Json;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using AppaDocx.Helpers;

namespace AppaDocx.Application.Helpers
{
    public class HlprDocumentManager
    {
        HlprWebService objHlprWebService = new HlprWebService();
        public string response { get; set; }
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HlprDocumentManager));
        string strMsgData = "";

        public string StripHtml(string source)
        {
            string output;

            output = Regex.Replace(source, "<[^>]*>", string.Empty);

            // blank lines
            output = Regex.Replace(output, @"^\s*$\n", string.Empty, RegexOptions.Multiline);

            return output;
        }

        //satish added to get a col count
        public string getColomsOfTable(string source)
        {
            string output;


            var s = source.Split("cols");
            var a = s[1].Trim();
            a = a.Substring(2, 1);
            return a;

            //output = Regex.Replace(source, "<[^>]*>", string.Empty);

            //// blank lines
            //output = Regex.Replace(output, @"^\s*$\n", string.Empty, RegexOptions.Multiline);

            //return output;
        }

        //public List<DocumentNames> DocumentSearch_GetData(String search, string strBookStatus, string strDocType)
        //{
        //    List<DocumentNames> lstDocumentNames = null;
        //    lstDocumentNames = new List<DocumentNames>();
        //    // create a list of requests to run on the server
        //    try
        //    {
        //        string queryParams = "?keyword=" + HttpUtility.UrlEncode(search) + "&bookStatus=" + strBookStatus + "&bookType=" + strDocType;
        //        Dictionary<string, string> docNameSearchWebServiceInfo = new Dictionary<string, string>();
        //        docNameSearchWebServiceInfo.Add("service.name", "docnamesearch");
        //        docNameSearchWebServiceInfo.Add("service.queryparams", queryParams);
        //        response = objHlprWebService.callWebService(docNameSearchWebServiceInfo);

        //        if (response == "" && HlprErrorHandling.SourceName != null)
        //        {
        //            string strData = "Could not reach the server.  Please try again later or check your internet connection.";
        //            //System.Windows.Forms.MessageBox.Show(strData, "AppaDocx Message",MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            bool? blnResult = MD_MsgBox.MaterialDesign_MessageBoxShow(strData, 1);
        //            return null;
        //        } 
        //        //UserInterfaces.AppaDocxUI objAppaDocxUI = new UserInterfaces.AppaDocxUI();
        //        //bool blnResponse = objAppaDocxUI.ProcessResponse(response);
        //        //if (blnResponse == true)
        //        if(response== "loginrequired")
        //            return null;
        //        else
        //        {
        //            lstDocumentNames = JsonConvert.DeserializeObject<List<DocumentNames>>(response);
        //            if (lstDocumentNames.Count >0)
        //            {
        //                foreach (DocumentNames objDocumentNames in lstDocumentNames)
        //                {
        //                    objDocumentNames.documentType = strDocType.ToString();
        //                }
        //            }
        //        }

        //        return lstDocumentNames;
        //    }
        //    catch (Exception ex)
        //    {
        //        strMsgData = "Could not reach the server.  Please try again later or check your internet connection.";
        //        MD_MsgBox.MaterialDesign_MessageBoxShow(strMsgData, 1);
        //        //System.Windows.Forms.MessageBox.Show(strMsgData);
        //        log.Error("Document Search", ex);
        //    }
        //    return lstDocumentNames;
        //}

        //public void AddPictureByRange(string strPicturePath, Range rngImage)
        //{
        //    float nWidth = 160, nHeight = 64;
        //    var inlineShape = Globals.ThisAddIn.Application.Selection.InlineShapes.AddPicture(strPicturePath, false, true, rngImage);
        //    /*var shape = inlineShape.ConvertToShape();
        //    shape.Width = nWidth;
        //    shape.Height = nHeight;*/
        //}
        //public string GetVariableValueFromStartEnd(string strSingleVariableValue, Common.Enums.ContentControlType ccType)
        //{
        //    try
        //    {
        //        strSingleVariableValue = strSingleVariableValue.TrimStart('\r');
        //        strSingleVariableValue = strSingleVariableValue.TrimEnd('\r');
        //        if (ccType == ContentControlType.VariableTypeContentControl)
        //        {
        //            strSingleVariableValue = strSingleVariableValue.Replace(ConstantsValues.VariableStart, "");
        //            strSingleVariableValue = strSingleVariableValue.Replace(ConstantsValues.VariableEnd, "");
        //            strSingleVariableValue = strSingleVariableValue.Split(ConstantsValues.VariableSeparator).Last();
        //        }
        //        if (ccType == ContentControlType.FootnoteTypeContentControl)
        //        {
        //            strSingleVariableValue = strSingleVariableValue.Replace(ConstantsValues.FootnoteStart, "");
        //            strSingleVariableValue = strSingleVariableValue.Replace(ConstantsValues.FootnoteEnd, "");
        //            strSingleVariableValue = strSingleVariableValue.Split(ConstantsValues.FootnoteSeparator).Last();
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        log.Error("GetVariableValueFromStartEnd", ex);
        //    }
        //    return (strSingleVariableValue);
        //}


        //public List<KeyValuePair<string, string>> GetKeyValuePairsForContentControl(Common.Enums.ContentControlType ccType, Microsoft.Office.Interop.Word.Document activeDocument)
        //{
        //    List<KeyValuePair<string, string>> lstFindText = new List<KeyValuePair<string, string>>();
        //    try
        //    {

        //        if (ccType == ContentControlType.VariableTypeContentControl)
        //        {
        //            foreach (Microsoft.Office.Interop.Word.Bookmark singleBookmark in activeDocument.Bookmarks)
        //            {
        //                string strBookmarkData = singleBookmark.Range.Text;
        //                if (strBookmarkData != null && strBookmarkData.Contains(ConstantsValues.VariableStart) &&
        //                    strBookmarkData.Contains(ConstantsValues.VariableEnd)) //If Bookmark Range Contains Variable
        //                {
        //                    List<int> lstIndexStart = strBookmarkData
        //                        .AllIndexOf(ConstantsValues.VariableStart, StringComparison.CurrentCulture).ToList();
        //                    List<int> lstIndexEnd = strBookmarkData
        //                        .AllIndexOf(ConstantsValues.VariableEnd, StringComparison.CurrentCulture).ToList();
        //                    int iIndexesVariable = 0;
        //                    foreach (int iStartVariable in lstIndexStart)
        //                    {
        //                        int length = lstIndexEnd[iIndexesVariable++] - iStartVariable;
        //                        string strSingleVariableValueWithStartEnd = strBookmarkData.Substring(iStartVariable,
        //                            length + ConstantsValues.VariableEnd.Length);
        //                        string strSingleVariableValue =
        //                           GetVariableValueFromStartEnd(strSingleVariableValueWithStartEnd, ccType);
        //                        lstFindText.Add(new KeyValuePair<string, string>(strSingleVariableValueWithStartEnd,
        //                            strSingleVariableValue));
        //                    }
        //                }
        //            }
        //        }
        //        if (ccType == ContentControlType.FootnoteTypeContentControl)
        //        {
        //            foreach (Microsoft.Office.Interop.Word.Bookmark singleBookmark in activeDocument.Bookmarks)
        //            {
        //                string strBookmarkData = singleBookmark.Range.Text;
        //                if (strBookmarkData != null && strBookmarkData.Contains(ConstantsValues.FootnoteStart) &&
        //                    strBookmarkData.Contains(ConstantsValues.FootnoteEnd)) //If Bookmark Range Contains Footnote
        //                {
        //                    List<int> lstIndexStart = strBookmarkData
        //                        .AllIndexOf(ConstantsValues.FootnoteStart, StringComparison.CurrentCulture).ToList();
        //                    List<int> lstIndexEnd = strBookmarkData
        //                        .AllIndexOf(ConstantsValues.FootnoteEnd, StringComparison.CurrentCulture).ToList();
        //                    int iIndexesFootnote = 0;
        //                    foreach (int iStartFootnote in lstIndexStart)
        //                    {
        //                        int length = lstIndexEnd[iIndexesFootnote++] - iStartFootnote;
        //                        string strSingleFootnoteValueWithStartEnd = strBookmarkData.Substring(iStartFootnote,
        //                            length + ConstantsValues.FootnoteEnd.Length);
        //                        string strSingleFootnoteValue =
        //                            GetVariableValueFromStartEnd(strSingleFootnoteValueWithStartEnd, ccType);
        //                        lstFindText.Add(new KeyValuePair<string, string>(strSingleFootnoteValueWithStartEnd,
        //                            strSingleFootnoteValue));
        //                    }
        //                }
        //            }
        //        }
        //        if (ccType == ContentControlType.EmbeddedTypeContentControl)
        //        {
        //            foreach (Microsoft.Office.Interop.Word.Bookmark singleBookmark in Globals.ThisAddIn.Application
        //                .ActiveDocument.Bookmarks)
        //            {
        //                string strBookmarkData = singleBookmark.Range.Text;
        //                if (strBookmarkData != null && strBookmarkData.Contains("$$IMG_START$$") &&
        //                    strBookmarkData.Contains("$$IMG_END$$")) //If Bookmark Range Contains Footnote
        //                {
        //                    List<int> lstIndexStart = strBookmarkData
        //                        .AllIndexOf("$$IMG_START$$", StringComparison.CurrentCulture).ToList();
        //                    List<int> lstIndexEnd = strBookmarkData
        //                        .AllIndexOf("$$IMG_END$$", StringComparison.CurrentCulture).ToList();
        //                    int iIndexesFootnote = 0;
        //                    foreach (int iStartFootnote in lstIndexStart)
        //                    {
        //                        int length = lstIndexEnd[iIndexesFootnote++] - iStartFootnote;
        //                        string strSingleFootnoteValueWithStartEnd = strBookmarkData.Substring(iStartFootnote,
        //                            length + "$$IMG_END$$".Length);
        //                        /*string strSingleFootnoteValue =
        //                            GetVariableValueFromStartEnd(strSingleFootnoteValueWithStartEnd, ccType);*/
        //                        lstFindText.Add(new KeyValuePair<string, string>(strSingleFootnoteValueWithStartEnd,
        //                            strSingleFootnoteValueWithStartEnd));
        //                    }
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("GetKeyValuePairsForContentControl", ex);
        //    }
        //    return (lstFindText);

        //}

        public void RenderCompMetaDataFromDML(string documentId, string strResponse)
        {
            try
            {
                string originalXml = strResponse;
                if (originalXml.Contains("<data>not found</data>"))
                {
                    strMsgData = "Document info not found...";
                    // MD_MsgBox.MaterialDesign_MessageBoxShow(strMsgData, 1);
                    System.Windows.MessageBox.Show(strMsgData);
                    return;
                }
                string docBookDecodedXml = HlprDocumentTransformation.RemoveDocbookEncoding(originalXml);
                string strXSLTFile = "DocBookToCompMetaData.xml";
                string data = AppaDocx.Rendering.HlprCoreXSLTTrans.ProcessDML_CompMetaDataTransform(docBookDecodedXml, strXSLTFile);
                XmlDocument componentXmlDoc = new XmlDocument();
                componentXmlDoc.LoadXml(data);
                var proofCount = componentXmlDoc.SelectSingleNode(@"//book/proofcount");
                if (proofCount != null)
                {
                    ConstantsValues.ProofCount = proofCount.InnerText;
                }
                var componentNodes = componentXmlDoc.SelectNodes(@"//component");
                Dictionary<string, ComponentModel> componentMetaData = new Dictionary<string, ComponentModel>();

                foreach (XmlNode item in componentNodes)
                {
                    string elementInstanceId = item.SelectSingleNode("./id").InnerText;
                    string name = item.SelectSingleNode("./name").InnerText;
                    string docDtlId = item.SelectSingleNode("./docdtlid").InnerText;
                    string lastUpdatedTime = item.SelectSingleNode("./lastupdated").InnerText;
                    string type = item.SelectSingleNode("./type").InnerText;
                    string key = elementInstanceId + "~" + docDtlId;
                    ComponentModel objComponentModel = new ComponentModel
                    {
                        CompID = elementInstanceId,
                        ComponentName = name,
                        BookDetailId = docDtlId,
                        lastUpdatedBy = lastUpdatedTime,
                        CompType = type
                    };
                    componentMetaData.Add(key, objComponentModel);
                }
                if (componentMetaData != null && componentMetaData.Count > 0)
                {
                    //read from the document Property
                    if (ConstantsValues.componentMetaDataDictionary == null)
                    {
                        ConstantsValues.componentMetaDataDictionary = new Dictionary<string, Dictionary<string, ComponentModel>>();
                    }
                    if (documentId != null && !documentId.Equals("") && ConstantsValues.componentMetaDataDictionary != null && ConstantsValues.componentMetaDataDictionary.ContainsKey(documentId))
                    {
                        ConstantsValues.componentMetaDataDictionary.Remove(documentId);
                    }
                    ConstantsValues.componentMetaDataDictionary.Add(documentId, componentMetaData);
                }
                //Now iterate on this xml and make a dictionary object in memory
            }
            catch (Exception ex)
            {
                log.Error("RenderCompMetaDataFromDML", ex);
            }

        }

        //#region Document Transformation 

        //public void RenderWMLFromDML(string clientId, string docType, string strResponse, Microsoft.Office.Interop.Word.Document document)
        //{
        //    try
        //    {
        //        //string strXSLTFile = clientId + "_" + docType + "_DocBookToWordML.xml";
        //        string originalXml = strResponse;
        //        if (originalXml.Contains("<data>not found</data>"))
        //        {
        //            //System.Windows.MessageBox.Show("Document info not found...");
        //            return;
        //        }
        //        string docBookDecodedXml = HlprDocumentTransformation.RemoveDocbookEncoding(originalXml);
        //        //string data = data = AppaDocx.Rendering.HlprCoreXSLTTrans.ProcessDW_WDTransform(docBookDecodedXml,strXSLTFile);
        //        String strData = docBookDecodedXml;
        //        Microsoft.Office.Interop.Word.Range user_range = document.Paragraphs.Last.Range;
        //        int pCount = document.Paragraphs.Count;
        //        user_range.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);
        //        if (strData.Contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>") && strData.Contains("<w:wordDocument xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\">"))
        //        {
        //            strData = strData.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
        //        }
        //        //user_range.InsertXML(strData);
        //        user_range.InsertXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?><w:wordDocument xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\"><w:lists><w:listDef w:listDefId=\"0\"><w:lvl w:ilvl=\"0\"><w:lvlText w:val=\"·\"/><w:rPr><w:rFonts w:ascii=\"Symbol\" w:h-ansi=\"Symbol\" w:hint=\"default\"/></w:rPr><w:pPr><w:ind w:left=\"180\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\"><w:lvlText w:val=\"·\"/><w:rPr><w:rFonts w:ascii=\"Symbol\" w:h-ansi=\"Symbol\" w:hint=\"default\"/><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"360\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\"><w:lvlText w:val=\"·\"/><w:rPr><w:rFonts w:ascii=\"Symbol\" w:h-ansi=\"Symbol\" w:hint=\"default\"/><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"540\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\"><w:lvlText w:val=\"·\"/><w:rPr><w:rFonts w:ascii=\"Symbol\" w:h-ansi=\"Symbol\" w:hint=\"default\"/><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"720\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"1\"><w:plt w:val=\"HybridMultilevel\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1\"/><w:pPr><w:ind w:left=\"180\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:lvl><w:lvl w:ilvl=\"1\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%2\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"360\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%3\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"540\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"2\"><w:plt w:val=\"HybridMultilevel\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1\"/><w:pPr><w:ind w:left=\"180\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:lvl><w:lvl w:ilvl=\"1\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%2\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"360\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%3\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"540\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"3\"><w:plt w:val=\"HybridMultilevel\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1\"/><w:pPr><w:ind w:left=\"180\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:lvl><w:lvl w:ilvl=\"1\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%2\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"360\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%3\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"540\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"4\"><w:plt w:val=\"HybridMultilevel\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1\"/><w:pPr><w:ind w:left=\"180\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:lvl><w:lvl w:ilvl=\"1\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%2\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"360\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%3\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"540\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"5\"><w:plt w:val=\"HybridMultilevel\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1\"/><w:pPr><w:ind w:left=\"180\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:lvl><w:lvl w:ilvl=\"1\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%2\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"360\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%3\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"540\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"6\"><w:plt w:val=\"HybridMultilevel\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1\"/><w:pPr><w:ind w:left=\"180\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:lvl><w:lvl w:ilvl=\"1\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%2\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"360\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%3\"/><w:rPr><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr><w:pPr><w:ind w:left=\"540\" w:hanging=\"180\"/><w:spacing w:before=\"90\" w:after=\"90\" w:beforeAutospacing=\"0\" w:afterAutospacing=\"0\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"0\"/></w:list><w:list w:ilfo=\"2\"><w:ilst w:val=\"1\"/></w:list><w:list w:ilfo=\"3\"><w:ilst w:val=\"2\"/></w:list><w:list w:ilfo=\"4\"><w:ilst w:val=\"3\"/></w:list><w:list w:ilfo=\"5\"><w:ilst w:val=\"4\"/></w:list><w:list w:ilfo=\"6\"><w:ilst w:val=\"5\"/></w:list><w:list w:ilfo=\"7\"><w:ilst w:val=\"6\"/></w:list></w:lists><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"00\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"E0002AFF\" w:usb-1=\"C0007841\" w:usb-2=\"00000009\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Arial\"><w:panose-1 w:val=\"020B0604020202020204\"/><w:charset w:val=\"00\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"E0002AFF\" w:usb-1=\"C0007843\" w:usb-2=\"00000009\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"00\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"E00002FF\" w:usb-1=\"4000ACFF\" w:usb-2=\"00000001\" w:usb-3=\"00000000\" w:csb-0=\"0000019F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Arial Black\"><w:panose-1 w:val=\"020B0A04020102020204\"/><w:charset w:val=\"00\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000287\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Bookman Old Style\"><w:panose-1 w:val=\"02050604050505020204\"/><w:charset w:val=\"00\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000287\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"371\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"Normal Table\"/><w:lsdException w:name=\"Table Simple 1\"/><w:lsdException w:name=\"Table Simple 2\"/><w:lsdException w:name=\"Table Simple 3\"/><w:lsdException w:name=\"Table Classic 1\"/><w:lsdException w:name=\"Table Classic 2\"/><w:lsdException w:name=\"Table Classic 3\"/><w:lsdException w:name=\"Table Classic 4\"/><w:lsdException w:name=\"Table Colorful 1\"/><w:lsdException w:name=\"Table Colorful 2\"/><w:lsdException w:name=\"Table Colorful 3\"/><w:lsdException w:name=\"Table Columns 1\"/><w:lsdException w:name=\"Table Columns 2\"/><w:lsdException w:name=\"Table Columns 3\"/><w:lsdException w:name=\"Table Columns 4\"/><w:lsdException w:name=\"Table Columns 5\"/><w:lsdException w:name=\"Table Grid 1\"/><w:lsdException w:name=\"Table Grid 2\"/><w:lsdException w:name=\"Table Grid 3\"/><w:lsdException w:name=\"Table Grid 4\"/><w:lsdException w:name=\"Table Grid 5\"/><w:lsdException w:name=\"Table Grid 6\"/><w:lsdException w:name=\"Table Grid 7\"/><w:lsdException w:name=\"Table Grid 8\"/><w:lsdException w:name=\"Table List 1\"/><w:lsdException w:name=\"Table List 2\"/><w:lsdException w:name=\"Table List 3\"/><w:lsdException w:name=\"Table List 4\"/><w:lsdException w:name=\"Table List 5\"/><w:lsdException w:name=\"Table List 6\"/><w:lsdException w:name=\"Table List 7\"/><w:lsdException w:name=\"Table List 8\"/><w:lsdException w:name=\"Table 3D effects 1\"/><w:lsdException w:name=\"Table 3D effects 2\"/><w:lsdException w:name=\"Table 3D effects 3\"/><w:lsdException w:name=\"Table Contemporary\"/><w:lsdException w:name=\"Table Elegant\"/><w:lsdException w:name=\"Table Professional\"/><w:lsdException w:name=\"Table Subtle 1\"/><w:lsdException w:name=\"Table Subtle 2\"/><w:lsdException w:name=\"Table Web 1\"/><w:lsdException w:name=\"Table Web 2\"/><w:lsdException w:name=\"Table Web 3\"/><w:lsdException w:name=\"Table Theme\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"Light Shading\"/><w:lsdException w:name=\"Light List\"/><w:lsdException w:name=\"Light Grid\"/><w:lsdException w:name=\"Medium Shading 1\"/><w:lsdException w:name=\"Medium Shading 2\"/><w:lsdException w:name=\"Medium List 1\"/><w:lsdException w:name=\"Medium List 2\"/><w:lsdException w:name=\"Medium Grid 1\"/><w:lsdException w:name=\"Medium Grid 2\"/><w:lsdException w:name=\"Medium Grid 3\"/><w:lsdException w:name=\"Dark List\"/><w:lsdException w:name=\"Colorful Shading\"/><w:lsdException w:name=\"Colorful List\"/><w:lsdException w:name=\"Colorful Grid\"/><w:lsdException w:name=\"Light Shading Accent 1\"/><w:lsdException w:name=\"Light List Accent 1\"/><w:lsdException w:name=\"Light Grid Accent 1\"/><w:lsdException w:name=\"Medium Shading 1 Accent 1\"/><w:lsdException w:name=\"Medium Shading 2 Accent 1\"/><w:lsdException w:name=\"Medium List 1 Accent 1\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Medium List 2 Accent 1\"/><w:lsdException w:name=\"Medium Grid 1 Accent 1\"/><w:lsdException w:name=\"Medium Grid 2 Accent 1\"/><w:lsdException w:name=\"Medium Grid 3 Accent 1\"/><w:lsdException w:name=\"Dark List Accent 1\"/><w:lsdException w:name=\"Colorful Shading Accent 1\"/><w:lsdException w:name=\"Colorful List Accent 1\"/><w:lsdException w:name=\"Colorful Grid Accent 1\"/><w:lsdException w:name=\"Light Shading Accent 2\"/><w:lsdException w:name=\"Light List Accent 2\"/><w:lsdException w:name=\"Light Grid Accent 2\"/><w:lsdException w:name=\"Medium Shading 1 Accent 2\"/><w:lsdException w:name=\"Medium Shading 2 Accent 2\"/><w:lsdException w:name=\"Medium List 1 Accent 2\"/><w:lsdException w:name=\"Medium List 2 Accent 2\"/><w:lsdException w:name=\"Medium Grid 1 Accent 2\"/><w:lsdException w:name=\"Medium Grid 2 Accent 2\"/><w:lsdException w:name=\"Medium Grid 3 Accent 2\"/><w:lsdException w:name=\"Dark List Accent 2\"/><w:lsdException w:name=\"Colorful Shading Accent 2\"/><w:lsdException w:name=\"Colorful List Accent 2\"/><w:lsdException w:name=\"Colorful Grid Accent 2\"/><w:lsdException w:name=\"Light Shading Accent 3\"/><w:lsdException w:name=\"Light List Accent 3\"/><w:lsdException w:name=\"Light Grid Accent 3\"/><w:lsdException w:name=\"Medium Shading 1 Accent 3\"/><w:lsdException w:name=\"Medium Shading 2 Accent 3\"/><w:lsdException w:name=\"Medium List 1 Accent 3\"/><w:lsdException w:name=\"Medium List 2 Accent 3\"/><w:lsdException w:name=\"Medium Grid 1 Accent 3\"/><w:lsdException w:name=\"Medium Grid 2 Accent 3\"/><w:lsdException w:name=\"Medium Grid 3 Accent 3\"/><w:lsdException w:name=\"Dark List Accent 3\"/><w:lsdException w:name=\"Colorful Shading Accent 3\"/><w:lsdException w:name=\"Colorful List Accent 3\"/><w:lsdException w:name=\"Colorful Grid Accent 3\"/><w:lsdException w:name=\"Light Shading Accent 4\"/><w:lsdException w:name=\"Light List Accent 4\"/><w:lsdException w:name=\"Light Grid Accent 4\"/><w:lsdException w:name=\"Medium Shading 1 Accent 4\"/><w:lsdException w:name=\"Medium Shading 2 Accent 4\"/><w:lsdException w:name=\"Medium List 1 Accent 4\"/><w:lsdException w:name=\"Medium List 2 Accent 4\"/><w:lsdException w:name=\"Medium Grid 1 Accent 4\"/><w:lsdException w:name=\"Medium Grid 2 Accent 4\"/><w:lsdException w:name=\"Medium Grid 3 Accent 4\"/><w:lsdException w:name=\"Dark List Accent 4\"/><w:lsdException w:name=\"Colorful Shading Accent 4\"/><w:lsdException w:name=\"Colorful List Accent 4\"/><w:lsdException w:name=\"Colorful Grid Accent 4\"/><w:lsdException w:name=\"Light Shading Accent 5\"/><w:lsdException w:name=\"Light List Accent 5\"/><w:lsdException w:name=\"Light Grid Accent 5\"/><w:lsdException w:name=\"Medium Shading 1 Accent 5\"/><w:lsdException w:name=\"Medium Shading 2 Accent 5\"/><w:lsdException w:name=\"Medium List 1 Accent 5\"/><w:lsdException w:name=\"Medium List 2 Accent 5\"/><w:lsdException w:name=\"Medium Grid 1 Accent 5\"/><w:lsdException w:name=\"Medium Grid 2 Accent 5\"/><w:lsdException w:name=\"Medium Grid 3 Accent 5\"/><w:lsdException w:name=\"Dark List Accent 5\"/><w:lsdException w:name=\"Colorful Shading Accent 5\"/><w:lsdException w:name=\"Colorful List Accent 5\"/><w:lsdException w:name=\"Colorful Grid Accent 5\"/><w:lsdException w:name=\"Light Shading Accent 6\"/><w:lsdException w:name=\"Light List Accent 6\"/><w:lsdException w:name=\"Light Grid Accent 6\"/><w:lsdException w:name=\"Medium Shading 1 Accent 6\"/><w:lsdException w:name=\"Medium Shading 2 Accent 6\"/><w:lsdException w:name=\"Medium List 1 Accent 6\"/><w:lsdException w:name=\"Medium List 2 Accent 6\"/><w:lsdException w:name=\"Medium Grid 1 Accent 6\"/><w:lsdException w:name=\"Medium Grid 2 Accent 6\"/><w:lsdException w:name=\"Medium Grid 3 Accent 6\"/><w:lsdException w:name=\"Dark List Accent 6\"/><w:lsdException w:name=\"Colorful Shading Accent 6\"/><w:lsdException w:name=\"Colorful List Accent 6\"/><w:lsdException w:name=\"Colorful Grid Accent 6\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:styleId=\"blockquoteindentparagraphstyle1\"><w:name w:val=\"Block Quote Indent ParagraphStyle 1\"/><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"blockquoteindentparagraphstyle2\"><w:name w:val=\"Block Quote Indent ParagraphStyle 2\"/><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"blockquoteindentparagraphstyle3\"><w:name w:val=\"Block Quote Indent ParagraphStyle 3\"/><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"blockquoteindentparagraphstyle4\"><w:name w:val=\"Block Quote Indent ParagraphStyle 4\"/><w:hidden/></w:style><w:style w:type=\"character\" w:styleId=\"defaultcharstyle\"><w:name w:val=\"Default Char Style\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"TableNormal\"><w:name w:val=\"Normal Table\"/></w:style><w:style w:type=\"character\" w:styleId=\"stsize3\"><w:name w:val=\"St Size 3\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:b w:val=\"true\"/><w:sz w:val=\"24\"/><w:szCs w:val=\"24\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"fctx1size3pt5\"><w:name w:val=\"Fctx 1 Size 3 Pt 5\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"fnunsqsize4\"><w:name w:val=\"Fnunsq Size 4\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"14\"/><w:szCs w:val=\"14\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"notesize3pt5\"><w:name w:val=\"Note Size 3 Pt 5\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:i w:val=\"true\"/><w:sz w:val=\"14\"/><w:szCs w:val=\"14\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"TOC1\"><w:name w:val=\"TOC1\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"ulsize7pt5\"><w:name w:val=\"Ul Size 7 Pt 5\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"fctx2\"><w:name w:val=\"fctx2\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:color w:val=\"FF0000\"/><w:b w:val=\"true\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"shsize3pt5\"><w:name w:val=\"Sh Size 3 Pt 5\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:b w:val=\"true\"/><w:sz w:val=\"24\"/><w:szCs w:val=\"24\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"fch2size3pt0\"><w:name w:val=\"Fch 2 Size 3 Pt 0\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:b w:val=\"true\"/><w:sz w:val=\"32\"/><w:szCs w:val=\"32\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"fnsqsize4\"><w:name w:val=\"Fnsq Size 4\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"14\"/><w:szCs w:val=\"14\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"fnsqfootnotestylelabelsize4\"><w:name w:val=\"Fnsq Footnote Style Label Size 4\"/><w:hidden/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"14\"/><w:szCs w:val=\"14\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"h3size4pt5\"><w:name w:val=\"H 3 Size 4 Pt 5\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:b w:val=\"true\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"h2size8\"><w:name w:val=\"H 2 Size 8\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:i w:val=\"true\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"fch3size4pt5\"><w:name w:val=\"Fch 3 Size 4 Pt 5\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"20\"/><w:szCs w:val=\"20\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"fch3size4\"><w:name w:val=\"Fch 3 Size 4\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"20\"/><w:szCs w:val=\"20\"/></w:rPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleFCTCSH\"><w:name w:val=\"Paragraph Rowstyle FCTCSH\"/><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleFCTCH\"><w:name w:val=\"Paragraph Rowstyle FCTCH\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/><w:b w:val=\"true\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTCH11\"><w:name w:val=\"Paragraph Rowstyle TCH11\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"14\"/><w:szCs w:val=\"14\"/><w:b w:val=\"true\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTR12\"><w:name w:val=\"Paragraph Rowstyle TR12\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstylePF1\"><w:name w:val=\"Paragraph Rowstyle PF1\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/><w:b w:val=\"true\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTCSH11\"><w:name w:val=\"Paragraph Rowstyle TCSH11\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"14\"/><w:szCs w:val=\"14\"/><w:b w:val=\"true\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTH1\"><w:name w:val=\"Paragraph Rowstyle TH1\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/><w:b w:val=\"true\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTRB1\"><w:name w:val=\"Paragraph Rowstyle TRB1\"/><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTRI1\"><w:name w:val=\"Paragraph Rowstyle TRI1\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTRH\"><w:name w:val=\"Paragraph Rowstyle TRH\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/><w:b w:val=\"true\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTR9B\"><w:name w:val=\"Paragraph Rowstyle TR9B\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTR9A\"><w:name w:val=\"Paragraph Rowstyle TR9A\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTR2\"><w:name w:val=\"Paragraph Rowstyle TR2\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/><w:b w:val=\"true\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleTR13\"><w:name w:val=\"Paragraph Rowstyle TR13\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/></w:rPr><w:hidden/></w:style><w:style w:type=\"paragraph\" w:styleId=\"paragraphrowstyleFCTCSH\"><w:name w:val=\"Paragraph Rowstyle FCTCSH\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\"/><wx:font xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" wx:val=\"Arial\"/><w:sz w:val=\"16\"/><w:szCs w:val=\"16\"/><w:b w:val=\"true\"/></w:rPr><w:hidden/></w:style></w:styles><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:defaultTabStop w:val=\"720\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:allowPNG/><w:validateAgainstSchema/><w:saveInvalidXML w:val=\"off\"/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"><wsp:rsidRoot wsp:val=\"00820EBE\"/><wsp:rsid wsp:val=\"00145DA3\"/><wsp:rsid wsp:val=\"004D0FB9\"/><wsp:rsid wsp:val=\"006E3020\"/><wsp:rsid wsp:val=\"007001E6\"/><wsp:rsid wsp:val=\"00820EBE\"/><wsp:rsid wsp:val=\"008A0377\"/><wsp:rsid wsp:val=\"00930C65\"/><wsp:rsid wsp:val=\"00C11C73\"/><wsp:rsid wsp:val=\"00D63D66\"/><wsp:rsid wsp:val=\"00D80B98\"/><wsp:rsid wsp:val=\"00E55FED\"/><wsp:rsid wsp:val=\"00F76EFB\"/><wsp:rsid wsp:val=\"00F92C5E\"/></wsp:rsids></w:docPr><w:body> <w:t xml:space=\"preserve\"> hello</w:t></w:r></w:p><aml:annotation xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" aml:id=\"para_248846_15077_tt\" w:type=\"Word.Bookmark.End\"/></w:sect></w:body></w:wordDocument>");
        //        user_range.Select();

        //        #region Insert Heading1 and Heading2
        //        List<Microsoft.Office.Interop.Word.Bookmark> bmBookmarks = SortBookmark(document.Bookmarks);
        //        bool isFirst = true;
        //        foreach (Microsoft.Office.Interop.Word.Bookmark bm in bmBookmarks)
        //        {

        //            if (bm.Name.Contains("sect1_"))
        //            {
        //                bm.Range.set_Style(Microsoft.Office.Interop.Word.WdBuiltinStyle.wdStyleHeading2);
        //            }
        //        }
        //        #endregion

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        //internal List<Microsoft.Office.Interop.Word.Bookmark> SortBookmark(Microsoft.Office.Interop.Word.Bookmarks bookmarks)
        //{
        //    // List to store all bookmarks sorted by position.
        //    List<Microsoft.Office.Interop.Word.Bookmark> bmList = new List<Microsoft.Office.Interop.Word.Bookmark>();
        //    // Iterate over all the Bookmarks and add them to the list (unordered).
        //    foreach (Microsoft.Office.Interop.Word.Bookmark curBookmark in bookmarks)
        //    {
        //        //if(curBookmark.Range.Text.Contains("\r\f"))
        //        bmList.Add(curBookmark);
        //    }

        //    // Sort the List by the Start member of each Bookmark.
        //    // After this line the bmList will be ordered.
        //    bmList.Sort(delegate (Microsoft.Office.Interop.Word.Bookmark bm1, Microsoft.Office.Interop.Word.Bookmark bm2)
        //    {
        //        return bm1.Start.CompareTo(bm2.Start);
        //    });
        //    return (bmList);
        //}





        public List<ComponentNames> DocumentSearch_GetComponentData(String search, string strComponentType)
        {
            List<ComponentNames> lstComponentNames = null;
            lstComponentNames = new List<ComponentNames>();
            try
            {
                string queryParams = "?keyword=" + HttpUtility.UrlEncode(search) + strComponentType;

                Dictionary<string, string> docNameSearchWebServiceInfo = new Dictionary<string, string>();
                docNameSearchWebServiceInfo.Add("service.name", "getcomponentinfo");
                docNameSearchWebServiceInfo.Add("service.queryparams", queryParams);
                response = objHlprWebService.callWebService(docNameSearchWebServiceInfo);

                //bool blnResponse = ProcessResponse(strResponse);
                //if (blnResponse == true)
                //    return;

                var xElems = XDocument.Parse(response);

                if (xElems != null)
                {
                    foreach (var xRow in xElems.Root.Elements("component"))
                    {
                        ComponentNames component = new ComponentNames();
                        component.FADDMORE = xRow.Element("FADDMORE").Value;
                        component.FCOMPSTATUS = xRow.Element("FCOMPSTATUS").Value;
                        component.FCOMPSTATUS_DESC = xRow.Element("FCOMPSTATUS_DESC").Value;
                        component.FELEMENTINSTANCEID = xRow.Element("FELEMENTINSTANCEID").Value;
                        component.FELEMENT_DETAIL = xRow.Element("FELEMENT_DETAIL").Value;
                        component.FELEMENT_ID = xRow.Element("FELEMENT_ID").Value;
                        component.FEXPIRATIONDATE = xRow.Element("FEXPIRATIONDATE").Value;
                        component.FQUALDATA_DESC = xRow.Element("FQUALDATA_DESC").Value;
                        lstComponentNames.Add(component);
                    }
                }

                return lstComponentNames;
            }
            catch (Exception ex)
            {
                strMsgData = "Could not reach the server.  Please try again later or check your internet connection.";
                //MD_MsgBox.MaterialDesign_MessageBoxShow(strMsgData, 1);
                System.Windows.Forms.MessageBox.Show(strMsgData);
                log.Error("Document Search", ex);
            }
            return lstComponentNames;
        }


        //public void RenderWMLFromDMLForComponent(string strResponse)
        //{
        //    try
        //    {
        //        #region Code for Bookmark
        //        /*Microsoft.Office.Interop.Word.Document activeDocument = Globals.ThisAddIn.Application.ActiveDocument;
        //        Microsoft.Office.Tools.Word.Document vstoDoc = Globals.Factory.GetVstoObject(activeDocument);

        //        vstoDoc.Bookmarks.ShowHidden = true;
        //        foreach (Microsoft.Office.Interop.Word.Bookmark bookmark in vstoDoc.Bookmarks)
        //        {
        //            string name = bookmark.Name;
        //            String text = bookmark.Range.Text;
        //            string xml = bookmark.Range.XML;
        //            string wordml = bookmark.Range.WordOpenXML;
        //        }*/
        //        //string originalXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><book><part><chapter><sect1><table assetclass=\"American Beacon Funds\" bookInstId=\"1672\" cols=\"3\" compstatus=\"2\" datatype=\"1\" docdtlid=\"237966\" edgarTableType =\"C\" eid=\"38190\" languageid=\"1\" maxwidth=\"100\" primary =\"\" render=\"0\" renderwidth=\"100\" rows=\"3\" style=\"\" tabletype =\"contentsubad\"><tbody><row condition =\"TCH11\" htmlpagebreak=\"htmlpagebreak-row-950\"><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Impala Asset Management LLC(\"Impala\") </entry><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\" /><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\" /></row><row condition=\"TCH11\" htmlpagebreak=\"htmlpagebreak-row-951\"><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Controlling Person / Entity </entry><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Basis of Control </entry><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Nature of Controlling Person / Entity Business </entry></row><row condition=\"TR12\" htmlpagebreak=\"htmlpagebreak-row-952\"><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Robert Bishop </entry ><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Member Manager </entry ><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Financial Services Executive </entry></row><row condition=\"dummyrow\"><entry align=\"left\" condition=\"dummyrow\" maxwidth=\"33.33\" type=\"text\" valign=\"top\" /><entry align=\"left\" condition=\"dummyrow\" maxwidth=\"33.33\" type =\"text\" valign=\"top\" /><entry align=\"left\" condition=\"dummyrow\" maxwidth=\"33.33\" type =\"text\" valign=\"top\" /></row></tbody></table></sect1></chapter></part></book>";
        //        #endregion

        //        string originalXml = strResponse;
        //        if (originalXml.Contains("<data>not found</data>"))
        //        {
        //            strMsgData = "Document info not found...";

        //            System.Windows.MessageBox.Show(strMsgData);
        //            return;
        //        }
        //        string docBookDecodedXml = HlprDocumentTransformation.RemoveDocbookEncoding(originalXml);
        //        string strXSLTFile = "CompXmlToWordML.xml";
        //        string data = data = AppaDocx.Rendering.HlprCoreXSLTTrans.ProcessDW_WDTransform(docBookDecodedXml, strXSLTFile);
        //        Microsoft.Office.Interop.Word.Document document = Globals.ThisAddIn.Application.ActiveDocument;
        //        String strData = data;
        //        Microsoft.Office.Interop.Word.Range user_range = Globals.ThisAddIn.Application.Selection.Range;
        //        int pCount = Globals.ThisAddIn.Application.ActiveDocument.Paragraphs.Count;
        //        user_range.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);
        //        if (strData.Contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>") && strData.Contains("<w:wordDocument xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\">"))
        //        {
        //            strData = strData.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
        //        }
        //        user_range.InsertXML(strData);
        //        user_range.Select();

        //        #region Insert Heading1 and Heading2

        //        //List<Microsoft.Office.Interop.Word.Bookmark> bmBookmarks = SortBookmark(Globals.ThisAddIn.Application.ActiveDocument.Bookmarks);
        //        //bool isFirst = true;

        //        //foreach (Microsoft.Office.Interop.Word.Bookmark bm in bmBookmarks)
        //        //{

        //        //    if (bm.Name.Contains("sect1_"))
        //        //    {
        //        //        bm.Range.set_Style(Microsoft.Office.Interop.Word.WdBuiltinStyle.wdStyleHeading2);
        //        //    }
        //        //}
        //        #endregion

        //        #region Code for Bookmark Testing
        //        /*Microsoft.Office.Interop.Word.Document activeDocument1 = Globals.ThisAddIn.Application.ActiveDocument;
        //        Microsoft.Office.Tools.Word.Document vstoDoc1 = Globals.Factory.GetVstoObject(activeDocument1);

        //        vstoDoc1.Bookmarks.ShowHidden = true;
        //        foreach (Microsoft.Office.Interop.Word.Bookmark bookmark in vstoDoc1.Bookmarks)
        //        {
        //            string name = bookmark.Name;
        //            String text = bookmark.Range.Text;
        //            string xml = bookmark.Range.XML;
        //            string wordml = bookmark.Range.WordOpenXML;
        //        }*/
        //        #endregion

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        // SATISH ADDED
        public string RenderPresentationMLFromDMLForComponent(string strResponse)
        {
            //string originalXml = strResponse;
            //if (originalXml.Contains("<data>not found</data>"))
            //{
            //    strMsgData = "Document info not found...";

            //    System.Windows.MessageBox.Show(strMsgData);
            //    return "";
            //}
            //string docBookDecodedXml = HlprDocumentTransformation.RemoveDocbookEncoding(originalXml);
            //return docBookDecodedXml;



            //************************ XSLT code here ************************//

            try
            {
                #region Code for Bookmark
                /*Microsoft.Office.Interop.Word.Document activeDocument = Globals.ThisAddIn.Application.ActiveDocument;
                Microsoft.Office.Tools.Word.Document vstoDoc = Globals.Factory.GetVstoObject(activeDocument);

                vstoDoc.Bookmarks.ShowHidden = true;
                foreach (Microsoft.Office.Interop.Word.Bookmark bookmark in vstoDoc.Bookmarks)
                {
                    string name = bookmark.Name;
                    String text = bookmark.Range.Text;
                    string xml = bookmark.Range.XML;
                    string wordml = bookmark.Range.WordOpenXML;
                }*/
                //string originalXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><book><part><chapter><sect1><table assetclass=\"American Beacon Funds\" bookInstId=\"1672\" cols=\"3\" compstatus=\"2\" datatype=\"1\" docdtlid=\"237966\" edgarTableType =\"C\" eid=\"38190\" languageid=\"1\" maxwidth=\"100\" primary =\"\" render=\"0\" renderwidth=\"100\" rows=\"3\" style=\"\" tabletype =\"contentsubad\"><tbody><row condition =\"TCH11\" htmlpagebreak=\"htmlpagebreak-row-950\"><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Impala Asset Management LLC(\"Impala\") </entry><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\" /><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\" /></row><row condition=\"TCH11\" htmlpagebreak=\"htmlpagebreak-row-951\"><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Controlling Person / Entity </entry><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Basis of Control </entry><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Nature of Controlling Person / Entity Business </entry></row><row condition=\"TR12\" htmlpagebreak=\"htmlpagebreak-row-952\"><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Robert Bishop </entry ><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Member Manager </entry ><entry align=\"left\" maxwidth=\"33.33\" type=\"text\" valign=\"top\">Financial Services Executive </entry></row><row condition=\"dummyrow\"><entry align=\"left\" condition=\"dummyrow\" maxwidth=\"33.33\" type=\"text\" valign=\"top\" /><entry align=\"left\" condition=\"dummyrow\" maxwidth=\"33.33\" type =\"text\" valign=\"top\" /><entry align=\"left\" condition=\"dummyrow\" maxwidth=\"33.33\" type =\"text\" valign=\"top\" /></row></tbody></table></sect1></chapter></part></book>";
                #endregion

                string originalXml = strResponse;
                if (originalXml.Contains("<data>not found</data>"))
                {
                    strMsgData = "Document info not found...";

                    System.Windows.MessageBox.Show(strMsgData);
                    return "";
                }
                string docBookDecodedXml = HlprDocumentTransformation.RemoveDocbookEncoding(originalXml);
                string strXSLTFile = "XSLTFileForHtmlNew.xsl";// "CompXmlToWordML.xml";
                string data = data = AppaDocx.Rendering.HlprCoreXSLTTrans.ProcessDW_WDTransform(docBookDecodedXml, strXSLTFile);
                //string data = "<html><body><h1>satish</h1>  <hr/> <i>teset1</i> <br/> <u>underlinr</u>  <table><tr><td> th1 </td> <td> th2 </td> </tr> <tr> <td> th1adsdasd </td> <td> tasdsah2 </td> </tr></table></body></html> ";
                // "<html> < body > < table style = 'width:100%' border = '1' > < tr style = 'background-color:lightblue;font-weight:bold' >< th > Expenses </ th > < th > test2 </ th ></ tr >< tr > < td >100</ td >< td > 200 </ td ></ tr ></ table > </ body ></ html > ";//data = AppaDocx.Rendering.HlprCoreXSLTTrans.ProcessDW_WDTransform(docBookDecodedXml, strXSLTFile);
                //satish added this line
                return data;




                //Microsoft.Office.Interop.Word.Document document = Globals.ThisAddIn.Application.ActiveDocument;
                //String strData = data;
                //Microsoft.Office.Interop.Word.Range user_range = Globals.ThisAddIn.Application.Selection.Range;
                //int pCount = Globals.ThisAddIn.Application.ActiveDocument.Paragraphs.Count;
                //user_range.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);
                //if (strData.Contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>") && strData.Contains("<w:wordDocument xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\">"))
                //{
                //    strData = strData.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
                //}
                //user_range.InsertXML(strData);
                //user_range.Select();

                #region Insert Heading1 and Heading2

                //List<Microsoft.Office.Interop.Word.Bookmark> bmBookmarks = SortBookmark(Globals.ThisAddIn.Application.ActiveDocument.Bookmarks);
                //bool isFirst = true;

                //foreach (Microsoft.Office.Interop.Word.Bookmark bm in bmBookmarks)
                //{

                //    if (bm.Name.Contains("sect1_"))
                //    {
                //        bm.Range.set_Style(Microsoft.Office.Interop.Word.WdBuiltinStyle.wdStyleHeading2);
                //    }
                //}
                #endregion

                #region Code for Bookmark Testing
                /*Microsoft.Office.Interop.Word.Document activeDocument1 = Globals.ThisAddIn.Application.ActiveDocument;
                Microsoft.Office.Tools.Word.Document vstoDoc1 = Globals.Factory.GetVstoObject(activeDocument1);

                vstoDoc1.Bookmarks.ShowHidden = true;
                foreach (Microsoft.Office.Interop.Word.Bookmark bookmark in vstoDoc1.Bookmarks)
                {
                    string name = bookmark.Name;
                    String text = bookmark.Range.Text;
                    string xml = bookmark.Range.XML;
                    string wordml = bookmark.Range.WordOpenXML;
                }*/
                #endregion

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
