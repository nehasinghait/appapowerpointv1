﻿using AppaDocx.Common.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AppaDocx.Helpers
{
    public class HlprDocumentTransformation
    {
        #region Variable Declaration
        private static List<Tuple<string, string>> matchingReplacements;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HlprDocumentTransformation));

        #endregion

        #region Function Declaration

        ///<summary>
        ///<para>Creates an method to replace all docbook entities into unicode</para>
        ///</summary>
        public static void InitailizeReplacementRegex(string dir)
        {
            try
            {
                if (!AppaDocx.Common.GlobalsData.dir.Contains("TemplatesRequires"))
                {
                    AppaDocx.Common.GlobalsData.dir = AppaDocx.Common.GlobalsData.rootDir + "\\TemplatesRequires";
                }
                //read all the text from the dtd for DocBook entities
                string sourceDtd = File.ReadAllText(AppaDocx.Common.GlobalsData.rootDir + "/TemplatesRequires/entities.xml");

                // this regex finds all pairs in the dtd
                // example format and outputForImgWithAttr 
                // <!ENTITY nbsp "&#160;">
                // first capture group: nbsp
                // second capture group: &#160;
                Regex replacement = new Regex("<!ENTITY ([^\\s]+)\\s+\"(&#x*[0-9A-F]+;)\">");

                // matches is a collection of each match from the outputForImgWithAttr of the regex
                MatchCollection matches = replacement.Matches(sourceDtd);

                // maps capture groups to their replacement
                matchingReplacements = new List<Tuple<string, string>>();
                foreach (Match match in matches)
                {
                    // add each first group to the regex and second to the parallel matching replacements
                    Tuple<string, string> tuple = new Tuple<string, string>("&" + match.Groups[1].Value + ";", match.Groups[2].Value);
                    matchingReplacements.Add(tuple);
                }
            }
            catch (Exception ex)
            {
                log.Error("InitailizeReplacementRegex", ex);
            }
        }

        ///<summary>
        ///<para>replaces docbook encoded symbols</para>
        ///</summary>
        public static string RemoveDocbookEncoding(string xml)
        {
            try
            {
                if (matchingReplacements == null)
                {
                    InitailizeReplacementRegex(Common.GlobalsData.dir);
                }
                foreach (Tuple<string, string> tuple in matchingReplacements)
                {
                    xml = xml.Replace(tuple.Item1, tuple.Item2);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }


        #endregion
         
    }
}
