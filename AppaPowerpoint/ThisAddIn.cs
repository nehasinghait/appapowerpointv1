﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Office = Microsoft.Office.Core;
using System.Windows;
using System.IO;
using Microsoft.Office.Core;
using AppaPowerpoint.Ribbon;
using AppaPowerpoint.ReuseSlidePane;
using AppaPowerpoint.WatermarkHelpers;

namespace AppaPowerpoint
{
    public partial class ThisAddIn
    {
        #region Delaration
        private AppaPowerpointUserControl objUserControl;
        private Microsoft.Office.Tools.CustomTaskPane myCustomTaskPane;
        #endregion
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        { 
            //home dir
            SetRootDirectory();
            InstantiateAppaPowerpointUI();
            System.Threading.Thread TrackerThread = new System.Threading.Thread(PropertyManager.ExecuteTrackerForSlideModify);
            TrackerThread.Start();
            //InstantiateCustomReuseSlidePane();
            //  ConvertPresentationSlidesToImages();


        }

       


        private void SetRootDirectory()
        {
            AppaDocx.Common.GlobalsData.dir = AppDomain.CurrentDomain.BaseDirectory;
            string DirName = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            string ParentName = Directory.GetParent((Directory.GetParent(DirName).FullName)).FullName;
            AppaDocx.Common.GlobalsData.rootDir = ParentName;
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        ///to exicute the custome ribbon
        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new rbnShowUI();
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion

        #region Methods
        /// <summary>
        /// this method create the custom task pane
        /// </summary>
        private void InstantiateAppaPowerpointUI()
        {
            try
            {
                //remove all task pane before creating new one
                RemoveTaskPane();

                //AppaPowerpointUI appaPowerpointUI = new AppaPowerpointUI();
                ////AppaDocx.Common.GlobalsData.dicBkmrkEmbBindWithImg = new Dictionary<string, string>();
                //AppaPowerpointUserControl myAddInUserControl = new AppaPowerpointUserControl(appaPowerpointUI);
                ////  myAddInUserControl.Resize += myAddInUserControl_Resize;
                //string strVersionNumber = " ";
                //myCustomTaskPane = Globals.ThisAddIn.CustomTaskPanes.Add(myAddInUserControl, strVersionNumber);
                //myCustomTaskPane.Width = 350;
                //myCustomTaskPane.DockPositionRestrict = Microsoft.Office.Core.MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoChange;
                //myCustomTaskPane.Visible = true;

                AppaPowerpointUI appaPowerpointUI = new AppaPowerpointUI();
                objUserControl = new AppaPowerpointUserControl(appaPowerpointUI);
                myCustomTaskPane = this.CustomTaskPanes.Add(objUserControl, " ");
                myCustomTaskPane.Visible = true;
                myCustomTaskPane.Width = 350;
               




                myCustomTaskPane.DockPositionRestrict = Microsoft.Office.Core.MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoChange;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            #region Trying to Adjust CustomTaskPane(ref)
            //int iCommandBarHeight = Application.CommandBars["Ribbon"].Height;
            //iCommandBarHeight += 10;
            //Application.CommandBars["Ribbon"].Height = iCommandBarHeight;
            ////int iTopMargin = Convert.ToInt32( Application.ActiveDocument.PageSetup.TopMargin);
            ////int iAppTop = Application.Top;
            ////int top = iAppTop +  iTopMargin + iCommandBarHeight;
            ////int left = Application.ActiveDocument.Left + Application.ActiveDocument.PageSetup.LeftMargin;
            ////CustomTaskPane.Width= (left / 2);   //Left=Width
            //CustomTaskPane.Width= Convert.ToInt32(Application.ActiveDocument.PageSetup.PageWidth);   //Left=Width
            #endregion
        }

        public void create_Taskpane(PowerPoint.Presentation presentaion)
        {
            InstantiateAppaPowerpointUI();
        }

        /// <summary>
        /// this method remove the allready created task panes
        /// </summary>
        public void RemoveTaskPane()
        {
            for (int i = this.CustomTaskPanes.Count; i > 0; i--)
            {
                var ctp = this.CustomTaskPanes[i - 1];
                this.CustomTaskPanes.RemoveAt(i - 1);
            }
        }


        #endregion

        //CODE FOR REUSE SLIDE PANE
        private ReuseSlideUserControl objReuseSlideUserControl;
        private void InstantiateCustomReuseSlidePane()
        {
            try
            {
                //remove all task pane before creating new one
                //RemoveTaskPane();

                //AppaPowerpointUI appaPowerpointUI = new AppaPowerpointUI();
                ////AppaDocx.Common.GlobalsData.dicBkmrkEmbBindWithImg = new Dictionary<string, string>();
                //AppaPowerpointUserControl myAddInUserControl = new AppaPowerpointUserControl(appaPowerpointUI);
                ////  myAddInUserControl.Resize += myAddInUserControl_Resize;
                //string strVersionNumber = " ";
                //myCustomTaskPane = Globals.ThisAddIn.CustomTaskPanes.Add(myAddInUserControl, strVersionNumber);
                //myCustomTaskPane.Width = 350;
                //myCustomTaskPane.DockPositionRestrict = Microsoft.Office.Core.MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoChange;
                //myCustomTaskPane.Visible = true;

                CustomReuseSlidePane customReuseSlidePane = new CustomReuseSlidePane();
                objReuseSlideUserControl = new ReuseSlideUserControl(customReuseSlidePane);
                myCustomTaskPane = this.CustomTaskPanes.Add(objReuseSlideUserControl, "Reuse Slide");
                myCustomTaskPane.DockPosition = MsoCTPDockPosition.msoCTPDockPositionLeft;
                myCustomTaskPane.Visible = true;
                myCustomTaskPane.Width = 265;



               // myCustomTaskPane.DockPositionRestrict = Microsoft.Office.Core.MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoChange;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            #region Trying to Adjust CustomTaskPane(ref)
            //int iCommandBarHeight = Application.CommandBars["Ribbon"].Height;
            //iCommandBarHeight += 10;
            //Application.CommandBars["Ribbon"].Height = iCommandBarHeight;
            ////int iTopMargin = Convert.ToInt32( Application.ActiveDocument.PageSetup.TopMargin);
            ////int iAppTop = Application.Top;
            ////int top = iAppTop +  iTopMargin + iCommandBarHeight;
            ////int left = Application.ActiveDocument.Left + Application.ActiveDocument.PageSetup.LeftMargin;
            ////CustomTaskPane.Width= (left / 2);   //Left=Width
            //CustomTaskPane.Width= Convert.ToInt32(Application.ActiveDocument.PageSetup.PageWidth);   //Left=Width
            #endregion
        }
    }
}
