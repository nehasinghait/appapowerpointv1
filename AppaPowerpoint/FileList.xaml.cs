﻿using AppaDocx.Common;
using AppaPowerpoint.ReuseSlidePane;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppaPowerpoint
{
    /// <summary>
    /// Interaction logic for FileList.xaml
    /// </summary>
    public partial class FileList : Window
    {
        Google.Apis.Drive.v3.DriveService ServiceGlobalVarialbe = null;
        public FileList()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        public void PopulateFileButtons(Google.Apis.Drive.v3.DriveService service, List<Google.Apis.Drive.v3.Data.File> DriveFiles)
        {
            try
            {
                ServiceGlobalVarialbe = service;
                foreach (var File in DriveFiles)
                {
                    Button b = new Button();
                    b.Content = File.Name;
                    //b.Name = File.Id;
                    b.ToolTip = File.Id;
                    b.Tag = File;
                    b.Height = 50;
                    b.Width = 200;
                    b.Background = Brushes.White;
                    b.Foreground = Brushes.Gray;
                    b.Margin = new Thickness(10, 10, 10, 10);
                    b.Click += new RoutedEventHandler(b_Click);
                    StackPanelForFiles.Children.Add(b);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void b_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button FileButton = sender as Button;
                Google.Apis.Drive.v3.Data.File DriveFile = FileButton.Tag as Google.Apis.Drive.v3.Data.File;
                string fileURI = GlobalsData.rootDir + "\\StorageConnectors\\GoogleDriveDownloads\\" + DriveFile.Name;
                ////Code for download the file
                AppPowerpoint.StorageConnectors.GoogleDriveAccess.DownloadFile(ServiceGlobalVarialbe, DriveFile, fileURI);
                Google.Apis.Drive.v3.Data.File file = ServiceGlobalVarialbe.Files.Get(DriveFile.Id).Execute();
                OpenPresentation(fileURI);

                // MessageBox.Show(DriveFile.Name + " Downloaded");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        //function created to oprn ppt on main window of powerpoint.
        //added by srijan

        private Microsoft.Office.Tools.CustomTaskPane myCustomTaskPane;
        private ReuseSlideUserControl objReuseSlideUserControl;
        public void OpenPresentation(string fileUri)
        {
            try
            {
                //Globals.ThisAddIn.Application.ActivePresentation.Close();
                // Globals.ThisAddIn.Application.Quit();
                //Marshal.FinalReleaseComObject(Globals.ThisAddIn.Application);

                // Globals.ThisAddIn.Application.Presentations.Open(fileUri);


               
                CustomReuseSlidePaneBuilder customReuseSlidePaneBuilder = new CustomReuseSlidePaneBuilder();
                customReuseSlidePaneBuilder.InstantiateCustomReuseSlidePane(fileUri);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

        }
    }
}
