﻿using Dropbox.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StorageConnectors;
using Dropbox.Api.Files;
using Microsoft.Office.Core;
using AppaDocx.Common;

namespace AppaPowerpoint
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Variables    
        private string strAppKey = "gajl01wj2ozq009";//"[Yor Application App Key]";
        private string strAccessToken = string.Empty;
        private string strAuthenticationURL = string.Empty;
        private DropBoxBase DBB;
        #endregion

        #region Constructor    
        public MainWindow()
        {
            InitializeComponent();
        }
        #endregion

        #region Private Methods    
        public void Authenticate()
        {
            try
            {
                if (string.IsNullOrEmpty(strAppKey))
                {
                    MessageBox.Show("Please enter valid App Key !");
                    return;
                }
                if (DBB == null)
                {
                    DBB = new DropBoxBase(strAppKey, "TestApp");

                    strAuthenticationURL = DBB.GeneratedAuthenticationURL(); // This method must be executed before generating Access Token.    
                    strAccessToken = DBB.GenerateAccessToken();
                    gbDropBox.IsEnabled = true;
                    if (!string.IsNullOrEmpty(strAccessToken))
                    {
                        AppaPowerpoint.MainWindow objMainWindow = new MainWindow();
                        objMainWindow.Show();
                    }
                }
                else gbDropBox.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }
        #endregion

        private void btnApiKey_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                strAppKey = txtApiKey.Text.Trim();
                Authenticate();
            }
            catch (Exception)
            {
                throw;
            }

        }

        private void btnCreateFolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DBB != null)
                {
                    if (strAccessToken != null && strAuthenticationURL != null)
                    {
                        if (DBB.FolderExists("/Dropbox/DotNetApi") == false)
                        {
                            DBB.CreateFolder("/Dropbox/DotNetApi");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex = ex.InnerException ?? ex;
            }
        }

        private void btlUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DBB != null)
                {
                    if (strAccessToken != null && strAuthenticationURL != null)
                    {
                        DBB.Upload("/Dropbox/DotNetApi", "Sample-test.jpg", @"D:\Capture4-test.PNG");
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DBB != null)
                {
                    if (strAccessToken != null && strAuthenticationURL != null)
                    {
                        //  DBB.Download("/Dropbox/DotNetApi", "Sample-test.jpg", @"D:\Satish work\AppaPowerpoint\AppaPowerpoint\StorageConnectors\DropBoxDownloads", "capture4_dwnld.png");
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DBB != null)
                {
                    if (strAccessToken != null && strAuthenticationURL != null)
                    {
                        DBB.Delete("/Dropbox/DotNetApi");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //satish added


        public void FileList(string path, string AccessToken)
        {
            try
            {
                DBB = new DropBoxBase(strAppKey, "TestApp");
                strAuthenticationURL = DBB.GeneratedAuthenticationURL(); // This method must be executed before generating Access Token.    
                strAccessToken = AccessToken;//DBB.GenerateAccessToken();
                gbDropBox.IsEnabled = true;

                List<string> FileNameList = DBB.FileList(path, AccessToken).Result;
                PopulateFileButtons(FileNameList, path);

                //display acc info
                var AccountInfo = DBB.GetAccountInfo().Result;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        public void PopulateFileButtons(List<string> FileNameList, string path)
        {
            foreach (var File in FileNameList)
            {
                Button b = new Button();
                b.Content = File.ToString();
                //b.Name = File.Id;
                // b.ToolTip = File.Id;
                b.Tag = path;
                b.Height = 50;
                //b.Width = 100;
                b.Background = Brushes.White;
                b.Foreground = Brushes.Gray;
                b.Margin = new Thickness(10, 10, 10, 10);
                b.Click += new RoutedEventHandler(btnDropBoxFileDownload_Click);
                StackPanelForFiles.Children.Add(b);
            }

          
        }



        private async void btnDropBoxFileDownload_Click(object sender, RoutedEventArgs e)
        {
            Button btn1 = sender as Button;
            string fileuri = GlobalsData.rootDir + "\\" + "StorageConnectors\\DropBoxDownloads\\" + btn1.Content.ToString();
            try
            {
                string LocalFilePath = GlobalsData.rootDir + "\\" + "StorageConnectors\\DropBoxDownloads";
                Button btn = sender as Button;
                string Path = Convert.ToString(btn.Tag);
                string FileName = btn.Content.ToString();
                if (DBB != null)
                {
                    if (strAccessToken != null && strAuthenticationURL != null)
                    {
                        bool IsDownloaded = await DBB.Download(Path, FileName, LocalFilePath, FileName);

                        if (IsDownloaded)
                        {

                            //MessageBox.Show("Downloaded and ready to open");
                           

                        }
                    }
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            FileList fl = new FileList();
            fl.OpenPresentation(fileuri);


        }

        public void OpenDownloadedFile(string LocalFilePath, string FileName)
        {
            //Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
            //Microsoft.Office.Interop.PowerPoint.Presentation objPres = ppApp.ActivePresentation;
            var app = new Microsoft.Office.Interop.PowerPoint.Application();
            var pres = app.Presentations;
            var file = pres.Open(LocalFilePath + "\\" + FileName, MsoTriState.msoTrue, MsoTriState.msoTrue, MsoTriState.msoFalse);

        }
    }
}

