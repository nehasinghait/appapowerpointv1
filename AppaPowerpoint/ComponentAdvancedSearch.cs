﻿using AppaDocx.Common.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AppaDocx.WebService;
using System.IO;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using AppaDocx.Common;
using System.Collections;
using System.Text.RegularExpressions;
using AppaDocx.Application.Helpers;
using AppaDocx;

namespace AppaDocx.UserInterfaces
{
    /// <summary>
    /// Interaction logic for MsgBoxComment.xaml
    /// </summary>
    public partial class ComponentAdvancedSearch : Window
    {
        AppaDocx.WebService.HlprWebService objHlprWebService = new HlprWebService();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ComponentAdvancedSearch));
        #region Events Definations
        public ComponentAdvancedSearch()
        {
            try
            {
                InitializeComponent();

                Dictionary<string, string> SearchComponentWebServiceInfo = new Dictionary<string, string>();
                ConstantsValues.lstComponentAddedtoCart = new List<ComponentNames>();

                SearchComponentWebServiceInfo.Add("service.name", "getSearchData");
                string strResponse = objHlprWebService.callWebService(SearchComponentWebServiceInfo);

                //bool blnResponse = Globals.ThisAddIn.app.ProcessResponse(strResponse);
                //if (blnResponse == true)
                //{
                //    this.Visibility = Visibility.Hidden;
                //    this.Hide();
                //    return;

                List<ComponentAdvanceSearch> lstResult = null;
                JavaScriptSerializer JsonSerializer = new JavaScriptSerializer();

                var result = JsonSerializer.Deserialize<ComponentAdvanceSearch>(strResponse);

                AddSelectAlltoListBox(AssetListBoxSearchesPnl);
                AddSelectAlltoListBox(ContextListBoxSearchesPnl);
                AddSelectAlltoListBox(FundListBoxSearchesPnl);
                AddSelectAlltoListBox(LocaleListBoxSearchesPnl);
                AddSelectAlltoListBox(StatusListBoxSearchesPnl);
                AddSelectAlltoListBox(TypeListBoxSearchesPnl);

                if (result != null)
                {
                    foreach (var item in result.assetclasses)
                    {
                        ListViewItem lbi = new ListViewItem();
                        lbi.Content = item.assetClassName;
                        lbi.Tag = item.assetClassId;
                        lbi.Selected += Child_Selected;
                        AssetListBoxSearchesPnl.Items.Add(lbi);
                    }

                    foreach (var item in result.contexts)
                    {
                        ListViewItem lbi = new ListViewItem();
                        lbi.Content = item.elementContextDesc;
                        lbi.Tag = item.elementContextId;
                        lbi.Selected += Child_Selected;
                        ContextListBoxSearchesPnl.Items.Add(lbi);
                    }

                    foreach (var item in result.funds)
                    {
                        ListViewItem lbi = new ListViewItem();
                        lbi.Content = item.fundName;
                        lbi.Tag = item.fundId;
                        lbi.Selected += Child_Selected;
                        FundListBoxSearchesPnl.Items.Add(lbi);
                    }

                    foreach (var item in result.locales)
                    {
                        ListViewItem lbi = new ListViewItem();
                        lbi.Content = item.localeName;
                        lbi.Tag = item.localeId;
                        lbi.Selected += Child_Selected;
                        LocaleListBoxSearchesPnl.Items.Add(lbi);
                    }

                    foreach (var item in result.status)
                    {
                        ListViewItem lbi = new ListViewItem();
                        lbi.Content = item.statusName;
                        lbi.Tag = item.statusId;
                        lbi.Selected += Child_Selected;
                        StatusListBoxSearchesPnl.Items.Add(lbi);
                    }

                    foreach (var item in result.componentTypes)
                    {
                        ListViewItem lbi = new ListViewItem();
                        lbi.Content = item.componentTypeName;
                        lbi.Tag = item.componentTypeId;
                        lbi.Selected += Child_Selected;
                        TypeListBoxSearchesPnl.Items.Add(lbi);

                    }
                }

                //btnCartItems.Visibility = Visibility.Collapsed;
                RefreshCartItems();
            }
            catch (Exception ex)
            {
                log.Error("Error in Component Advanced Search", ex);
            }
        }
        public void AddSelectAlltoListBox(ListBox lsttoAdd)
        {
            ListViewItem selectall = new ListViewItem();
            selectall.Content = "--Select All--";
            selectall.Tag = "-1";
            selectall.Selected += SelectAlItem_Click;
            lsttoAdd.Items.Insert(0, selectall);
            lsttoAdd.SelectedIndex = 0;
        }
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string queryParam = "";
                string fund = "";
                string context = "";
                string asset = "";
                string local = "";
                string status = "";
                string componentType = "";

                foreach (ListViewItem item in FundListBoxSearchesPnl.SelectedItems)
                {
                    if (fund != "")
                        fund += "," + item.Tag;
                    else
                        fund += item.Tag;
                }

                foreach (ListViewItem item in ContextListBoxSearchesPnl.SelectedItems)
                {
                    if (context != "")
                        context += "," + item.Tag;
                    else
                        context += item.Tag;
                }

                foreach (ListViewItem item in AssetListBoxSearchesPnl.SelectedItems)
                {
                    if (asset != "")
                        asset += "," + item.Tag;
                    else
                        asset += item.Tag;
                }

                foreach (ListViewItem item in LocaleListBoxSearchesPnl.SelectedItems)
                {
                    if (local != "")
                        local += "," + item.Tag;
                    else
                        local += item.Tag;
                }

                foreach (ListViewItem item in StatusListBoxSearchesPnl.SelectedItems)
                {
                    if (status != "")
                        status += "," + item.Tag;
                    else
                        status += item.Tag;
                }

                foreach (ListViewItem item in TypeListBoxSearchesPnl.SelectedItems)
                {
                    if (componentType != "")
                        componentType += "," + item.Tag;
                    else
                        componentType += item.Tag;
                }

                if (fund != "")
                    queryParam += "funds=" + fund;
                if (asset != "")
                    queryParam += "&assetclasses=" + asset;
                if (context != "")
                    queryParam += "&contexts=" + context;
                if (local != "")
                    queryParam += "&locales=" + local;
                if (status != "")
                    queryParam += "&status=" + status;
                if (componentType != "")
                    queryParam += "&types=" + componentType;


                if (txtcomponentname.Text != "")
                    queryParam += "&componentname=" + txtcomponentname.Text;

                if (txtkeyword.Text != "")
                    queryParam += "&searchtext=" + txtkeyword.Text;

                Dictionary<string, string> SearchComponentWebServiceInfo = new Dictionary<string, string>();
                SearchComponentWebServiceInfo.Add("service.name", "searchcomponents");
                SearchComponentWebServiceInfo.Add("service.method", "POST");
                SearchComponentWebServiceInfo.Add("service.data", queryParam);
                SearchComponentWebServiceInfo.Add("service.content-type", "application/x-www-form-urlencoded");
                string strResponse = objHlprWebService.callWebService(SearchComponentWebServiceInfo);

                //satish added
                AppaPowerpoint.AppaPowerpointUI objAppaPowerpointUI = new AppaPowerpoint.AppaPowerpointUI();
                //ends
                bool blnResponse = objAppaPowerpointUI.ProcessResponse(strResponse);// Globals.ThisAddIn.appaDocxUI.ProcessResponse(strResponse);
                if (blnResponse == true)
                {
                    this.Visibility = Visibility.Hidden;
                    this.Hide();
                    return;
                }

                ListBoxSearchesPnl.Children.Clear();
                List<ComponentNames> lstResult = null;

                lstResult = JsonConvert.DeserializeObject<List<ComponentNames>>(strResponse);

                if (lstResult != null)
                {
                    //TreeViewItem tvi1 = new TreeViewItem();
                    CheckBox Parent = new CheckBox();
                    Parent.Content = "Select All";
                    Parent.Margin = new Thickness(0,8, 0, 5);

                    ////lblResultCount.Content = "(Search Result : " + lstResult.Count.ToString() + ")";
                    Parent.Click += _ParentClick;
                    //tvi1.Header = Parent;
                    //tvi1.Margin = new Thickness(0, 0, 0, 0);
                    ListBoxSearchesPnl.Children.Add(Parent);
                    foreach (ComponentNames SingleComponent in lstResult)
                    {
                        bool isFoundFlag = false;

                        foreach (var i in ConstantsValues.lstComponentAddedtoCart)
                        {
                            if (i.FELEMENTINSTANCEID == SingleComponent.FELEMENTINSTANCEID)
                                isFoundFlag = true;
                        }
                        if (isFoundFlag == false)
                        {
                            ListItemSearch lstItemSearch = new ListItemSearch();
                            //lstItemSearch.Margin = new Thickness(-2, 0, 0, 0);

                            //lstItemSearch.BorderThickness = new Thickness(1);
                            //lstItemSearch.BorderBrush = Brushes.LightGray;
                            //lstItemSearch.Margin = new Thickness(0, 8, 0, 0);

                            //DockPanel txtMainBlock = new DockPanel();
                            //CheckBox chkChild = new CheckBox();
                            //TextBlock txtBlockCText = new TextBlock();
                            ////TextBlock txtBlock = new TextBlock();

                            //Run link = new Run(SingleComponent.FQUALDATA_DESC);
                            //link.FontFamily = new System.Windows.Media.FontFamily("Arial");
                            //link.FontSize = 12;

                            //TextBlock ctext = new TextBlock(link);
                            //ctext.Tag = SingleComponent;
                            //ctext.TextWrapping = TextWrapping.Wrap;

                            //TextBlock txtBlock = new TextBlock();
                            //txtBlock.Margin = new Thickness(2, 0, 0, 0);

                            //TextBlock txtBlockdata = new TextBlock();

                            string text = "";
                            if (SingleComponent.FELEMENT_DETAIL != null)
                                text = SingleComponent.FELEMENT_DETAIL.ToString().Trim();

                            AppaDocx.Application.Helpers.HlprDocumentManager objhlprDocumentManager = new HlprDocumentManager();
                            text = objhlprDocumentManager.StripHtml(text);

                            string strdata = "";
                            if (text.Length > 140)
                                strdata = text.Substring(0, 140);
                            else
                                strdata = text;

                            string strcomponentname = "";

                            if (SingleComponent.FQUALDATA_DESC.Length > 45)
                                strcomponentname = SingleComponent.FQUALDATA_DESC.Substring(0, 45);
                            else
                                strcomponentname = SingleComponent.FQUALDATA_DESC;

                            lstItemSearch.txtcomponentname.Content = strcomponentname;
                            lstItemSearch.txtContent.Text = strdata;

                            //satish commented for icons
                            //switch (SingleComponent.FELEMENT_ID)
                            //{
                            //    case "para":
                            //        lstItemSearch.imgcompType.Source = (ImageSource)FindResource("Imagepara");
                            //        break;
                            //    case "bridgehead":
                            //        lstItemSearch.imgcompType.Source = (ImageSource)FindResource("Imagesubhead");
                            //        break;
                            //    case "footnote":
                            //        lstItemSearch.imgcompType.Source = (ImageSource)FindResource("Imagefootnote");
                            //        break;
                            //    case "table":
                            //        lstItemSearch.imgcompType.Source = (ImageSource)FindResource("Imagetable");
                            //        break;
                            //    case "note":
                            //        lstItemSearch.imgcompType.Source = (ImageSource)FindResource("Imagenote");
                            //        break;
                            //}

                            lstItemSearch.imgcompType.Width = 22;
                            lstItemSearch.imgcompType.Height = 22;

                            //txtBlockdata.Text = 
                            //txtBlockdata.TextWrapping = TextWrapping.Wrap;
                            //lstItemSearch.chkComponent.FontFamily = new System.Windows.Media.FontFamily("Arial");
                            //chkChild.FontSize = 12;
                            //chkChild.Padding = new Thickness(0, 0, 0, 0);
                            lstItemSearch.chkComponent.Tag = SingleComponent;
                            lstItemSearch.chkComponent.Click += _ListElementClick;
                            lstItemSearch.Margin = new Thickness(0, 0, 0, 5);
                            //txtBlockCText.Padding = new Thickness(0.1, 0, 0, 5);
                            //txtBlockCText.Inlines.Add(txtBlock);
                            //txtBlockCText.Inlines.Add(ctext);
                            //txtBlockCText.Inlines.Add(txtBlockdata);
                            //txtBlockCText.TextWrapping = TextWrapping.Wrap;

                            //string tooltip = "";
                            //if (text.Length > 700)
                            //    tooltip = text.Substring(0, 700);
                            //else
                            //    tooltip = text;

                            //TextBlock objtxttooltip = new TextBlock();
                            //objtxttooltip.Text = tooltip;
                            //objtxttooltip.TextWrapping = TextWrapping.Wrap;

                            //Grid toolTipPanel = new Grid();
                            //toolTipPanel.Width = 250;
                            //toolTipPanel.Height = 150;
                            //toolTipPanel.Children.Add(objtxttooltip);

                            //ToolTipService.SetToolTip(txtMainBlock, toolTipPanel);

                            //txtMainBlock.Width = 230;

                            //txtMainBlock.Children.Add(chkChild);
                            //txtMainBlock.Children.Add(txtBlockCText);
                            //tvi1.Items.Add(new TreeViewItem { Header = lstItemSearch });
                            ListBoxSearchesPnl.Children.Add(lstItemSearch);
                        }
                    }

                    //tvi1.IsExpanded = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Submit Button for Component Advance Search", ex);
            }

        }
        private void RefreshCartItems()
        {
            try
            {
                ConstantsValues.lstComponentAddedtoCart.Clear();
                Dictionary<string, string> cartComponentWebServiceInfo = new Dictionary<string, string>();
                List<ComponentNames> lstComponentNames = null;
                cartComponentWebServiceInfo.Add("service.name", "getAllComponentsForCart");
                string strResponse = objHlprWebService.callWebService(cartComponentWebServiceInfo);

                //satish added
                AppaPowerpoint.AppaPowerpointUI objAppaPowerpointUI = new AppaPowerpoint.AppaPowerpointUI();
                //ends
                bool blnResponse = objAppaPowerpointUI.ProcessResponse(strResponse);// Globals.ThisAddIn.appaDocxUI.ProcessResponse(strResponse);
                                                                                    // bool blnResponse = Globals.ThisAddIn.appaDocxUI.ProcessResponse(strResponse);
                if (blnResponse == true)
                    return;

                lstComponentNames = JsonConvert.DeserializeObject<List<ComponentNames>>(strResponse);
                if (lstComponentNames != null)
                {
                    foreach (var item in lstComponentNames)
                    {
                        ConstantsValues.lstComponentAddedtoCart.Add(item);
                    }
                }

                foreach (var item in ListBoxSearchesPnl.Children)
                {
                    //TreeViewItem tv = (TreeViewItem)item;

                    //foreach (TreeViewItem child in tv.Items)
                    //{
                        //DockPanel objdockpanel = (DockPanel)child.Header;
                        //CheckBox objcheckbox = (CheckBox)objdockpanel.Children[0];
                        bool isfoundflag = false;
                    if (item.GetType().ToString() != "System.Windows.Controls.CheckBox")
                    {
                        ListItemSearch objlstSearchItem = (ListItemSearch)item;

                        ComponentNames _CurrentSelectedComponent = ((ComponentNames)objlstSearchItem.chkComponent.Tag);
                        if (objlstSearchItem.chkComponent.IsChecked == true)
                        {
                            foreach (var i in ConstantsValues.lstComponentAddedtoCart)
                            {
                                if (i.FELEMENTINSTANCEID == _CurrentSelectedComponent.FELEMENTINSTANCEID)
                                    isfoundflag = true;
                            }

                            if (!isfoundflag)
                                ConstantsValues.lstComponentAddedtoCart.Add(_CurrentSelectedComponent);
                        }
                        else
                        {
                            ConstantsValues.lstComponentAddedtoCart.Remove(_CurrentSelectedComponent);
                        }
                    }
                }

                if (ConstantsValues.lstComponentAddedtoCart.Count > 0)
                {
                    btnCartItems.Visibility = Visibility.Visible;
                    btnCartItems.Content = "Cart (" + ConstantsValues.lstComponentAddedtoCart.Count.ToString() + ")";
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in RefreshCartItems", ex);
            }
        }
        private void _ParentClick(object sender, EventArgs e)
        {
            CheckBox _Parent = (CheckBox)sender;
            //ListItemSearch tv = (ListItemSearch)(ListBoxSearchesPnl.Children[1]);

            foreach (object child in ListBoxSearchesPnl.Children)
            {
                if (child.GetType().ToString() != "System.Windows.Controls.CheckBox")
                {
                    ListItemSearch c = (ListItemSearch)child;
                    //CheckBox ch = (CheckBox)c.Children[0];

                    if (_Parent.IsChecked == true)
                    {
                        c.chkComponent.IsChecked = true;
                    }
                    else
                    {
                        c.chkComponent.IsChecked = false;
                    }
                }
            }
        }
        private void _CartParentClick(object sender, EventArgs e)
        {
            CheckBox _Parent = (CheckBox)sender;

            foreach (object child in ListBoxCartPnl.Children)
            {
                if (child.GetType().ToString() != "System.Windows.Controls.StackPanel")
                {
                    ListItemSearch c = (ListItemSearch)child;
                    if (_Parent.IsChecked == true)
                    {
                        c.chkComponent.IsChecked = true;
                    }
                    else
                    {
                        c.chkComponent.IsChecked = false;
                    }
                }
            }
        }
        private void _ListElementClick(object sender, EventArgs e)
        {
            CheckBox _SelectedChkBox = (CheckBox)sender;
        }
        private void SelectAlItem_Click(object sender, EventArgs e)
        {
            ListViewItem lbi = (ListViewItem)sender;
            ListView lb = (ListView)lbi.Parent;
            if (lb.SelectedItems.Count > 0)
            {
                lb.SelectedIndex = 0;
            }
        }
        private void Child_Selected(object sender, EventArgs e)
        {
            ListViewItem lbi = (ListViewItem)sender;
            ListView lb = (ListView)lbi.Parent;

            foreach (ListViewItem o in lb.Items)
            {
                if (o.Content.ToString() == "--Select All--")
                    o.IsSelected = false;
            }

        }
        private void btnDone_Click(object sender, EventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            this.Hide();
        }
        private void AddtoCart_Click(object sender, EventArgs e)
        {
            try
            {
                RefreshCartItems();
                AddCartItemsToDatabase();
                btnSubmit_Click(sender, e);

                if (ListBoxSearchesPnl.Children.Count > 0)
                {
                    //TreeViewItem tv = (TreeViewItem)(ListBoxSearchesPnl.Children[0]);

                    foreach (object child in ListBoxSearchesPnl.Children)
                    {
                        if (child.GetType().ToString() != "System.Windows.Controls.CheckBox")
                        {
                            ListItemSearch objlstItemSearch = (ListItemSearch)child;
                            objlstItemSearch.chkComponent.IsChecked = false;
                        }
                            //DockPanel c = (DockPanel)child.Header;
                            //CheckBox ch = (CheckBox)c.Children[0];
                            //ch.IsChecked = false;

                        }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while adding in cart", ex);
            }
        }
        private void AddCartItemsToDatabase()
        {
            try
            {
                string queryParam = "componentids=";
                foreach (var item in ConstantsValues.lstComponentAddedtoCart)
                {
                    if (queryParam == "componentids=")
                        queryParam += item.FELEMENTINSTANCEID;
                    else
                        queryParam += "," + item.FELEMENTINSTANCEID;
                }

                queryParam += "&days=3";

                Dictionary<string, string> addCartComponentWebServiceInfo = new Dictionary<string, string>();
                addCartComponentWebServiceInfo.Add("service.name", "saveAllComponentsToCart");
                addCartComponentWebServiceInfo.Add("service.method", "POST");
                addCartComponentWebServiceInfo.Add("service.data", queryParam);
                addCartComponentWebServiceInfo.Add("service.content-type", "application/x-www-form-urlencoded");
                string strResponse = objHlprWebService.callWebService(addCartComponentWebServiceInfo);


                //satish added
                AppaPowerpoint.AppaPowerpointUI objAppaPowerpointUI = new AppaPowerpoint.AppaPowerpointUI();
                //ends
                bool blnResponse = objAppaPowerpointUI.ProcessResponse(strResponse);
                // Globals.ThisAddIn.appaDocxUI.ProcessResponse(strResponse);// bool blnResponse = Globals.ThisAddIn.appaDocxUI.ProcessResponse(strResponse);
                if (blnResponse == true)
                {
                    this.Visibility = Visibility.Hidden;
                    this.Hide();
                    return;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while Add Cart Items To Database", ex);
            }

        }
        private void ViewCart_Click(object sender, EventArgs e)
        {
            try
            {
                //ViewCartCustomDialog customDialog = new ViewCartCustomDialog();
                //customDialog.ShowDialog();
                //RefreshCartItems();
            }
            catch (Exception ex)
            {
                log.Error("Error in View Cart", ex);
            }
        }
        private void Item_Selected(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ListBox listboxsender = (ListBox)sender;
                bool selectallFlag = false;
                if (listboxsender.SelectedItems.Count > 1)
                {
                    foreach (ListViewItem listboxitem in listboxsender.SelectedItems)
                    {
                        if (listboxitem.Content.ToString().Contains("--Select All--"))
                        {
                            selectallFlag = true;
                        }
                    }

                    if (selectallFlag)
                    {
                        listboxsender.SelectedItems.Remove(listboxsender.Items.GetItemAt(0));
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Item_Selected", ex);
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            //this.Visibility = Visibility.Hidden;
            //this.Hide();
            txtcomponentname.Text = "";
            txtkeyword.Text = "";
            ListBoxSearchesPnl.Children.Clear();

            AssetListBoxSearchesPnl.SelectedIndex = 0;
            ContextListBoxSearchesPnl.SelectedIndex = 0;
            FundListBoxSearchesPnl.SelectedIndex = 0;
            LocaleListBoxSearchesPnl.SelectedIndex = 0;
            StatusListBoxSearchesPnl.SelectedIndex = 0;
            TypeListBoxSearchesPnl.SelectedIndex = 0;

        }
        private void Window_Close(object sender, EventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }
        #endregion
        private void btnViewCartItems_Click(object sender,EventArgs e)
        {
            try
            {
                ListBoxCartPnlScroll.Visibility = Visibility.Visible;
                ListBoxSearchesPnlScroll.Visibility = Visibility.Collapsed;

                ListBoxCartPnl.Children.Clear();

                ConstantsValues.lstComponentAddedtoCart.Clear();

                Dictionary<string, string> cartComponentWebServiceInfo = new Dictionary<string, string>();
                List<ComponentNames> lstComponentNames = null;
                cartComponentWebServiceInfo.Add("service.name", "getAllComponentsForCart");
                string strResponse = objHlprWebService.callWebService(cartComponentWebServiceInfo);

                //satish added
                AppaPowerpoint.AppaPowerpointUI objAppaPowerpointUI = new AppaPowerpoint.AppaPowerpointUI();
                //ends
                bool blnResponse = objAppaPowerpointUI.ProcessResponse(strResponse);// Globals.ThisAddIn.appaDocxUI.ProcessResponse(strResponse);
                                                                                    // bool blnResponse = Globals.ThisAddIn.appaDocxUI.ProcessResponse(strResponse);
                if (blnResponse == true)
                    return;

                lstComponentNames = JsonConvert.DeserializeObject<List<ComponentNames>>(strResponse);
                if (lstComponentNames != null)
                {
                    foreach (var item in lstComponentNames)
                    {
                        ConstantsValues.lstComponentAddedtoCart.Add(item);
                    }
                }

                if (ConstantsValues.lstComponentAddedtoCart.Count > 0)
                {
                    btnCartItems.Content = "(Cart: " + ConstantsValues.lstComponentAddedtoCart.Count.ToString() + ")";

                    if (ConstantsValues.lstComponentAddedtoCart != null)
                    {
                        StackPanel spParent = new StackPanel();
                        spParent.Orientation = Orientation.Horizontal;
                      
                        CheckBox Parent = new CheckBox();
                        Parent.Content = "Select All";
                        Parent.Margin = new Thickness(0, 8, 0, 5);

                        ////lblResultCount.Content = "(Search Result : " + lstResult.Count.ToString() + ")";
                        Parent.Click += _CartParentClick;
                        Parent.HorizontalAlignment = HorizontalAlignment.Left;
                       
                        //tvi1.Header = Parent;
                        //tvi1.Margin = new Thickness(0, 0, 0, 0);
                        Button btndelete = new Button();
                        btndelete.Content = "Delete";
                        btndelete.HorizontalAlignment = HorizontalAlignment.Right;
                        Style btnStyle =  FindResource("MaterialDesignFlatButton") as Style;
                        btndelete.Style = btnStyle;
                        btndelete.Margin = new Thickness(175, 0, 0, 0);
                        btndelete.Click += BtnRemove_Click;
                        spParent.Children.Add(Parent);
                        spParent.Children.Add(btndelete);

                        ListBoxCartPnl.Children.Add(spParent);
                       
                        foreach (ComponentNames SingleComponent in ConstantsValues.lstComponentAddedtoCart)
                        {
                            ListItemSearch lstItemSearch = new ListItemSearch();
                            //lstItemSearch.Margin = new Thickness(-2, 0, 0, 0);

                            //lstItemSearch.BorderThickness = new Thickness(1);
                            //lstItemSearch.BorderBrush = Brushes.LightGray;
                            //lstItemSearch.Margin = new Thickness(0, 8, 0, 0);

                            string text = "";
                            if (SingleComponent.FELEMENT_DETAIL != null)
                                text = SingleComponent.FELEMENT_DETAIL.ToString().Trim();

                            AppaDocx.Application.Helpers.HlprDocumentManager objhlprDocumentManager = new HlprDocumentManager();
                            text = objhlprDocumentManager.StripHtml(text);

                            string strdata = "";
                            if (text.Length > 140)
                                strdata = text.Substring(0, 140);
                            else
                                strdata = text;

                            string strcomponentname = "";

                            if (SingleComponent.FQUALDATA_DESC.Length > 45)
                                strcomponentname = SingleComponent.FQUALDATA_DESC.Substring(0, 45);
                            else
                                strcomponentname = SingleComponent.FQUALDATA_DESC;

                            lstItemSearch.txtcomponentname.Content = strcomponentname;
                            lstItemSearch.txtContent.Text = strdata;

                            lstItemSearch.chkComponent.Tag = SingleComponent;
                            lstItemSearch.chkComponent.Click += _ListElementClick;

                            //satish comment for icon
                            //switch (SingleComponent.FELEMENT_ID)
                            //{
                            //    case "para":
                            //        lstItemSearch.imgcompType.Source = (ImageSource)FindResource("Imagepara");
                            //        break;
                            //    case "bridgehead":
                            //        lstItemSearch.imgcompType.Source = (ImageSource)FindResource("Imagesubhead");
                            //        break;
                            //    case "footnote":
                            //        lstItemSearch.imgcompType.Source = (ImageSource)FindResource("Imagefootnote");
                            //        break;
                            //    case "table":
                            //        lstItemSearch.imgcompType.Source = (ImageSource)FindResource("Imagetable");
                            //        break;
                            //    case "note":
                            //        lstItemSearch.imgcompType.Source = (ImageSource)FindResource("Imagenote");
                            //        break;
                            //}

                            lstItemSearch.imgcompType.Width = 22;
                            lstItemSearch.imgcompType.Height = 22;
                            lstItemSearch.Margin = new Thickness(0, 0, 0, 5);
                            ListBoxCartPnl.Children.Add(lstItemSearch);
                        }                  
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Add Cart Data", ex);
            }
        }
        private void btnResult_Click(object sender,EventArgs e)
        {
            ListBoxCartPnlScroll.Visibility = Visibility.Collapsed;
            ListBoxSearchesPnlScroll.Visibility = Visibility.Visible;
        }
        private void BtnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                string queryParam = "componentids=";
                foreach (var item in ListBoxCartPnl.Children)
                {
                    string itemtype = item.GetType().ToString();

                    if (itemtype == "AppaDocx.UserInterfaces.ListItemSearch")

                    {
                        ListItemSearch lis = (ListItemSearch)item;

                        //foreach (ListItemSearch child in item)
                        //{
                        //    DockPanel objdockpanel = (DockPanel)child.Header;
                        CheckBox objcheckbox = lis.chkComponent;

                        ComponentNames _CurrentSelectedComponent = ((ComponentNames)objcheckbox.Tag);
                        if (objcheckbox.IsChecked == true)
                        {
                            ConstantsValues.lstComponentAddedtoCart.Remove(_CurrentSelectedComponent);
                            if (queryParam == "componentids=")
                                queryParam += _CurrentSelectedComponent.FELEMENTINSTANCEID;
                            else
                                queryParam += "," + _CurrentSelectedComponent.FELEMENTINSTANCEID;
                        }

                    }
                }

                Dictionary<string, string> removeCartComponentWebServiceInfo = new Dictionary<string, string>();
                removeCartComponentWebServiceInfo.Add("service.name", "removeComponentsFromCart");
                removeCartComponentWebServiceInfo.Add("service.method", "POST");
                removeCartComponentWebServiceInfo.Add("service.data", queryParam);
                removeCartComponentWebServiceInfo.Add("service.content-type", "application/x-www-form-urlencoded");
                string strResponse = objHlprWebService.callWebService(removeCartComponentWebServiceInfo);

                //satish added
                AppaPowerpoint.AppaPowerpointUI objAppaPowerpointUI = new AppaPowerpoint.AppaPowerpointUI();
                //ends
                bool blnResponse = objAppaPowerpointUI.ProcessResponse(strResponse);// Globals.ThisAddIn.appaDocxUI.ProcessResponse(strResponse);
                //bool blnResponse = Globals.ThisAddIn.appaDocxUI.ProcessResponse(strResponse);
                if (blnResponse == true)
                {
                    this.Visibility = Visibility.Hidden;
                    this.Hide();
                    return;
                }

                btnSubmit_Click(sender, e);

                btnViewCartItems_Click(sender,e);
            }
            catch (Exception ex)
            {
                log.Error("Error in Button Remove", ex);
            }
        }
    }
}

