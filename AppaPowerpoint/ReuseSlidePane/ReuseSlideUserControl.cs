﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppaPowerpoint.ReuseSlidePane
{
    public partial class ReuseSlideUserControl: UserControl
    {
        public ReuseSlideUserControl(CustomReuseSlidePane customReuseSlidePane)
        {
            InitializeComponent();
            this.AppaPowerPointUIElementHost.HostContainer.Children.Add(customReuseSlidePane);
        }
    }
}
