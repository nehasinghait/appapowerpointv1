﻿namespace AppaPowerpoint.ReuseSlidePane
{
    partial class ReuseSlideUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //satish added
            this.AppaPowerPointUIElementHost = new System.Windows.Forms.Integration.ElementHost();
            this.SuspendLayout();
            //
            //AppaPowerPointUIElementHost
            //
            this.AppaPowerPointUIElementHost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AppaPowerPointUIElementHost.Location = new System.Drawing.Point(0, 0);
            this.AppaPowerPointUIElementHost.Name = "AppaPowerPointUIElementHost";
            this.AppaPowerPointUIElementHost.Size = new System.Drawing.Size(396, 501);
            this.AppaPowerPointUIElementHost.TabIndex = 0;
            this.AppaPowerPointUIElementHost.Child = null;

            // 
            // ReuseSlideUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.AppaPowerPointUIElementHost);
            this.Name = "ReuseSlideUserControl";
            this.Size = new System.Drawing.Size(250, 400);
            this.ResumeLayout(false);

        }

        #endregion

        //satish added
        private System.Windows.Forms.Integration.ElementHost AppaPowerPointUIElementHost;
    }
}
