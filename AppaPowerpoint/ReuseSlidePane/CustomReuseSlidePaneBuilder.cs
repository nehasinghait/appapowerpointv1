﻿using Microsoft.Office.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppaPowerpoint.ReuseSlidePane
{
    internal class CustomReuseSlidePaneBuilder
    {
        private Microsoft.Office.Tools.CustomTaskPane myCustomTaskPane;
        private ReuseSlideUserControl objReuseSlideUserControl;
        internal void InstantiateCustomReuseSlidePane(string fileUri)
        {
            try
            {
               // MessageBox.Show("Instantiate_Custom_ReuseSlidePane(); =>Custom reuse pane will show");
                RemoveOpenedCustomReuseSlidePane();

                CustomReuseSlidePane customReuseSlidePane = PrepareReuseThumbnails(fileUri);

                objReuseSlideUserControl = new ReuseSlideUserControl(customReuseSlidePane);
                myCustomTaskPane = Globals.ThisAddIn.CustomTaskPanes.Add(objReuseSlideUserControl, "Reuse Slides");
                myCustomTaskPane.DockPosition = Microsoft.Office.Core.MsoCTPDockPosition.msoCTPDockPositionLeft;
                myCustomTaskPane.Visible = true;
                myCustomTaskPane.Width = 265;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private CustomReuseSlidePane PrepareReuseThumbnails(string fileUri)
        {
            CustomReuseSlidePane customReuseSlidePane = new CustomReuseSlidePane();
            customReuseSlidePane.PrepareReuseSlidePane(fileUri);
            return customReuseSlidePane;
        }

        private void RemoveOpenedCustomReuseSlidePane()
        {

            CustomTaskPaneCollection customTaskPanes = Globals.ThisAddIn.CustomTaskPanes;//.Where(x => x.Title == "Reuse Slides");

            for (int i = 0; i < customTaskPanes.Count;)
            {
                if (customTaskPanes[i].Title == "Reuse Slides")
                {
                    customTaskPanes.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }

        }
    }
}
