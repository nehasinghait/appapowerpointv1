﻿using AppaDocx.Common;
using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppaPowerpoint.ReuseSlidePane
{
    /// <summary>
    /// Interaction logic for CustomReuseSlidePane.xaml
    /// </summary>
    public partial class CustomReuseSlidePane : UserControl
    {
        public CustomReuseSlidePane()
        {
            InitializeComponent();
           
        }

        internal void PrepareReuseSlidePane(string fileUri)
        {
            //********* how to set window to left slide of screen **************

            //fileUri = @"D:\Satish work\AppaPowerpoint1\AppaPowerpoint\AppaPowerpoint\StorageConnectors\DropBoxDownloads\Power Point POC Demo1.pptx";
            //validation : isExist,isNullPath, not have ppt(x) extention
            if (!File.Exists(fileUri))
            {
                MessageBox.Show("File Not Exist");
                return;
            }

            if (string.IsNullOrEmpty(fileUri))
            {
                MessageBox.Show("Blank Path");
                return;
            }

            if (!(System.IO.Path.GetExtension(fileUri).ToLower() == ".pptx") && !(System.IO.Path.GetExtension(fileUri).ToLower() == ".ppt"))
            {
                MessageBox.Show("Dot have .pptx OR .ppt extension");
                return;
            }

            List<BitmapImage> ThumbnailList = GetSlideThumbnailsFromFile(fileUri);
            if (ThumbnailList != null)
            {
                lblSlideCount.Content = "Slides: "+ ThumbnailList.Count.ToString();
            }


            ComponentListBoxSearchesPnl.Children.Clear();
            //ComponentListBoxSearchesPnl.RowDefinitions.Clear();

            int SrNo = 1;

            foreach (BitmapImage image in ThumbnailList)
            {
                Grid resultGrid = new Grid();
                RowDefinition row1 = new RowDefinition();
                row1.MinHeight = 20;
                resultGrid.RowDefinitions.Add(row1);

                StackPanel slideBox = new StackPanel();
                slideBox.Orientation = Orientation.Horizontal;

                Label lblSrNo = new Label();
                lblSrNo.Content = SrNo.ToString();
                slideBox.Children.Add(lblSrNo);

                Image imgControl = new Image();
                imgControl.Width = 150;
                imgControl.Height = 150;
                imgControl.Source = image;
                Button button = new Button();
                button.BorderThickness = new Thickness(1);
                button.BorderBrush = Brushes.Black;
                button.Width = 150;
                button.Height = 100;

                string[] SlideNoAndPath = new string[2];
                SlideNoAndPath[0] = SrNo.ToString();
                SlideNoAndPath[1] = fileUri;
                button.Tag = SlideNoAndPath;
                button.Content = imgControl;

                button.Click += Slide_Clicked;

                slideBox.Children.Add(button);
                resultGrid.Children.Add(slideBox);
                resultGrid.Margin = new Thickness(0, 5, 0, 5);
                ComponentListBoxSearchesPnl.Children.Add(resultGrid);

                SrNo++;
            }


        }



        private List<BitmapImage> GetSlideThumbnailsFromFile(string fileUri)
        {
            List<BitmapImage> BitmapImages = new List<BitmapImage>();

            Microsoft.Office.Interop.PowerPoint.Application application = new Microsoft.Office.Interop.PowerPoint.Application();
            Microsoft.Office.Interop.PowerPoint.Presentation presentation = application.Presentations.Open(fileUri, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoFalse);
            //presentation.Slides.InsertFromFile(fileUri, 1, 1, -1);
            foreach (Slide slide in presentation.Slides)
            {
                slide.Copy();
                BitmapImage bitmapImage = GetBitmapImageFromClipBoardData();
                BitmapImages.Add(bitmapImage);
            }
            presentation.Close();
            return BitmapImages;
        }

        private BitmapImage GetBitmapImageFromClipBoardData()
        {
            BitmapImage bitmapImg = new BitmapImage();

            if (!Clipboard.GetDataObject().GetDataPresent("PNG"))
            {
                return bitmapImg;
            }

            System.IO.Stream ms = (System.IO.Stream)Clipboard.GetDataObject().GetData("PNG");
            bitmapImg.BeginInit();
            bitmapImg.StreamSource = ms;
            bitmapImg.EndInit();
            Clipboard.Clear();
            return bitmapImg;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //ReuseSlidePane.CustomReuseSlidePane customReuseSlidePane = new ReuseSlidePane.CustomReuseSlidePane();
            //customReuseSlidePane.PrepareReuseSlidePane("");
            //PrepareReuseSlidePane("");
        }

        private void Slide_Clicked(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            string[] SlideNoAndPath = (string[])button.Tag;
            int SourceSlideSrNo = Convert.ToInt32(SlideNoAndPath[0]);
            string fileUri = SlideNoAndPath[1]; //@"D:\Satish work\AppaPowerpoint1\AppaPowerpoint\AppaPowerpoint\StorageConnectors\DropBoxDownloads\Power Point POC Demo1.pptx";
            MessageBox.Show("Slide clicked. Slide No:" + SourceSlideSrNo);
            Presentation ActivePresentation = Globals.ThisAddIn.Application.ActivePresentation;

            Globals.ThisAddIn.Application.Windows[1].ViewType = PpViewType.ppViewNormal;
            if (Globals.ThisAddIn.Application.ActiveWindow.View.Type == PpViewType.ppViewNormal)
            {
                Globals.ThisAddIn.Application.ActiveWindow.Panes[2].Activate();
                Slide activeSlide = Globals.ThisAddIn.Application.ActiveWindow.View.Slide as Slide;
                int ActiveSlideNo = activeSlide.SlideNumber;
                ActivePresentation.Slides.InsertFromFile(fileUri, ActiveSlideNo, SourceSlideSrNo, SourceSlideSrNo);
                if (chkKeepSourceFormat.IsChecked == true)
                {
                    ActivePresentation.Slides[ActiveSlideNo + 1].ApplyTemplate(fileUri); //for Keep source formatting
                }
                else
                {
                    string FullPath = ActivePresentation.FullName;
                    ActivePresentation.Slides[ActiveSlideNo + 1].FollowMasterBackground = Microsoft.Office.Core.MsoTriState.msoTrue;//
                }

            }
            else
            {
                MessageBox.Show("ActiveWindow.View.Type is not PpViewType.ppViewNormal");
            }

        }
    }
}
