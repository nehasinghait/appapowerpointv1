<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
	xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage"
	xmlns:wo="http://schemas.microsoft.com/office/word/2003/wordml" 
	xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
	xmlns:v="urn:schemas-microsoft-com:vml" 
	xmlns:w10="urn:schemas-microsoft-com:office:word" 
	xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" 
	xmlns:aml="http://schemas.microsoft.com/aml/2001/core" 
	xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" 
	xmlns:o="urn:schemas-microsoft-com:office:office" 
	xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" 
	exclude-result-prefixes="r pkg wo w v w10 sl aml wx o dt"
	w:macrosPresent="no" 
	w:embeddedObjPresent="no" 
	w:ocxPresent="no">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>	
	
	<xsl:include href="Template/inlinestyle.xsl"/> 
	
	<xsl:template name="findStyle">
		<xsl:param name="depth" select="0" />
		<xsl:choose>
			<!--<xsl:when test="$depth = 0 and count(parent::*/w:pPr/w:numPr/w:numId[@w:val='1']) != 0">
				<itemizedlist>
					<listitem>
            <para>
						  <xsl:call-template name="findStyle">
							  <xsl:with-param name="depth" select="$depth + 1"/>
						  </xsl:call-template>
            </para>
					</listitem>
				</itemizedlist>
			</xsl:when>-->
      <xsl:when test="$depth = 1 and count(w:rPr/w:i) != 0">
				<emphasis>
					<xsl:call-template name="findStyle">
						<xsl:with-param name="depth" select="$depth + 1"/>
					</xsl:call-template>
				</emphasis>
			</xsl:when>
			<xsl:when test="$depth = 2 and count(w:rPr/w:b) != 0">
				<emphasis role="bold">
					<xsl:call-template name="findStyle">
						<xsl:with-param name="depth" select="$depth + 1"/>
					</xsl:call-template>
				</emphasis>
			</xsl:when>
			<xsl:when test="$depth = 3 and count(w:rPr/w:u[@w:val = 'single']) != 0">
				<u>
					<xsl:call-template name="findStyle">
						<xsl:with-param name="depth" select="$depth + 1"/>
					</xsl:call-template>
				</u>
			</xsl:when>
			<xsl:when test="$depth = 4 and count(w:rPr/w:vertAlign[@w:val = 'subscript']) != 0">
				<sub>
					<xsl:call-template name="findStyle">
						<xsl:with-param name="depth" select="$depth + 1"/>
					</xsl:call-template>
				</sub>
			</xsl:when>
			<xsl:when test="$depth = 5 and count(w:rPr/w:vertAlign[@w:val = 'superscript']) != 0">
				<u>
					<xsl:call-template name="findStyle">
						<xsl:with-param name="depth" select="$depth + 1"/>
					</xsl:call-template>
				</u>
			</xsl:when>
			<xsl:when test="$depth = 6 and name(..) = 'w:hyperlink'">
				<xsl:variable name="id" select="../@r:id" />
				<xsl:variable name="relationship" select="(/pkg:package/pkg:part/pkg:xmlData/child::*[name() = 'Relationships'])[2]" />
				<xsl:variable name="target" select="($relationship/child::*[@Id = $id])[1]/@Target" />
				<a>
					<xsl:attribute name="href">
						<xsl:value-of select="$target"/>
					</xsl:attribute>
					<xsl:call-template name="findStyle">
						<xsl:with-param name="depth" select="$depth + 1"/>
					</xsl:call-template>
				</a> 
			</xsl:when>
			<xsl:when test="$depth = 7">
				<xsl:call-template name="inlinestyle"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="findStyle">
					<xsl:with-param name="depth" select="$depth + 1"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="/">
		<root>
      <xsl:for-each select=".//w:p[not(ancestor::*[name() = 'w:tbl']) and not(normalize-space() = '')]">
        <xsl:choose>
          <xsl:when test="count(w:pPr/w:numPr/w:numId[@w:val='1']) != 0">
            <xsl:if test="count(preceding-sibling::*[1][w:pPr/w:numPr/w:numId[@w:val='1']]) = 0">
            <![CDATA[<orderedlist>]]>
            </xsl:if>
            <listitem>
              <para>
                <xsl:for-each select="w:r">
                  <xsl:call-template name="findStyle">
                    <xsl:with-param name="depth" select="1"/>
                  </xsl:call-template>
                </xsl:for-each>
              </para>
            </listitem>
            <xsl:if test="count(following-sibling::*[1][w:pPr/w:numPr/w:numId[@w:val='1']]) = 0">
            <![CDATA[</orderedlist>]]>
            </xsl:if>
          </xsl:when>
          <xsl:when test="count(w:pPr/w:numPr/w:numId[@w:val='2']) != 0">
            <xsl:if test="count(preceding-sibling::*[1][w:pPr/w:numPr/w:numId[@w:val='2']]) = 0">
              <![CDATA[<itemizedlist>]]>
            </xsl:if>
            <listitem>
              <para>
                <xsl:for-each select="w:r">
                  <xsl:call-template name="findStyle">
                    <xsl:with-param name="depth" select="1"/>
                  </xsl:call-template>
                </xsl:for-each>
              </para>
            </listitem>
            <xsl:if test="count(following-sibling::*[1][w:pPr/w:numPr/w:numId[@w:val='2']]) = 0">
              <![CDATA[</itemizedlist>]]>
            </xsl:if>
          </xsl:when>
          <xsl:otherwise>
            <para>
              <xsl:for-each select="w:r">
                <xsl:call-template name="findStyle"/>
              </xsl:for-each>
            </para>
          </xsl:otherwise>
        </xsl:choose>
  		</xsl:for-each>
			<xsl:for-each select=".//w:tbl">
				<tbody>
					<xsl:for-each select="w:tr">
						<row>
							<xsl:for-each select="w:tc">
								<entry>
									<xsl:for-each select="w:p">
										<para>
											<xsl:for-each select="w:r">
												<xsl:call-template name="findStyle"/>	
											</xsl:for-each>
										</para>
									</xsl:for-each>
								</entry>
							</xsl:for-each>
						</row>
					</xsl:for-each>
				</tbody>
			</xsl:for-each>
		</root>
	</xsl:template>
</xsl:stylesheet>
