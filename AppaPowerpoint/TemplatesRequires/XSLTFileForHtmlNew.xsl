<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="root">
    <html>
      <body>
        <xsl:variable name="eid" select="@eid" />
        <xsl:for-each select="para|bridgehead|note|table|footnote|beginpage">
          <xsl:if test ="name()='para' or name()='note' or name()='beginpage'">
            <xsl:call-template name ="processcomponents"/>
          </xsl:if>
          <xsl:if test ="name()='footnote'">
            <xsl:for-each select ="para">
              <xsl:choose>
                <xsl:when test ="child::*">
                  <xsl:for-each select ="child::*">
                    <xsl:if test ="name()='inlinestyle'">
                      <xsl:variable name ="style" select ="@condition"/>
                      <xsl:call-template name ="inlinestyle">
                        <xsl:with-param name ="style"/>
                      </xsl:call-template>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select ="."/>
                </xsl:otherwise>
              </xsl:choose>
             </xsl:for-each>
          </xsl:if>
          <xsl:if test ="name()='bridgehead'">
            <xsl:for-each select="para">
              <xsl:call-template name ="processcomponents"/>
            </xsl:for-each >
          </xsl:if>
          <xsl:if test ="name()='table'">
            <xsl:choose>
              <xsl:when test ="@*='tickersfsai'">
                <table  style="border-bottom:1px solid black; width:=100%; font-size:8" >
                  <!--.tickermfsai_defaultone {text-indent:-12; margin:0;padding-left:12;padding-right:4;padding-top:2.9;padding-bottom:0;
              font-family:Frutiger45Light;font-size:8;line-height:10;align:left;valign:top;letter-spacing:0;border-bottom:0.5 solid 
              cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:14;requote:true;overprint:true;}
.tickermfsai_defaultone p{align:left;}
.tickermfsai_default {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;padding-right:0;padding-top:2.9;
padding-bottom:0;font-family:Frutiger45Light;font-size:8;line-height:10;align:left;valign:top;letter-spacing:0;border-bottom:0.5 
solid cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:14;requote:true;overprint:true;}
.tickermfsai_default p{align:left;}-->
                  <xsl:for-each select="tbody/row[not(@condition='dummyrow')]">
                    <xsl:variable name ="rowStyle" select ="@condition"/>
                    <xsl:call-template name ="tableRowStyles">
                      <xsl:with-param name ="rowStyle" select ="$rowStyle"/>
                    </xsl:call-template>
                  </xsl:for-each>
                </table>
              </xsl:when>
              <xsl:otherwise>
                <table style="width:100%;" border="1">
                  <xsl:for-each select="tbody/row[not(@condition='dummyrow')]">
                    <xsl:variable name ="rIndex" select ="position()"/>
                    <xsl:if test ="$rIndex!=1">
                      <tr>
                        <xsl:for-each select ="child::*[name()='entry']">
                          <td>
                            <xsl:choose>
                              <xsl:when test ="child::*[name()='emphisis']">
                                <p>
                                  <xsl:call-template name ="emphasis"/>
                                </p>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select ="."/>
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                        </xsl:for-each>
                      </tr>
                    </xsl:if>
                    <xsl:if test ="$rIndex=1">
                      <tr>
                        <xsl:for-each select ="child::*[name()='entry']">
                          <th>
                            <xsl:choose>
                              <xsl:when test ="child::*[name()='emphisis']">
                                <p>
                                  <xsl:call-template name ="emphasis"/>
                                </p>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select ="."/>
                              </xsl:otherwise>
                            </xsl:choose>
                          </th>
                        </xsl:for-each>
                      </tr>
                    </xsl:if>
                  </xsl:for-each>
                </table>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
  <xsl:template name="emphasis">
    <xsl:choose>
      <xsl:when test="@* = 'bold'">
        <b>
          <xsl:value-of select ="."/>
        </b>
      </xsl:when>
      <xsl:otherwise>
        <i>
          <xsl:value-of select ="."/>
        </i>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name ="processcomponents">
      <xsl:choose>
          <xsl:when test ="name()='para'"> 
            <xsl:choose>
              <xsl:when test ="child::*">
                <xsl:for-each select="node()">
                  <xsl:choose>
                    <xsl:when test ="name()='inlinestyle'">
                      <xsl:call-template name ="inlinestyle"/>
                      <xsl:if test ="child::*[name()='img']">
                        <xsl:for-each select ="img">
                          <xsl:variable name ="variableValue" select ="@value"/>
                          <xsl:call-template name ="img">
                            <xsl:with-param name ="variableValue" select ="$variableValue"/>
                          </xsl:call-template>
                        </xsl:for-each>
                      </xsl:if>
                    </xsl:when>
                    <xsl:when test ="name()='sbr'">
                      <br/>
                    </xsl:when>
                    <xsl:when test ="name()='u'">
                      <u>
                        <xsl:value-of select ="."/>
                        </u>
                    </xsl:when>
                    <xsl:when test ="name()='emphasis'">
                      <xsl:choose>
                        <xsl:when test ="name()='emphasis'">
                          <xsl:call-template name ="emphasis"/>
                        </xsl:when>
                        <xsl:when test ="name()!='emphasis'">
                          <xsl:value-of select ="."/>
                        </xsl:when>
                      </xsl:choose>
                    </xsl:when>
                    <xsl:when test ="name()='img'">
                      <xsl:variable name ="variableValue" select ="@value"/>
                      <xsl:call-template name ="img">
                        <xsl:with-param name ="variableValue" select ="$variableValue"/>
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:when test ="name()='para'">
                      <p>
                      <xsl:call-template name ="processcomponents"/>
                        </p>
                    </xsl:when>
                    <xsl:otherwise>
                      <!--<p>
                        <xsl:choose>
                          <xsl:when test ="child::*[name()='emphasis']">
                              <xsl:call-template name ="emphasis"/>
                          </xsl:when>
                            <xsl:when test ="name()!='emphasis'">
                              <xsl:value-of select ="."/>
                            </xsl:when>
                          <xsl:when test ="child::*[name()='u']">
                            <u>
                              <xsl:value-of select ="."/>
                            </u>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select ="."/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </p>-->
                      <xsl:value-of select ="."/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </xsl:when>
              <xsl:otherwise>
                <p>
                  <xsl:choose>
                    <xsl:when test ="child::*[name()='emphasis']">
                      <xsl:for-each select ="emphasis">
                        <xsl:call-template name ="emphasis"/>
                      </xsl:for-each>
                      <xsl:value-of select ="node()[3]"/>
                    </xsl:when>
                    <xsl:when test ="child::*[name()='u']">
                      <u>
                        <xsl:value-of select ="."/>
                      </u>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select ="."/>
                    </xsl:otherwise>
                  </xsl:choose>
                </p>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select ="."/>
        </xsl:otherwise>
      </xsl:choose>
  </xsl:template>
  <xsl:template name="inlinestyle">
    <xsl:variable name="condition" select="@condition" />
    <xsl:variable name="size" select="@size" />
    <xsl:if test="not($condition = '')">
      <xsl:choose>
        <xsl:when test="$condition='fch3'">
          <!--.fch3 {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:10;
          line-height:13;color:cmyk(0%,0%,0%,100%);letter-spacing:-0.1;vertical-align:top;align:center;requote:true;font-weight:normal
          ;width:100%;overprint:true;}
.fch3 p{align:center;}-->
          <p style= "text-align:left;font-size:22;valign:top">
            <xsl:for-each select="child::*">
              <xsl:choose>
                <xsl:when test ="name()='emphasis'">
                  <xsl:call-template name="emphasis"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select ="."/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </p>
        </xsl:when>
        <xsl:when test="$condition='st'">
          <!--.st {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-weight:bold;font-size:12;
line-height:15;color:cmyk(0%,0%,0%,100%);letter-spacing:0;align:left;valign:top;requote:true;overprint:true;}
.st p{align:left;}-->
          <p style = "text-align:left;font-size:22;valign:top">
            <b>
              <xsl:choose>
                <xsl:when test ="child::*[name()='emphisis']">
                  <xsl:call-template name="emphasis"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select ="."/>
                </xsl:otherwise>
              </xsl:choose>
            </b>
          </p>
        </xsl:when>
        <xsl:when test="$condition='h3'">
          <!--.h3 {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%
,0%,100%);letter-spacing:0;align:left;valign:top;requote:true;font-weight:bold;width:100%;overprint:true;}
.h3 p{align:left;}-->
          <p style = "text-align:left;font-size:22;valign:top">
            <b>
              <xsl:choose>
                <xsl:when test ="child::*[name()='emphisis']">
                  <xsl:call-template name="emphasis"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select ="."/>
                </xsl:otherwise>
              </xsl:choose>
            </b>
          </p>
        </xsl:when>
        <xsl:when test="$condition='fnsq'">
          <!--.fnsq{margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;color:cmyk(0%,0%
,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;overprint:true;}
.fnsq p{align:left;}-->
          <p style = "text-align:left;font-size:10;valign:top">
            <xsl:choose>
              <xsl:when test ="child::*[name()='emphisis']">
                <xsl:call-template name="emphasis"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select ="."/>
              </xsl:otherwise>
            </xsl:choose>
          </p>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test ="child::*[name()='emphisis']">
              <xsl:call-template name="emphasis"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select ="."/>
            </xsl:otherwise>
          </xsl:choose >
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  <xsl:template name ="tableRowStyles">
    <xsl:param name ="rowStyle" select ="@rowStyle"/>
    <xsl:choose>
      <xsl:when test ="$rowStyle = 'FCTCSH'">
        <tr style="border-top:1px solid ;font-size:8">
          <!--.FCTCSHone {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;padding-right:4;padding-top:2.4;
padding-bottom:0;font-family:Frutiger47Cond;font-size:8;font-weight:bold;line-height:10;align:left;valign:top;letter-
spacing:0;color:cmyk(0%,0%,0%,100%);border-top:0.5 solid cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}
.FCTCSHone p{align:left;}
.FCTCSH {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;padding-right:0;padding-top:2.4;padding-
bottom:0;font-family:Frutiger47Cond;font-size:8;font-weight:bold;line-height:10;align:center;valign:top;letter-spacing:0;
border-top:0.5 solid cmyk(0%,0%,0%,100%);border-bottom:0.5 solid cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:16;
requote:true;overprint:true;}
.FCTCSH p{align:center;}-->
          <xsl:if test ="child::*[name()='entry']">
            <xsl:for-each select ="entry">
              <xsl:variable name ="maxwidth" select ="@p_maxwidth"/>
              <xsl:call-template name ="td">
                <xsl:with-param name ="maxwidth" select ="$maxwidth"/>
              </xsl:call-template >
            </xsl:for-each>
          </xsl:if>
        </tr>
      </xsl:when>
      <xsl:when test ="$rowStyle = 'FCTCH'">
        <tr style="border-bottom:1px solid ;font-size:8 ; font-weight:bold;">
          <!--.FCTCHone {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;padding-right:4;padding-top:2.2;
          padding-bottom:0;font-family:Frutiger47Cond;font-size:8;font-weight:bold;line-height:10;align:left;valign:top;letter-spacing:0
          ;border-bottom:0.5 solid cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}
.FCTCHone p{align:left;}
.FCTCH {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;padding-right:0;padding-top:2.2;padding-bottom:0;
font-family:Frutiger47Cond;font-size:8;font-weight:bold;line-height:10;align:left;valign:top;letter-spacing:0;border-bottom:0.5 solid 
cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}
.FCTCH p{align:left;}-->
          <xsl:if test ="child::*[name()='entry']">
            <xsl:for-each select ="entry">
              <xsl:variable name ="maxwidth" select ="@p_maxwidth"/>
              <xsl:call-template name ="td">
                <xsl:with-param name ="maxwidth" select ="$maxwidth"/>
              </xsl:call-template >
            </xsl:for-each>
          </xsl:if>
        </tr>
      </xsl:when>
      <xsl:when test ="$rowStyle = 'TCH11'">
        <tr style="border-bottom:1px solid ;font-size:8 ; font-weight:bold;">
          <!--.TCH11one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:3;padding-right:6;padding-bottom:2;
          padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk
          (0%,0%,0%,100%);border-bottom:0.5;letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:bold;width:100%;height:13;overprint:true;}
.TCH11one p{align:left;}
.TCH11 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:3;padding-right:4;padding-bottom:2;padding-left:4;
font-family:Frutiger45Light;font-size:7;line-height:8;color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:
0.5;letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:bold;width:100%;height:13;overprint:true;}
.TCH11 p{align:left;}-->
          <xsl:for-each select ="child::*[name()='entry']">
            <xsl:call-template name ="td"/>
          </xsl:for-each>
        </tr>
      </xsl:when>
      <xsl:otherwise>
        <tr>
          <xsl:if test ="child::*[name()='entry']">
            <xsl:for-each select ="entry">
              <xsl:variable name ="maxwidth" select ="@p_maxwidth"/>
              <xsl:call-template name ="td">
                <xsl:with-param name ="maxwidth" select ="$maxwidth"/>
              </xsl:call-template >
            </xsl:for-each>
          </xsl:if>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name ="td">
    <xsl:param name ="maxwidth"/>
    <td style="border-top:1px solid ;font-size:8;width:{$maxwidth}%">
      <xsl:choose>
        <xsl:when test ="child::*">
          <xsl:for-each select ="child::*">
            <xsl:choose>
              <xsl:when test ="emphisis">
                <p>
                  <xsl:call-template name ="emphasis"/>
                </p>
              </xsl:when>
              <xsl:when test ="name()='para'">
                <xsl:call-template name ="processcomponents"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select ="."/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select ="."/>
        </xsl:otherwise>
      </xsl:choose>
    </td>
  </xsl:template>
  <xsl:template name ="img">
    <xsl:param name ="variableValue" select ="@variableValue"/>
    <xsl:value-of select ="$variableValue"/>
  </xsl:template >
</xsl:stylesheet>
