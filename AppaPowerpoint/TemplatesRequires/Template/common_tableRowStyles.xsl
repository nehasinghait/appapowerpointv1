<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
  xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
	w:macrosPresent="no"
	w:embeddedObjPresent="no"
	w:ocxPresent="no" xml:space="preserve">
  <xsl:template name="processTableCellProperties">
    <xsl:param name="tabletype" select="'na'" />
    <xsl:param name="rowstyle"/>
    <xsl:param name="rowstyleflag"/>
    <xsl:param name="entryIndex" select="1" />
    <xsl:param name="tcrunflag" select="'cell'" />
    <xsl:param name="colspan" select="0"/>
    <xsl:param name="colspanflag" select="false"/>
    <xsl:variable name="rtypeflag" select="boolean(ancestor::table[1][@rtype='1'])" />
    <xsl:if test="$tcrunflag = 'cell'">
    <w:tcPr>
    <xsl:choose>
      <xsl:when test="ancestor::tbody/row[@condition='dummyrow']">
        <xsl:for-each select="ancestor::tbody/row[@condition='dummyrow']">
          <xsl:for-each select="entry">
            <xsl:variable name="eIndex" select="position()" />
            <xsl:variable name="maxwidth" select="@maxwidth" />
            <xsl:variable name ="vAlign" select ="@valign" />
            <xsl:variable name="align" select="@align" />
            <xsl:if test="$entryIndex = $eIndex">
              <!--<xsl:choose>
                <xsl:when test="$rtypeflag">
                  <w:tcW w:w="{@p_maxwidth}" w:type="pct"/>
                </xsl:when>
                <xsl:otherwise>
                  <w:tcW w:w="{@maxwidth}" w:type="pct"/>
                </xsl:otherwise>
              </xsl:choose>-->
              <xsl:if test ="$align = 'left'">
               <w:tcMar>
                 <w:left w:w="0" w:type="dxa"/>
                 <w:top w:w="0" w:type="dxa"/>
                 <w:center w:w="0" w:type="dxa"/>
                 <w:bottom w:w="0" w:type="dxa"/>
               </w:tcMar>
             </xsl:if> 
            </xsl:if>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="boolean(ancestor::table[@cols])">
          <xsl:variable name="noOfCols" select="number(ancestor::table/@cols)" />
          <xsl:variable name="colwidth" select="number(100 div $noOfCols)" />
          <!--<w:tcW w:w="{$colwidth}" w:type="pct"/>-->
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="$colspanflag and not($colspan = '')">
      <w:gridSpan w:val="{$colspan}"/>
    </xsl:if>
    
    <xsl:choose>
              <xsl:when test="$rowstyleflag and not($rowstyle = '')">
<!--FCTCHone {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;
                padding-right:4;padding-top:2.2;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;
                font-weight:bold;line-height:10;align:left;valign:top;letter-spacing:0;border-bottom:0.5 solid
                cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}
                FCTCH {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;
                padding-right:0;padding-top:2.2;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;
                font-weight:bold;line-height:10;align:left;valign:top;letter-spacing:0;border-bottom:0.5 
                solid cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}-->
               <xsl:if test="$rowstyle = 'FCTCH'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--FCTCSHone {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;
                 padding-right:4;padding-top:2.4;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;
                 font-weight:bold;line-height:10;align:left;valign:top;letter-spacing:0;color:
                 cmyk(0%,0%,0%,100%);border-top:0.5 solid cmyk(0%,0%,0%,100%);height:16;requote:true;
                 overprint:true;}
                FCTCSH {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;
                padding-right:0;padding-top:2.4;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;
                font-weight:bold;line-height:10;align:center;valign:top;letter-spacing:0;border-top:0.5 
                solid cmyk(0%,0%,0%,100%);border-bottom:0.5 solid cmyk(0%,0%,0%,100%);color:
                cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}-->
               <xsl:if test="$rowstyle = 'FCTCSH'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:top w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:top w:val="single" w:sz="1"/>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--TCSH11one {margin:0;padding-top:0;padding-right:6;padding-bottom:2;padding-left:0;
font-family:Frutiger45Light;font-size:7;font-weight:bold;line-height:8;color:cmyk(0%,0%,0%,100%);
letter-spacing:0;vertical-align:bottom;align:left;requote:true;width:100%;height:13;overprint:true;}
.TCSH11 {margin-left:2;margin-right:2;padding-top:0;padding-right:4;padding-bottom:2;padding-left:4;
font-family:Frutiger45Light;font-size:7;font-weight:bold;line-height:8;color:cmyk(0%,0%,0%,100%);
letter-spacing:0;vertical-align:bottom;align:left;border-bottom-color:cmyk(0%,0%,0%,100%);
border-bottom:0.5;requote:true;width:100%;height:13;overprint:true;}-->
               <xsl:if test="$rowstyle = 'TCSH11'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:vAlign w:val="bottom"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--TH1one {margin:0;padding-top:0;padding-right:6;padding-bottom:1;padding-left:0;
font-family:Frutiger45Light;font-weight:bold;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);
letter-spacing:0;vertical-align:bottom;align:left;requote:true;width:100%;height:13;overprint:true;}
TH1 {margin:0;padding-top:0;padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;
font-weight:bold;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:bottom;
align:left;requote:true;width:100%;height:13;overprint:true;}-->
               <xsl:if test="$rowstyle = 'TH1'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:vAlign w:val="bottom"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:vAlign w:val="bottom"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--PF1one {margin:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;
                font-family:Frutiger45Light;font-size:8;line-height:9;color:cmyk(0%,0%,0%,100%);
                letter-spacing:-0.1;vertical-align:bottom;align:center;requote:true;
                font-weight:bold;width:100%;overprint:true;}
                PF1 {margin:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;
                font-family:Frutiger45Light;font-size:8;line-height:9;color:cmyk(0%,0%,0%,100%);
                letter-spacing:-0.1;vertical-align:bottom;align:center;requote:true;font-weight:bold;
                width:100%;overprint:true;}-->
               <xsl:if test="$rowstyle = 'PF1'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:top w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:top w:val="single" w:sz="1"/>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--TCH11one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:3;padding-right:
;padding-bottom:2;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;
letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:bold;width:100%;height:13;
overprint:true;}
.TCH11 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:3;padding-right:4;
padding-bottom:2;padding-left:4;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;
letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:bold;width:100%;
height:13;overprint:true;}-->
               <xsl:if test="$rowstyle = 'TCH11'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1" />
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1" />
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--TR12one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;
padding-bottom:0.5;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}
TR12 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;
padding-bottom:0.5;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->
               <xsl:if test="$rowstyle = 'TR12'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1" />
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1" />
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>

				<!--Table Styles added today starts-->              
<!--TRB1one {margin:0;padding-top:0;padding-right:6;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;
font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,50%);
border-bottom:0.5;letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:bold;
width:100%;height:13;overprint:true;}
TRB1 {margin:0;padding-top:0;padding-right:4;padding-bottom:0;padding-left:4;font-family:Frutiger45Light;
font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,50%);
border-bottom:0.5;letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:bold;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$rowstyle = 'TRB1'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:top w:val="single" w:sz="1"/>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--TRI1one {margin:0;padding-top:0;padding-right:6;padding-bottom:0;padding-left:4;font-family:Frutiger45Light;
font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,50%);
border-bottom:0.5;letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
.TRI1 {margin:0;padding-top:0;padding-right:4;padding-bottom:0;padding-left:4;font-family:Frutiger45Light;f
ont-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,50%);
border-bottom:0.5;letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$rowstyle = 'TRI1'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--TRHone {margin:0;padding-top:0;padding-right:6;padding-bottom:3;padding-left:0;font-family:Frutiger45Light;
font-weight:bold;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;
vertical-align:bottom;align:left;requote:true;width:100%;height:13;overprint:true;}
TRH {margin:0;padding-top:0;padding-right:4;padding-bottom:3;padding-left:4;font-family:Frutiger45Light;
font-weight:bold;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;
vertical-align:bottom;align:left;requote:true;width:100%;height:13;overprint:true;}-->     
                <xsl:if test="$rowstyle = 'TRH'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--TR9Bone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;
padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;width:100%;height:13;overprint:true;}
TR9B {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;
padding-bottom:0;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$rowstyle = 'TR9B'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--TR9Aone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;
padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;width:100%;height:13;overprint:true;}
0TR9A {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;
-bottom:0;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$rowstyle = 'TR9A'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                       <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--TR13one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;padding-right:6;
padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:11;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
TR13 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;padding-right:0;
padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:11;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$rowstyle = 'TR13'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:top w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:top w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--TR2one {margin:0;padding-top:0;padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;
font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:bottom;align:left;
requote:true;font-weight:bold;border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;height:13;
width:100%;overprint:true;}
TR2 {margin:0;padding-top:0;padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;
font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:bottom;align:right;
requote:true;font-weight:normal;border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;height:13;
width:100%;overprint:true;}-->
                <xsl:if test="$rowstyle = 'TR2'">
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                      </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                       </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
				<!--Table Styles added today ends-->              
			  </xsl:when>
              <xsl:otherwise>
                <!-- apply table type styles -->
<!--tickermfsai_defaultone {text-indent:-12; margin:0;padding-left:12;padding-right:4;padding-top:2.9;padding-bottom:0;
font-family:Frutiger45Light;font-size:8;line-height:10;align:left;valign:top;letter-spacing:0;border-bottom:0.5 solid cmyk(0%,0%,0%,100%);
color:cmyk(0%,0%,0%,100%);height:14;requote:true;overprint:true;}
.tickermfsai_default {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;padding-right:0;padding-top:2.9;padding-bottom:0;
font-family:Frutiger45Light;font-size:8;line-height:10;align:left;valign:top;letter-spacing:0;border-bottom:0.5 solid cmyk(0%,0%,0%,100%);
color:cmyk(0%,0%,0%,100%);height:14;requote:true;overprint:true;}-->
                <xsl:if test="$tabletype = 'tickermfsai'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--footer_defaultone {margin:0;padding-left:0;padding-right:0;padding-top:0;padding-bottom:16.85;font-family:Frutiger45Light;font-size:8;
                line-height:9;valign:bottom;align:center;letter-spacing:0;color:cmyk(0%,0%,0%,100%);requote:true;overprint:true;}
footer_default {margin:0;padding-left:0;padding-right:0;padding-top:0;padding-bottom:16.85;font-family:Frutiger45Light;font-size:8;line-height:9;
align:center;valign:bottom;letter-spacing:0;color:cmyk(0%,0%,0%,100%);requote:true;overprint:true;}-->
                <xsl:if test="$tabletype = 'footer'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--trusthold_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;
                padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;
                align:left;requote:true;font-weight:normal;width:100%;height:13;overprint:true;}
trusthold_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'trusthold'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                      </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--trustcomp_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}
trustcomp_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'trustcomp'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:vAlign w:val="top"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--nonindeptrus3col_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;
padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;
requote:true;font-weight:normal;width:100%;height:13;overprint:true;}
nonindeptrus3col_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'nonindeptrus3col'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:vAlign w:val="top"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--contentsubad_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}
contentsubad_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'contentsubad'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--subadfees_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}
subadfees_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'subadfees'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:vAlign w:val="top"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--othraccmanag_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}
othraccmanag_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:center;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}--> 
                <xsl:if test="$tabletype = 'othraccmanag'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                      </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--pmshareown_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}
pmshareown_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:center;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'pmshareown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                       </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--brokecomm_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}
brokecomm_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'brokecomm'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                        </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                      </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--trustees_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}
trustees_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'trustees'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                       </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                      </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--portfoholdis_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
portfoholdis_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'portfoholdis'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:vAlign w:val="center"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:vAlign w:val="center"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--principaloff_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;font-family:
Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;width:100%;
height:13;overprint:true;}
principaloff_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'principaloff'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:vAlign w:val="top"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>

				<!--Table styles added today depending on table title start.-->
<!--entities_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:3;
padding-right:12;padding-bottom:0;padding-left:10;font-family:Frutiger45Light;font-size:8;
line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;text-indent:-10;width:100%;height:13;overprint:true;}
entities_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:3;
padding-right:0;padding-bottom:0;padding-left:10;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
text-indent:-10;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'entities'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--baltable_TCH11one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;
padding-right:0;padding-bottom:2;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;letter-spacing:0;
vertical-align:bottom;align:left;requote:true;font-weight:bold;width:100%;height:13;overprint:true;}
baltable_TCH11 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;padding-right:0;
padding-bottom:2;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;letter-spacing:0;
vertical-align:bottom;align:right;requote:true;font-weight:bold;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'baltable_TCH11'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--baltable_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}
baltable_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:0;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:right;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'baltable'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--porturnrate_TCH11one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;
padding-right:0;padding-bottom:2;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;letter-spacing:0;
vertical-align:bottom;align:left;requote:true;font-weight:bold;width:100%;height:13;overprint:true;}
porturnrate_TCH11 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;
padding-right:0;padding-bottom:2;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;letter-spacing:0;
vertical-align:bottom;align:right;requote:true;font-weight:bold;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'porturnrate_TCH11'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="bottom"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--porturnrate_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}
porturnrate_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:0;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:right;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'porturnrate'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--commofopera_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}
commofopera_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'commofopera'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--pmincenperiod_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
pmincenperiod_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'pmincenperiod'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--incenperiod_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
incenperiod_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'incenperiod'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--portmashown_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
portmashown_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:center;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'portmashown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--rebrokerdeal_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
rebrokerdeal_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'realbrokerdeal'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--brokerdeal_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
brokerdeal_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'brokerdeal'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--aggrecomm_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
aggrecomm_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'aggrecomm'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--disfebyascls_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
disfebyascls_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'disfebyascls'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--subadfecompa_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
subadfecompa_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'subadfecompa'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--subadfecomre_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
subadfecomre_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'subadfecomre'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--subadfebyascls_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
subadfebyascls_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'subadfebyascls'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--managfees_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
managfees_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'managfees'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--subadfebyfund_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
subadfebyfund_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'subadfebyfund'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--contentad_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
contentad_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'contentad'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--contentities_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
contentities_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'contentities'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--benefiown_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
benefiown_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:center;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'benefiown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--intrushareown_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
intrushareown_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'intrushareown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--trushareown_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
trushareown_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'trushareown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--nonindeptrus6col_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;
padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;
line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}
nonindeptrus6col_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;width:100%;height:13;
overprint:true;}-->
                <xsl:if test="$tabletype = 'nonindeptrus6col'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--trusqual2col_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
trusqual2col_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'trusqual'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--addstratrisk_defaultone {text-indent:-8;margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;
padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:8;font-family:Frutiger45Light;font-size:8;
line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;
overprint:true;}
addstratrisk_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:0;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:center;requote:true;
font-weight:normal;border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;
:true;}-->
                <xsl:if test="$tabletype = 'addstratrisk'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'tickersfsai'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:tcBorders>
                        <w:bottom w:val="single" w:sz="1"/>
                      </w:tcBorders>
                      <w:vAlign w:val="top"/>
                     </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
						<!--Table styles added today depending on table title ends.-->              
			  </xsl:otherwise>
      </xsl:choose>  
    </w:tcPr>
    </xsl:if>
    <xsl:if test="$tcrunflag = 'run'">
      <xsl:choose>
        <xsl:when test="$rowstyleflag and not($rowstyle = '')">
          <!--FCTCHone {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;
                padding-right:4;padding-top:2.2;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;
                font-weight:bold;line-height:10;align:left;valign:top;letter-spacing:0;border-bottom:0.5 solid
                cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}
                FCTCH {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;
                padding-right:0;padding-top:2.2;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;
                font-weight:bold;line-height:10;align:left;valign:top;letter-spacing:0;border-bottom:0.5 
                solid cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}-->
          <xsl:if test="$rowstyle = 'FCTCH'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--FCTCSHone {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;
                 padding-right:4;padding-top:2.4;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;
                 font-weight:bold;line-height:10;align:left;valign:top;letter-spacing:0;color:
                 cmyk(0%,0%,0%,100%);border-top:0.5 solid cmyk(0%,0%,0%,100%);height:16;requote:true;
                 overprint:true;}
                FCTCSH {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;
                padding-right:0;padding-top:2.4;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;
                font-weight:bold;line-height:10;align:center;valign:top;letter-spacing:0;border-top:0.5 
                solid cmyk(0%,0%,0%,100%);border-bottom:0.5 solid cmyk(0%,0%,0%,100%);color:
                cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}-->
          <xsl:if test="$rowstyle = 'FCTCSH'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--TCH11one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:3;padding-right:
;padding-bottom:2;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;
letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:bold;width:100%;height:13;
overprint:true;}
.TCH11 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:3;padding-right:4;
padding-bottom:2;padding-left:4;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;
letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:bold;width:100%;
height:13;overprint:true;}-->
          <xsl:if test="$rowstyle = 'TCH11'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="14"/>
                <w:szCs w:val="14"/>
                <w:b />
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="14"/>
                <w:szCs w:val="14"/>
                <w:b />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--TR12one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;
padding-bottom:0.5;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}
TR12 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;
padding-bottom:0.5;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->
          <xsl:if test="$rowstyle = 'TR12'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--PF1one {margin:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;
                font-family:Frutiger45Light;font-size:8;line-height:9;color:cmyk(0%,0%,0%,100%);
                letter-spacing:-0.1;vertical-align:bottom;align:center;requote:true;
                font-weight:bold;width:100%;overprint:true;}
                PF1 {margin:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;
                font-family:Frutiger45Light;font-size:8;line-height:9;color:cmyk(0%,0%,0%,100%);
                letter-spacing:-0.1;vertical-align:bottom;align:center;requote:true;font-weight:bold;
                width:100%;overprint:true;}-->
          <xsl:if test="$rowstyle = 'PF1'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--TCSH11one {margin:0;padding-top:0;padding-right:6;padding-bottom:2;padding-left:0;
font-family:Frutiger45Light;font-size:7;font-weight:bold;line-height:8;color:cmyk(0%,0%,0%,100%);
letter-spacing:0;vertical-align:bottom;align:left;requote:true;width:100%;height:13;overprint:true;}
.TCSH11 {margin-left:2;margin-right:2;padding-top:0;padding-right:4;padding-bottom:2;padding-left:4;
font-family:Frutiger45Light;font-size:7;font-weight:bold;line-height:8;color:cmyk(0%,0%,0%,100%);
letter-spacing:0;vertical-align:bottom;align:left;border-bottom-color:cmyk(0%,0%,0%,100%);
border-bottom:0.5;requote:true;width:100%;height:13;overprint:true;}-->         
          <xsl:if test="$rowstyle = 'TCSH11'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="14"/>
                <w:szCs w:val="14"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="14"/>
                <w:szCs w:val="14"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--TH1one {margin:0;padding-top:0;padding-right:6;padding-bottom:1;padding-left:0;
font-family:Frutiger45Light;font-weight:bold;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);
letter-spacing:0;vertical-align:bottom;align:left;requote:true;width:100%;height:13;overprint:true;}
TH1 {margin:0;padding-top:0;padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;
font-weight:bold;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:bottom;
align:left;requote:true;width:100%;height:13;overprint:true;}-->
          <xsl:if test="$rowstyle = 'TH1'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="14"/>
                <w:szCs w:val="14"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
           
					<!--Table Styles added today starts--> 
          <!--TRB1one {margin:0;padding-top:0;padding-right:6;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;
font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,50%);
border-bottom:0.5;letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:bold;
width:100%;height:13;overprint:true;}
TRB1 {margin:0;padding-top:0;padding-right:4;padding-bottom:0;padding-left:4;font-family:Frutiger45Light;
font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,50%);
border-bottom:0.5;letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:bold;
width:100%;height:13;overprint:true;}-->
          <xsl:if test="$rowstyle = 'TRB1'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--TRI1one {margin:0;padding-top:0;padding-right:6;padding-bottom:0;padding-left:4;font-family:Frutiger45Light;
font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,50%);
border-bottom:0.5;letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
.TRI1 {margin:0;padding-top:0;padding-right:4;padding-bottom:0;padding-left:4;font-family:Frutiger45Light;f
ont-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,50%);
border-bottom:0.5;letter-spacing:0;vertical-align:bottom;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
          <xsl:if test="$rowstyle = 'TRI1'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--TRHone {margin:0;padding-top:0;padding-right:6;padding-bottom:3;padding-left:0;font-family:Frutiger45Light;
font-weight:bold;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;
vertical-align:bottom;align:left;requote:true;width:100%;height:13;overprint:true;}
TRH {margin:0;padding-top:0;padding-right:4;padding-bottom:3;padding-left:4;font-family:Frutiger45Light;
font-weight:bold;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;
vertical-align:bottom;align:left;requote:true;width:100%;height:13;overprint:true;}-->   
          <xsl:if test="$rowstyle = 'TRH'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--TR9Bone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;
padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;width:100%;height:13;overprint:true;}
TR9B {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;
padding-bottom:0;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->
          <xsl:if test="$rowstyle = 'TR9B'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--TR9Aone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;
padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;width:100%;height:13;overprint:true;}
0TR9A {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;
-bottom:0;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->
          <xsl:if test="$rowstyle = 'TR9A'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--TR2one {margin:0;padding-top:0;padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;
font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:bottom;align:left;
requote:true;font-weight:bold;border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;height:13;
width:100%;overprint:true;}
TR2 {margin:0;padding-top:0;padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;
font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:bottom;align:right;
requote:true;font-weight:normal;border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;height:13;
width:100%;overprint:true;}-->
          <xsl:if test="$rowstyle = 'TR2'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:b />
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--TR13one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;padding-right:6;
padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:11;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
TR13 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;padding-right:0;
padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:11;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
          <xsl:if test="$rowstyle = 'TR13'">
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
						<!--Table Styles added today ends-->     
		</xsl:when>
        <xsl:otherwise>
          <!-- apply table type styles -->
                <xsl:if test="$tabletype = 'tickermfsai'" >
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:tcBorders>
                  <w:bottom w:val="single" w:sz="1"/>
                </w:tcBorders>
                <w:vAlign w:val="center"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:tcBorders>
                  <w:bottom w:val="single" w:sz="1"/>
                </w:tcBorders>
                <w:vAlign w:val="center"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
           <xsl:if test="$tabletype = 'footer'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/><w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'trusthold'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'trustcomp'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'nonindeptrus3col'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'contentsubad'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'subadfees'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'othraccmanag'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'pmshareown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'brokecomm'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'trustees'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="center"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'portfoholdis'" >
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:vAlign w:val="center"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:vAlign w:val="center"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
<!--principaloff_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;font-family:
Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;width:100%;
height:13;overprint:true;}
principaloff_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;padding-right:4;padding-bottom:1;padding-left:4;
font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'principaloff'" >
            <xsl:choose>
              <xsl:when test="$entryIndex = 1">
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:when>
              <xsl:otherwise>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                <wx:font wx:val="Arial"/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
			
			<!--Table row styles added today depending on Table Title Start-->         
<!--addstratrisk_defaultone {text-indent:-8;margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;
padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:8;font-family:Frutiger45Light;font-size:8;
line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;
overprint:true;}
addstratrisk_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:0;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:center;requote:true;
font-weight:normal;border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;
:true;}-->          
                <xsl:if test="$tabletype = 'addstratrisk'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--trusqual2col_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
trusqual2col_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'trusqual'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--trushareown_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
trushareown_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'trushareown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--nonindeptrus6col_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;
padding-top:2.4;padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;
line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}
nonindeptrus6col_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;width:100%;height:13;
overprint:true;}-->          
                <xsl:if test="$tabletype = 'nonindeptrus6col'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--intrushareown_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
intrushareown_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->    
                <xsl:if test="$tabletype = 'intrushareown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--benefiown_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
benefiown_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:center;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'benefiown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--contentities_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
contentities_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'benefiown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                     <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--contentad_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
contentad_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->  
                <xsl:if test="$tabletype = 'contentad'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--subadfebyfund_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
subadfebyfund_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->          
                <xsl:if test="$tabletype = 'subadfebyfund'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--managfees_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
managfees_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->          
                <xsl:if test="$tabletype = 'subadfebyfund'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--subadfebyascls_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
subadfebyascls_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->     
                <xsl:if test="$tabletype = 'subadfebyascls'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--subadfecomre_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
subadfecomre_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'subadfecomre'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                     <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--subadfecompa_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
subadfecompa_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}--> 
                <xsl:if test="$tabletype = 'subadfecompa'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--disfebyascls_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
disfebyascls_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->          
                <xsl:if test="$tabletype = 'disfebyascls'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--aggrecomm_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
aggrecomm_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'aggrecomm'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--brokerdeal_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
brokerdeal_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->            
                <xsl:if test="$tabletype = 'brokerdeal'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--rebrokerdeal_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
rebrokerdeal_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}--> 
                <xsl:if test="$tabletype = 'rebrokerdeal'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--portmashown_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
portmashown_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:center;requote:true;
font-weight:normal;width:100%;height:13;overprint:true;}-->          
                <xsl:if test="$tabletype = 'portmashown'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--incenperiod_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
incenperiod_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->  
                <xsl:if test="$tabletype = 'incenperiod'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--pmincenperiod_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}
pmincenperiod_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
width:100%;height:13;overprint:true;}-->  
                <xsl:if test="$tabletype = 'pmincenperiod'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--commofopera_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}
commofopera_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:4;padding-bottom:1;padding-left:4;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->  
                <xsl:if test="$tabletype = 'commofopera'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--porturnrate_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}
porturnrate_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:0;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:right;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->  
                <xsl:if test="$tabletype = 'porturnrate'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--porturnrate_TCH11one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;
padding-right:0;padding-bottom:2;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;letter-spacing:0;
vertical-align:bottom;align:left;requote:true;font-weight:bold;width:100%;height:13;overprint:true;}
porturnrate_TCH11 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;
padding-right:0;padding-bottom:2;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;letter-spacing:0;
vertical-align:bottom;align:right;requote:true;font-weight:bold;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'porturnrate_TCH11'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="bottom"/>
                      <w:b />
                      <w:sz w:val="14"/>
                      <w:szCs w:val="14"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="bottom"/>
                      <w:b />
                      <w:sz w:val="14"/>
                      <w:szCs w:val="14"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--baltable_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:6;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}
baltable_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:2.4;
padding-right:0;padding-bottom:1;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:right;requote:true;font-weight:normal;
border-bottom-color:cmyk(0%,0%,0%,50%);border-bottom:0.5;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'baltable'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:b />
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:b />
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--baltable_TCH11one {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;
padding-right:0;padding-bottom:2;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;letter-spacing:0;
vertical-align:bottom;align:left;requote:true;font-weight:bold;width:100%;height:13;overprint:true;}
baltable_TCH11 {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;padding-right:0;
padding-bottom:2;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;
color:cmyk(0%,0%,0%,100%);border-bottom-color:cmyk(0%,0%,0%,100%);border-bottom:0.5;letter-spacing:0;
vertical-align:bottom;align:right;requote:true;font-weight:bold;width:100%;height:13;overprint:true;}-->
                <xsl:if test="$tabletype = 'baltable_TCH11'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="bottom"/>
                      <w:b />
                      <w:sz w:val="14"/>
                      <w:szCs w:val="14"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="bottom"/>
                      <w:b />
                      <w:sz w:val="14"/>
                      <w:szCs w:val="14"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
<!--entities_defaultone {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:3;
padding-right:12;padding-bottom:0;padding-left:10;font-family:Frutiger45Light;font-size:8;
line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;
font-weight:normal;text-indent:-10;width:100%;height:13;overprint:true;}
entities_default {margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:3;
padding-right:0;padding-bottom:0;padding-left:10;font-family:Frutiger45Light;font-size:8;line-height:10;
color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;font-weight:normal;
text-indent:-10;width:100%;height:13;overprint:true;}-->          
                <xsl:if test="$tabletype = 'entities_TCH11'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:vAlign w:val="top"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="$tabletype = 'tickersfsai'" >
                  <xsl:choose>
                    <xsl:when test="$entryIndex = 1">
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
                      <wx:font wx:val="Arial"/>
                      <w:sz w:val="16"/>
                      <w:szCs w:val="16"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
				<!--Table row styles added today depending on Table Title Ends-->
		</xsl:otherwise>
      </xsl:choose>
    </xsl:if> 
  </xsl:template>
</xsl:stylesheet>