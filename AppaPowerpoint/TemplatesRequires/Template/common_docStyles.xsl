<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
  xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
	w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no">
  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template name="fonts">
    <w:fonts xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
      <w:defaultFonts w:ascii="Calibri" w:fareast="Calibri"
				w:h-ansi="Calibri" w:cs="Times New Roman" />
      <w:font w:name="Times New Roman">
        <w:panose-1 w:val="02020603050405020304" />
        <w:charset w:val="00" />
        <w:family w:val="Roman" />
        <w:pitch w:val="variable" />
        <w:sig w:usb-0="E0002AFF" w:usb-1="C0007841" w:usb-2="00000009"
					w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000" />
      </w:font>
      <w:font w:name="Arial">
        <w:panose-1 w:val="020B0604020202020204" />
        <w:charset w:val="00" />
        <w:family w:val="Swiss" />
        <w:pitch w:val="variable" />
        <w:sig w:usb-0="E0002AFF" w:usb-1="C0007843" w:usb-2="00000009"
					w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000" />
      </w:font>
      <w:font w:name="Cambria Math">
        <w:panose-1 w:val="02040503050406030204" />
        <w:charset w:val="01" />
        <w:family w:val="Roman" />
        <w:notTrueType />
        <w:pitch w:val="variable" />
      </w:font>
      <w:font w:name="Calibri">
        <w:panose-1 w:val="020F0502020204030204" />
        <w:charset w:val="00" />
        <w:family w:val="Swiss" />
        <w:pitch w:val="variable" />
        <w:sig w:usb-0="E00002FF" w:usb-1="4000ACFF" w:usb-2="00000001"
					w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000" />
      </w:font>
      <w:font w:name="Arial Black">
        <w:panose-1 w:val="020B0A04020102020204" />
        <w:charset w:val="00" />
        <w:family w:val="Swiss" />
        <w:pitch w:val="variable" />
        <w:sig w:usb-0="00000287" w:usb-1="00000000" w:usb-2="00000000"
					w:usb-3="00000000" w:csb-0="0000009F" w:csb-1="00000000" />
      </w:font>
      <w:font w:name="Bookman Old Style">
        <w:panose-1 w:val="02050604050505020204" />
        <w:charset w:val="00" />
        <w:family w:val="Roman" />
        <w:pitch w:val="variable" />
        <w:sig w:usb-0="00000287" w:usb-1="00000000" w:usb-2="00000000"
					w:usb-3="00000000" w:csb-0="0000009F" w:csb-1="00000000" />
      </w:font>
    </w:fonts>
  </xsl:template>
  <xsl:template name="styles">
    <w:styles>
      <w:versionOfBuiltInStylenames w:val="7" />
      <w:latentStyles w:defLockedState="off" w:latentStyleCount="371">
        <w:lsdException w:name="Normal" />
        <w:lsdException w:name="heading 1" />
        <w:lsdException w:name="heading 2" />
        <w:lsdException w:name="heading 3" />
        <w:lsdException w:name="heading 4" />
        <w:lsdException w:name="heading 5" />
        <w:lsdException w:name="heading 6" />
        <w:lsdException w:name="heading 7" />
        <w:lsdException w:name="heading 8" />
        <w:lsdException w:name="heading 9" />
        <w:lsdException w:name="caption" />
        <w:lsdException w:name="Title" />
        <w:lsdException w:name="Subtitle" />
        <w:lsdException w:name="Strong" />
        <w:lsdException w:name="Emphasis" />
        <w:lsdException w:name="Normal Table" />
        <w:lsdException w:name="Table Simple 1" />
        <w:lsdException w:name="Table Simple 2" />
        <w:lsdException w:name="Table Simple 3" />
        <w:lsdException w:name="Table Classic 1" />
        <w:lsdException w:name="Table Classic 2" />
        <w:lsdException w:name="Table Classic 3" />
        <w:lsdException w:name="Table Classic 4" />
        <w:lsdException w:name="Table Colorful 1" />
        <w:lsdException w:name="Table Colorful 2" />
        <w:lsdException w:name="Table Colorful 3" />
        <w:lsdException w:name="Table Columns 1" />
        <w:lsdException w:name="Table Columns 2" />
        <w:lsdException w:name="Table Columns 3" />
        <w:lsdException w:name="Table Columns 4" />
        <w:lsdException w:name="Table Columns 5" />
        <w:lsdException w:name="Table Grid 1" />
        <w:lsdException w:name="Table Grid 2" />
        <w:lsdException w:name="Table Grid 3" />
        <w:lsdException w:name="Table Grid 4" />
        <w:lsdException w:name="Table Grid 5" />
        <w:lsdException w:name="Table Grid 6" />
        <w:lsdException w:name="Table Grid 7" />
        <w:lsdException w:name="Table Grid 8" />
        <w:lsdException w:name="Table List 1" />
        <w:lsdException w:name="Table List 2" />
        <w:lsdException w:name="Table List 3" />
        <w:lsdException w:name="Table List 4" />
        <w:lsdException w:name="Table List 5" />
        <w:lsdException w:name="Table List 6" />
        <w:lsdException w:name="Table List 7" />
        <w:lsdException w:name="Table List 8" />
        <w:lsdException w:name="Table 3D effects 1" />
        <w:lsdException w:name="Table 3D effects 2" />
        <w:lsdException w:name="Table 3D effects 3" />
        <w:lsdException w:name="Table Contemporary" />
        <w:lsdException w:name="Table Elegant" />
        <w:lsdException w:name="Table Professional" />
        <w:lsdException w:name="Table Subtle 1" />
        <w:lsdException w:name="Table Subtle 2" />
        <w:lsdException w:name="Table Web 1" />
        <w:lsdException w:name="Table Web 2" />
        <w:lsdException w:name="Table Web 3" />
        <w:lsdException w:name="Table Theme" />
        <w:lsdException w:name="No Spacing" />
        <w:lsdException w:name="Light Shading" />
        <w:lsdException w:name="Light List" />
        <w:lsdException w:name="Light Grid" />
        <w:lsdException w:name="Medium Shading 1" />
        <w:lsdException w:name="Medium Shading 2" />
        <w:lsdException w:name="Medium List 1" />
        <w:lsdException w:name="Medium List 2" />
        <w:lsdException w:name="Medium Grid 1" />
        <w:lsdException w:name="Medium Grid 2" />
        <w:lsdException w:name="Medium Grid 3" />
        <w:lsdException w:name="Dark List" />
        <w:lsdException w:name="Colorful Shading" />
        <w:lsdException w:name="Colorful List" />
        <w:lsdException w:name="Colorful Grid" />
        <w:lsdException w:name="Light Shading Accent 1" />
        <w:lsdException w:name="Light List Accent 1" />
        <w:lsdException w:name="Light Grid Accent 1" />
        <w:lsdException w:name="Medium Shading 1 Accent 1" />
        <w:lsdException w:name="Medium Shading 2 Accent 1" />
        <w:lsdException w:name="Medium List 1 Accent 1" />
        <w:lsdException w:name="List Paragraph" />
        <w:lsdException w:name="Quote" />
        <w:lsdException w:name="Intense Quote" />
        <w:lsdException w:name="Medium List 2 Accent 1" />
        <w:lsdException w:name="Medium Grid 1 Accent 1" />
        <w:lsdException w:name="Medium Grid 2 Accent 1" />
        <w:lsdException w:name="Medium Grid 3 Accent 1" />
        <w:lsdException w:name="Dark List Accent 1" />
        <w:lsdException w:name="Colorful Shading Accent 1" />
        <w:lsdException w:name="Colorful List Accent 1" />
        <w:lsdException w:name="Colorful Grid Accent 1" />
        <w:lsdException w:name="Light Shading Accent 2" />
        <w:lsdException w:name="Light List Accent 2" />
        <w:lsdException w:name="Light Grid Accent 2" />
        <w:lsdException w:name="Medium Shading 1 Accent 2" />
        <w:lsdException w:name="Medium Shading 2 Accent 2" />
        <w:lsdException w:name="Medium List 1 Accent 2" />
        <w:lsdException w:name="Medium List 2 Accent 2" />
        <w:lsdException w:name="Medium Grid 1 Accent 2" />
        <w:lsdException w:name="Medium Grid 2 Accent 2" />
        <w:lsdException w:name="Medium Grid 3 Accent 2" />
        <w:lsdException w:name="Dark List Accent 2" />
        <w:lsdException w:name="Colorful Shading Accent 2" />
        <w:lsdException w:name="Colorful List Accent 2" />
        <w:lsdException w:name="Colorful Grid Accent 2" />
        <w:lsdException w:name="Light Shading Accent 3" />
        <w:lsdException w:name="Light List Accent 3" />
        <w:lsdException w:name="Light Grid Accent 3" />
        <w:lsdException w:name="Medium Shading 1 Accent 3" />
        <w:lsdException w:name="Medium Shading 2 Accent 3" />
        <w:lsdException w:name="Medium List 1 Accent 3" />
        <w:lsdException w:name="Medium List 2 Accent 3" />
        <w:lsdException w:name="Medium Grid 1 Accent 3" />
        <w:lsdException w:name="Medium Grid 2 Accent 3" />
        <w:lsdException w:name="Medium Grid 3 Accent 3" />
        <w:lsdException w:name="Dark List Accent 3" />
        <w:lsdException w:name="Colorful Shading Accent 3" />
        <w:lsdException w:name="Colorful List Accent 3" />
        <w:lsdException w:name="Colorful Grid Accent 3" />
        <w:lsdException w:name="Light Shading Accent 4" />
        <w:lsdException w:name="Light List Accent 4" />
        <w:lsdException w:name="Light Grid Accent 4" />
        <w:lsdException w:name="Medium Shading 1 Accent 4" />
        <w:lsdException w:name="Medium Shading 2 Accent 4" />
        <w:lsdException w:name="Medium List 1 Accent 4" />
        <w:lsdException w:name="Medium List 2 Accent 4" />
        <w:lsdException w:name="Medium Grid 1 Accent 4" />
        <w:lsdException w:name="Medium Grid 2 Accent 4" />
        <w:lsdException w:name="Medium Grid 3 Accent 4" />
        <w:lsdException w:name="Dark List Accent 4" />
        <w:lsdException w:name="Colorful Shading Accent 4" />
        <w:lsdException w:name="Colorful List Accent 4" />
        <w:lsdException w:name="Colorful Grid Accent 4" />
        <w:lsdException w:name="Light Shading Accent 5" />
        <w:lsdException w:name="Light List Accent 5" />
        <w:lsdException w:name="Light Grid Accent 5" />
        <w:lsdException w:name="Medium Shading 1 Accent 5" />
        <w:lsdException w:name="Medium Shading 2 Accent 5" />
        <w:lsdException w:name="Medium List 1 Accent 5" />
        <w:lsdException w:name="Medium List 2 Accent 5" />
        <w:lsdException w:name="Medium Grid 1 Accent 5" />
        <w:lsdException w:name="Medium Grid 2 Accent 5" />
        <w:lsdException w:name="Medium Grid 3 Accent 5" />
        <w:lsdException w:name="Dark List Accent 5" />
        <w:lsdException w:name="Colorful Shading Accent 5" />
        <w:lsdException w:name="Colorful List Accent 5" />
        <w:lsdException w:name="Colorful Grid Accent 5" />
        <w:lsdException w:name="Light Shading Accent 6" />
        <w:lsdException w:name="Light List Accent 6" />
        <w:lsdException w:name="Light Grid Accent 6" />
        <w:lsdException w:name="Medium Shading 1 Accent 6" />
        <w:lsdException w:name="Medium Shading 2 Accent 6" />
        <w:lsdException w:name="Medium List 1 Accent 6" />
        <w:lsdException w:name="Medium List 2 Accent 6" />
        <w:lsdException w:name="Medium Grid 1 Accent 6" />
        <w:lsdException w:name="Medium Grid 2 Accent 6" />
        <w:lsdException w:name="Medium Grid 3 Accent 6" />
        <w:lsdException w:name="Dark List Accent 6" />
        <w:lsdException w:name="Colorful Shading Accent 6" />
        <w:lsdException w:name="Colorful List Accent 6" />
        <w:lsdException w:name="Colorful Grid Accent 6" />
        <w:lsdException w:name="Subtle Emphasis" />
        <w:lsdException w:name="Intense Emphasis" />
        <w:lsdException w:name="Subtle Reference" />
        <w:lsdException w:name="Intense Reference" />
        <w:lsdException w:name="Book Title" />
        <w:lsdException w:name="TOC Heading" />
      </w:latentStyles>
      <!--<w:style w:type="paragraph" w:default="on" w:styleId="Normal">
        <w:name w:val="Normal" />
        <w:rsid w:val="006E3020" />
        <w:pPr>
          <w:spacing w:after="160" w:line="259" w:line-rule="auto" />
        </w:pPr>
        <w:rPr>
          <wx:font wx:val="Arial" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" />
          <w:sz w:val="22" />
          <w:sz-cs w:val="22" />
          <w:lang w:val="EN-US" w:fareast="EN-US" w:bidi="AR-SA" />
        </w:rPr>
      </w:style>-->
      <w:style w:type="paragraph" w:styleId="blockquoteindentparagraphstyle1">
        <w:name w:val="Block Quote Indent ParagraphStyle 1" />
        <w:hidden />
      </w:style>
      <w:style w:type="paragraph" w:styleId="blockquoteindentparagraphstyle2">
        <w:name w:val="Block Quote Indent ParagraphStyle 2" />
        <w:hidden />
      </w:style>
      <w:style w:type="paragraph" w:styleId="blockquoteindentparagraphstyle3">
        <w:name w:val="Block Quote Indent ParagraphStyle 3" />
        <w:hidden />
      </w:style>
      <w:style w:type="paragraph" w:styleId="blockquoteindentparagraphstyle4">
        <w:name w:val="Block Quote Indent ParagraphStyle 4" />
        <w:hidden />
      </w:style>
      <w:style w:type="character" w:styleId="defaultcharstyle">
        <w:name w:val="Default Char Style" />
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:sz w:val="16" />
          <w:sz-cs w:val="16" />
          <!--<w:lang w:val="EN-US" w:fareast="EN-US" w:bidi="AR-SA" />-->
        </w:rPr>
      </w:style>
      <w:style w:type="table" w:default="on" w:styleId="TableNormal">
        <w:name w:val="Normal Table" />
        <!--<wx:uiName wx:val="Table Normal" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" />
        <w:rPr>
          <wx:font wx:val="Calibri" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" />
          <w:lang w:val="EN-US" w:fareast="EN-US" w:bidi="AR-SA" />
        </w:rPr>-->
        <!--<w:tblPr>
          <w:tblInd w:w="0" w:type="dxa" />
          <w:tblCellMar>
            <w:top w:w="0" w:type="dxa" />
            <w:left w:w="0" w:type="dxa" />
            <w:bottom w:w="0" w:type="dxa" />
            <w:right w:w="0" w:type="dxa" />
          </w:tblCellMar>
        </w:tblPr>-->
      </w:style>
      <!--<w:style w:type="list" w:default="on" w:styleId="NoList">
        <w:name w:val="No List" />
      </w:style>-->
      <w:style w:type="character" w:styleId="stsize3">
        <!-- .st {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-weight:bold;font-size:12;line-height:15;color:cmyk(0%,0%,0%,100%);letter-spacing:0;align:left;valign:top;requote:true;overprint:true;} -->
        <w:name w:val="St Size 3"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:b w:val="true"/>
          <w:sz w:val="24"/>
          <w:szCs w:val="24"/>
        </w:rPr>
      </w:style>
      <!--Styles Added Today-->
      <!--.fctx1 {margin:0;padding-top:0;padding-right:0;padding-bottom:5;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:11;color:cmyk(0%,0%,0%,100%);letter-spacing:-0.1;vertical-align:top;align:left;requote:true;overprint:true;}-->
      <w:style w:type="character" w:styleId="fctx1size3pt5">
        <w:name w:val="Fctx 1 Size 3 Pt 5"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:sz w:val="16"/>
          <w:szCs w:val="16"/>
        </w:rPr>
      </w:style>
      <!--.fnunsq {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-weight:normal;font-style:italic;font-size:7;line-height:8;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;overprint:true;}-->
      <w:style w:type="character" w:styleId="fnunsqsize4">
        <w:name w:val="Fnunsq Size 4"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:sz w:val="14"/>
          <w:szCs w:val="14"/>
        </w:rPr>
      </w:style>
      <!--.note {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-style:italic;font-size:7;line-height:8;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;overprint:true;}-->
      <w:style w:type="character" w:styleId="notesize3pt5">
        <w:name w:val="Note Size 3 Pt 5"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:i w:val="true"/>
          <w:sz w:val="14"/>
          <w:szCs w:val="14"/>
        </w:rPr>
      </w:style>
      <!--TOC1 {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;border-bottom-color:cmyk(0%,0%,0%,30%);border-bottom:0.5;font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:right;requote:true;font-weight:normal;width:100%;height:14;overprint:true;}-->
      <w:style w:type="character" w:styleId="TOC1">
        <w:name w:val="TOC1"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:sz w:val="16"/>
          <w:szCs w:val="16"/>
        </w:rPr>
      </w:style>
      <!--.ul {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:11;color:cmyk(0%,0%,0%,100%);letter-spacing:0;align:left;valign:top;requote:true;font-weight:normal;width:100%;overprint:true;}-->
      <w:style w:type="character" w:styleId="ulsize7pt5">
        <w:name w:val="Ul Size 7 Pt 5"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:sz w:val="16"/>
          <w:szCs w:val="16"/>
        </w:rPr>
      </w:style>
      <!--.fctx2 {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:11;color:spot('PMS 032 C',cmyk(0%,93%,76%,0%),100%);letter-spacing:-0.1;font-weight:bold;valign:top;align:left;requote:true;width:100%;}-->
      <w:style w:type="character" w:styleId="fctx2">
        <w:name w:val="fctx2"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:color w:val="FF0000"/>
          <w:b w:val="true"/>
          <w:sz w:val="16"/>
          <w:szCs w:val="16"/>
        </w:rPr>
      </w:style>
      <!--.sh {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-weight:bold;font-size:12;line-height:15;color:cmyk(0%,0%,0%,100%);letter-spacing:0;align:left;valign:top;requote:true;overprint:true;}-->
      <w:style w:type="character" w:styleId="shsize3pt5">
        <w:name w:val="Sh Size 3 Pt 5"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:b w:val="true"/>
          <w:sz w:val="24"/>
          <w:szCs w:val="24"/>
        </w:rPr>
      </w:style>
      <!--.fch2 {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:16;line-height:17;color:cmyk(0%,0%,0%,100%);letter-spacing:-0.1;vertical-align:top;align:center;requote:true;font-weight:bold;width:100%;overprint:true;}-->
      <w:style w:type="character" w:styleId="fch2size3pt0">
        <w:name w:val="Fch 2 Size 3 Pt 0"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:b w:val="true"/>
          <w:sz w:val="32"/>
          <w:szCs w:val="32"/>
        </w:rPr>
      </w:style>
      <!--Styles added today ended-->
      <w:style w:type="character" w:styleId="fnsqsize4">
        <!-- .fnsq{margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;overprint:true;} -->
        <w:name w:val="Fnsq Size 4"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:sz w:val="14"/>
          <w:szCs w:val="14"/>
        </w:rPr>
      </w:style>
      <w:style w:type="character" w:styleId="fnsqfootnotestylelabelsize4">
        <!-- .fnsq{margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:7;line-height:8;color:cmyk(0%,0%,0%,100%);letter-spacing:0;vertical-align:top;align:left;requote:true;overprint:true;} -->
        <w:name w:val="Fnsq Footnote Style Label Size 4"/>
        <w:hidden />
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:sz w:val="14"/>
          <w:szCs w:val="14"/>
        </w:rPr>
      </w:style>
      <w:style w:type="character" w:styleId="h3size4pt5">
        <!--.h3 {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;align:left;valign:top;requote:true;font-weight:bold;width:100%;overprint:true;}-->
        <w:name w:val="H 3 Size 4 Pt 5"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:b w:val="true"/>
          <w:sz w:val="16"/>
          <w:szCs w:val="16"/>
        </w:rPr>
      </w:style>
      <w:style w:type="character" w:styleId="h2size8">
        <!--.h2 {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:8;line-height:10;color:cmyk(0%,0%,0%,100%);letter-spacing:0;align:left;valign:top;requote:true;font-style:italic;width:100%;overprint:true;}-->
        <w:name w:val="H 2 Size 8"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:i w:val="true"/>
          <w:sz w:val="16"/>
          <w:szCs w:val="16"/>
        </w:rPr>
      </w:style>
      <w:style w:type="character" w:styleId="fch3size4pt5">
        <!--.fch3 {margin:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-family:Frutiger45Light;font-size:10;line-height:13;color:cmyk(0%,0%,0%,100%);letter-spacing:-0.1;vertical-align:top;align:center;requote:true;font-weight:normal;width:100%;overprint:true;}-->
        <w:name w:val="Fch 3 Size 4 Pt 5"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
          <wx:font wx:val="Arial"/>
          <w:sz w:val="20"/>
          <w:szCs w:val="20"/>
        </w:rPr>
      </w:style>
      <!--<w:style w:type="table" w:styleId="FCTCSH">
        -->
      <!-- 
        .FCTCSHone {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;padding-right:4;padding-top:2.4;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;font-weight:bold;line-height:10;align:left;valign:top;letter-spacing:0;color:cmyk(0%,0%,0%,100%);border-top:0.5 solid cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}
        .FCTCSH {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;padding-right:0;padding-top:2.4;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;font-weight:bold;line-height:10;align:center;valign:top;letter-spacing:0;border-top:0.5 solid cmyk(0%,0%,0%,100%);border-bottom:0.5 solid cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}
        -->
      <!--
        <w:name w:val="FCTCSH"/>
        -->
      <!-- <w:tblPr>
        <w:tblLayout w:type="fixed"/>
        </w:tblPr>-->
      <!--
        <w:tcPr>
          <w:tcBorders>
            <w:top w:val="single" w:sz="1"/>
            <w:bottom w:val="single" w:sz="1"/>
          </w:tcBorders>
          <w:sz w:val="16"/>
          <w:szCs w:val="16"/>
        </w:tcPr>
        <w:tblStylePr w:type="firstCol">
          <w:tcPr>
            <w:tcBorders>
              <w:top w:val="single" w:sz="1"/>
            </w:tcBorders>
            <w:sz w:val="16"/>
            <w:szCs w:val="16"/>
          </w:tcPr>
        </w:tblStylePr>
      </w:style>-->
      <!--<w:style w:type="table" w:styleId="FCTCH">
        -->
      <!-- 
        .FCTCSHone {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;padding-right:4;padding-top:2.4;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;font-weight:bold;line-height:10;align:left;valign:top;letter-spacing:0;color:cmyk(0%,0%,0%,100%);border-top:0.5 solid cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}
        .FCTCSH {margin-bottom:0;margin-top:0;margin-left:0;margin-right:0;padding-left:0;padding-right:0;padding-top:2.4;padding-bottom:0;font-family:Frutiger47Cond;font-size:8;font-weight:bold;line-height:10;align:center;valign:top;letter-spacing:0;border-top:0.5 solid cmyk(0%,0%,0%,100%);border-bottom:0.5 solid cmyk(0%,0%,0%,100%);color:cmyk(0%,0%,0%,100%);height:16;requote:true;overprint:true;}
        -->
      <!--
        <w:name w:val="FCTCH"/>
        -->
      <!-- <w:tblPr>
        <w:tblLayout w:type="fixed"/>
        </w:tblPr>-->
      <!--
        <w:tcPr>
          <w:tcBorders>
            <w:top w:val="single" w:sz="1"/>
            <w:bottom w:val="single" w:sz="1"/>
          </w:tcBorders>
          <w:sz w:val="16"/>
          <w:szCs w:val="16"/>
        </w:tcPr>
        <w:tblStylePr w:type="firstCol">
          <w:tcPr>
            <w:tcBorders>
              <w:top w:val="single" w:sz="1"/>
              <w:bottom w:val="single" w:sz="1"/>
            </w:tcBorders>
            <w:sz w:val="16"/>
            <w:szCs w:val="16"/>
          </w:tcPr>
        </w:tblStylePr>
      </w:style>-->
    </w:styles>
  </xsl:template>
  <xsl:template name="docPr">
    <w:docPr xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
      <w:view w:val="print" />
      <w:zoom w:percent="100" />
      <w:doNotEmbedSystemFonts />
      <w:defaultTabStop w:val="720" />
      <w:punctuationKerning />
      <w:characterSpacingControl w:val="DontCompress" />
      <w:optimizeForBrowser />
      <w:allowPNG />
      <w:validateAgainstSchema />
      <w:saveInvalidXML w:val="off" />
      <w:ignoreMixedContent w:val="off" />
      <w:alwaysShowPlaceholderText w:val="off" />
      <w:compat>
        <w:breakWrappedTables />
        <w:snapToGridInCell />
        <w:wrapTextWithPunct />
        <w:useAsianBreakRules />
        <w:dontGrowAutofit />
      </w:compat>
      <wsp:rsids xmlns:wsp="http://schemas.microsoft.com/office/word/2003/wordml/sp2">
        <wsp:rsidRoot wsp:val="00820EBE" />
        <wsp:rsid wsp:val="00145DA3" />
        <wsp:rsid wsp:val="004D0FB9" />
        <wsp:rsid wsp:val="006E3020" />
        <wsp:rsid wsp:val="007001E6" />
        <wsp:rsid wsp:val="00820EBE" />
        <wsp:rsid wsp:val="008A0377" />
        <wsp:rsid wsp:val="00930C65" />
        <wsp:rsid wsp:val="00C11C73" />
        <wsp:rsid wsp:val="00D63D66" />
        <wsp:rsid wsp:val="00D80B98" />
        <wsp:rsid wsp:val="00E55FED" />
        <wsp:rsid wsp:val="00F76EFB" />
        <wsp:rsid wsp:val="00F92C5E" />
      </wsp:rsids>
    </w:docPr>
  </xsl:template>
</xsl:stylesheet>