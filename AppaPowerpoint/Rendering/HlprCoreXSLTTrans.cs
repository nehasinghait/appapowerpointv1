﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;
using AppaDocx.Common.Helpers;
using System.Text.RegularExpressions;

namespace AppaDocx.Rendering
{
    public class HlprCoreXSLTTrans
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HlprCoreXSLTTrans));
        public static string ProcessDW_WDTransform(string strInputData, string strXSLTName)
        {
            string strOutputData = "";
            try
            {
                XmlDocument componentXml = new XmlDocument();
                componentXml.LoadXml(strInputData);

                // Enable XSLT debugging.
                XslCompiledTransform xslt = new XslCompiledTransform(enableDebug: true);
                string strStyleSheetFullPath = AppaDocx.Common.GlobalsData.dir + "\\" + strXSLTName;
                xslt.Load(strStyleSheetFullPath);
                string outputFile = @"OutputFile_WML2DML.xml";
                // Execute the XSLT transform.
                FileStream outputStream = new FileStream(AppaDocx.Common.GlobalsData.dir + "\\" + outputFile, FileMode.Create);
                xslt.Transform(new XmlNodeReader(componentXml), null, outputStream);
                outputStream.Close();
                strOutputData = File.ReadAllText(AppaDocx.Common.GlobalsData.dir + "\\" + outputFile);
                if(File.Exists(AppaDocx.Common.GlobalsData.dir + "\\" + outputFile))
                {
                    File.Delete(AppaDocx.Common.GlobalsData.dir + "\\" + outputFile);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.ToString());
                log.Error("DW to WD Transform", ex);
            }
            return (strOutputData);
        }

        public static string ProcessDML_CompMetaDataTransform(string strInputData, string strXSLTName)
        {
            string strOutputData = "";
            try
            {
                XmlDocument componentXml = new XmlDocument();
                componentXml.LoadXml(strInputData);

                // Enable XSLT debugging.
                XslCompiledTransform xslt = new XslCompiledTransform(enableDebug: true);
                string strStyleSheetFullPath = AppaDocx.Common.GlobalsData.dir + "\\" + strXSLTName;
                xslt.Load(strStyleSheetFullPath);
                string outputFile = @"OutputFile_DML2CompMetaData.xml";
                // Execute the XSLT transform.
                FileStream outputStream = new FileStream(AppaDocx.Common.GlobalsData.dir + "\\" + outputFile, FileMode.Create);
                xslt.Transform(new XmlNodeReader(componentXml), null, outputStream);
                outputStream.Close();
                strOutputData = File.ReadAllText(AppaDocx.Common.GlobalsData.dir + "\\" + outputFile);
                if(File.Exists(AppaDocx.Common.GlobalsData.dir + "\\" + outputFile))
                {
                    File.Delete(AppaDocx.Common.GlobalsData.dir + "\\" + outputFile);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.ToString());
                log.Error("DWL CompMetaDataTransform", ex);
            }
            return (strOutputData);
        }
    }
}
