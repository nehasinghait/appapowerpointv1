﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using System.Threading;
using AppaDocx.Modules;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;

namespace AppPowerpoint.StorageConnectors
{
    class GoogleDriveAccess
    {

        /// <summary>
        /// This method requests Authentcation from a user using Oauth2.  
        /// Credentials are stored in System.Environment.SpecialFolder.Personal
        /// Documentation https://developers.google.com/accounts/docs/OAuth2
        /// </summary>
        /// <param name="clientSecretJson">Path to the client secret json file from Google Developers console.</param>
        /// <param name="userName">Identifying string for the user who is being authentcated.</param>
        /// <returns>DriveService used to make requests against the Drive API</returns>
        public static DriveService AuthenticateOauth(string clientSecretJson, string userName)
        {
            try
            {
                if (string.IsNullOrEmpty(userName))
                    throw new ArgumentNullException("userName");
                if (string.IsNullOrEmpty(clientSecretJson))
                    throw new ArgumentNullException("clientSecretJson");
                if (!System.IO.File.Exists(clientSecretJson))
                    throw new Exception("clientSecretJson file does not exist.");

                // These are the scopes of permissions you need. It is best to request only what you need and not all of them
                string[] scopes = new string[] { DriveService.Scope.DriveReadonly };         	//View the files in your Google Drive                                                 
                UserCredential credential;
                using (var stream = new FileStream(clientSecretJson, FileMode.Open, FileAccess.Read))
                {
                    string credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                    credPath = Path.Combine(credPath, ".credentials/", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                    // Requesting Authentication or loading previously stored authentication for userName
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(GoogleClientSecrets.Load(stream).Secrets,
                                                                             scopes,
                                                                             userName,
                                                                             CancellationToken.None,
                                                                             new FileDataStore(credPath, true)).Result;


                }
                //////////////////////////////////

                //ClientSecrets secrets = new ClientSecrets()
                //{
                //    ClientId = "388037863719-kngk1j4a4p04kjuoe4akcg0l4g1o1r3h.apps.googleusercontent.com",
                //    ClientSecret = "Vda8Ly45mhUyZh-0IhSGkF39"
                //};

                //var token = new TokenResponse { RefreshToken = "1/fYlgS0dbA6eIJjqSX1ZAQFuBSsmS-L9PrrHF4i2ckBg" };
                //var credential = new UserCredential(new GoogleAuthorizationCodeFlow(
                //    new GoogleAuthorizationCodeFlow.Initializer
                //    {
                //        ClientSecrets = secrets
                //    }),
                //    "user",
                //    token);

                ///////////////////////////////////////
                // Create Drive API service.
                return new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Google Drive"
                });

            }
            catch (Exception ex)
            {
                Console.WriteLine("Create Oauth2 account DriveService failed" + ex.Message);
                throw new Exception("CreateServiceAccountDriveFailed", ex);
            }
        }

        //private static void DownloadFile(Google.Apis.Drive.v3.DriveService service, Google.Apis.Drive.v3.Data.File file, string saveTo)
        //{

        //    var request = service.Files.Get(file.Id);
        //    var stream = new System.IO.MemoryStream();

        //    // Add a handler which will be notified on progress changes.
        //    // It will notify on each chunk download and when the
        //    // download is completed or failed.
        //    request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
        //    {
        //        switch (progress.Status)
        //        {
        //            case Google.Apis.Download.DownloadStatus.Downloading:
        //                {
        //                    Console.WriteLine(progress.BytesDownloaded);
        //                    break;
        //                }
        //            case Google.Apis.Download.DownloadStatus.Completed:
        //                {
        //                    Console.WriteLine("Download complete.");
        //                    SaveStream(stream, saveTo);
        //                    break;
        //                }
        //            case Google.Apis.Download.DownloadStatus.Failed:
        //                {
        //                    Console.WriteLine("Download failed.");
        //                    break;
        //                }
        //        }
        //    };
        //    request.Download(stream);

        //}

        public static void DownloadFile(Google.Apis.Drive.v3.DriveService service, Google.Apis.Drive.v3.Data.File file, string saveTo)
        {

            var request = service.Files.Get(file.Id);
            var stream = new System.IO.MemoryStream();

            // Add a handler which will be notified on progress changes.
            // It will notify on each chunk download and when the
            // download is completed or failed.
            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                switch (progress.Status)
                {
                    case Google.Apis.Download.DownloadStatus.Downloading:
                        {
                            Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                    case Google.Apis.Download.DownloadStatus.Completed:
                        {
                            Console.WriteLine("Download complete.");
                            SaveStream(stream, saveTo);
                            break;
                        }
                    case Google.Apis.Download.DownloadStatus.Failed:
                        {
                            Console.WriteLine("Download failed.");
                            break;
                        }
                }
            };
            request.Download(stream);

        }

        private static void SaveStream(System.IO.MemoryStream stream, string saveTo)
        {
            using (System.IO.FileStream file = new System.IO.FileStream(saveTo, System.IO.FileMode.Create, System.IO.FileAccess.Write))
            {
                stream.WriteTo(file);
            }
        }

        public static Google.Apis.Drive.v3.Data.FileList ListFiles(DriveService service, FilesListOptionalParms optional = null)
        {
            try
            {
                // Initial validation.
                if (service == null)
                    throw new ArgumentNullException("service");

                // Building the initial request.
                var request = service.Files.List();
                // Applying optional parameters to the request.                
                request = (FilesResource.ListRequest)ApplyOptionalParms(request, optional);
                // Requesting data.
                return request.Execute();
            }
            catch (Exception ex)
            {
                throw new Exception("Request Files.List failed.", ex);
            }
        }

        public static object ApplyOptionalParms(object request, object optional)
        {
            if (optional == null)
                return request;

            System.Reflection.PropertyInfo[] optionalProperties = (optional.GetType()).GetProperties();

            foreach (System.Reflection.PropertyInfo property in optionalProperties)
            {
                // Copy value from optional parms to the request.  They should have the same names and datatypes.
                System.Reflection.PropertyInfo piShared = (request.GetType()).GetProperty(property.Name);
                if (property.GetValue(optional, null) != null) // TODO Test that we do not add values for items that are null
                    piShared.SetValue(request, property.GetValue(optional, null), null);
            }

            return request;
        }
    }

    public class FilesListOptionalParms
    {
        /// 

        /// The source of files to list.
        /// 
        public string Corpus { get; set; }
        /// 

        /// A comma-separated list of sort keys. Valid keys are 'createdTime', 'folder', 'modifiedByMeTime', 'modifiedTime', 'name', 'quotaBytesUsed', 'recency', 'sharedWithMeTime', 'starred', and 'viewedByMeTime'. Each key sorts ascending by default, but may be reversed with the 'desc' modifier. Example usage: ?orderBy=folder,modifiedTime desc,name. Please note that there is a current limitation for users with approximately one million files in which the requested sort order is ignored.
        /// 
        public string OrderBy { get; set; }
        /// 

        /// The maximum number of files to return per page.
        /// 
        public int? PageSize { get; set; }
        /// 

        /// The token for continuing a previous list request on the next page. This should be set to the value of 'nextPageToken' from the previous response.
        /// 
        public string PageToken { get; set; }
        /// 

        /// A query for filtering the file results. See the "Search for Files" guide for supported syntax.
        /// 
        public string Q { get; set; }
        /// 

        /// A comma-separated list of spaces to query within the corpus. Supported values are 'drive', 'appDataFolder' and 'photos'.
        /// 
        public string Spaces { get; set; }
        /// 

        /// Selector specifying a subset of fields to include in the response.
        /// 
        public string fields { get; set; }
        /// 

        /// Alternative to userIp.
        /// 
        public string quotaUser { get; set; }
        /// 

        /// IP address of the end user for whom the API call is being made.
        /// 
        public string userIp { get; set; }

        public string extension { get; set; }
    }
}
