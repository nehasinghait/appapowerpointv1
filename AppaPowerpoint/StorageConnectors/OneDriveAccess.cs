﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graph;
using AppaDocx.Modules;
using AppaDocx.UserInterfaces;

namespace AppaDocx.Modules
{
    class OneDriveAccess
    {
        private GraphServiceClient graphClient { get; set; }

        private enum ClientType
        {
            Consumer,
            Business
        }

        private ClientType clientType { get; set; }


        public async Task SignIn()
        {
            try
            {
               // this.graphClient = AuthenticationHelper.GetAuthenticatedClient(); //satish comment
            }
            catch (ServiceException exception)
            {

                //PresentServiceException(exception);

            }

            try
            {
                await LoadFolderFromPath();

            }
            catch (ServiceException exception)
            {
                //PresentServiceException(exception);
                this.graphClient = null;
            }
        }

        public async Task LoadFolderFromPath(string path = null)
        {
            if (null == this.graphClient) return;

            // Update the UI for loading something new
          
            LoadChildren(new DriveItem[0]);

            DriveItem folder;

          


            try
            {
             

                var expandValue = this.clientType == ClientType.Consumer
                    ? "thumbnails,children($expand=thumbnails)"
                    : "thumbnails,children";

                if (path == null)
                {
                    folder = await this.graphClient.Drive.Root.Request().Expand(expandValue).GetAsync();
                }
                else
                {
                    folder =
                        await
                            this.graphClient.Drive.Root.ItemWithPath("/" + path)
                                .Request()
                                .Expand(expandValue)
                                .GetAsync();
                }

                ProcessFolder(folder);
            }
            catch (Exception exception)
            {
                //PresentServiceException(exception);
            }

            //ShowWork(false);
        }

        public void LoadChildren(IList<DriveItem> items)
        {
            // Load the children
            foreach (var obj in items)
            {
                //AddItemToFolderContents(obj);
            }
        }

        private void CreateControlForChildObject(DriveItem item)
        {
            //ListItem tile = new ListItem(this.graphClient);
            //tile.SourceItem = item;
            //tile.Click += ChildObject_Click;
            //tile.DoubleClick += ChildObject_DoubleClick;
            //tile.Name = item.Id;
            //return tile;
        }

        public void ProcessFolder(DriveItem folder)
        {
            if (folder != null)
            {
                //this.CurrentFolder = folder;

                LoadProperties(folder);

                if (folder.Folder != null && folder.Children != null && folder.Children.CurrentPage != null)
                {
                    LoadChildren(folder.Children.CurrentPage);
                }
            }
        }

        public void LoadProperties(DriveItem item)
        {
            //this.SelectedItem = item;
            //AppaDocxUI. .SelectedItem = item;

         //ListBoxSearchesPnl.Children.Add(sli);
        }
    }
}
