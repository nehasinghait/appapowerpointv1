﻿using AppaDocx.Application.Helpers;
using AppaDocx.Common;
using AppaDocx.Common.Models;
using AppaDocx.Helpers;
using AppaDocx.UserInterfaces;
using AppaDocx.WebService;
using AppPowerpoint.StorageConnectors;
using Microsoft.Office.Interop.PowerPoint;
using OneDriveApiBrowser;
using Spire.Presentation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
//using Aspose.Slides;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using TheArtOfDev.HtmlRenderer.WPF;
using Presentation = Spire.Presentation.Presentation;
using Shape = Microsoft.Office.Interop.PowerPoint.Shape;
using TextRange = Microsoft.Office.Interop.PowerPoint.TextRange;

namespace AppaPowerpoint
{
    /// <summary>
    /// Interaction logic for AppaPowerpointUI.xaml
    /// </summary>
    public partial class AppaPowerpointUI : UserControl
    {

        #region declaration
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AppaPowerpointUI));
        public string Keywords { get; set; }
        private HlprDocumentManager objHlprDocumentManager = new HlprDocumentManager();
        private List<ComponentNames> component_list = new List<ComponentNames>();
        #endregion declaration
        public AppaPowerpointUI()
        {
            InitializeComponent();

            try
            {
                IsAuthorized();
                SetMenuIcons();

                #region code for add component type in the componant type combo
                List<ComponentType> ctypeList = new List<ComponentType>();

                ctypeList.Add(new ComponentType(true, "Text", "para", 0));
                ctypeList.Add(new ComponentType(false, "SubHead", "bridgehead", 1));
                ctypeList.Add(new ComponentType(false, "Note", "note", 2));
                ctypeList.Add(new ComponentType(false, "Footnote", "footnote", 3));
                ctypeList.Add(new ComponentType(false, "Table", "table", 4));

                cboComponentType.ItemsSource = null;
                cboComponentType.ItemsSource = ctypeList;
                cboComponentType.SelectedIndex = 0;

                ConstantsValues.IsComponentSearchSelectedFlag = false;
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("AppaDocxUI", ex);
            }

        }
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string userId = txtUsername.Text;
                string strUserPassword = txtPassword.Password;
                if (userId == "" && strUserPassword == "")
                {
                    MD_MsgBox.MaterialDesign_MessageBoxOK("Please fill user name and password !");
                    return;
                }

                if (userId == "")
                {
                    MD_MsgBox.MaterialDesign_MessageBoxOK("Please fill user name!");
                    //MessageBox.Show("Please fill user name!");
                    return;
                }

                if (strUserPassword == "")
                {
                    MD_MsgBox.MaterialDesign_MessageBoxOK("Please fill password!");
                    //MessageBox.Show("Please fill password!");
                    return;
                }

                Authenticate(userId, strUserPassword);

            }
            catch (Exception ex)
            {
                log.Error("Login Button", ex);
            }

        }

        private void miSignOut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                strData = "Are you sure you want to sign out?";
                bool? blnResult = MD_MsgBox.MaterialDesign_MessageBoxShow(strData, 2);

                if (blnResult == true)// MessageBox.Show(strData, " ", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    objHlprWebService.removeAccessToken();
                    objHlprWebService.removeUserId();
                    AppaDocx.Common.GlobalsData.IsLogin = false;
                    IsAuthorized();
                    txtUsername.Clear();
                    txtPassword.Clear();
                }
                else
                {
                    //do yes stuff
                }

            }
            catch (Exception ex)
            {
                strData = "Error: " + ex.Message;
                // bool? blnResult = MD_MsgBox.MaterialDesign_MessageBoxShow(strData, 1);
                MessageBox.Show(strData);
            }
        }

        //to set the menu image on the UI
        public void SetMenuIcons()
        {
            //string rootDirPath = AppaDocx.Common.GlobalsData.rootDir;
            //string signOutPath = rootDirPath + "//Images//loginbutton.png";
            //MenuItem item = new MenuItem();
            //string imagePath = signOutPath;
            //Image icon = new Image();
            //icon.Source = new BitmapImage(new Uri(imagePath, UriKind.Absolute));
            //miSignOut.Icon = icon;
        }

        #region supportive code

        #region declaration
        HlprWebService objHlprWebService = new HlprWebService();
        string strData = "";
        #endregion supportive code

        #region Methods

        private void Authenticate(string userId, string strUserPassword)
        {
            try
            {
                string strResponse = objHlprWebService.Authenticate(userId, strUserPassword);
                strResponse = objHlprWebService.Authenticate(userId, strUserPassword);
                strData = "DefaultText";
                #region Authentication with Config File
                if (strResponse != null && !strResponse.ToLower().Contains("error") && !strResponse.ToLower().Contains("Exception"))
                {
                    AppaDocx.Common.GlobalsData.IsLogin = false;
                    if (strResponse.ToLower().Contains("success"))
                    {
                       // bool? blnResult1 = MD_MsgBox.MaterialDesign_MessageBoxOK("login successfull");
                        //MessageBox.Show("login successfull.");
                        AppaDocx.Common.GlobalsData.IsLogin = true;
                        IsAuthorized();
                        txtUsername.Clear();
                        txtPassword.Clear();
                    }
                    else if (strResponse.ToLower().Contains("invalid password"))
                    {
                        strData = "Invalid username or password";
                    }
                    else if (strResponse.ToLower().Contains("invalid user"))
                    {
                        strData = "Invalid username or password";
                    }
                    else if (strResponse.ToLower().Contains("locked"))
                    {
                        strData = "User locked";
                    }
                    else if (strResponse.ToLower().Contains("expired"))
                    {
                        strData = "User / password expired";
                    }
                    else if (strResponse.ToLower().Contains("deactivated"))
                    {
                        strData = "User deactivated";
                    }
                    else if (strResponse.ToLower().Contains("force change"))
                    {
                        strData = "Force Change Password";
                    }
                    else if (strResponse.Equals(""))
                    {
                        strData = "Kindly Check Web Service. It seems that Web Service is down and unable to reach. Inconvenience Regretted ";
                    }
                }
                else
                {
                    strData = "Please try again";
                }
                if (strData != "DefaultText")
                {
                    bool? blnResult = MD_MsgBox.MaterialDesign_MessageBoxOK(strData);
                    //MessageBox.Show(strData);
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("Login Button", ex);
            }
        }
        #endregion

        private void IsAuthorized()
        {
            try
            {
                if (AppaDocx.Common.GlobalsData.IsLogin == true)    //When Login Sucess
                {
                    //MnuCurrentUserNameTextBlock.Text = txtUsername.Text;
                    pnlSearchComponent.Visibility = Visibility.Visible;
                    pnlLogin.Visibility = Visibility.Collapsed;
 
                    AppaDocx.Common.ConstantsValues.CurrentWebServiceUserName = objHlprWebService.getUserName();
                    miUsername.Header = AppaDocx.Common.ConstantsValues.CurrentWebServiceUserName;

                    #region Commented code
                    //#region Stack Login Switching
                    //StackPanelHeader.Visibility = Visibility.Visible;
                    //StackPanelSearch.Visibility = Visibility.Visible;
                    //StackPanelLogin.Visibility = Visibility.Collapsed;
                    //StackPanelComponentSearch.Visibility = Visibility.Collapsed;
                    //StackPanelRevision.Visibility = Visibility.Collapsed;




                    //colorzoneDocument.Mode = MaterialDesignThemes.Wpf.ColorZoneMode.Light;
                    //colorzoneComponent.Mode = MaterialDesignThemes.Wpf.ColorZoneMode.PrimaryMid;
                    //colorzoneRevision.Mode = MaterialDesignThemes.Wpf.ColorZoneMode.PrimaryMid;

                    //txtcolorzoneComponent.Foreground = System.Windows.Media.Brushes.White;
                    //txtcolorzoneRevision.Foreground = System.Windows.Media.Brushes.White;
                    //txtcolorzoneDocument.ClearValue(System.Windows.Controls.TextBlock.ForegroundProperty);
                    //#endregion

                    //AppaDocx.Common.ConstantsValues.CurrentWebServiceUserName = objHlprWebService.getUserName();
                    //MnuCurrentUserNameTextBlock.Text = AppaDocx.Common.ConstantsValues.CurrentWebServiceUserName;

                    //Dictionary<string, string> bookTypeWebServiceInfossl = new Dictionary<string, string>();
                    //bookTypeWebServiceInfossl.Add("service.name", "booktypes");
                    //string strResponse = objHlprWebService.callWebService(bookTypeWebServiceInfossl);


                    //#region Load Document Type and Status
                    //Dictionary<string, string> bookTypeWebServiceInfo = new Dictionary<string, string>();
                    //bookTypeWebServiceInfo.Add("service.name", "booktypes");
                    //strResponse = objHlprWebService.callWebService(bookTypeWebServiceInfo);
                    //bool blnResponse = ProcessResponse(strResponse);
                    //if (blnResponse == true)
                    //    return;
                    //List<DocumentTypeModel> lstDocumentTypeModel = objHlprWebService.loadDocumentTypeModelData(strResponse);

                    //if (strResponse != "")
                    //{
                    //    cboDocType.Items.Clear();
                    //    foreach (DocumentTypeModel objDocumentTypeModel in lstDocumentTypeModel)
                    //    {
                    //        cboDocType.SelectedValuePath = "Key";
                    //        cboDocType.DisplayMemberPath = "Value";
                    //        cboDocType.Items.Add(new KeyValuePair<string, string>(objDocumentTypeModel.BookType,
                    //        objDocumentTypeModel.BookTypeDisplay));
                    //    }
                    //}
                    //if (cboDocType.Items.Count > 0)
                    //    cboDocType.SelectedIndex = 1;
                    ////strURL = ConstantsValues.CurrentWebServiceHomeURL + ConstantsValues.ComponentInfoServices + "/getAllBookStatus/" + ConstantsValues.CurrentWebServiceUserName + "/" + ConstantsValues.CurrentClientID;
                    //Dictionary<string, string> bookStatusWebServiceInfo = new Dictionary<string, string>();
                    //bookStatusWebServiceInfo.Add("service.name", "bookstatus");
                    //strResponse = objHlprWebService.callWebService(bookStatusWebServiceInfo);
                    //blnResponse = ProcessResponse(strResponse);
                    //if (blnResponse == true)
                    //    return;

                    //if (strResponse != "")
                    //{
                    //    cboDocStatus.Items.Clear();
                    //    List<DocumentStatusModel> lstDocumentStatusModel = objHlprWebService.loadDocumentStatusModelData(strResponse);
                    //    foreach (DocumentStatusModel objDocumentStatusModel in lstDocumentStatusModel)
                    //    {
                    //        cboDocStatus.SelectedValuePath = "Key";
                    //        cboDocStatus.DisplayMemberPath = "Value";
                    //        cboDocStatus.Items.Add(new KeyValuePair<string, string>(objDocumentStatusModel.StatusDescription, objDocumentStatusModel.StatusDescription));
                    //    }
                    //}
                    //if (cboDocStatus.Items.Count > 0)
                    //    cboDocStatus.SelectedIndex = 1;
                    //#endregion 
                    #endregion
                }
                if (AppaDocx.Common.GlobalsData.IsLogin == false)   //After Logout
                {
                    pnlLogin.Visibility = Visibility.Visible;
                    pnlSearchComponent.Visibility = Visibility.Collapsed;
                    txtUsername.Clear();
                    txtPassword.Clear();
                    txtUsername.Focus();

                    //#region Stack Logout Switching
                    //StackPanelLogin.Visibility = Visibility.Visible;
                    //StackPanelHeader.Visibility = Visibility.Collapsed;
                    //StackPanelComponentSearch.Visibility = Visibility.Collapsed;
                    //StackPanelRevision.Visibility = Visibility.Collapsed;
                    //StackPanelSearch.Visibility = Visibility.Collapsed;
                    //#endregion
                    //txtUsername.Clear();
                    //txtPassword.Clear();
                    //txtUsername.Focus();
                }
            }
            catch (Exception ex)
            {
                log.Error("AppaDocxMD", ex);
            }
        }

        #endregion Methods


        #region Component Search code

        private void btnComponentSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //ClearAll(Common.Enums.ClearAllType.ButtonClearAll);
                Keywords = txtComponentSearch.Text;

                lblComponentResultCount.Content = "Showing " + ComponentListBoxSearchesPnl.Children.Count.ToString() + " Results";

                if (Keywords != "Enter keyword or component name you would like to search for..." && cboComponentType.SelectedValue != null)
                {
                    ComponentListBoxSearchesPnl.Children.Clear();
                    ComponentListBoxSearchesPnl.RowDefinitions.Clear();

                    string strComponentType = "";
                    foreach (var item in cboComponentType.ItemsSource)
                    {
                        if (((ComponentType)item).IsSelected == true)
                        {
                            strComponentType += "&componentType=" + ((ComponentType)item).ComponentTypeUse;
                        }
                    }
                    component_list = objHlprDocumentManager.DocumentSearch_GetComponentData(Keywords, strComponentType);
                    AddComponentListtoListBox(component_list);
                }
                else
                {
                    string strMsgData = "Please Select proper option";
                    // MD_MsgBox.MaterialDesign_MessageBoxShow(strMsgData, 1);
                    MessageBox.Show(strMsgData);
                }
            }
            catch (Exception ex)
            {
                string strMethodName = new StackTrace(ex).GetFrame(0).GetMethod().Name;
                log.Error(strMethodName, ex);
            }
        }

        private void Selection_Changed(object sender, RoutedEventArgs e)
        {
            if (sender.GetType().ToString() == "System.Windows.Controls.CheckBox")
            {
                CheckBox chkselectedItem = (System.Windows.Controls.CheckBox)sender;
                if (((ComponentType)chkselectedItem.DataContext).IsSelected == true)
                    cboComponentType.SelectedIndex = ((ComponentType)chkselectedItem.DataContext).Index;
            }

        }

        public void AddComponentListtoListBox(List<ComponentNames> lstcomponent_list, sbyte UserInputSelected = 0)
        {
            try
            {
                ComponentListBoxSearchesPnl.Children.Clear();
                ComponentListBoxSearchesPnl.RowDefinitions.Clear();

                if (lstcomponent_list != null)
                {
                    //SearchListItem listItemtop = new SearchListItem();
                    switch (UserInputSelected)
                    {
                        case 0:
                            lblComponentResultCount.Content = "Showing  " + lstcomponent_list.Count.ToString() + " Search Results";
                            break;
                        case 1:
                            lblComponentResultCount.Content = "Showing  " + lstcomponent_list.Count.ToString() + " Favorite Components Results";
                            break;
                        case 2:
                            lblComponentResultCount.Content = "Showing  " + lstcomponent_list.Count.ToString() + " Advancesearch Results";
                            break;
                        case 3:
                            lblComponentResultCount.Content = "Showing  " + lstcomponent_list.Count.ToString() + " Cart Results";
                            break;

                    }


                    foreach (var item in lstcomponent_list)
                    {


                        string text = "";
                        if (item.FELEMENT_DETAIL != null)
                            text = item.FELEMENT_DETAIL.ToString().Trim();
                        AppaDocx.Application.Helpers.HlprDocumentManager objhlprDocumentManager = new HlprDocumentManager();
                        text = objhlprDocumentManager.StripHtml(text);


                        //listItem.Tag = item;

                        string tooltip = "";
                        if (text.Length > 700)
                            tooltip = text.Substring(0, 700);
                        else
                            tooltip = text;

                        RowDefinition mainrow = new RowDefinition();
                        ComponentListBoxSearchesPnl.RowDefinitions.Add(mainrow);

                        Grid resultGrid = new Grid();
                        RowDefinition row1 = new RowDefinition();
                        row1.MinHeight = 20;

                        resultGrid.RowDefinitions.Add(row1);

                        ColumnDefinition col1 = new ColumnDefinition();
                       // col1.Width = new GridLength(25, GridUnitType.Star); //25
                        col1.Width = new GridLength(100, GridUnitType.Star); //25
                        ColumnDefinition col2 = new ColumnDefinition();
                        col2.Width = new GridLength(260); //260 300
                        ColumnDefinition col3 = new ColumnDefinition();

                        RowDefinition row = new RowDefinition();
                        row.MinHeight = 35;
                        resultGrid.RowDefinitions.Add(row);

                        resultGrid.ColumnDefinitions.Add(col1);
                        resultGrid.ColumnDefinitions.Add(col2);
                        resultGrid.ColumnDefinitions.Add(col3);

                        int newRow = ComponentListBoxSearchesPnl.RowDefinitions.Count;



                        TextBlock txtblock = new TextBlock();
                        //satish comment
                        //FontFamily fontFamily = FindResource("MaterialDesignFont") as FontFamily;
                        GroupBox b = new GroupBox();

                        if (text.Length > 140)
                            txtblock.Text = text.Substring(0, 140);
                        else
                            txtblock.Text = text;

                        txtblock.TextWrapping = TextWrapping.Wrap;
                        txtblock.FontSize = 11;
                        txtblock.Margin = new Thickness(7, 2, 0, 2);
                        txtblock.Width = 260;//260;// 370;
                        txtblock.HorizontalAlignment = HorizontalAlignment.Left;
                        //satish comment
                        //txtblock.FontFamily = fontFamily;
                        //Image img = new Image();


                        //switch (item.FELEMENT_ID)
                        //{
                        //    case "para":
                        //        img.Source = (ImageSource)FindResource("Imagepara");
                        //        break;
                        //    case "bridgehead":
                        //        img.Source = (ImageSource)FindResource("Imagesubhead");
                        //        break;
                        //    case "footnote":
                        //        img.Source = (ImageSource)FindResource("Imagefootnote");
                        //        break;
                        //    case "table":
                        //        img.Source = (ImageSource)FindResource("Imagetable");
                        //        break;
                        //    case "note":
                        //        img.Source = (ImageSource)FindResource("Imagenote");
                        //        break;
                        //}

                        //img.HorizontalAlignment = HorizontalAlignment.Left;
                        //img.Margin = new Thickness(0, 7, 0, 0);

                        //resultGrid.Children.Add(img);


                        //Grid.SetRow(img, 0);
                        //Grid.SetColumn(img, 0);

                        //Button btnComponentName = new Button();

                        TextBlock btnComponentName = new TextBlock();

                        btnComponentName.Text = AppaDocx.Common.Helpers.HlprStringHandling.GetNumberOfWords(item.FQUALDATA_DESC, 7);/* new TextBlock() { Text = AppaDocx.Common.Helpers.HlprStringHandling.GetNumberOfWords(item.FQUALDATA_DESC, 7), TextWrapping = TextWrapping.Wrap ,HorizontalAlignment = HorizontalAlignment.Left,TextAlignment = TextAlignment.Left}; */

                        ToolTip t = new System.Windows.Controls.ToolTip();
                        t.Width = 250;
                        t.Content = new TextBlock() { Text = tooltip, MaxWidth = 250, TextWrapping = TextWrapping.Wrap };
                        btnComponentName.ToolTip = t;
                        //satish comment
                        //Brush btnCompStyle = FindResource("PrimaryHueMidBrush") as Brush;
                        //btnComponentName.Foreground = btnCompStyle;
                        //btnComponentName.FontFamily = fontFamily;
                        btnComponentName.FontSize = 10;
                        btnComponentName.FontWeight = FontWeights.Bold;
                        //btnComponentName.MouseDoubleClick += lstComponent_DoubleClick;
                        //btnComponentName.MouseDown += _ListCSElementMouseButtonDown;
                        btnComponentName.Tag = item;
                        btnComponentName.HorizontalAlignment = HorizontalAlignment.Left;
                        btnComponentName.Margin = new Thickness(-5, -23, 0, 0);
                        //btnComponentName.HorizontalContentAlignment = HorizontalAlignment.Left;
                        btnComponentName.Foreground = System.Windows.Media.Brushes.White;
                        resultGrid.Children.Add(btnComponentName);
                        resultGrid.HorizontalAlignment = HorizontalAlignment.Left;
                        Grid.SetRow(btnComponentName, 0);
                        Grid.SetColumn(btnComponentName, 0);//1

                        resultGrid.Children.Add(txtblock);

                        Grid.SetRow(txtblock, 1);
                        Grid.SetColumn(txtblock, 0);
                        Grid.SetColumnSpan(txtblock, 3);

                        resultGrid.Margin = new Thickness(0, 0, 0, 10);


                        //satish added
                        //b.Width = 280;
                        //ends here
                        b.Content = resultGrid;
                        b.SetValue(Grid.RowProperty, newRow - 1);
                        b.SetValue(Grid.ColumnProperty, 0);
                        b.Margin = new Thickness(0, 0, 0, 10);
                        //satish comment this
                        b.MouseDoubleClick += lstComponent_DoubleClick;
                        b.HorizontalContentAlignment = HorizontalAlignment.Left;
                        //satish comment this
                        // b.MouseDown += _ListCSElementMouseButtonDown;
                        b.HorizontalContentAlignment = HorizontalAlignment.Left;

                        //satish comment
                        // Style btnStyle = FindResource("GroupBoxStyle1") as Style;
                        //  b.Style = btnStyle;


                        ComponentListBoxSearchesPnl.Children.Add(b);
                    }
                    if (lstcomponent_list.Count == 0)
                    {
                        MD_MsgBox.MaterialDesign_MessageBoxOK("No results found.");
                        //MessageBox.Show("No results found");
                        //MD_MsgBox.MaterialDesign_MessageBoxShow(strMsgData, 1);
                        //MessageBox.Show(strMsgData);
                    }
                    log.Info("Records Loaded to List" + lstcomponent_list.Count().ToString() + " Records Addded in ListBox");
                }
                else if (lstcomponent_list == null && objHlprDocumentManager.response == "loginrequired")
                {
                    ProcessResponse(objHlprDocumentManager.response);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Add ComponentList to ListBox", ex);
            }
        }

        private void lstComponent_DoubleClick(object sender, RoutedEventArgs e)
        {

            try
            {
                // MessageBox.Show("Click event occured.");

                //NOT WORKING
                //AddCustomXmlPartToPresentation(Globals.ThisAddIn.Application.ActivePresentation);



                //NOT WORKING Aspose DLL
                #region NOT WORKING Aspose DLL
                //Microsoft.Office.Interop.PowerPoint.Application ppApp1 = Globals.ThisAddIn.Application;
                //Microsoft.Office.Interop.PowerPoint.SlideRange ppSR1 = ppApp1.ActiveWindow.Selection
                //    .SlideRange;

                //Aspose.Slides.Presentation ppt = new Aspose.Slides.Presentation();
                //IAutoShape shape = ppt.Slides[0].Shapes.AddAutoShape(ShapeType.Rectangle, 50, 50, 50, 100);// (IAutoShape)ppSR.Shapes;//  AppendShape( IShape q, new RectangleF(50, 50, 400, 100));
                //shape.TextFrame.Paragraphs.Clear();
                //string htmlText = "<html><body><p>THIS IS A HTML OUTPUT</p><p></p></body></html>";
                //shape.TextFrame.Paragraphs.AddFromHtml(htmlText);
                #endregion

                //WORKING CODE REUSE SLIDE PANE ON CLICK
                #region WORKING CODE REUSE SLIDE PANE ON CLICK
                //Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
                //ppApp.CommandBars.ExecuteMso("SlidesReuseSlides"); 
                #endregion

                //R&D for insert HTMlL. mot working
                #region NOT WORKING CODE FOR INSERT HTML
                //Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
                //Microsoft.Office.Interop.PowerPoint.SlideRange ppSR = ppApp.ActiveWindow.Selection.SlideRange;
                //Microsoft.Office.Interop.PowerPoint.Shape ppShap = ppSR.Shapes
                //    .AddLabel(Microsoft.Office.Core.MsoTextOrientation
                //    .msoTextOrientationHorizontal, 0, 0, 200, 25);
                //ppShap.TextEffect.Text = @" <html> < body > < h2 style ='color: #2e6c80;' > Input text for slide </ h2 ></ body > </ html > "; 
                #endregion


                // InsertNewSlide("", 0, "");


                //activeDocument.TrackRevisions = false;
                //activeDocument.ShowRevisions = false;

                ConstantsValues.IsComponentSearchSelectedFlag = true;

                GroupBox grpData = (GroupBox)sender;
                var grid = (Grid)grpData.Content;
                var componentdata = (TextBlock)grid.Children[0];
                ComponentNames currSelectedComponent = (ComponentNames)componentdata.Tag;

                string componentData = currSelectedComponent.FELEMENT_DETAIL;
                string componentId = currSelectedComponent.FELEMENTINSTANCEID;
                string componentType = currSelectedComponent.FELEMENT_ID;
                string componentName = currSelectedComponent.FQUALDATA_DESC;


                if (componentType == "table")
                {
                    string queryParams = "/" + componentId;
                    Dictionary<string, string> tableeComponentWebServiceInfo = new Dictionary<string, string>();

                    tableeComponentWebServiceInfo.Add("service.name", "getComponentDetailById");
                    tableeComponentWebServiceInfo.Add("service.queryparams", queryParams);
                    string strResponse = objHlprWebService.callWebService(tableeComponentWebServiceInfo);
                    bool blnResponse = ProcessResponse(strResponse);
                    if (blnResponse == true)
                        return;

                    string element_detail = "";
                    var xElems = XDocument.Parse(strResponse);

                    if (xElems != null)
                    {
                        XElement xRow = xElems.Root;
                        if (xRow != null)
                        {
                            element_detail = xRow.Element("FELEMENT_DETAIL").Value;
                        }
                    }
                    //SATISH ADDED
                    if (string.IsNullOrEmpty(element_detail))
                    {
                        MD_MsgBox.MaterialDesign_MessageBoxOK("selected component have no data.");
                        return;
                    }
                    //
                    componentData = element_detail;
                    //satish added below 2 line
                    //InsertSpireTableOnNewSlide(componentData);
                    //InsertSpireTableOnSameSlide(componentData);

                    componentData = componentData.Replace("<para/>", "");
                    componentData = "<root eid=\"" + componentId + "\" taletype=\"tt\">" + componentData + "</root>";
                    string DecodedDocbookXML = objHlprDocumentManager.RenderPresentationMLFromDMLForComponent(componentData);// RenderWMLFromDMLForComponent(componentData);
                    InsertSpireHTMLTableOnSameSlide(DecodedDocbookXML);
                    return;
                }

                if (componentData != null && !componentData.Equals(""))
                {
                    componentData = componentData.Replace("<para/>", "");
                    componentData = "<root eid=\"" + componentId + "\" taletype=\"tt\">" + componentData + "</root>";

                    // InsertTextResultOnNewSlide(componentData);
                    //InsertTextOnCurrentSlide(DecodedDocbookXML);

                    #region 02.Insert component XML to document
                    string DecodedDocbookXML = objHlprDocumentManager.RenderPresentationMLFromDMLForComponent(componentData);// RenderWMLFromDMLForComponent(componentData);
                                                                                                                             //string DecodedDocbookXMLTest = RenderWMLFromDMLForComponent(componentData);

                    //  InsertHTMLResultOnNewSlide(DecodedDocbookXML);
                    InsertHTMLResultOnSameSlide(DecodedDocbookXML);




                    //WORKING CODE FOR INSERT A NEW SLIDE AND INSERT A TEXT IN TO IT


                    #region old commented
                    //string strcomponentIds = "componentids=" + componentId;
                    //Dictionary<string, string> getComponentWebServiceInfo = new Dictionary<string, string>();
                    //getComponentWebServiceInfo.Add("service.name", "getAllComponentInfo");
                    //getComponentWebServiceInfo.Add("service.method", "POST");
                    //getComponentWebServiceInfo.Add("service.data", strcomponentIds);
                    //getComponentWebServiceInfo.Add("service.content-type", "application/xml");
                    //string strResult = objHlprWebService.callWebService(getComponentWebServiceInfo); 
                    #endregion

                    #region satish comment
                    //List<KeyValuePair<string, string>> lstFindText = objHlprDocumentManager.GetKeyValuePairsForContentControl(Common.Enums.ContentControlType.VariableTypeContentControl, activeDocument);
                    //foreach (KeyValuePair<string, string> textToFind in lstFindText)
                    //{
                    //    Microsoft.Office.Interop.Word.Range rng = Globals.ThisAddIn.Application.ActiveDocument.Content;
                    //    if (rng.Find.Execute(textToFind.Key))
                    //    {
                    //        objHlprDocumentManager.InsertContentControl(rng, textToFind.Key, textToFind.Value, AppaDocx.Common.Enums.ContentControlType.VariableTypeContentControl, activeDocument);
                    //    }
                    //    rng.Find.ClearFormatting();
                    //}

                    //List<KeyValuePair<string, string>> lstFindFootNoteText = objHlprDocumentManager.GetKeyValuePairsForContentControl(Common.Enums.ContentControlType.FootnoteTypeContentControl, activeDocument);
                    //foreach (KeyValuePair<string, string> textToFind in lstFindFootNoteText)
                    //{
                    //    Microsoft.Office.Interop.Word.Range rng = Globals.ThisAddIn.Application.ActiveDocument.Content;
                    //    if (rng.Find.Execute(textToFind.Key))
                    //    {
                    //        objHlprDocumentManager.InsertContentControl(rng, textToFind.Key, textToFind.Value, AppaDocx.Common.Enums.ContentControlType.FootnoteTypeContentControl, activeDocument);
                    //    }
                    //    rng.Find.ClearFormatting();
                    //}

                    #endregion
                }
                //activeDocument.TrackRevisions = true;
                //activeDocument.ShowRevisions = true; 
                #endregion

            }
            catch (Exception ex)
            {
                log.Error("Component Search", ex);
            }
        }

        private void InsertHTMLResultOnNewSlide(string responseString)
        {
            #region WORKING CODE FOR INSERT A NEW SLIDE AND INSERT A TEXT IN TO IT
            Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
            Microsoft.Office.Interop.PowerPoint.Presentation objPres = ppApp.ActivePresentation;
            //MessageBox.Show("Slide height:" + objPres.PageSetup.SlideHeight.ToString() + ", width:" + objPres.PageSetup.SlideWidth + ", slide size:" + objPres.PageSetup.SlideSize);
            //Microsoft.Office.Interop.PowerPoint.Slides objSlides = objPres.Slides;
            //  Microsoft.Office.Interop.PowerPoint.Slide objSlide = objSlides. Add(1, Microsoft.Office.Interop.PowerPoint.PpSlideLayout.ppLayoutCustom);

            #region ass OLEDB object NOT WORKING 
            //code for add oledb object
            //string path = GlobalsData.rootDir + "\\testHTML.html";
            //objSlide.Shapes.AddOLEObject(0,0,10,10,"", path, MsoTriState.msoFalse,"",0,"");
            //objSlide.Shapes.AddPicture2(GlobalsData.rootDir + "\\zzz.png", MsoTriState.msoFalse, MsoTriState.msoFalse,1,1); 
            #endregion

            #region ADD IMAGE TO SHAPE. WORKING
            //ADD THE IMAGE TO THE SHAPE(WORKING)
            //  objSlide.Shapes.AddPicture(GlobalsData.rootDir + "\\zzz.png", MsoTriState.msoFalse, MsoTriState.msoTrue, 0, 0, 300, 500); 
            #endregion



            // TextRange objTextRng = objSlide.Shapes[1].TextFrame.TextRange;
            // AppaDocx.Application.Helpers.HlprDocumentManager objhlprDocumentManager = new HlprDocumentManager();
            // objTextRng.Text = objhlprDocumentManager.StripHtml(responseString);  // "SLIDE INSERT AND INSERT A TEXT";
            // objTextRng.Font.Name = "Calibri";
            // objTextRng.Font.Size = 20;

            // objSlide.Shapes[1].TextFrame.AutoSize = PpAutoSize.ppAutoSizeShapeToFitText;

            #region EXTEA CODE
            //objSlide.Shapes[1].TextFrame.AutoSize = PpAutoSize.ppAutoSizeShapeToFitText;
            //TextRange objTextRng = objSlide.Shapes[1].TextFrame.TextRange;
            //AppaDocx.Application.Helpers.HlprDocumentManager objhlprDocumentManager = new HlprDocumentManager();
            //objTextRng.Text = objhlprDocumentManager.StripHtml(DecodedDocbookXML);  // "SLIDE INSERT AND INSERT A TEXT";
            //objTextRng.Font.Name = "Calibri";
            //objTextRng.Font.Size = 20;
            //ADDING SHAPE
            //Shape oshape =    Globals.ThisAddIn.Application.ActiveWindow.View.Slide.Shapes.AddShape(
            //Microsoft.Office.Core.MsoAutoShapeType.msoShapeRectangle, 0, 0, 10, 20);
            //oshape.Tags.Add("MyTagName", "MyTagValue"); 

            #endregion


            //SPIRE DLL. CODE FOR ADDING HTML OUTPUT TO THE SLIDE SHAPE
            Presentation ppt = new Presentation();

            IAutoShape shape = ppt.Slides[0].Shapes.AppendShape(ShapeType.Rectangle, new System.Drawing.RectangleF(50, 50, 400, 100));
            shape.TextFrame.Paragraphs.Clear();
            string htmlText = responseString;//"<html><body><p><B style='color:red'>This HTML output can be replace </B></p></body></html>";
            shape.TextFrame.Paragraphs.AddFromHtml(htmlText); string fileName = "testfile_.pptx";
            //ppt.SaveToFile(GlobalsData.rootDir + "\\testGenratedPPT.pptx", FileFormat.Pptx2013);
            ppt.SaveToFile(GlobalsData.rootDir + "\\" + fileName, FileFormat.Pptx2013);
            // copy the html output from the temp ppt and place it on active presentaion

            #region extra code
            //Microsoft.Office.Interop.PowerPoint.Presentation TempPresentation = Globals.ThisAddIn.Application.Presentations.Open(GlobalsData.rootDir + "\\" + fileName, MsoTriState.msoFalse, MsoTriState.msoFalse); //new Microsoft.Office.Interop.PowerPoint.Presentation();



            // Microsoft.Office.Interop.PowerPoint.Presentation tempPPt = new Microsoft.Office.Interop.PowerPoint.Presentation();
            ////   tempPPt.open("");
            //Microsoft.Office.Interop.PowerPoint._Application tempAPP = new Microsoft.Office.Interop.PowerPoint.Application(); ;
            //Microsoft.Office.Interop.PowerPoint.Presentation TempPresentation = tempAPP.Presentations.Open(GlobalsData.rootDir + "\\" + fileName, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoTrue);

            //var app = new Microsoft.Office.Interop.PowerPoint.Application();
            //var pres = app.Presentations;
            //var file = pres.Open(GlobalsData.rootDir + "\\" + fileName, MsoTriState.msoTrue, MsoTriState.msoFalse, MsoTriState.msoTrue);
            //var TempPresentation = file;
            //var tempSlide = TempPresentation.Slides[1];
            //[1];
            //foreach (Microsoft.Office.Interop.PowerPoint.Shape origShape in tempSlide.Shapes)
            //{
            // origShape.Copy();
            //objPres.Slides.Paste(-1);
            //} 
            #endregion


            // Microsoft.Office.Interop.PowerPoint.SlideRange slideRange = ppApp.ActiveWindow.Selection.SlideRange;


            var destSlide = objPres.Slides;
            destSlide.InsertFromFile(GlobalsData.rootDir + "\\" + fileName, 1, 1, -1);
            //Microsoft.Office.Interop.PowerPoint.Slide slide = objPres.Slides[0];
            //Microsoft.Office.Interop.PowerPoint.Shapes shapes = slide.Shapes;








            // pres.Application.Quit();
            // app.Quit();





            #endregion
        }

        private void InsertHTMLResultOnSameSlide(string responseString)
        {
            // ProcessWordDocAndCreatePPT();//extra function call for expose


            //responseString = "<html><body><p style='font-family:Arial,Helvetica,sans-serif;font-size:15px;background-color:azure;border:solid 1px lightgray;padding:10px;width:100%'><b style='font-size:30px'>T</b>his is the paragraph text.This is the paragraph text.This is the paragraph text.This is the paragraph text.This is the paragraph text.This is the paragraph text.</p></body></html>";

            Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
            Microsoft.Office.Interop.PowerPoint.Presentation objPres = ppApp.ActivePresentation;
            

            //SPIRE DLL. CODE FOR ADDING HTML OUTPUT TO THE SLIDE SHAPE
            Presentation ppt = new Presentation();

            IAutoShape shape = ppt.Slides[0].Shapes.AppendShape(ShapeType.Rectangle, new System.Drawing.RectangleF(0, 0, 958, 100));

            shape.TextFrame.Paragraphs.Clear();
            string htmlText = responseString;//"<html><body><p><B style='color:red'>This HTML output can be replace </B></p></body></html>";
            shape.TextFrame.Paragraphs.AddFromHtml(htmlText);
            shape.TextFrame.AutofitType = TextAutofitType.Shape;

            shape.ShapeStyle.LineColor.Color = System.Drawing.Color.Transparent;
            shape.ShapeStyle.FillColor.Color = System.Drawing.Color.Transparent;
            shape.ShapeStyle.FontColor.Color = System.Drawing.Color.Black;






            //directly insert HTML in to the shape
            //Spire.Presentation.Collections.ShapeList shapes = ppt.Slides[0].Shapes;
            //shapes.AddFromHtml(responseString);


            string fileName = "testfile_.pptx";
            ppt.SaveToFile(GlobalsData.rootDir + "\\" + fileName, FileFormat.Pptx2013);
            // copy the html output from the temp ppt and place it on active presentaion slide
            var destSlide = objPres.Slides;
            destSlide.InsertFromFile(GlobalsData.rootDir + "\\" + fileName, 1, 1, -1);
            objPres.Slides[2].Shapes[1].Copy();
            Microsoft.Office.Interop.PowerPoint.SlideRange ppSR2 = ppApp.ActiveWindow.Selection.SlideRange;
            float CalculatedTopValue = getPositionValueForTop(objPres.Slides[ppSR2.SlideIndex].Shapes);//call new method
                                                                                                       //CALL METHOD TO CHECK FOR OVERFLOW THE CONTENT ON THE SLIDE
            bool IsOverflow = IsOverflowSlideByCurrentShape(objPres.PageSetup.SlideHeight, objPres.Slides[2].Shapes[1].Height, CalculatedTopValue);
            //END HERE


            objPres.Slides[ppSR2.SlideIndex].Shapes.Paste().Top = CalculatedTopValue + 5;//+5 for giving some space;
            if (objPres.Slides[ppSR2.SlideIndex].Shapes.Count > 0)
            {
                objPres.Slides[ppSR2.SlideIndex].Shapes[objPres.Slides[ppSR2.SlideIndex].Shapes.Count].Left = 0;

                Shape myshape = objPres.Slides[ppSR2.SlideIndex].Shapes[objPres.Slides[ppSR2.SlideIndex].Shapes.Count];
                #region commented code for styling
                //myshape.BackgroundStyle = MsoBackgroundStyleIndex.msoBackgroundStylePreset1;
                //myshape.BackgroundStyle = MsoBackgroundStyleIndex.msoBackgroundStylePreset1;// .Fill.ForeColor.RGB = System.Drawing.Color.Gray.ToArgb();
                //myshape.Fill.BackColor.RGB = System.Drawing.Color.White.ToArgb();
                //myshape.TextFrame.TextRange.Font.Color.RGB = System.Drawing.Color.Black.ToArgb(); 
                #endregion




                objPres.Slides[2].Delete();
                //MessageBox.Show("Active slide index:" + Convert.ToString(ppSR2.fiSlideIndex));

                #region commented code
                //app.CommandBars.ExecuteMso("PasteSourceFormatting");
                //int a = ppApp.ActiveWindow.Selection.SlideRange.sectionIndex;
                //if (Globals.ThisAddIn.Application.ActiveWindow.ViewType == PpViewType.ppViewNormal)
                //    Globals.ThisAddIn.Application.ActiveWindow.Panes[2].Activate();

                //Panes panes = Globals.ThisAddIn.Application.ActiveWindow.Panes;
                ////MessageBox.Show("Active slide index:" + Convert.ToString(a.SlideIndex));

                //foreach (Pane pane in panes)
                //{
                //    if (pane.ViewType ==   PpViewType.ppViewSlide)
                //    {
                //        pane.Activate();
                //        break;
                //    }
                //}
                //var  currentSlideIndex = Globals.ThisAddIn.Application.ActiveWindow.View.Slide;
                //  MessageBox.Show("Active slide index:" + Convert.ToString(currentSlideIndex.SlideIndex)); 
                #endregion
            }



        }


        public void DemoFunction(String test)
        {
            String path = "C:\\Users\\Srijan\\Desktop\\appaPowerpoint\\AppaPowerPoint\\AppaPowerpoint\\AppaPowerpoint\\AppaPowerpoint\\StorageConnectors\\GoogleDriveDownloads\\Power Point POC Demo1.pptx";
            FileStream fs = File.OpenRead(path);
          

//string conv = fs.

           //InsertHTMLResultOnSameSlide();
        }

        private void InsertSpireTableOnSameSlide(string responseString)
        {


            #region WORKING CODE FOR INSERT A NEW SLIDE AND INSERT A TEXT IN TO IT
            Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
            Microsoft.Office.Interop.PowerPoint.Presentation objPres = ppApp.ActivePresentation;

            #region commented code
            // MessageBox.Show("Slide height:" + objPres.PageSetup.SlideHeight.ToString() + ", width:" + objPres.PageSetup.SlideWidth + ", slide size:" + objPres.PageSetup.SlideSize);
            //Microsoft.Office.Interop.PowerPoint.Slides objSlides = objPres.Slides;
            //  Microsoft.Office.Interop.PowerPoint.Slide objSlide = objSlides. Add(1, Microsoft.Office.Interop.PowerPoint.PpSlideLayout.ppLayoutCustom); 
            #endregion


            //SPIRE DLL. CODE FOR ADDING HTML OUTPUT TO THE SLIDE SHAPE
            Presentation ppt = new Presentation();

            #region commented code
            //   IAutoShape shape = ppt.Slides[0].Shapes.AppendShape(ShapeType.Rectangle, new System.Drawing.RectangleF(50, 50, 400, 100));
            //  shape.TextFrame.Paragraphs.Clear();
            //  string htmlText = responseString;//"<html><body><p><B style='color:red'>This HTML output can be replace </B></p></body></html>";
            //  shape.TextFrame.Paragraphs.AddFromHtml(htmlText);


            //ppt.SaveToFile(GlobalsData.rootDir + "\\testGenratedPPT.pptx", FileFormat.Pptx2013);

            // copy the html output from the temp ppt and place it on active presentaion
            // Microsoft.Office.Interop.PowerPoint.SlideRange slideRange = ppApp.ActiveWindow.Selection.SlideRange; 
            #endregion

            //CODE FOR TABLE
            AppaDocx.Application.Helpers.HlprDocumentManager objhlprDocumentManager = new HlprDocumentManager();
            //get colom count
            var colomCount = Convert.ToInt32(objhlprDocumentManager.getColomsOfTable(responseString));
            //get text
            var TableData = objhlprDocumentManager.StripHtml(responseString);
            var tableDataArray = TableData.Split(' ');

            //TRIAL CODE

            string[] nodeArray;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(responseString);
            var NodeArray = new List<string>();
            XmlNodeList NodeList = doc.GetElementsByTagName("entry");
            if (NodeList.Count != 0)
            {
                NodeArray = new List<XmlNode>(NodeList.Cast<XmlNode>()).Select(x => x.InnerText).ToList();
            }
            else
            {
                // The tag could be found!
                string ErrorId = "not found";
            }

            #region commented code
            //string xml = responseString;

            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xml);

            //string json = JsonConvert.SerializeXmlNode(doc);

            //var data = (JObject)JsonConvert.DeserializeObject(json);
            //string timeZone = data["entry"].Value<string>();

            //END 
            #endregion


            Double[] widths = new double[] { 100, 100, 150, 100, 100 };

            Double[] heights = new double[] { 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15 };

            ITable table = ppt.Slides[0].Shapes.AppendTable(ppt.SlideSize.Size.Width / 2 - 275, 80, widths, heights);


            Spire.Presentation.Collections.ShapeList shapes = ppt.Slides[0].Shapes;

            shapes.AddFromHtml(@"<html>
                                <body><table>
                                <tr>
                                <th> Expense </th><th> 1st year </th><th> 3 year </th><th> 5 year </th>
                                </tr>
                                <tr>
                                <td></td><td>$77 </td><td>$803 </td> <td>$1, 013 </td>
                                </tr>
                                </table> </body>
                                </html>");


            //set the style of table

            table.StylePreset = TableStylePreset.MediumStyle2Accent2;//.LightStyle1;//ThemedStyle2Accent6;// None;// .MediumStyle2Accent2;

            //CODE FOR FILL 2D ARRAY
            Random random = new Random(Guid.NewGuid().GetHashCode());
            string[,] tableArray = new string[colomCount, colomCount];

            #region commented code
            //for (int m = 0; m < colomCount; m++)
            //{
            //    for (int n = 0; n < colomCount; n++)
            //    {
            //        tableArray[m, n] = Convert.ToString(NodeArray[n]);
            //    }
            //} 
            #endregion

            int counter = 0;
            int nodeIndex = 0;
            for (int i = 0; i < NodeArray.Count; i++)
            {

                for (int j = 0; j < colomCount; j++)
                {
                    if (counter < NodeArray.Count)
                    {
                        //fill the table with data
                        table[j, i].TextFrame.Text = NodeArray[counter == 0 ? nodeIndex : nodeIndex += 1];//tableArray[i, j];
                                                                                                          //set the Font
                        table[j, i].TextFrame.Paragraphs[0].TextRanges[0].LatinFont = new TextFont("Arial Narrow");
                        counter++;
                    }

                }
            }


            #region commented code

            //            String[,] dataStr = new String[,]{

            //{"Name",    "Capital",  "Continent",    "Area", "Population"},

            //{"Venezuela",   "Caracas",  "South America",    "912047",   "19700000"},

            //{"Bolivia", "La Paz",   "South America",    "1098575",  "7300000"},

            //{"Brazil",  "Brasilia", "South America",    "8511196",  "150400000"},

            //{"Canada",  "Ottawa",   "North America",    "9976147",  "26500000"},

            //{"Chile",   "Santiago", "South America",    "756943",   "13200000"},

            //{"Colombia",    "Bagota",   "South America",    "1138907",  "33000000"},

            //{"Cuba",    "Havana",   "North America",    "114524",   "10600000"},

            //{"Ecuador", "Quito",    "South America",    "455502",   "10600000"},

            //{"Paraguay",    "Asuncion","South America", "406576",   "4660000"},

            //{"Peru",    "Lima", "South America",    "1285215",  "21600000"},

            //{"Jamaica", "Kingston", "North America",    "11424",    "2500000"},

            //{"Mexico",  "Mexico City",  "North America",    "1967180",  "88600000"}

            //};


            //for (int i = 0; i < 13; i++)

            //    for (int j = 0; j < 5; j++)
            //    {
            //        //fill the table with data
            //        table[j, i].TextFrame.Text = dataStr[i, j];
            //        //set the Font
            //        table[j, i].TextFrame.Paragraphs[0].TextRanges[0].LatinFont = new TextFont("Arial Narrow");

            //    }

            #endregion
            //ends here
            string fileName = "testfile_.pptx";
            ppt.SaveToFile(GlobalsData.rootDir + "\\" + fileName, FileFormat.Pptx2013);

            var destSlide = objPres.Slides;
            destSlide.InsertFromFile(GlobalsData.rootDir + "\\" + fileName, 1, 1, -1);

            //CODE FOR PLACE CONTENT ON CURRENT SLIDE
            objPres.Slides[2].Shapes[1].Copy();
            //foreach (Shape shape in objPres.Slides[2].Shapes)
            //{

            //    shape.Copy();
            //}
            Microsoft.Office.Interop.PowerPoint.SlideRange ppSR2 = ppApp.ActiveWindow.Selection.SlideRange;
            objPres.Slides[ppSR2.SlideIndex].Shapes.Paste();
            objPres.Slides[2].Delete();

            #region commented code
            //Microsoft.Office.Interop.PowerPoint.Slide slide = objPres.Slides[0];
            //Microsoft.Office.Interop.PowerPoint.Shapes shapes = slide.Shapes;






            // pres.Application.Quit();
            // app.Quit(); 
            #endregion

            #endregion
        }

        private void InsertSpireHTMLTableOnSameSlide(string responseString)
        {

            #region STATIC HTML CODE
            //      responseString = "<html><body>" + @"<table>

            //   <tr style='background-color:#d6f7fa;'>
            //      <td style='color:red'>
            //           Share Class  
            //      </td>
            //      <td>
            //          A 
            //      </td>
            //      <td>
            //          C 
            //      </td>
            //      <td>
            //          Y 
            //      </td>
            //      <td>
            //         Institutional
            //      </td>
            //      <td>
            //         Investor 
            //      </td>
            //   </tr>
            //   <tr>
            //      <td>
            //        American Beacon Grosvenor Long/Short Fund
            //      </td>
            //      <td>
            //          GSVAX
            //      </td>
            //      <td>
            //         GVRCX 
            //      </td>
            //      <td>
            //         GVRYX 
            //      </td>
            //      <td>
            //         GVRIX
            //      </td>
            //      <td>
            //         GVRPX
            //      </td>
            //   </tr>
            //</table>" + "</body></html>"; 
            #endregion


            // responseString = "<html><body><table margin='0' border='0' cellspacing='0' width='100%' cellpadding='0'><tr style='background-color: #d6f7fa;'><td style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>Share class</td><td style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>A</td><td style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>C</td><td style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>Y</td><td style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>Institutional</td><td style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>Investor</td></tr><tr><td style='padding: 0; valign: bottom;'><span style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>American Beacon Grosvenor Long/Short Fund</td><td style='padding: 0; valign: bottom;'><span style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>GSVAX</td><td style='padding: 0; valign: bottom;'><span style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>GVRCX</td><td style='padding: 0; valign: bottom;'><span style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>GVRYX</td><td style='padding: 0; valign: bottom;'><span style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>GVRIX</td><td style='padding: 0; valign: bottom;'><span style='font-family: Arial, Helvetica, sans-serif; font-size: xx-small;'>GVRPX</td></tr></table></body></html>";
            // ConvertHtmlToImage(responseString);
            //ENDS HERE
            Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
            Microsoft.Office.Interop.PowerPoint.Presentation objPres = ppApp.ActivePresentation;



            Presentation ppt = new Presentation();
            Spire.Presentation.Collections.ShapeList shapes = ppt.Slides[0].Shapes;

            #region Static HTML for test
            //shapes.AddFromHtml(@"<html>
            //                    <body><table>
            //                    <tr>
            //                    <th> Expense </th><th> 1st year </th><th> 3 year </th><th> 5 year </th>
            //                    </tr>
            //                    <tr>
            //                    <td></td><td>$77 </td><td>$803 </td> <td>$1, 013 </td>
            //                    </tr>
            //                    </table> </body>
            //                    </html>"); 
            #endregion

            shapes.AddFromHtml(responseString);

            string fileName = "testfile_.pptx";
            ppt.SaveToFile(GlobalsData.rootDir + "\\" + fileName, FileFormat.Pptx2013);

            var destSlide = objPres.Slides;
            destSlide.InsertFromFile(GlobalsData.rootDir + "\\" + fileName, 1, 1, -1);

            //CODE FOR PLACE CONTENT ON CURRENT SLIDE
            objPres.Slides[2].Shapes[1].Copy();//first check if shape is present or not

            #region commented code
            //foreach (Shape shape in objPres.Slides[2].Shapes)
            //{

            //    shape.Copy();
            //} 
            #endregion

            Microsoft.Office.Interop.PowerPoint.SlideRange ppSR2 = ppApp.ActiveWindow.Selection.SlideRange;
            float CalculatedTopValue = getPositionValueForTop(objPres.Slides[ppSR2.SlideIndex].Shapes);//call new method
            objPres.Slides[ppSR2.SlideIndex].Shapes.Paste().Top = CalculatedTopValue + 5;//+5 for giving some space;
            bool IsOverflow = IsOverflowSlideByCurrentShape(objPres.PageSetup.SlideHeight, objPres.Slides[2].Shapes[1].Height, CalculatedTopValue);//call new method for content getting overflow
            if (objPres.Slides[ppSR2.SlideIndex].Shapes.Count > 0)
            {
                objPres.Slides[ppSR2.SlideIndex].Shapes[objPres.Slides[ppSR2.SlideIndex].Shapes.Count].Left = 0;
                objPres.Slides[ppSR2.SlideIndex].Shapes[objPres.Slides[ppSR2.SlideIndex].Shapes.Count].Width = 958;
            }
            objPres.Slides[2].Delete();

            #region ASPODE CODE FOR UPLOAD HTML
            // Acesss the default first slide of presentation
            //Aspose.Slides.Presentation  pres = new Aspose.Slides.Presentation();
            //Aspose.Slides.ISlide slide = pres.Slides[0];

            //// Adding the AutoShape to accomodate the HTML content
            //Aspose.Slides.IAutoShape ashape = slide.Shapes.AddAutoShape(Aspose.Slides.ShapeType.Rectangle, 10, 10, pres.SlideSize.Size.Width - 20, pres.SlideSize.Size.Height - 10);

            //ashape.FillFormat.FillType = Aspose.Slides.FillType.NoFill;

            //// Adding text frame to the shape
            //ashape.AddTextFrame("");

            //// Clearing all paragraphs in added text frame
            //ashape.TextFrame.Paragraphs.Clear();

            //// Loading the HTML file using stream reader
            //TextReader tr = new StreamReader(GlobalsData.rootDir + "\\htmlProcessFIle.html");

            //// Adding text from HTML stream reader in text frame
            //ashape.TextFrame.Paragraphs.AddFromHtml(tr.ReadToEnd());

            //// Saving Presentation
            //pres.Save(GlobalsData.rootDir + "\\pptOutputByhtml.pptx", Aspose.Slides.Export.SaveFormat.Pptx); 
            #endregion
        }

        private void InsertSpireTableOnNewSlide(string responseString)
        {
            #region WORKING CODE FOR INSERT A NEW SLIDE AND INSERT A TEXT IN TO IT
            Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
            Microsoft.Office.Interop.PowerPoint.Presentation objPres = ppApp.ActivePresentation;
            // MessageBox.Show("Slide height:" + objPres.PageSetup.SlideHeight.ToString() + ", width:" + objPres.PageSetup.SlideWidth + ", slide size:" + objPres.PageSetup.SlideSize);
            //Microsoft.Office.Interop.PowerPoint.Slides objSlides = objPres.Slides;
            //  Microsoft.Office.Interop.PowerPoint.Slide objSlide = objSlides. Add(1, Microsoft.Office.Interop.PowerPoint.PpSlideLayout.ppLayoutCustom);











            //SPIRE DLL. CODE FOR ADDING HTML OUTPUT TO THE SLIDE SHAPE
            Presentation ppt = new Presentation();

            //   IAutoShape shape = ppt.Slides[0].Shapes.AppendShape(ShapeType.Rectangle, new System.Drawing.RectangleF(50, 50, 400, 100));
            //  shape.TextFrame.Paragraphs.Clear();
            //  string htmlText = responseString;//"<html><body><p><B style='color:red'>This HTML output can be replace </B></p></body></html>";
            //  shape.TextFrame.Paragraphs.AddFromHtml(htmlText);


            //ppt.SaveToFile(GlobalsData.rootDir + "\\testGenratedPPT.pptx", FileFormat.Pptx2013);

            // copy the html output from the temp ppt and place it on active presentaion
            // Microsoft.Office.Interop.PowerPoint.SlideRange slideRange = ppApp.ActiveWindow.Selection.SlideRange;

            //CODE FOR TABLE
            AppaDocx.Application.Helpers.HlprDocumentManager objhlprDocumentManager = new HlprDocumentManager();
            //get colom count
            var colomCount = Convert.ToInt32(objhlprDocumentManager.getColomsOfTable(responseString));
            //get text
            var TableData = objhlprDocumentManager.StripHtml(responseString);
            var tableDataArray = TableData.Split(' ');

            //TRIAL CODE

            string[] nodeArray;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(responseString);
            var NodeArray = new List<string>();
            XmlNodeList NodeList = doc.GetElementsByTagName("entry");
            if (NodeList.Count != 0)
            {
                NodeArray = new List<XmlNode>(NodeList.Cast<XmlNode>()).Select(x => x.InnerText).ToList();
            }
            else
            {
                // The tag could be found!
                string ErrorId = "not found";
            }

            //string xml = responseString;

            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xml);

            //string json = JsonConvert.SerializeXmlNode(doc);

            //var data = (JObject)JsonConvert.DeserializeObject(json);
            //string timeZone = data["entry"].Value<string>();

            //END


            Double[] widths = new double[] { 100, 100, 150, 100, 100 };

            Double[] heights = new double[] { 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15 };

            ITable table = ppt.Slides[0].Shapes.AppendTable(ppt.SlideSize.Size.Width / 2 - 275, 80, widths, heights);

            //set the style of table

            table.StylePreset = TableStylePreset.LightStyle1Accent2;

            //CODE FOR FILL 2D ARRAY
            Random random = new Random(Guid.NewGuid().GetHashCode());
            string[,] tableArray = new string[colomCount, colomCount];
            //for (int m = 0; m < colomCount; m++)
            //{
            //    for (int n = 0; n < colomCount; n++)
            //    {
            //        tableArray[m, n] = Convert.ToString(NodeArray[n]);
            //    }
            //}

            int counter = 0;
            int nodeIndex = 0;
            for (int i = 0; i < NodeArray.Count; i++)
            {

                for (int j = 0; j < colomCount; j++)
                {
                    if (counter < NodeArray.Count)
                    {
                        //fill the table with data
                        table[j, i].TextFrame.Text = NodeArray[counter == 0 ? nodeIndex : nodeIndex += 1];//tableArray[i, j];
                                                                                                          //set the Font
                        table[j, i].TextFrame.Paragraphs[0].TextRanges[0].LatinFont = new TextFont("Arial Narrow");
                        counter++;
                    }

                }
            }



            //            String[,] dataStr = new String[,]{

            //{"Name",    "Capital",  "Continent",    "Area", "Population"},

            //{"Venezuela",   "Caracas",  "South America",    "912047",   "19700000"},

            //{"Bolivia", "La Paz",   "South America",    "1098575",  "7300000"},

            //{"Brazil",  "Brasilia", "South America",    "8511196",  "150400000"},

            //{"Canada",  "Ottawa",   "North America",    "9976147",  "26500000"},

            //{"Chile",   "Santiago", "South America",    "756943",   "13200000"},

            //{"Colombia",    "Bagota",   "South America",    "1138907",  "33000000"},

            //{"Cuba",    "Havana",   "North America",    "114524",   "10600000"},

            //{"Ecuador", "Quito",    "South America",    "455502",   "10600000"},

            //{"Paraguay",    "Asuncion","South America", "406576",   "4660000"},

            //{"Peru",    "Lima", "South America",    "1285215",  "21600000"},

            //{"Jamaica", "Kingston", "North America",    "11424",    "2500000"},

            //{"Mexico",  "Mexico City",  "North America",    "1967180",  "88600000"}

            //};


            //for (int i = 0; i < 13; i++)

            //    for (int j = 0; j < 5; j++)
            //    {
            //        //fill the table with data
            //        table[j, i].TextFrame.Text = dataStr[i, j];
            //        //set the Font
            //        table[j, i].TextFrame.Paragraphs[0].TextRanges[0].LatinFont = new TextFont("Arial Narrow");

            //    }









            //ends here
            string fileName = "testfile_.pptx";
            ppt.SaveToFile(GlobalsData.rootDir + "\\" + fileName, FileFormat.Pptx2013);



            var destSlide = objPres.Slides;
            destSlide.InsertFromFile(GlobalsData.rootDir + "\\" + fileName, 1, 1, -1);
            //Microsoft.Office.Interop.PowerPoint.Slide slide = objPres.Slides[0];
            //Microsoft.Office.Interop.PowerPoint.Shapes shapes = slide.Shapes;






            // pres.Application.Quit();
            // app.Quit();





            #endregion
        }

        private void InsertTextResultOnNewSlide(string responseString)
        {
            Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
            Microsoft.Office.Interop.PowerPoint.Presentation objPres = ppApp.ActivePresentation;
            // MessageBox.Show("Slide height:" + objPres.PageSetup.SlideHeight.ToString() + ", width:" + objPres.PageSetup.SlideWidth + ", slide size:" + objPres.PageSetup.SlideSize);
            Microsoft.Office.Interop.PowerPoint.Slides objSlides = objPres.Slides;
            Microsoft.Office.Interop.PowerPoint.Slide objSlide = objSlides.Add(1, Microsoft.Office.Interop.PowerPoint.PpSlideLayout.ppLayoutCustom);

            TextRange objTextRng = objSlide.Shapes[1].TextFrame.TextRange;
            AppaDocx.Application.Helpers.HlprDocumentManager objhlprDocumentManager = new HlprDocumentManager();
            objTextRng.Text = objhlprDocumentManager.StripHtml(responseString);  // "SLIDE INSERT AND INSERT A TEXT";

            objTextRng.Font.Name = "Calibri";
            objTextRng.Font.Size = 20;

            objSlide.Shapes[1].TextFrame.AutoSize = PpAutoSize.ppAutoSizeShapeToFitText;

        }

        private void InsertTextOnCurrentSlide(string responseString)
        {
            //WORKING CODE INSERT A TEXT IN TO CURRENT SLIDE
            #region WORKING CODE INSERT A TEXT IN TO CURRENT SLIDE
            Microsoft.Office.Interop.PowerPoint.Application ppApp2 = Globals.ThisAddIn.Application;
            Microsoft.Office.Interop.PowerPoint.SlideRange ppSR2 = ppApp2.ActiveWindow.Selection
                .SlideRange;
            Microsoft.Office.Interop.PowerPoint.Shape ppShap = ppSR2.Shapes
                .AddLabel(Microsoft.Office.Core.MsoTextOrientation
                .msoTextOrientationHorizontal, 0, 0, 200, 25);
            AppaDocx.Application.Helpers.HlprDocumentManager objhlprDocumentManager = new HlprDocumentManager();
            ppShap.TextEffect.Text = objhlprDocumentManager.StripHtml(responseString);  // "SLIDE INSERT AND INSERT A TEXT";

            #endregion
        }

        //TEST FOR ADD CUSTOM XML PART
        private void AddCustomXmlPartToPresentation(Microsoft.Office.Interop.PowerPoint.Presentation presentation)
        {
            string xmlString =
                "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                "<employees xmlns=\"http://schemas.microsoft.com/vsto/samples\">" +
                    "<employee>" +
                        "<name>Karina Leal</name>" +
                        "<hireDate>1999-04-01</hireDate>" +
                        "<title>Manager</title>" +
                    "</employee>" +
                "</employees>";

            Microsoft.Office.Core.CustomXMLPart employeeXMLPart =
                presentation.CustomXMLParts.Add(xmlString);

        }

        //public static void InsertNewSlide(string presentationFile, int position, string slideTitle)
        //{
        //    // Open the source document as read/write. 
        //    presentationFile = "D:\\presentationFile.xml";
        //    // StreamReader sr = new StreamReader(GlobalsData.rootDir + "\\presentationFile.xml");


        //    //  System.IO.Stream textStream = new System.IO.StreamReader(GlobalsData.rootDir + "\\presentationFile.xml");

        //    var stream = new MemoryStream();
        //    var writer = new StreamWriter(stream);

        //    writer.Write("<p:sp>  <p:nvSpPr>   <p:cNvPr id=\"1\" name=\"TextShape\" />   <p:cNvSpPr />   <p:nvPr />  </p:nvSpPr>  <p:spPr>   <a:xfrm>    <a:off x=\"285720\" y=\"428604\" />    <a:ext cx=\"1928826\" cy=\"369332\" />   </a:xfrm>  </p:spPr>  <p:txBody>   <a:bodyPr />   <a:p>    <a:r>       <a:t>Shape Text</a:t>    </a:r>   </a:p>  </p:txBody> </p:sp> ");
        //    writer.Flush();
        //    stream.Position = 0;
        //    // return stream;

        //    using (PresentationDocument presentationDocument = PresentationDocument.Open(stream, true))
        //    {
        //        // Pass the source document and the position and title of the slide to be inserted to the next method.
        //        InsertNewSlide(presentationDocument, position, slideTitle);
        //    }
        //}

        //public static void InsertNewSlide(PresentationDocument presentationDocument, int position, string slideTitle)
        //{
        //    if (presentationDocument == null)
        //    {
        //        throw new ArgumentNullException("presentationDocument");
        //    }

        //    if (slideTitle == null)
        //    {
        //        throw new ArgumentNullException("slideTitle");
        //    }

        //    PresentationPart presentationPart = presentationDocument.PresentationPart;

        //    // Verify that the presentation is not empty.
        //    if (presentationPart == null)
        //    {
        //        throw new InvalidOperationException("The presentation document is empty.");
        //    }

        //    // Declare and instantiate a new slide.
        //    DocumentFormat.OpenXml.Presentation.Slide slide = new DocumentFormat.OpenXml.Presentation.Slide(new CommonSlideData(new ShapeTree()));
        //    uint drawingObjectId = 1;

        //    // Construct the slide content.            
        //    // Specify the non-visual properties of the new slide.
        //    NonVisualGroupShapeProperties nonVisualProperties = slide.CommonSlideData.ShapeTree.AppendChild(new NonVisualGroupShapeProperties());
        //    nonVisualProperties.NonVisualDrawingProperties = new NonVisualDrawingProperties() { Id = 1, Name = "" };
        //    nonVisualProperties.NonVisualGroupShapeDrawingProperties = new NonVisualGroupShapeDrawingProperties();
        //    nonVisualProperties.ApplicationNonVisualDrawingProperties = new ApplicationNonVisualDrawingProperties();

        //    // Specify the group shape properties of the new slide.
        //    slide.CommonSlideData.ShapeTree.AppendChild(new GroupShapeProperties());

        #endregion


        private float getPositionValueForTop(Microsoft.Office.Interop.PowerPoint.Shapes shapes)
        {
            List<Tuple<float, float>> ShapeValueForTopAndHeight = new List<Tuple<float, float>>();

            float CalculatedTopValue = 0;
            foreach (Shape shape in shapes)
            {
                ShapeValueForTopAndHeight.Add(new Tuple<float, float>(shape.Top, shape.Height));

                float top = shape.Top;
                float height = shape.Height;
                CalculatedTopValue = top + height;
                //float left  = shape.Left;
                //float width = shape.Width;
            }

            if (ShapeValueForTopAndHeight.Count > 0)
            {
                ShapeValueForTopAndHeight = ShapeValueForTopAndHeight.OrderByDescending(x => x.Item1).ToList();
                //MessageBox.Show("TOP VALUE:" + ShapeValueForTopAndHeight[0].Item1.ToString() + ". HEIGHT VALUE:" + ShapeValueForTopAndHeight[0].Item2.ToString());
                //MessageBox.Show("New shape top:" + ShapeValueForTopAndHeight[0].Item1 + ShapeValueForTopAndHeight[0].Item2);
                CalculatedTopValue = 0;
                CalculatedTopValue = ShapeValueForTopAndHeight[0].Item1 + ShapeValueForTopAndHeight[0].Item2;
            }

            return CalculatedTopValue;
        }

        private float getLastExistingShapeHeight(Microsoft.Office.Interop.PowerPoint.Shapes shapes)
        {
            float LastExistingShapeHeight = 0;
            int totalShape = shapes.Count;
            int currentShape = 0;
            foreach (Shape shape in shapes)
            {
                currentShape += 1;
                if (totalShape == currentShape)
                {
                    LastExistingShapeHeight = shape.Height;
                }
                //float left  = shape.Left;
                //float width = shape.Width;
            }
            return LastExistingShapeHeight;
        }

        public bool ProcessResponse(string strResponse)
        {

            if (strResponse == "loginrequired")
            {
                strData = "Please Re-Login to AppaDocx.";

                MessageBox.Show(strData);

                AppaDocx.Common.GlobalsData.IsLogin = false;
                IsAuthorized();
                return (true);
            }
            return (false);
        }


        #region CODE FOR ASPOSE FUNCTIONALITY FOR READ THE DOC AND CREATE THE CONTENT IN SLIDE
        private void ProcessWordDocAndCreatePPT()
        {
            Aspose.Words.Document doc = new Aspose.Words.Document(GlobalsData.rootDir + "\\testDocForConvertToPPT.docx");



            //writing ppt using spire
            Microsoft.Office.Interop.PowerPoint.Application ppApp = Globals.ThisAddIn.Application;
            Microsoft.Office.Interop.PowerPoint.Presentation objPres = ppApp.ActivePresentation;

            //SPIRE DLL. CODE FOR ADDING HTML OUTPUT TO THE SLIDE SHAPE
            Presentation ppt = new Presentation();


            foreach (Aspose.Words.Paragraph para in doc.GetChildNodes(Aspose.Words.NodeType.Paragraph, true))
            {
                IAutoShape shape = ppt.Slides[0].Shapes.AppendShape(ShapeType.Rectangle, new System.Drawing.RectangleF(0, 0, 958, 100));

                //MessageBox.Show("Content of DOC: " + para.ToString(Aspose.Words.SaveFormat.Text));
                MessageBox.Show("Content Font Name: " + para.ParagraphFormat.Style.Font.Name);
                MessageBox.Show("Content Font Size: " + para.ParagraphFormat.Style.Font.Size);


                shape.TextFrame.Paragraphs.Append(new TextParagraph());


                shape.TextFrame.Paragraphs[1].TextRanges.Append(new Spire.Presentation.TextRange(para.ToString(Aspose.Words.SaveFormat.Text)));
                shape.TextFrame.AutofitType = TextAutofitType.Shape;
                shape.ShapeStyle.LineColor.Color = System.Drawing.Color.Transparent;
                shape.ShapeStyle.FillColor.Color = System.Drawing.Color.Transparent;
                shape.ShapeStyle.FontColor.Color = System.Drawing.Color.Black;

            }
            //

            string fileName = "testfile_.pptx";
            ppt.SaveToFile(GlobalsData.rootDir + "\\" + fileName, FileFormat.Pptx2013);
            // copy the html output from the temp ppt and place it on active presentaion slide
            var destSlide = objPres.Slides;
            destSlide.InsertFromFile(GlobalsData.rootDir + "\\" + fileName, 1, 1, -1);
            objPres.Slides[2].Shapes[1].Copy();
            Microsoft.Office.Interop.PowerPoint.SlideRange ppSR2 = ppApp.ActiveWindow.Selection.SlideRange;
            float CalculatedTopValue = getPositionValueForTop(objPres.Slides[ppSR2.SlideIndex].Shapes);//call new method
                                                                                                       // float LastExistingShapeHeight = getLastExistingShapeHeight(objPres.Slides[ppSR2.SlideIndex].Shapes);
            objPres.Slides[ppSR2.SlideIndex].Shapes.Paste().Top = CalculatedTopValue + 5;//+5 for giving some space;
            if (objPres.Slides[ppSR2.SlideIndex].Shapes.Count > 0)
            {
                objPres.Slides[ppSR2.SlideIndex].Shapes[objPres.Slides[ppSR2.SlideIndex].Shapes.Count].Left = 0;

                Shape myshape = objPres.Slides[ppSR2.SlideIndex].Shapes[objPres.Slides[ppSR2.SlideIndex].Shapes.Count];



            }

            objPres.Slides[2].Delete();



        }
        #endregion


        //CODE FOR CONVERT HTML TO SLIDE
        public void ConvertHtmlToImage(string htmlString)
        {
            //Bitmap m_Bitmap = new Bitmap(400, 600);
            //PointF point = new PointF(0, 0);
            //SizeF maxSize = new System.Drawing.SizeF(500, 500);
            //TheArtOfDev.HtmlRenderer.WinForms.HtmlRender.Render(Graphics.FromImage(m_Bitmap), htmlString, point, maxSize);
            //m_Bitmap.Save("D:\\Satish work\\testImage.png", System.Drawing.Imaging.ImageFormat.Png);

            //
            //BitmapFrame bitmapFrame = TheArtOfDev.HtmlRenderer.WPF.HtmlRender.RenderToImage(htmlString, 650, 400);
            //System.IO.Stream imageStream = bitmapFrame.to image.toStream(System.Drawing.Imaging.ImageFormat.Png);
            //RectangleF rect = new RectangleF(40, 80, image.Width, image.Height);
            //IEmbedImage embImage = slide.Shapes.AppendEmbedImage(ShapeType.Rectangle, imageStream, rect);
            //embImage.ShapeLocking.AspectRatioProtection = true;

            BitmapFrame image = HtmlRender.RenderToImage(htmlString);//"<p><h1>Hello World</h1>This is html rendered text</p>"
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(image);
            using (FileStream stream = new FileStream("D:\\Satish work\\zimage.png", FileMode.OpenOrCreate))
                encoder.Save(stream);
        }

        private bool IsOverflowSlideByCurrentShape(double slideHeight, double currentShapeHeight, double LastExistingShapeHeight)
        {
            double RemainingHeightOnSlide = slideHeight - LastExistingShapeHeight;
            if (RemainingHeightOnSlide < currentShapeHeight)
            {
                MessageBox.Show("Content are getting overflow on current slide...!");
                return true;
            }

            return false;

        }

        private void btnAdvanceSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ComponentAdvancedSearch objadvancesearch = new ComponentAdvancedSearch();
                objadvancesearch.ShowInTaskbar = true;
                objadvancesearch.ResizeMode = ResizeMode.NoResize;
                objadvancesearch.ShowDialog();

                if (ConstantsValues.lstComponentAddedtoCart.Count > 0)
                {
                    AddComponentListtoListBox(ConstantsValues.lstComponentAddedtoCart, 2);
                }

            }
            catch (Exception ex)
            {
                log.Error("Error Advance Search Navigate", ex);
            }
        }

        private void cboConnectors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboConnectors.SelectedIndex > 0)
            {
                ComboBoxItem cboselectedItem = (ComboBoxItem)cboConnectors.SelectedItem;

                if (cboselectedItem.Content.ToString() == "Google Drive")
                {
                    //LoginGoogleDrive obj = new LoginGoogleDrive();
                    //obj.Show();
                    //obj.Navigate("https://drive.google.com/");
                    var service = AppPowerpoint.StorageConnectors.GoogleDriveAccess.AuthenticateOauth("C:\\Users\\Srijan\\Desktop\\appaPowerpoint\\AppaPowerPoint\\AppaPowerpoint\\AppaPowerpoint\\AppaPowerpoint\\StorageConnectors\\credentials.json", "AppaPowerPointUser3");
                   
                    //code for fetch the file details from the drive.
                    var files = AppPowerpoint.StorageConnectors.GoogleDriveAccess.ListFiles(service,
                              // new FilesListOptionalParms() { Q = "mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'" });//satish comment and add this below line
                              //new FilesListOptionalParms() { Q = "mimeType = 'application/vnd.google-apps.folder'" }); //for get the folder
                              new FilesListOptionalParms() { Q = "mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'" }); //for presentaion PPT

                    List<Google.Apis.Drive.v3.Data.File> DriveFiles = new List<Google.Apis.Drive.v3.Data.File>();
                    foreach (var item in files.Files)
                    {
                        DriveFiles.Add(item);
                    }
                        FileList objFileList = new FileList();
                        objFileList.PopulateFileButtons(service, DriveFiles);
                        objFileList.Show();

                        //////Code for download the file
                        //AppPowerpoint.StorageConnectors.GoogleDriveAccess.DownloadFile(service, item, "D:\\" + item.Name);
                        //Google.Apis.Drive.v3.Data.File file = service.Files.Get(item.Id).Execute();


                        //// //var bytes = service.HttpClient.GetByteArrayAsync(item.); 


                   
                    //Process.Start("https://drive.google.com/");
                   

                    #region Code for download file from google drive
                    //foreach (var item in files.Files)
                    //{
                    //    // download each file
                    //    //Download(service, item, string.Format(@"C:\FilesFromGoogleDrive\{0}", item.Name));
                    //    AppPowerpoint.StorageConnectors.GoogleDriveAccess.DownloadFile(service, item, "E:\\Download\\" + item.Name);

                    //    //Google.Apis.Drive.v3.Data.File file = service.Files.Get(item.Id).Execute();
                    //    //var bytes = service.HttpClient.GetByteArrayAsync(item.);
                    //}

                    //foreach (var item in files.Files)
                    //{
                    //    var fileId = item.Id;
                    //    var request = service.Files.Export(fileId, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                    //    var stream = new System.IO.MemoryStream();
                    //    // Add a handler which will be notified on progress changes.
                    //    // It will notify on each chunk download and when the
                    //    // download is completed or failed.
                    //    request.MediaDownloader.ProgressChanged +=
                    //            (IDownloadProgress progress) =>
                    //            {
                    //                switch (progress.Status)
                    //                {
                    //                    case DownloadStatus.Downloading:
                    //                        {
                    //                            Console.WriteLine(progress.BytesDownloaded);
                    //                            break;
                    //                        }
                    //                    case DownloadStatus.Completed:
                    //                        {
                    //                            Console.WriteLine("Download complete.");
                    //                            break;
                    //                        }
                    //                    case DownloadStatus.Failed:
                    //                        {
                    //                            Console.WriteLine("Download failed.");
                    //                            break;
                    //                        }
                    //                }
                    //            };
                    //    request.Download(stream);

                    //} 
                    #endregion

                }
                else if (cboselectedItem.Content.ToString() == "DropBox")
                {
                    MainWindow objMainWindow = new MainWindow();
                    objMainWindow.Authenticate();
                   
                }
                else if(cboselectedItem.Content.ToString()=="Microsoft OneDrive")
                {
                    FormBrowser form = new FormBrowser();
                    form.SignIn();
                    
                   
                   




                }
            }
        }
    }



    public class ComponentType
    {
        public ComponentType(bool isCheckedFlag, string strComponentType, string strComponentTypeUse, int iIndex)
        {
            IsSelected = isCheckedFlag;
            ComponentTypeName = strComponentType;
            ComponentTypeUse = strComponentTypeUse;
            Index = iIndex;
        }
        public Boolean IsSelected

        { get; set; }

        public String ComponentTypeName
        { get; set; }

        public String ComponentTypeUse
        { get; set; }

        public int Index
        { get; set; }

    }

}
