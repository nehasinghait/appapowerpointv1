﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppaPowerpoint
{
    /// <summary>
    /// Interaction logic for LoginGoogleDrive.xaml
    /// </summary>
    public partial class LoginGoogleDrive : Window
    {
        public LoginGoogleDrive()
        {
            InitializeComponent();
        }

        public void Navigate(string uri)
        {
            try
            {
                if (!string.IsNullOrEmpty(uri))
                {
                    Uri authorizeUri = new Uri(uri);
                    Browser.Navigate(authorizeUri);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Browser_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            
        }

        private void Browser_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {

        }
    }
}
