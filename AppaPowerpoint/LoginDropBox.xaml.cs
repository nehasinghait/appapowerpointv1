﻿using Dropbox.Api;
using StorageConnectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AppaPowerpoint
{
    /// <summary>
    /// Interaction logic for LoginDropBox.xaml
    /// </summary>
    /// 

    public partial class LoginDropBox : Window
    {
        public LoginDropBox()
        {
            InitializeComponent();

        }

        public LoginDropBox(string AppKey, string AuthenticationURL, string oauth2State)
        {
            InitializeComponent();
            DBAppKey = AppKey;
            DBAuthenticationURL = AuthenticationURL;
            DBoauth2State = oauth2State;
            Dispatcher.BeginInvoke(new Action(Navigate));
        }


        #region Variables  
        private const string RedirectUri = "https://www.dropbox.com/login";
        private string DBAppKey = string.Empty;
        private string DBAuthenticationURL = string.Empty;
        private string DBoauth2State = string.Empty;
        #endregion

        #region Properties  
        public string AccessToken { get; private set; }

        public string UserId { get; private set; }

        public bool Result { get; private set; }
        #endregion


        public void Navigate()
        {
            try
            {
                if (!string.IsNullOrEmpty(DBAppKey))
                {
                    Uri authorizeUri = new Uri(DBAuthenticationURL);
                    Browser.Navigate(authorizeUri);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(Navigate));
            Navigate();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception)
            {
                throw;
            }

        }

        private void Browser_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (!e.Uri.AbsoluteUri.ToString().StartsWith(RedirectUri.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                // we need to ignore all navigation that isn't to the redirect uri.  
                return;
            }


            try
            {
                //if (e.Uri.ToString().EndsWith(".pdf"))
                //{
                //    e.Cancel = true;
                //    byte[] b = new WebClient().DownloadData(e.Uri.ToString());
                //    MessageBox.Show(b.Length.ToString());
                //}

                OAuth2Response result = DropboxOAuth2Helper.ParseTokenFragment(e.Uri);
                if (result.State != DBoauth2State)
                {
                    return;
                }

                this.AccessToken = result.AccessToken;
                this.Uid = result.Uid;
                this.Result = true;

                //satish addded
                if (AccessToken != "")
                {
                    //

                    AppaPowerpoint.MainWindow objMainWindow = new MainWindow();
                    objMainWindow.FileList("", AccessToken); ///Dropbox/DotNetApi
                    objMainWindow.Show();


                }
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                // e.Cancel = true;
                // this.Close();
            }
        }

        private void Browser_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {


        }
    }

}

