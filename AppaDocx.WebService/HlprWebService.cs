﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using AppaDocx.Common;
using AppaDocx.Common.Helpers;
using AppaDocx.Common.Models;
using CredentialManagement;

namespace AppaDocx.WebService
{
    public partial class HlprWebService
    {
        public static readonly string AUTH_HEADER = "X-Auth-App-TokenAccess";
        public static readonly string GUID_HEADER = "X-Auth-App-Guid";
        public static readonly string APP_NAME = "Appadocx";
        public static readonly string TOKEN = "token";
        public static readonly string USERID = "userid";
        public static readonly string GUID = "guid";
        public static readonly string BEARER = "Bearer ";
        public static readonly string LOGINMANDATORY = "loginrequired";
        private Credential credential = null;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HlprWebService));

        private Dictionary<string, string> webServicesDict = new Dictionary<string, string>
        {
            { "booktypes", "/services/getAllBookTypes" },
            { "bookstatus", "/services/getAllBookStatus" },
            { "docnamesearch", "/services/getdocnames" },
            { "docbookxml", "/services/getDocBookXML" },
            { "savecomponent", "/services/saveComponentInfo" },
            { "getdocumentusage", "/services/getDocumentUsage" },
            { "getupdatedcomponents", "/services/updatedcomponents" },
            { "blacklinecomponents", "/services/blacklinecomponents" },
            { "imageservice", "/services/getImage" },
            { "authlogin", "/auth/login" },
            { "wordml", "/services/getWordML" },
            { "getcomponentinfo","/services/getcomponentinfo" },
            { "getfavoritecomponent","/services/getFavoriteComponents" },
            { "getComponentDetailById","/services/getComponentDetailById" },
            { "getSearchData","/services/getSearchData" },
            { "searchcomponents","/services/searchcomponents" },
            { "getAllComponentInfo", "/services/getAllComponentInfo" },
            { "getAllComponentsForCart", "/services/getAllComponentsForCart" },
            { "saveAllComponentsToCart", "/services/saveAllComponentsToCart" },
            { "removeComponentsFromCart", "/services/removeComponentsFromCart" }
        };
        #region Dependents Methods
        private string makeWebServiceUrl(Dictionary<string, string> webServiceInfo)
        {
            string userId = getUserId();
            string webServiceUrl = ConstantsValues.CurrentWebServiceHomeURL + "$$webServiceName";
            try
            {
                if (!webServiceInfo.ContainsKey("service.auth"))
                {
                    webServiceUrl = webServiceUrl + "/" + userId + "/" + ConstantsValues.CurrentClientID;
                }
                string webServiceRequired = webServiceInfo["service.name"];
                string webServiceName = "";
                if (webServicesDict.ContainsKey(webServiceRequired))
                {
                    webServiceName = webServicesDict[webServiceRequired];
                }
                webServiceUrl = webServiceUrl.Replace("$$webServiceName", webServiceName);
                if (webServiceInfo.ContainsKey("service.queryparams"))
                {
                    webServiceUrl = webServiceUrl + webServiceInfo["service.queryparams"];
                }
            }
            catch (Exception ex)
            {
                log.Error("Web Service url", ex);
            }
            return webServiceUrl;
        }

        public string callWebService(Dictionary<string, string> webServiceInfo)
        {
            string guId = getGUID();
            if (guId == "")
            {
                setGUID(Guid.NewGuid().ToString());
            }
            string strResponse = String.Empty;
            string webServiceUrl = makeWebServiceUrl(webServiceInfo);
            log.Info("WebService webServiceUrl is : " + webServiceUrl);
            string requestMethod = webServiceInfo.ContainsKey("service.method") ? webServiceInfo["service.method"] : "GET";
            string auth = webServiceInfo.ContainsKey("service.auth") ? webServiceInfo["service.auth"] : "true";
            Dictionary<string, string> webRequestInfo = new Dictionary<string, string>();
            string servicename = webServiceInfo.ContainsKey("service.name") ? webServiceInfo["service.name"].ToString() : "";
            log.Info("WebService Called :" + servicename);
            try
            {
                webRequestInfo.Add("url", webServiceUrl);
                webRequestInfo.Add("method", requestMethod);
                webRequestInfo.Add("auth", auth);
                webRequestInfo.Add("servicename", servicename);
                if (webServiceInfo.ContainsKey("service.data"))
                {
                    webRequestInfo.Add("data", webServiceInfo["service.data"]);
                }
                if (webServiceInfo.ContainsKey("service.content-type"))
                {
                    webRequestInfo.Add("content-type", webServiceInfo["service.content-type"]);
                }

                AgainTry:
                strResponse = GetResponseByURLAuth(webRequestInfo);
                if (strResponse.ToLower() == "tokenexpired" || strResponse.ToLower() == "tokenerror")
                {
                    strResponse = LOGINMANDATORY;
                    removeAccessToken();
                    removeUserId();
                }
                else if (strResponse.Contains("successaccesstokenstart"))
                {
                    if (webServiceInfo.ContainsKey("service.auth") && webServiceInfo["service.auth"].ToLower().Equals("false"))
                    {
                        if (webServiceInfo.ContainsKey("service.userid"))
                        {
                            string userId = webServiceInfo["service.userid"];
                            removeUserId();
                            setUserId(userId);
                        }
                    }
                    removeAccessToken();
                    setAccessToken(strResponse);
                    if (auth != null && auth.Equals("true"))
                    {
                        goto AgainTry;
                    }
                }
            } catch (Exception ex)
            {
                log.Error("Web Service", ex);
            }
            return strResponse;
        }
        private string GetResponseByURLAuth(Dictionary<string, string> webRequestInfo)
        {
            string accessToken = getAccessToken();
            if (webRequestInfo.ContainsKey("auth") && webRequestInfo["auth"].ToLower().Equals("true"))
            {
                if (accessToken == "")
                {
                    return LOGINMANDATORY;
                }
            }
            HttpWebRequest request = WebRequest.Create(webRequestInfo["url"]) as HttpWebRequest;
            request.Credentials = CredentialCache.DefaultCredentials;
            if (webRequestInfo.ContainsKey("content-type"))
            {
                request.ContentType = webRequestInfo["content-type"];
            }
            request.KeepAlive = true;
            request.Timeout = 15000;  //Decrease timeout
            request.Proxy = null;
            request.Method = webRequestInfo["method"];
            if (webRequestInfo.ContainsKey("auth") && webRequestInfo["auth"].ToLower().Equals("true"))
            {
                request.Headers.Add(AUTH_HEADER, accessToken);
            }
            string gUID = getGUID();
            request.Headers.Add(GUID_HEADER, gUID);

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
               | SecurityProtocolType.Tls11
               | SecurityProtocolType.Tls12
               | SecurityProtocolType.Ssl3; /*SecurityProtocolType.Tls12;*/
            //ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;


            string serviceName = webRequestInfo["servicename"];
            request.ServicePoint.ConnectionLeaseTimeout = 15000; //Manage service points connections
            request.ServicePoint.MaxIdleTime = 15000;
            if (webRequestInfo.ContainsKey("method") && webRequestInfo.ContainsKey("data"))
            {
                //We need to count how many bytes we're sending. 
                byte[] bytes = Encoding.UTF8.GetBytes(webRequestInfo["data"]);
                request.ContentLength = bytes.Length;
                System.IO.Stream os = request.GetRequestStream();
                os.Write(bytes, 0, bytes.Length); //Push it out there
                os.Close();
            }
            // Read stream
            string responseString = String.Empty;
            try
            {
                string strStatusCode = "";
                using (WebResponse response = request.GetResponse())
                {
                    strStatusCode = ResponseHeaderProcessing(response);
                    if (strStatusCode != "tokenexpired" && strStatusCode != "tokenerror")
                    {
                        if (serviceName == "imageservice")
                        {
                            string fileName = webRequestInfo["data"].Replace("filename=", "");
                            using (Stream objStream = response.GetResponseStream())//Object Streaming
                            {
                                using (Stream outputStream = File.OpenWrite(GlobalsData.dir + "\\DownloadedImages\\" + fileName))
                                {
                                    byte[] buffer = new byte[4096];
                                    int bytesRead;
                                    do
                                    {
                                        bytesRead = objStream.Read(buffer, 0, buffer.Length);
                                        outputStream.Write(buffer, 0, bytesRead);
                                    } while (bytesRead != 0);
                                }
                                responseString = fileName;
                                objStream.Flush();
                                objStream.Close();
                            }
                        }
                        else
                        {
                            using (Stream objStream = response.GetResponseStream())//Object Streaming
                            {
                                using (StreamReader objReader = new StreamReader(objStream))
                                {
                                    responseString = objReader.ReadToEnd();
                                    objReader.Close();
                                }
                                objStream.Flush();
                                objStream.Close();
                            }
                        }
                    }
                    else if (strStatusCode == "tokenexpired" || strStatusCode == "tokenerror")
                    {
                        responseString = strStatusCode;
                        log.Info("Token Expired or Token Error");
                    }
                    response.Close();
                }
            }
            catch (WebException ex)
            {
                HlprErrorHandling.SourceName = ex.Status + " Source: " + ex.Source;
                throw ex;
            }
            finally
            {
                request.Abort();
                request = null;  //Update request object to null
            }
            return responseString;
        }
        private string ResponseHeaderProcessing(WebResponse response)
        {
            string iStatusCode = "";
            if (response.Headers["tokenexpired"] != null && response.Headers["tokenexpired"].Contains("JWT Token expired"))
            {
                iStatusCode = "tokenexpired";
            }
            else if (response.Headers["tokenerror"] != null)
            {
                iStatusCode = "tokenerror";
            }
            return (iStatusCode);
        }
        #endregion

        #region Helper Methods  
        public string getCompChangesFroWebService(string documentId, string queryParams)
        {
            string strResponse = "";
            try
            {
                List<ChangeComponentModel> lstResult = new List<ChangeComponentModel>();
                string strCurrentDocumentId = "/" + documentId;
                Dictionary<string, string> usageDocumentWebServiceInfo = new Dictionary<string, string>();
                usageDocumentWebServiceInfo.Add("service.name", "getupdatedcomponents");
                usageDocumentWebServiceInfo.Add("service.method", "POST");
                usageDocumentWebServiceInfo.Add("service.queryparams", strCurrentDocumentId);
                usageDocumentWebServiceInfo.Add("service.data", "componentdata=" + queryParams);
                usageDocumentWebServiceInfo.Add("service.content-type", "application/x-www-form-urlencoded");
                strResponse = callWebService(usageDocumentWebServiceInfo);
            }
            catch (Exception exx)
            {
                log.Error("WebService(getCompChangesFroWebService)", exx);
                strResponse = "Exception";
            }
            return (strResponse);
        }

        public string Authenticate(string userId, string strUserPassword)
        {
            string strResponse = "";
            try
            {
                string postData = "userid=" + System.Web.HttpUtility.UrlEncode(userId) + "&password=" + System.Web.HttpUtility.UrlEncode(strUserPassword);
                Dictionary<string, string> authLoginWebServiceInfo = new Dictionary<string, string>();
                authLoginWebServiceInfo.Add("service.name", "authlogin");
                authLoginWebServiceInfo.Add("service.method", "POST");
                authLoginWebServiceInfo.Add("service.data", postData);
                authLoginWebServiceInfo.Add("service.content-type", "application/x-www-form-urlencoded");
                authLoginWebServiceInfo.Add("service.auth", "false");
                authLoginWebServiceInfo.Add("service.userid", userId);
                strResponse = callWebService(authLoginWebServiceInfo);
            }
            catch(Exception exx)
            {
                log.Error("WebService(Authenticate)", exx);
                strResponse = "Exception";
            } 
            return (strResponse);
        }
        #endregion
    }
}
