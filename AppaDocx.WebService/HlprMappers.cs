﻿using System;
using System.Collections.Generic;
using System.Text;
using AppaDocx.Common.Models;
using Newtonsoft.Json;

namespace AppaDocx.WebService
{
    public partial class HlprWebService
    { 
        #region Conversion/Mapper Methods
        public List<DocumentStatusModel> loadDocumentStatusModelData(string strResponse)
        {
            List<DocumentStatusModel> lstDocumentStatusModel = new List<DocumentStatusModel>();
            try
            {
                lstDocumentStatusModel = JsonConvert.DeserializeObject<List<DocumentStatusModel>>(strResponse);
            }
            catch (Exception ex)
            {
                log.Error("loadDocumentStatusModelData", ex);
            } 
            return (lstDocumentStatusModel);
        }
        public List<T> ConvertToJSon<T>(string strResponse)
        {
            List<T> lstResult = null;
            try
            {
                lstResult = JsonConvert.DeserializeObject<List<T>>(strResponse);
            }
            catch (Exception ex)
            {
                log.Error("ConvertToJSon Cannot Convert" + ex.Message);
            }
            return (lstResult);
        } 
        public string ConvertToXmlCharacterReference(string xml)
        {
            StringBuilder sb = new StringBuilder(xml.Length);
            try
            {
                const char SP = '\u0020'; // anything lower than SP is a control character
                const char DEL = '\u007F'; // anything above DEL isn't ASCII, per se.

                foreach (char ch in xml)
                {
                    bool isPrintableAscii = ch >= SP && ch <= DEL;  // Detect standard ASCII characters

                    if (isPrintableAscii) { sb.Append(ch); }
                    else { sb.AppendFormat("&#x{0:X4};", (int)ch); }

                }
            }
            catch (Exception ex)
            {
                log.Error("Convert To XML Character", ex);
            }

            string instance = sb.ToString();
            return instance;
        }
        public List<DocumentNames> loadDocumentNames(string strResponse)
        {
            List<DocumentNames> lstDocumentNames = new List<DocumentNames>();
            try
            {
                lstDocumentNames = JsonConvert.DeserializeObject<List<DocumentNames>>(strResponse);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return (lstDocumentNames);
        }
        public List<DocumentTypeModel> loadDocumentTypeModelData(string strResponse)
        {
            List<DocumentTypeModel> lstDocumentTypeModel = new List<DocumentTypeModel>();
            try
            {
                lstDocumentTypeModel = JsonConvert.DeserializeObject<List<DocumentTypeModel>>(strResponse);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return (lstDocumentTypeModel);
        } 
        #endregion   
    }
}
