﻿using System;
using AppaDocx.Common.Helpers;
using CredentialManagement;

namespace AppaDocx.WebService
{
    public partial class HlprWebService
    {
        #region Core Vault Properties
        private string getPropertyFromVault(string propertyName)
        {
          
                using (credential = new Credential())
                {
                    try
                    {
                        credential.Target = APP_NAME + "-" + propertyName;
                        credential.Load();
                    }
                    catch (Exception ex)
                    {
                        log.Error("Get Vault Property", ex);
                    } 
                        return credential.Password.ToString();
                } 
        }
        private void setVaultProperty(string resource, string key, string value)
        {
            try
            {
                using (credential = new Credential())
                {
                    credential.Username = key;
                    credential.Password = value;
                    credential.Target = resource + "-" + key;
                    credential.Type = CredentialType.Generic;
                    credential.PersistanceType = PersistanceType.LocalComputer;
                    credential.Save();
                }
            }
            catch (Exception ex)
            {
                log.Error("Set Vault Property", ex);
            }

            //vault.Add(new Windows.Security.Credentials.PasswordCredential(resource, key, value));
        }
        private void removePropertyFromVault(string key)
        { 
            using (credential = new Credential())
            {
                try
                {
                    credential.Target = APP_NAME + "-" + key;
                    credential.Delete();
                }
                catch (Exception ex)
                {
                    log.Error("Get Vault Property", ex);
                }
            } 
        }
        #endregion

        #region Getter and Setters Methods
        public string getAccessToken()
        {
            string strAccessToken = getPropertyFromVault(TOKEN);
            string strData = "";
            try
            {
                if (strAccessToken != null && strAccessToken != "")
                {
                    strData = BEARER + strAccessToken;
                }
            }
            catch (Exception ex)
            {
                log.Error("Get Vault Property", ex);
            }
            return strData;
        }
        private string getGUID()
        {
            return getPropertyFromVault(GUID);
        } 
        private string getUserId()
        {
            return getPropertyFromVault(USERID);
        }
        public string getUserName()
        {
            return getUserId();
        }
        private void setAccessToken(string strResponse)
        {
            string strAccessToken = HlprToken.GetToken(strResponse, "ACCESS");
            setVaultProperty(APP_NAME, TOKEN, strAccessToken);  
        }
        private void setGUID(string guID)
        {
            setVaultProperty(APP_NAME, GUID, guID);
        }
        private void setUserId(string userId)
        {
            try
            {
                removePropertyFromVault(USERID);
                setVaultProperty(APP_NAME, USERID, userId);
            }
            catch (Exception ex)
            {
                log.Error("Set User ID", ex);
            }
        } 
        public void removeAccessToken()
        {
            removePropertyFromVault(TOKEN);
        } 
        public void removeUserId()
        {
            removePropertyFromVault(USERID);
        }
        #endregion
    }
}
