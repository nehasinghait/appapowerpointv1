﻿using System.Collections.Generic;

namespace AppaDocx.Common.Models
{
    public class HiddenDataMappedWithBookmark
    {
        public HiddenDataMappedWithBookmark()
        {
            ImgTagsVarsInComp = new List<string>();
        }

        public string bookmarkName { get; set; }
        public string DisplayVarValueInComp { get; set; }
        public List<string> ImgTagsVarsInComp { get; set; }
    }
}