﻿using System.Xml;

namespace AppaDocx.Common.Models
{
    /// <summary>
    /// <para>Class to store the Font and style name.  Needed to call Template.ApplyStyle when the txtbox is double clicked</para>
    /// </summary>
    public class ComponentXMLModel
    {
        private XmlNode xmlNode;

        public string id { get; set; }
        public XmlDocument data { get; set; }

        public ComponentXMLModel()
        {
        }

        //public ComponentXMLModel(XmlNode xmlNode)
        //{
        //    this.xmlNode = xmlNode;
        //    data = new XmlDocument();
        //    data.LoadXml(head.OuterXml);
        //}
        //public ComponentXML(XmlNode head)
        //{
        //    objComponentXMLModel.id = data.GetElementsByTagName("FELEMENTINSTANCEID")[0].InnerText;
        //    version = data.GetElementsByTagName("FVERSIONID")[0].InnerText;
        //    componentType = data.GetElementsByTagName("FELEMENT_ID")[0].InnerText;
        //    bool nameTag = data.GetElementsByTagName("FQUALDATA_DESC").Count == 0;
        //    name = nameTag ? data.GetElementsByTagName("FDATA_DESC")[0].InnerText : data.GetElementsByTagName("FQUALDATA_DESC")[0].InnerText;
        //    // component s initalized from ID fetch if it has an FDATA tag, rather then a FELEMENT_DETAIL tag
        //    bool fromIdFetch = data.GetElementsByTagName("FDATA").Count == 0;
        //    InitializeComponentXml(fromIdFetch ? "FELEMENT_DETAIL" : "FDATA");
        //    //InsertVaribleText();
        //    CreateWordML();   //Calling when we Click on Search Component Button....
        //}
    }
}