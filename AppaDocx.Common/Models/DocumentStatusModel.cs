﻿namespace AppaDocx.Common.Models
{
    /// <summary>
    /// <para>Class to store the Font and style name.  Needed to call Template.ApplyStyle when the txtbox is double clicked</para>
    /// </summary>
    public class DocumentStatusModel
    {
        public string StatusId { get; set; }
        public string StatusDescription { get; set; }
        public string DocumentStatusId { get; set; }
        public string DocumentStatusOrder { get; set; }
    }
}