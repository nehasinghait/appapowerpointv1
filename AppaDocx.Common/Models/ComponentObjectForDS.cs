﻿namespace AppaDocx.Common.Models
{
    /// <summary>
    /// <para>Class to store the Font and style name.  Needed to call Template.ApplyStyle when the txtbox is double clicked</para>
    /// </summary>
    public class ComponentObjectForDS
    {
        public ComponentObject objComponentObject { get; set; }
        public string strBookmarkId { get; set; }
        public string strWMLData { get; set; }

        public ComponentObjectForDS()
        {
            objComponentObject = new ComponentObject();
        }
    }
}