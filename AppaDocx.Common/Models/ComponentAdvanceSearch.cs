﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppaDocx.Common.Models
{
   public class ComponentAdvanceSearch
    {
        public List<clscontexts> contexts { get; set; }
        public List<clsfunds> funds { get; set; }
        public List<clsstatus> status { get; set; }
        public List<clslocales> locales { get; set; }
        public List<clsassetclasses> assetclasses { get; set; }
        public List<clscomponenttype> componentTypes { get; set; }
        
    }

    public class clscontexts
    {
        public string elementContextData { get; set; }
        public string elementContextDesc { get; set; }
        public string elementContextId { get; set; }
    }

    public class clsfunds
    {
        public string fundId { get; set; }
        public string assetClass { get; set; }
        public string fundInternalId { get; set; }
        public string fundName { get; set; }
    }

    public class clsstatus
    {
        public string statusId { get; set; }
        public string statusName { get; set; }
    }

    public class clslocales
    {
        public string localeId { get; set; }
        public string localeName { get; set; }
    }

    public class clsassetclasses
    {
        public string assetClassId { get; set; }
        public string assetClassName { get; set; }
    }

    public class clscomponenttype
    {
        public string componentTypeId { get; set; }
        public string componentTypeName { get; set; }
    }
}
