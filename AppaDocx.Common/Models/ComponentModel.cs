﻿using System.Drawing;

namespace AppaDocx.Common.Models
{
    /// <summary>
    /// <para>Class to store the Font and style name.  Needed to call Template.ApplyStyle when the txtbox is double clicked</para>
    /// </summary>
    public class ComponentModel   
    {
        public string CompID { get; set; }
        public string BookmarkName { get; set; }
        public string ChangedText { get; set; }
        public string qualDataXML { get; set; }
        public string ComponentName { get; set; }
        public string BookDetailId { get; set; }
        public string lastUpdatedBy { get; set; }
        public string CompType { get; set; }
    }
    
}