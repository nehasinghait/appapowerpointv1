﻿namespace AppaDocx.Common.Models
{
    public class DocumentNames
    {
        public int bookInstanceId { get; set; }
        public string bookStatus { get; set; }
        public string documentName { get; set; }

        public string documentType { get; set; }
    }

  
}