﻿
namespace AppaDocx.Common.Models
{
   public  class ComponentNames
    {

      
        public string FADDMORE { get; set; }
        public string FCOMPSTATUS { get; set; }

        public string FCOMPSTATUS_DESC { get; set; }

        public string FELEMENTINSTANCEID { get; set; }
        public string FELEMENT_DETAIL { get; set; }

        public string FELEMENT_ID { get; set; }

        public string FEXPIRATIONDATE { get; set; }
        public string FQUALDATA_DESC { get; set; }

    }
}
