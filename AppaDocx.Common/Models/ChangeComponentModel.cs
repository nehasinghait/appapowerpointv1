﻿using System;
using System.Windows;
using System.Windows.Input;

namespace AppaDocx.Common.Models
{
    public class ChangeComponentModel
    {
        public string elementInstanceId { get; set; }
        public string bookDetailId { get; set; }
        public string componentType { get; set; }
        public string bookDetailType { get; set; }
        public string ComponentName { get; set; }
        public string qualDataShadowId { get; set; }

    } 
}