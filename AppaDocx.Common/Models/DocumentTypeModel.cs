﻿namespace AppaDocx.Common.Models
{
    /// <summary>
    /// <para>Class to store the Font and style name.  Needed to call Template.ApplyStyle when the txtbox is double clicked</para>
    /// </summary>
    public class DocumentTypeModel
    {
        public string BookType { get; set; }
        public string BookTypeDisplay { get; set; }
        public char CreateIndicator { get; set; }
        public string BookTypeId { get; set; }
        public string Order { get; set; }
    }
}