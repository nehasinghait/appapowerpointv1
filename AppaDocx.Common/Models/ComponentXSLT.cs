﻿namespace AppaDocx.Common.Models
{
    /// <summary>
    /// <para>Class to store the Font and style name.  Needed to call Template.ApplyStyle when the txtbox is double clicked</para>
    /// </summary>
    public class ComponentXSLT
    {
        public string CId { get; set; }
        public string DName { get; set; }
        public string CType { get; set; }
    }//public class ComponentXSLT
}