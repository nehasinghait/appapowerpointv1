﻿using System.Collections.Generic;

namespace AppaDocx.Common.Models
{
    public class HiddenDataMappedWithBookmarkForDS
    {
        public HiddenDataMappedWithBookmarkForDS()
        {
            lstImgTagsVarsInDS = new List<string>();
            lstDisplayVarValueInDS = new List<string>();
        }

        public string bookmarkName { get; set; }
        public List<string> lstDisplayVarValueInDS { get; set; }
        public List<string> lstImgTagsVarsInDS { get; set; }
    }
}