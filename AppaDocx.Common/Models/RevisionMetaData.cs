﻿namespace AppaDocx.Common.Models
{
    /// <summary>
    /// <para>Class to store the Font and style name.  Needed to call Template.ApplyStyle when the txtbox is double clicked</para>
    /// </summary>
    public class RevisionMetaData
    {
        public string CompId { get; set; }
        public string BookmarkName { get; set; }
        public string DMLTextWithDelimeters { get; set; }
    }
}