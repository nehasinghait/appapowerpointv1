﻿using System.Collections.Generic;

namespace AppaDocx.Common.Models
{
    /// <summary>
    ///
    /// </summary>
    public class CommanRenderingModel
    {
        public int iDocumentId { get; set; }
        public List<ComponentObject> lstComponentObject { get; set; }
        public Dictionary<string, string> dicStartEndDelImgForEmb { get; set; }
        public Dictionary<string, string> dicStartEndDelImgForVar { get; set; }

        public CommanRenderingModel()
        {
            lstComponentObject = new List<ComponentObject>();
            dicStartEndDelImgForEmb = new Dictionary<string, string>();
            dicStartEndDelImgForVar = new Dictionary<string, string>();
        }
    }
}