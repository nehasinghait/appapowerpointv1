﻿namespace AppaDocx.Common.Models
{
    public class VariableDetail
    {
        public long variableId { get; set; }
        public long bookDetailId { get; set; }
        public long elementInstanceId { get; set; }
        public int sequenceId { get; set; }
        public int scopeId { get; set; }
        public int recursiveType { get; set; }
        public long recursiveParentDetailId { get; set; }
        public string value { get; set; }
    }
}