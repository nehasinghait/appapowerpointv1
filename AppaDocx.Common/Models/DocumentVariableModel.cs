﻿namespace AppaDocx.Common.Models
{
    /// <summary>
    /// <para>Class to store the Font and style name.  Needed to call Template.ApplyStyle when the txtbox is double clicked</para>
    /// </summary>
    public class DocumentVariableModel
    {
        public long variableId { get; set; }
        public long bookDetailId { get; set; }
        public long elementInstanceId { get; set; }
        public long sequenceId { get; set; }
        public long scopeId { get; set; }
        public long recursiveType { get; set; }
        public long recursiveParentDetailId { get; set; }
        public string value { get; set; }
    }
}