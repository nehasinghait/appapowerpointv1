﻿using System.Collections.Generic;

namespace AppaDocx.Common.Models
{
    /// <summary>
    /// <para>Class to store the Font and style name.  Needed to call Template.ApplyStyle when the txtbox is double clicked</para>
    /// </summary>
    public class ComponentObject
    {
        public string felement_ID { get; set; }
        public string fdata { get; set; }
        public string fglobalordinal { get; set; }
        public long felementinstanceid { get; set; }
        public int fcompstatus { get; set; }
        public string fcompstatus_DESC { get; set; }
        public int flanguage { get; set; }
        public string flanguage_DESC { get; set; }
        public long fbookdetailid { get; set; }
        public bool isrecursiveobj { get; set; }
    }

    public class components
    {
        public components()
        {
            lstComponentObject = new List<ComponentObject>();
        }

        public List<ComponentObject> lstComponentObject { get; set; }
    }
}