﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace AppaDocx.Common.Helpers
{
    public class HlprErrorHandling
    {
        public static string PATH { get; set; }
        public static LogType ConfiglgType { get; set; }
        public static string SourceName { get; set; }

      
        public static void writeLogToFile(string strMessage, string strDescription, string strTimeStamp = "")
        {
            switch (ConfiglgType)
            {
                case LogType.TextFile:

                    #region TextFile Processing

                    if (strMessage == "$$")
                        if (strDescription == "CLEAN")
                        {
                            File.WriteAllText(PATH, "");
                            return;
                        } 
                    File.AppendAllText(PATH, "[".AppendTimeStamp() + "] [" + strMessage + "]" + Environment.NewLine+ "|| " + strDescription + Environment.NewLine );

                    #endregion 

                    break;

                case LogType.EventLogger:

                    #region EventLogger Processing

                    var appLog = new System.Diagnostics.EventLog("Application");
                    appLog.Source = "MySource";
                    appLog.WriteEntry(strMessage + "||" + strDescription);

                    #endregion EventLogger Processing

                    break;

                case LogType.CSVFile:

                    #region CSV Processing

                    if (strMessage == "$$")
                        if (strDescription == "CLEAN")
                        {
                            try
                            {
                                File.WriteAllText(PATH, "");
                            }
                            catch (Exception EX)
                            {
                            }
                            return;
                        }
                    if (strTimeStamp == "")
                    {
                        try
                        {
                            strDescription = strDescription.Replace(",", "|");  //if comma found then replace with Pipe sign...
                            strMessage = strMessage.Replace(",", "|");           //if comma found then replace with Pipe sign...
                            File.AppendAllText(PATH, "".AppendTimeStamp() + ", " + strMessage + "," + strDescription + Environment.NewLine);
                        }
                        catch (Exception Ex)
                        {
                        }
                    }
                    else
                    {
                        try
                        {
                            File.AppendAllText(PATH, strTimeStamp + ", " + strMessage + "," + strDescription + Environment.NewLine);
                        }
                        catch (Exception Ex)
                        {
                        }
                    }

                    #endregion CSV Processing

                    break;

                default:
                    break;
            }
        }


        public static void WriteToLog(Exception e, string strMessage, string strDescription, string strTimeStamp = "", SByte isAlertRequiredFlag = 1)
        {
          
           var st = new StackTrace(e, true);
            var frames = st.GetFrames();
            var traceString = new StringBuilder();

            foreach (var frame in frames)
            {
                if (frame.GetFileLineNumber() < 1)
                    continue;
                string[] FileName = frame.GetFileName().Split(@"\");
                int count = FileName.Length - 1;
                traceString.Append("File: " + FileName[count]);
                traceString.Append(", Method:" + frame.GetMethod().Name);
                traceString.Append(", LineNumber: " + frame.GetFileLineNumber());
                traceString.Append(", ColumnNumber: " + frame.GetFileColumnNumber());
                traceString.AppendLine("");
            }


            if (isAlertRequiredFlag == 1)
            {
                switch (isAlertRequiredFlag)
                {
                    case 1:

                        MessageBox.Show( 
                                 //e.Message + "\n\n" +
                                  "An exception occurred while processing your request.",
                                 "AppaDocx",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
                        return;
                    case 0:
                        return;
                }
            }


            writeLogToFile(strMessage, traceString.ToString(), strTimeStamp);
        }

        public enum LogType
        {
            TextFile, EventLogger, Email,
            CSVFile
        }

        public enum FolderType
        {
            Environment
        }

        public static bool ConfigureLogging(LogType lgtype)
        {
            bool blnResult = false;

            switch (lgtype)
            {
                case LogType.TextFile:
                    //PATH = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\AppaDocxLogging.txt";
                    GlobalsData.dir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\AppaDocx\\";
                    if (!Directory.Exists(GlobalsData.dir))
                        Directory.CreateDirectory(GlobalsData.dir);
                    PATH = GlobalsData.dir + "\\AppaDocxLogging.txt";
                    if (File.Exists(PATH))
                        File.Delete(PATH);
                    ConfiglgType = LogType.TextFile;
                    blnResult = true;
                    break;

                case LogType.EventLogger:
                    ConfiglgType = LogType.EventLogger;
                    blnResult = true;
                    break;

                case LogType.CSVFile:
                    //PATH = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\AppaDocxLogging.csv";
                    GlobalsData.dir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)+"\\AppaDocx\\";
                    if (!Directory.Exists(GlobalsData.dir))
                        Directory.CreateDirectory(GlobalsData.dir);
                    PATH = GlobalsData.dir  + "\\AppaDocxLogging.csv";
                    //AppData\Local\Apps\2.0
                    //PATH =  + "\\AppaDocxLogging.csv";
                    if (File.Exists(PATH))
                        File.Delete(PATH);
                    ConfiglgType = LogType.CSVFile;
                    blnResult = true;
                    break;

                default:
                    break;
            }
            return (blnResult);
        }
    }
}