﻿using System;
using System.IO;

namespace AppaDocx.Common.Helpers
{
    public class HlprFolderFiles
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HlprFolderFiles));
        public string getSourceLocation()
        {
            //Ref:  https://robindotnet.wordpress.com/2010/07/11/how-do-i-programmatically-find-the-deployed-files-for-a-vsto-add-in/
            //Get the assembly information
            System.Reflection.Assembly assemblyInfo = System.Reflection.Assembly.GetExecutingAssembly();

            //Location is where the assembly is run from
            string assemblyLocation = assemblyInfo.Location;

            //CodeBase is the location of the ClickOnce deployment files
            Uri uriCodeBase = new Uri(assemblyInfo.CodeBase);
            string ClickOnceLocation = Path.GetDirectoryName(uriCodeBase.LocalPath.ToString());
            log.Info("ClickOnceLocation" + ClickOnceLocation);
            return (ClickOnceLocation);
        }

        public void CopyFromCurrentToDeployedDirectory(string SourcePath, string DestinationPath)
        {
          
            log.Info("SourcePath : " + SourcePath);
            try
            {
                //Now Create all of the directories
                foreach (string dirPath in Directory.GetDirectories(SourcePath, "*",
                    SearchOption.AllDirectories))
                    Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

                //Copy all the files & Replaces any files with the same name
                foreach (string newPath in Directory.GetFiles(SourcePath, "*.*",
                    SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);
            }
            catch (Exception ExCopyFromCurrentToDeployedDirectory)
            {
                //HlprErrorHandling.writeLogToFile("Exception:CopyFromCurrentToDeployedDirectory", ExCopyFromCurrentToDeployedDirectory.Message);
                log.Error("CopyFromCurrentToDeployedDirectory", ExCopyFromCurrentToDeployedDirectory);
            }
            log.Info("DestinationPath : " + DestinationPath);

        }

        public void CopyAllRequiredFilesAndDirectories(string strDIR, bool IsFeatureRequired)
        {
            if (IsFeatureRequired == true)
            {
                //var source = new DirectoryInfo(@"c:\AppaDocx\FullPre");
                //source.CopyTo(strDIR, true);
                log.Info("FullPre need to copy FullPre on " + strDIR);
            }
        }
    }
}