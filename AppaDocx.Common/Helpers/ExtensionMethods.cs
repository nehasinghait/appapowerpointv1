﻿//using Microsoft.Office.Interop.Word;
using AppaDocx.Common.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;

namespace AppaDocx.Common.Helpers
{
    public static class ExtensionMethods
    {
        #region File and Directory Extensions

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ExtensionMethods));

        //public static void insertHTML(this Range range, string html)
        //{
        //    string path = System.IO.Path.GetTempFileName();
        //    System.IO.File.WriteAllText(path, "<html>" + html); // must start with certain tag to be detected as html: <html> or <body> or <table> ...
        //    //range.InsertAfter(System.IO.File.ReadAllText(path));
        //    range.InsertFile(path, ConfirmConversions: false);
        //    System.IO.File.Delete(path);
        //}
        public static FileInfo GetNewestFile(DirectoryInfo directory)
        {
            return directory.GetFiles()
                .Union(directory.GetDirectories().Select(d => GetNewestFile(d)))
                .OrderByDescending(f => (f == null ? DateTime.MinValue : f.LastWriteTime))
                .FirstOrDefault();
        }

        public static DirectoryInfo GetNewestDirectory(string path)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            directory.Refresh();
            directory = directory.GetDirectories("*", SearchOption.AllDirectories).OrderByDescending(d => d.LastWriteTimeUtc).First();
            return (directory);
        }

        // Copies all files from one directory to another.
        public static void CopyTo(this DirectoryInfo source, string destDirectory, bool recursive)
        {
            try
            {
                if (source == null)
                    throw new ArgumentNullException("source");
                if (destDirectory == null)
                    throw new ArgumentNullException("destDirectory");
                // If the source doesn't exist, we have to throw an exception.
                if (!source.Exists)
                    throw new DirectoryNotFoundException(
                            "Source directory not found: " + source.FullName);
                // Compile the target.
                DirectoryInfo target = new DirectoryInfo(destDirectory);
                // If the target doesn't exist, we create it.
                if (!target.Exists)
                    target.Create();
                // Get all files and copy them over.
                foreach (FileInfo file in source.GetFiles())
                {
                    file.CopyTo(Path.Combine(target.FullName, file.Name), true);
                }
                // Return if no recursive call is required.
                if (!recursive)
                    return;
                // Do the same for all sub directories.
                foreach (DirectoryInfo directory in source.GetDirectories())
                {
                    CopyTo(directory,
                        Path.Combine(target.FullName, directory.Name), recursive);
                }
            } catch(Exception ex)
            {
                log.Error("Copy To", ex);
            }
        }

        #endregion File and Directory Extensions

        #region String with LINQ Functions

        //Truncates a string to a certain length
        public static string Truncate(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        //SubArray Like Splice
        /*
            int[] data = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int[] sub = data.SubArray(3, 4); // contains {3,4,5,6}
        */

        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        /*
            string text = " How are you and where are you?";
            var allIndexOf = text.AllIndexOf("you", StringComparison.OrdinalIgnoreCase);
            Console.WriteLine(string.Join(",", allIndexOf));  // 9,27
         */

        public static IList<int> AllIndexOf(this string text, string str, StringComparison comparisonType)
        {
            IList<int> allIndexOf = new List<int>();
            try
            {
                int index = text.IndexOf(str, comparisonType);
                while (index != -1)
                {
                    allIndexOf.Add(index);
                    index = text.IndexOf(str, index + str.Length, comparisonType);
                }
            }
            catch (Exception ex)
            {
                log.Error("Indexof", ex);
            }
            return allIndexOf;
        }

        private static IEnumerable<T> WithoutLast<T>(this IEnumerable<T> source)
        {
            using (var e = source.GetEnumerator())
            {
                if (e.MoveNext())
                {
                    for (var value = e.Current; e.MoveNext(); value = e.Current)
                    {
                        yield return value;
                    }
                }
            }
        }

        #endregion String with LINQ Functions

        #region String with Regular Expression

        public static string ReplaceWithRegularExpression(this string OriginalStr, string PatternStr, PatternType TypeOfPatternStr, string ReplacementStr)
        {
            string pattern;

            switch (TypeOfPatternStr)
            {
                case PatternType.ShortTag: pattern = "<" + PatternStr + ".*?/>"; break;
                case PatternType.LongTag: pattern = "<" + PatternStr + ">.*?</" + PatternStr + ">"; break;
                case PatternType.String: pattern = PatternStr; break;
                default: pattern = String.Empty; break;
            }

            return Regex.Replace(OriginalStr, pattern, ReplacementStr);
        }

        #endregion String with Regular Expression

        #region HTML Tags Operations

        public static string GetStrBetweenTags(this string value, string startTag, string endTag)
        {
            if (value.Contains(startTag) && value.Contains(endTag))
            {
                int index = value.IndexOf(startTag) + startTag.Length;
                return value.Substring(index, value.IndexOf(endTag) - index);
            }
            else
                return null;
        }

        public static string GetImgTagFromString(this string value)
        {
            int iImgStart = value.IndexOf("<img");
            int iImgEnd = value.IndexOf("/>");
            int lenImgEmbVar = iImgEnd - iImgStart;
            value = value.Substring(iImgStart, lenImgEmbVar + 2);
            return (value);
        }

        #endregion HTML Tags Operations

        #region Dictionary with Contains Extension Method for Feature

        //public static bool ContainsKeyPattern<T>(this Dictionary<string, T> nodes, string search)
        //{
        //    var return = Nodes.Keys.Any(k => Regex.Matches(search, k));
        //}
        //public static T GetItemByKeyPattern<T>(this Dictionary<string, T> dict, string search)
        //{
        //    return
        //        (from p in dict
        //         where System.Text.RegularExpressions.Regex.Matches(search, p.Key).Count>0
        //         select p.Value)
        //        .First();
        //}

        #endregion Dictionary with Contains Extension Method for Feature

        #region Date Time Extension Methods

        public static string AppendTimeStamp(this string strStringValueForConcatination)
        {
            return string.Concat(strStringValueForConcatination, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ssfff"));
        }

        #endregion Date Time Extension Methods

        #region String Manipulations Extension Methods

        /// <summary>
        /// Usage: "abcdefg012345".MultiInsert("-",2,5); // yields "abc-def-g012345"
        /// </summary>
        /// <param name="str"></param>
        /// <param name="insertChar"></param>
        /// <param name="positions"></param>
        /// <returns></returns>
        public static string MultiInsert(this string str, string insertChar, params int[] positions)
        {
            StringBuilder sb = new StringBuilder(str.Length + (positions.Length * insertChar.Length));
            var posLookup = new HashSet<int>(positions);
            for (int i = 0; i < str.Length; i++)
            {
                sb.Append(str[i]);
                if (posLookup.Contains(i))
                    sb.Append(insertChar);
            }
            return sb.ToString();
        }

        /// <summary>
        /// string strResult=YOURSTRING.getStringPiece(2,3);
        /// </summary>
        /// <param name="str"></param>
        /// <param name="iStartIndex"></param>
        /// <param name="iEndIndex"></param>
        /// <returns></returns>
        public static string getStringPiece(this string str, int iStartIndex, int iEndIndex)
        {
            string strPiece = "";
            int length = iEndIndex - iStartIndex;
            string piece = str.Substring(iStartIndex, length);
            return (strPiece);
        }

        /// <summary>
        /// List<int> indexes = "fooStringfooBar".AllIndexesOf("foo");
        /// </summary>
        /// <param name="str"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static List<int> AllIndexesOfLists(this string str, string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentException("the string to find may not be empty", "value");
            List<int> indexes = new List<int>();
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    return indexes;
                indexes.Add(index);
            }
        }

        public static IEnumerable<int> AllIndexesOf(this string str, string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentException("the string to find may not be empty", "value");
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    break;
                yield return index;
            }
        }

        /// <summary>
        /// Just a simple wrapper to simplify the process of splitting a string using another string as a separator
        /// </summary>
        /// <param name="s"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static string[] Split(this string s, string separator)
        {
                return s.Split(new string[] { separator }, StringSplitOptions.None);
        }

        #endregion String Manipulations Extension Methods

        #region XML Parsers Extension Methods

        public static XmlDocument ToXmlDocument(this XDocument xDocument)
        {
            var xmlDocument = new XmlDocument();
            using (var xmlReader = xDocument.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }
            return xmlDocument;
        }

        public static XDocument ToXDocument(this XmlDocument xmlDocument)
        {
            using (var nodeReader = new XmlNodeReader(xmlDocument))
            {
                nodeReader.MoveToContent();
                return XDocument.Load(nodeReader);
            }
        }

        #endregion XML Parsers Extension Methods
    }
}