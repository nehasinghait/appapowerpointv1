﻿using System;

namespace AppaDocx.Common.Helpers
{
    public class HlprThisAddInsConfig
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HlprThisAddInsConfig));

        public bool FeatureRequired(HlprFolderFiles objHlprFolderFiles, bool IsFeatureRequired, bool IsNetworkDeployed, bool IsFirstRun)
        {
            #region Create all needed subfolders on first time running the App
            System.Windows.Forms.MessageBox.Show("Before Network");
            
            if (IsNetworkDeployed)
            {
            System.Windows.Forms.MessageBox.Show("In Network");
                //Set the directory to the Data folder that the installer creates
                log.Info("IsNetworkDeployed Data Directory is " + AppaDocx.Common.GlobalsData.dir);
                //If this is the first time the add-in is run, then create needed folders and moves files to the correct location
                if (IsFirstRun)
                {
                    System.Windows.Forms.MessageBox.Show("Inside FirstRun");
                    #region If First Run then Copy All FullPre Folder & Files

                    try
                    {
                        string strSourceLocation = objHlprFolderFiles.getSourceLocation();
                        log.Info("Location dir after ClickonceLocation" + AppaDocx.Common.GlobalsData.dir);
                        objHlprFolderFiles.CopyFromCurrentToDeployedDirectory(strSourceLocation, AppaDocx.Common.GlobalsData.dir);

                        System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                        //The following line (part of the original answer) is misleading.
                        //**Do not** use it unless you want to return the System.Reflection.Assembly type's GUID.
                        Console.WriteLine(assembly.GetType().GUID.ToString());
                        // The following is the correct code.
                        var attribute = (System.Runtime.InteropServices.GuidAttribute)assembly.GetCustomAttributes(typeof(System.Runtime.InteropServices.GuidAttribute), true)[0];
                        string id = attribute.Value;
                        System.Windows.Forms.MessageBox.Show("First Time Application Running: " + id);


                    }
                    catch (Exception exFullPre)
                    {
                        //HlprErrorHandling.writeLogToFile("EXCEPTION", "Message: " + exFullPre.Message);
                        log.Error("FeatureRequired", exFullPre);
                    }
                    #endregion If First Run then Copy All FullPre Folder & Files
                    log.Info("IsFirstRun End if (System.Deployment.Application.ApplicationDeployment.CurrentDeployment.IsFirstRun)");

                    IsFeatureRequired = true;
                }//if (IsFirstRun)
            }

            #endregion Create all needed subfolders on first time running the App

            return IsFeatureRequired;
        }
    }
}