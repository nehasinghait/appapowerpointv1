﻿//using Word = Microsoft.Office.Interop.Word;
// it's required for reading/writing into the registry:
using Microsoft.Win32;
using System;
using System.IO;

// and for the MessageBox function:
using System.Windows.Forms;

namespace AppaDocx.Common.Helpers
{
    public class HlprRegistry
    {
        public RegistryKey baseRegistryKey;
        public string subKey;
        public bool showError;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HlprRegistry));

        public string Read(string KeyName)
        {
            // Opening the registry key
            RegistryKey rk = baseRegistryKey;
            // Open a subKey as read-only
            RegistryKey sk1 = rk.OpenSubKey(subKey);
            // If the RegistrySubKey doesn't exist -> (null)
            if (sk1 == null)
            {
                return null;
            }
            else
            {
                try
                {
                    // If the RegistryKey exists I get its value
                    // or null is returned.
                    return (string)sk1.GetValue(KeyName.ToUpper());
                }
                catch (Exception e)
                {
                    // AAAAAAAAAAARGH, an error!
                    ShowErrorMessage(e, "Reading registry " + KeyName.ToUpper());
                    return null;
                }
            }
        }

        public bool Write(string KeyName, object Value)
        {
            try
            {
                // Setting
                RegistryKey rk = baseRegistryKey;
                // I have to use CreateSubKey
                // (create or open it if already exits),
                // 'cause OpenSubKey open a subKey as read-only
                RegistryKey sk1 = rk.CreateSubKey(subKey);
                // Save the value
                sk1.SetValue(KeyName.ToUpper(), Value);

                return true;
            }
            catch (Exception e)
            {
                // AAAAAAAAAAARGH, an error!
                ShowErrorMessage(e, "Writing registry " + KeyName.ToUpper());
                return false;
            }
        }

        public bool DeleteKey(string KeyName)
        {
            try
            {
                // Setting
                RegistryKey rk = baseRegistryKey;
                RegistryKey sk1 = rk.CreateSubKey(subKey);
                // If the RegistrySubKey doesn't exists -> (true)
                if (sk1 == null)
                    return true;
                else
                    sk1.DeleteValue(KeyName);

                return true;
            }
            catch (Exception e)
            {
                // AAAAAAAAAAARGH, an error!
                ShowErrorMessage(e, "Deleting SubKey " + subKey);
                return false;
            }
        }

        public int SubKeyCount()
        {
            try
            {
                // Setting
                RegistryKey rk = baseRegistryKey;
                RegistryKey sk1 = rk.OpenSubKey(subKey);
                // If the RegistryKey exists...
                if (sk1 != null)
                    return sk1.SubKeyCount;
                else
                    return 0;
            }
            catch (Exception e)
            {
                // AAAAAAAAAAARGH, an error!
                ShowErrorMessage(e, "Retriving subkeys of " + subKey);
                return 0;
            }
        }

        public int ValueCount()
        {
            try
            {
                // Setting
                RegistryKey rk = baseRegistryKey;
                RegistryKey sk1 = rk.OpenSubKey(subKey);
                // If the RegistryKey exists...
                if (sk1 != null)
                    return sk1.ValueCount;
                else
                    return 0;
            }
            catch (Exception e)
            {
                // AAAAAAAAAAARGH, an error!
                ShowErrorMessage(e, "Retriving keys of " + subKey);
                return 0;
            }
        }

        private void ShowErrorMessage(Exception e, string Title)
        {
            if (showError == true)
                MessageBox.Show(e.Message,
                        Title
                        , MessageBoxButtons.OK
                        , MessageBoxIcon.Error);
        }

        public RegistryKey GetRegistryKey(string strPath)
        {
            RegistryKey reg = Registry.CurrentUser.CreateSubKey(strPath);
            return (reg);
        }

        public static void SearchSubKeys(RegistryKey root, string searchKey)
        {
            if (root == null)
            {
                return;
            }

            foreach (string keyname in root.GetSubKeyNames())
            {
                try
                {
                    using (RegistryKey key = root.OpenSubKey(keyname,
                        RegistryKeyPermissionCheck.ReadWriteSubTree,
                        System.Security.AccessControl.RegistryRights.FullControl))
                    {
                        if (key == null)
                        {
                            return;
                        }

                        if (keyname == searchKey || key.GetValue("ProductName").ToString().Contains(searchKey))
                        {
                            try
                            {
                                root.DeleteSubKeyTree(key.Name);

                                string logPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\FClassroom\Config\CleanRegistry.log";

                                using (StreamWriter sw = File.AppendText(logPath))
                                {
                                    sw.WriteLine(string.Format("{0},{1},{2}", keyname, key.Name, root.Name));
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        SearchSubKeys(key, searchKey);
                    }
                }
                catch (System.Security.SecurityException se)
                {
                    log.Error("Search Sub Key", se);
                }
                catch (Exception ex)
                {
                    log.Error("Search Sub Key", ex);
                }
            }
        }
    }//Class
}