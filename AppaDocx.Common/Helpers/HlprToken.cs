﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AppaDocx.Common.Helpers
{
    public class HlprToken
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HlprToken));
        public static string GetToken(string strResponse, string strTokenType)
        {
            string strToken = "";
            try
            {
                if (strTokenType == "ACCESS")
                {
                    string accessTokenStart = "successaccesstokenstart~~~~#####";
                    string accessTokenEnd = "accesstokenend~~~~#####";
                    int indexOfAccessTokenStart = strResponse.IndexOf(accessTokenStart) + accessTokenStart.Length;
                    if(strResponse.Contains(accessTokenEnd) != true)    //if AccessToken without end
                    {
                        int indexOfAccessTokenEnd = strResponse.IndexOf(accessTokenEnd) - accessTokenStart.Length;
                        strToken = strResponse.Substring(indexOfAccessTokenStart);
                    }
                    else
                    {
                        int indexOfAccessTokenEnd = strResponse.IndexOf(accessTokenEnd) - accessTokenStart.Length;
                        strToken = strResponse.Substring(indexOfAccessTokenStart, indexOfAccessTokenEnd);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                log.Error("GetToken", ex);
            }
            return (strToken);
        }
    }
}