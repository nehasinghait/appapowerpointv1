﻿using System.Collections.Generic;
using System.Web;
using System.Xml.Linq;
using System.Xml.XPath;

namespace AppaDocx.Common.Helpers
{
    public class HlprXMLParsing
    {
        public string strComponents { get; set; }
        private XElement elementRoot { get; set; }

        public HlprXMLParsing(string _Components)
        {
            strComponents = _Components;
        }

        public HlprXMLParsing()
        {
            strComponents = "";
        }

        public string GetNodeValueByName(string strElementName)
        {
            XDocument xdoc = XDocument.Parse(strComponents);
            elementRoot = xdoc.Element("components");
            elementRoot = elementRoot.Element("component");
            elementRoot = elementRoot.Element(strElementName);
            return (elementRoot.Value.ToString());
        }

        public void AppendElementAtTheEnd(string nameOfElement, string valueOfElement)
        {
            XDocument xdoc = XDocument.Parse(strComponents);
            var elementRoot = xdoc.Element("components");
            elementRoot = xdoc.Element("component");
            XElement d2 = new XElement(nameOfElement, valueOfElement);
            elementRoot.Add(d2);
            strComponents = elementRoot.ToString();
        }

        //private List<XElement> xEleKeyValuePairs { get; set; }
        public void AppendKeyValuePair(string strKeyValuePair)
        {
            strComponents = strComponents + "\n" + strKeyValuePair;
            //prepareLstxEleKeyValuePairs();
            //XElement d2 = new XElement("KEYVALUEPAIR", xEleKeyValuePairs);
            //elementRoot.Add(d2);
            //strComponents = elementRoot.ToString();
            strComponents = strComponents + "\n" + "</component>";
        }

        private void prepareLstxEleKeyValuePairs()
        {
        }

        public List<string> Step2SeparateComps(string strResult)
        {
            List<string> lstComponents = new List<string>();
            XDocument xdoc = XDocument.Parse(strResult);
            foreach (var childElem in xdoc.XPathSelectElements("child::*/component"))
            {
                lstComponents.Add(childElem.ToString());
            }
            return lstComponents;
        }

        private XElement GetElement(XDocument doc, string elementName)
        {
            foreach (XNode node in doc.DescendantNodes())
            {
                if (node is XElement)
                {
                    XElement element = (XElement)node;
                    if (element.Name.LocalName.Equals(elementName))
                        return element;
                }
            }
            return null;
        }

        public string CreateXmlNode(string name, string value)
        {
            if (value == "")
                return "<" + name + "/>";

            return "<" + name + ">" + HttpUtility.HtmlEncode(value) + "</" + name + ">";
        }
    }
}