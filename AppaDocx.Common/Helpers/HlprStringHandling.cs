﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AppaDocx.Common.Helpers
{
    public class HlprStringHandling
    {
        public string GetSerialNumber()
        {
            Guid serialGuid = Guid.NewGuid();
            string uniqueSerial = serialGuid.ToString("N");

            string uniqueSerialLength = uniqueSerial.Substring(0, 28).ToUpper();

            char[] serialArray = uniqueSerialLength.ToCharArray();
            string finalSerialNumber = "";

            int j = 0;
            for (int i = 0; i < 28; i++)
            {
                for (j = i; j < 4 + i; j++)
                {
                    finalSerialNumber += serialArray[j];
                }
                if (j == 28)
                {
                    break;
                }
                else
                {
                    i = (j) - 1;
                    finalSerialNumber += "-";
                }
            }

            return finalSerialNumber;
        }

        public static string GetNumberOfWords(string rangeText, int iNumberOfWords)
        {
            //char[] characters = "_ ".ToCharArray();
            //List<string> lstRangeText = rangeText.Split(characters).Take(iNumberOfWords).ToList();
            //rangeText = string.Join(" ", lstRangeText).ToString();
            if (rangeText.ToCharArray().Count() > 60)
            {
                rangeText = rangeText.Substring(0, 60);
            }
            return (rangeText);
        }


    }
}