﻿using AppaDocx.Common.Helpers;
using Microsoft.Win32;
using System;
using System.Deployment.Application;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace AppaDocx.Common
{
    public class ClickOnceHelper
    {
        private const string UninstallString = "UninstallString";
        private const string DisplayNameKey = "DisplayName";
        private const string UninstallStringFile = "UninstallString.bat";
        private const string ApprefExtension = ".appref-ms";
        private readonly RegistryKey UninstallRegistryKey;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ClickOnceHelper));

        private static string Location
        {
            get { return Assembly.GetExecutingAssembly().Location; }
        }

        public string UninstallFile { get; private set; }

        public ClickOnceHelper()
        {
            UninstallFile = "AppaDocx.Uninstall.exe";
        }

        public ClickOnceHelper(string publisherName, string productName)
        {
            try
            {
                //HlprErrorHandling.ConfigureLogging(HlprErrorHandling.LogType.CSVFile);
                //HlprErrorHandling.writeLogToFile("$$", "CLEAN");
                //HlprErrorHandling.writeLogToFile("Status", "Details", "TimeStamp");
                MessageBox.Show("Constructor Start");
                //HlprErrorHandling.writeLogToFile("Start", "clickOnceHelper Constructor Start");
                var publisherFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), GlobalsData.PublisherName);
                MessageBox.Show("PublisherFolder: " + publisherFolder);
                if (!Directory.Exists(publisherFolder))
                    Directory.CreateDirectory(publisherFolder);
                UninstallFile = Path.Combine(publisherFolder, UninstallStringFile);
                MessageBox.Show("UninstallFile: " + UninstallFile);
                UninstallRegistryKey = GetUninstallRegistryKeyByProductName(GlobalsData.ProductName);
                MessageBox.Show("Done ClickOnceHelper() Constructor... ");
                //HlprErrorHandling.writeLogToFile("End", "clickOnceHelper Constructor End");
            }
            catch (Exception ex)
            {
                //MessageBox.Show("ClickOnceHelper Constructor Error" + ex.Message);
                log.Error("clickOnceHelper", ex);
            }
        }

        #region Shortcut related

        private string GetShortcutPath()
        {
            var allProgramsPath = Environment.GetFolderPath(Environment.SpecialFolder.Programs);
            var shortcutPath = Path.Combine(allProgramsPath, GlobalsData.PublisherName);
            return Path.Combine(shortcutPath, GlobalsData.ProductName) + ApprefExtension;
        }

        private string GetStartupShortcutPath()
        {
            var startupPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            return Path.Combine(startupPath, GlobalsData.ProductName) + ApprefExtension;
        }

        public void AddShortcutToStartup()
        {
            if (!ApplicationDeployment.IsNetworkDeployed)
                return;
            var startupPath = GetStartupShortcutPath();
            if (File.Exists(startupPath))
                return;
            File.Copy(GetShortcutPath(), startupPath);
        }

        private void RemoveShortcutFromStartup()
        {
            log.Info("RemoveShortcutFromStartup Start -----------------RemoveShortcutFromStartup Start-----------------");
            var startupPath = GetStartupShortcutPath();
            if (File.Exists(startupPath))
                File.Delete(startupPath);
            log.Info("RemoveShortcutFromStartup End -----------------RemoveShortcutFromStartup End-----------------");
        }

        #endregion Shortcut related

        #region Update registry

        public string UpdateUninstallParameters()
        {
            if (UninstallRegistryKey == null)
                return "";
            log.Info("UninstallRegistryKey Object is not null...");
            string strResult = UpdateUninstallString();
            //UpdateDisplayIcon();
            //SetNoModify();
            //SetNoRepair();
            //SetHelpLink();
            //SetURLInfoAbout();
            return (strResult);
        }

        private RegistryKey GetUninstallRegistryKeyByProductName(string productName)
        {
            var subKey = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Uninstall");
            if (subKey == null)
                return null;
            foreach (var name in subKey.GetSubKeyNames())
            {
                var application = subKey.OpenSubKey(name, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.QueryValues | RegistryRights.ReadKey | RegistryRights.SetValue);
                if (application == null)
                    continue;
                foreach (var appKey in application.GetValueNames().Where(appKey => appKey.Equals(DisplayNameKey)))
                {
                    if (application.GetValue(appKey).Equals(productName))
                        return application;
                    break;
                }
            }
            return null;
        }

        private string UpdateUninstallString()
        {
            string strLocation = "";
            try
            {
                var uninstallString = (string)UninstallRegistryKey.GetValue(UninstallString);
                MessageBox.Show("UpdateURLString Registry Value in uninstallString:" + uninstallString);
                if (!String.IsNullOrEmpty(UninstallFile) && uninstallString.StartsWith("rundll32.exe"))
                    File.WriteAllText(UninstallFile, uninstallString);
                log.Info("UninstallFile.bat File: " + uninstallString);
                strLocation = getFolderPath(Location);
                log.Info("strLocation: " + strLocation);

                //C:\Users\Administrator\AppData\Local\assembly\dl3\CARTKRA3.104\3H491J6H.R45\c68b9b25\1967a56c_18b2d201\"\AppaDocx.Uninstall.exe" uninstall
                var str = String.Format("\"" + strLocation + "{0}\" uninstall", Path.Combine(Path.GetDirectoryName(Location), "\\" + GlobalsData.UninstallFileName));
                str = strLocation + GlobalsData.UninstallFileName + " uninstall";
                log.Info("str: " + str);
                UninstallRegistryKey.SetValue(UninstallString, str);
                log.Info("UpdateUninstallString() Done: strLocation:" + strLocation);
            }
            catch (Exception ex)
            {
                //HlprErrorHandling.writeLogToFile("UpdateUninstallString() Error:", "Error:" + ex.Message);
                log.Error("Uninstall String", ex);
            }
            return (strLocation);
        }

        public string getFolderPath(string strLocation)
        {
            string strData = "";
            int iLastLength = strLocation.Split('\\').Length - 1;
            foreach (var item in strLocation.Split('\\'))
            {
                if (strLocation.Split('\\')[iLastLength] != item)
                {
                    strData += item + "\\";
                }
                else
                {
                    break;
                }
            }
            return (strData);
        }

        private void UpdateDisplayIcon()
        {
            var str = String.Format("{0},0", Path.Combine(Path.GetDirectoryName(Location), "uninstall.exe"));
            UninstallRegistryKey.SetValue("DisplayIcon", str);
        }

        private void SetNoModify()
        {
            UninstallRegistryKey.SetValue("NoModify", 1, RegistryValueKind.DWord);
        }

        private void SetNoRepair()
        {
            UninstallRegistryKey.SetValue("NoRepair", 1, RegistryValueKind.DWord);
        }

        private void SetHelpLink()
        {
            UninstallRegistryKey.SetValue("HelpLink", GlobalsData.HelpLink, RegistryValueKind.String);
        }

        private void SetURLInfoAbout()
        {
            UninstallRegistryKey.SetValue("URLInfoAbout", GlobalsData.AboutLink, RegistryValueKind.String);
        }

        #endregion Update registry

        #region uninstall

        public void Uninstall(string strPreviousVSTOPath = "file:///D:/AppaDocx/CurrentPublish/PostDeployCustomTool/V_1_0_0_4/DocuBuilder.W2K13.Application.vsto")
        {
            try
            {
                MessageBox.Show("-----------------Uninstall " + GlobalsData.ProductName + " Start-----------------");
                //kill all processes with ProductName=DocuBuilder.W2K13.Application
                foreach (var process in Process.GetProcessesByName(GlobalsData.ProductName))
                {
                    process.Kill();
                    break;
                }

                #region Working But not required...

                //var publisherFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), GlobalsData.PublisherName);
                //if (UninstallFile == "")
                //{
                //    UninstallFile = Path.Combine(publisherFolder, UninstallStringFile);
                //    MessageBox.Show("UninstallFile==EMPTY: " + UninstallFile);
                //}
                //MessageBox.Show("Condition Start"+ " if (!File.Exists(UninstallFile)) UninstallFile:"+UninstallFile);
                //if (!File.Exists(publisherFolder+"\\"+UninstallFile))
                //{
                //    MessageBox.Show("UninstallStringFile: " + UninstallStringFile);
                //    UninstallFile = Path.Combine(publisherFolder, UninstallStringFile);
                //    try
                //    {
                //        //UninstallFile=C:\Users\Administrator\AppData\Local\DocuBuilder Word 2013 AddIn\UninstallString.bat
                //        if (File.Exists(UninstallFile))
                //            System.Diagnostics.Process.Start(UninstallFile);
                //        else
                //            MessageBox.Show("UninstallFile File Not Exits: " + UninstallFile);
                //    }
                //    catch (Exception exUninstallString)
                //    {
                //        MessageBox.Show("Error: " + exUninstallString.Message);
                //    }
                //    MessageBox.Show("Done");
                //}
                //else
                //{
                //    try
                //    {
                //        //UninstallFile=C:\Users\Administrator\AppData\Local\DocuBuilder Word 2013 AddIn\UninstallString.bat
                //        if (File.Exists(UninstallFile))
                //        {
                //            System.Diagnostics.Process.Start(UninstallFile);
                //        }
                //        else
                //            MessageBox.Show("UninstallFile File Not Exits: " + UninstallFile);
                //    }
                //    catch (Exception exUninstallString)
                //    {
                //        MessageBox.Show("Error: " + exUninstallString.Message);
                //    }
                //}

                #endregion Working But not required...

                CleanManualTasks(strPreviousVSTOPath);
                MessageBox.Show("INFO" + "Process Started...");
                RespondToClickOnceRemovalDialog();
                MessageBox.Show("Uninstall Success" + "-----------------Uninstall Success-----------------");
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Uninstall() method Error" + "Error: " + ex.Message);
                log.Error("Uninstall Method", ex);
            }
            MessageBox.Show("Uninstall End" + "-----------------Uninstall End-----------------");
        }

        private void CleanManualTasks(string strVSTOLocation)
        {
            //1) Run Commands in Batch File rundll32 dfshim CleanOnlineAppCache
            Process proc = null;
            try
            {
                //C:\Program Files\Common Files\Microsoft Shared\VSTO\10.0\VSTOInstaller.exe /Uninstall file:///D:/AppaDocx/CurrentPublish/PostDeployCustomTool/V_1_0_0_4/DocuBuilder.W2K13.Application.vsto
                string batDir = string.Format(@"C:\Users\Administrator\AppData\Local\DocuBuilder Word 2013 AddIn");
                MessageBox.Show("CleanManualTasks Started With batDir is: " + batDir);
                string strCompleteBatchPath = batDir + "\\UninstallString.bat";
                if (!File.Exists(strCompleteBatchPath))
                {
                    StringBuilder sb = new StringBuilder();
                    string strContents = "rundll32 dfshim CleanOnlineAppCache";
                    sb.AppendLine(strContents);
                    strContents = @"C:\Program Files\Common Files\Microsoft Shared\VSTO\10.0\VSTOInstaller.exe /Uninstall " + strVSTOLocation;
                    sb.AppendLine(strContents);
                    File.WriteAllText(strCompleteBatchPath, sb.ToString());
                }
                proc = new Process();
                proc.StartInfo.WorkingDirectory = batDir;
                proc.StartInfo.FileName = "UninstallString.bat";
                proc.StartInfo.CreateNoWindow = false;
                proc.Start();
                proc.WaitForExit();
                MessageBox.Show("Bat file executed !!");
            }
            catch (Exception ExBatchExecution)
            {
                log.Error("Batch Execution", ExBatchExecution);
                //MessageBox.Show("ExBatchExecution: " + ExBatchExecution.Message);
                return;
            }
            //2) Delete All Files inside  Location
            deleteDirAndSubDir(@"%userprofile%\AppData\Local\Apps\2.0\");
            //3) Clearn Registry
            CleanAllRegistryEntries();
        }//CleanManualTasks

        public void deleteDirAndSubDir(string strpath)
        {
            var filePath = Environment.ExpandEnvironmentVariables(strpath);

            if (Directory.Exists(filePath))
            {
                RemoveDirectories(filePath);
            }
        }

        private void RemoveDirectories(string strpath)
        {
            //This condition is used to delete all files from the Directory
            foreach (string file in Directory.GetFiles(strpath))
            {
                File.Delete(file);
            }
            //This condition is used to check all child Directories and delete files
            foreach (string subfolder in Directory.GetDirectories(strpath))
            {
                RemoveDirectories(subfolder);
            }
            Directory.Delete(strpath);
        }

        private void CleanAllRegistryEntries()
        {
            bool isRecordDeleted = false;
            MessageBox.Show(@"Before HKEY_CURRENT_USER\SOFTWARE\Microsoft\VSTA\Solutions\");

            #region Working  //HKEY_CURRENT_USER\SOFTWARE\Microsoft\VSTA\Solutions\

            using (RegistryKey regFoundKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\VSTA\Solutions", true))
            {
                foreach (var item in regFoundKey.GetSubKeyNames())
                {
                    RegistryKey reg1 = regFoundKey.OpenSubKey(item);
                    if (reg1.GetValueNames().Contains("ProductName"))
                    {
                        if (reg1.GetValue("ProductName").Equals("DocuBuilder Word 2013 AddIn"))
                        {
                            regFoundKey.DeleteSubKeyTree(item);
                            isRecordDeleted = true;
                        }
                    }
                }
            }

            #endregion Working  //HKEY_CURRENT_USER\SOFTWARE\Microsoft\VSTA\Solutions\

            MessageBox.Show("After " + isRecordDeleted + @"HKEY_CURRENT_USER\SOFTWARE\Microsoft\VSTA\Solutions\");
            isRecordDeleted = false;
            MessageBox.Show(@"Before HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\16.0\Word\AddInLoadTimes\");

            #region Delete VSTO Entry //HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\16.0\Word\AddInLoadTimes

            using (RegistryKey regFoundKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Office\16.0\Word\AddInLoadTimes", true))
            {
                foreach (var item in regFoundKey.GetValueNames())
                {
                    if (regFoundKey.GetValueNames().Where(t => t.Contains("DocuBuilder.W2K13.Application")).FirstOrDefault() == "DocuBuilder.W2K13.Application")
                    {
                        if (item == "DocuBuilder.W2K13.Application")
                        {
                            regFoundKey.DeleteValue(item);
                            isRecordDeleted = true;
                        }
                    }
                }
            }

            #endregion Delete VSTO Entry //HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\16.0\Word\AddInLoadTimes

            MessageBox.Show("After " + isRecordDeleted + @"HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\16.0\Word\AddInLoadTimes\");
            isRecordDeleted = false;
            MessageBox.Show(@"Before HKEY_CURRENT_USER\SOFTWARE\Microsoft\VSTO\SolutionMetadata");

            #region HKEY_CURRENT_USER\SOFTWARE\Microsoft\VSTO\SolutionMetadata

            using (RegistryKey regFoundKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\VSTO\SolutionMetadata", true))
            {
                foreach (var item in regFoundKey.GetSubKeyNames())
                {
                    RegistryKey reg1 = regFoundKey.OpenSubKey(item);
                    if (reg1.GetValueNames().Where(t => t.Contains("addInName")).Count() >= 1)
                    {
                        if (reg1.GetValue("addInName").ToString() == "DocuBuilder.W2K13.Application")
                        {
                            regFoundKey.DeleteSubKey(item);
                            isRecordDeleted = true;
                        }
                    }
                }//foreach
            }//using

            #endregion HKEY_CURRENT_USER\SOFTWARE\Microsoft\VSTO\SolutionMetadata

            MessageBox.Show("After " + isRecordDeleted + @"HKEY_CURRENT_USER\SOFTWARE\Microsoft\VSTO\SolutionMetadata");
            isRecordDeleted = false;
            MessageBox.Show(@"Before HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");

            #region Uninstall String //HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall

            using (RegistryKey regFoundKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", true))
            {
                foreach (var item in regFoundKey.GetSubKeyNames())
                {
                    RegistryKey reg1 = regFoundKey.OpenSubKey(item);
                    if (reg1.GetValue("DisplayName").ToString().Equals("DocuBuilder Word 2013 AddIn"))
                    {
                        regFoundKey.DeleteSubKey(item);
                        isRecordDeleted = true;
                    }//if (reg1.GetValue("DisplayName").ToString().Equals("DocuBuilder Word 2013 AddIn"))
                }//foreach
            }//using

            #endregion Uninstall String //HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall

            MessageBox.Show("After " + isRecordDeleted + @"HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
            isRecordDeleted = false;
            MessageBox.Show(@"Before HKEY_USERS\S-1-5-21-2233629010-476243186-1967406168-500\SOFTWARE\Microsoft\Office\16.0\Word\AddInLoadTimes\DocuBuilder.W2K13.Application");

            #region HUser Key //HKEY_USERS\S-1-5-21-2233629010-476243186-1967406168-500\SOFTWARE\Microsoft\Office\16.0\Word\AddInLoadTimes\DocuBuilder.W2K13.Application

            //startWith("S-1-5-21-") and !endWith("_Classes")
            foreach (var item in Registry.Users.GetSubKeyNames())
            {
                if (item.StartsWith("S-1-5-21") && (!item.EndsWith("_Classes")))
                {
                    using (RegistryKey regFoundKey = Registry.Users.OpenSubKey(item + @"\SOFTWARE\Microsoft\Office\16.0\Word\AddInLoadTimes", true))
                    {
                        if (regFoundKey.GetValueNames().Where(t => t.Contains("DocuBuilder.W2K13.Application")).Count() >= 1)
                        {
                            regFoundKey.DeleteValue("DocuBuilder.W2K13.Application");
                            isRecordDeleted = true;
                        }
                    }//using
                }
            }

            #endregion HUser Key //HKEY_USERS\S-1-5-21-2233629010-476243186-1967406168-500\SOFTWARE\Microsoft\Office\16.0\Word\AddInLoadTimes\DocuBuilder.W2K13.Application

            MessageBox.Show("After " + isRecordDeleted + @"HKEY_USERS\S-1-5-21-2233629010-476243186-1967406168-500\SOFTWARE\Microsoft\Office\16.0\Word\AddInLoadTimes\DocuBuilder.W2K13.Application");
            isRecordDeleted = false;
        }//CleanAllRegistryEntries()

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool PostMessage(IntPtr hWnd, [MarshalAs(UnmanagedType.U4)] uint Msg, IntPtr wParam, IntPtr lParam);

        private const int WM_KEYDOWN = 0x0100;
        private const int WM_KEYUP = 0x0101;

        private void RespondToClickOnceRemovalDialog()
        {
            MessageBox.Show("RespondToClickOnceRemovalDialog() Start");
            var myWindowHandle = IntPtr.Zero;
            for (var i = 0; i < 250 && myWindowHandle == IntPtr.Zero; i++)
            {
                Thread.Sleep(150);
                foreach (var proc in Process.GetProcessesByName("dfsvc"))
                    if (!String.IsNullOrEmpty(proc.MainWindowTitle) && proc.MainWindowTitle.StartsWith(GlobalsData.ProductName))
                    {
                        myWindowHandle = proc.MainWindowHandle;
                        break;
                    }
            }
            if (myWindowHandle == IntPtr.Zero)
                return;

            SetForegroundWindow(myWindowHandle);
            Thread.Sleep(100);
            const uint wparam = 0 << 29 | 0;

            PostMessage(myWindowHandle, WM_KEYDOWN, (IntPtr)(Keys.Shift | Keys.Tab), (IntPtr)wparam);
            //PostMessage(myWindowHandle, WM_KEYUP, (IntPtr)(Keys.Shift | Keys.Tab), (IntPtr)wparam);
            PostMessage(myWindowHandle, WM_KEYDOWN, (IntPtr)(Keys.Shift | Keys.Tab), (IntPtr)wparam);
            //PostMessage(myWindowHandle, WM_KEYUP, (IntPtr)(Keys.Shift | Keys.Tab), (IntPtr)wparam);

            PostMessage(myWindowHandle, WM_KEYDOWN, (IntPtr)Keys.Down, (IntPtr)wparam);
            //PostMessage(myWindowHandle, WM_KEYUP, (IntPtr)Keys.Down, (IntPtr)wparam);

            PostMessage(myWindowHandle, WM_KEYDOWN, (IntPtr)Keys.Tab, (IntPtr)wparam);
            //PostMessage(myWindowHandle, WM_KEYUP, (IntPtr)Keys.Tab, (IntPtr)wparam);

            PostMessage(myWindowHandle, WM_KEYDOWN, (IntPtr)Keys.Enter, (IntPtr)wparam);
            //PostMessage(myWindowHandle, WM_KEYUP, (IntPtr)Keys.Enter, (IntPtr)wparam);
            MessageBox.Show("RespondToClickOnceRemovalDialog() End");
        }

        #endregion uninstall
    }
}