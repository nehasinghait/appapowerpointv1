﻿//using Word = Microsoft.Office.Interop.Word;
// it's required for reading/writing into the registry:
// and for the MessageBox function:

namespace AppaDocx.Common.Enums
{
    public enum PatternType
    {
        String,
        ShortTag,
        LongTag
    }

    public enum RevisionOperations
    {
        AcceptAll,
        AcceptSingle,
        RejectAll,
        RejectSingle
    }
    public enum ClearAllType
    {
        ButtonClearAll,
        DoubleClickClearAll,
        SignOutClearAll 
    }
}