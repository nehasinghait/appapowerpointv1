﻿namespace AppaDocx.Common.Enums
{
    /// <summary>
    /// <para> </para>
    /// </summary>
    public enum ComponentType
    {
        #region ComponentType

        Bridgehead = 0,
        Entity,
        Footnote,
        Graphic,
        Note,
        Table,
        Text

        #endregion ComponentType
    }//public enum ComponentType

    public enum TypeofSearch
    {
        #region TypeofSearch

        ComponentSearch = 0,
        DocumentSearch = 1

        #endregion TypeofSearch
    }//public enum TypeofSearch

    public enum ContentControlType
    {
        #region ContentControlType 
        VariableTypeContentControl = 0,
        FootnoteTypeContentControl= 1,
        EmbeddedTypeContentControl=2 
        #endregion ContentControlType
    }//public enum ContentControlType


}//namespace DocuBuilder.W2K13.Application.Helper.Components