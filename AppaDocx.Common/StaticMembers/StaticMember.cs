﻿using System.Xml.Xsl;

namespace AppaDocx.Common.StaticMembers
{
    public class StaticMember
    {
        public static XslCompiledTransform PlainTextTransform = new XslCompiledTransform(enableDebug: true);
        public static XslCompiledTransform WordMLTransform = new XslCompiledTransform(enableDebug: true);
        public static XslCompiledTransform DocBookTransform = new XslCompiledTransform(enableDebug: true);
    }
}