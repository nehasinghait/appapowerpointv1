﻿using AppaDocx.Common.Enums;
using AppaDocx.Common.Models;
using System.Collections.Generic;
using System.Web;

namespace AppaDocx.Common
{
    //These parameters should be read from some config in real applciation
    //Here they're just hard coded
    public class GlobalsData
    {
        //Bools to remember if a process is still going on, so not to trigger two processes at once
        public static bool isUpdating;

        public static bool isTemplateLoading;
        public static List<DocumentVariableModel> lstDocumentVariableModel { get; set; }

        public static string dir { set; get; }

        //satish added
        public static string rootDir { set; get; }
        public static TypeofSearch CurrentSearchType { set; get; }
        public static string strBasePath { get; set; }
        public static Dictionary<string, string> dicBkmrkBasedImages { get; set; }
        public static Dictionary<string, string> dicBkmrkEmbVarBasedImg { get; set; }
        public static Dictionary<string, string> dicBkmrkEmbBindWithImg { get; set; }
        public static Dictionary<string, string> dicBkmrkWithText { get; set; }
        public static Dictionary<KeyValuePair<string, string>, string> dicVariableKeyValue { get; set; }
        public static string SlugLine { get; set; }
        public static string CurrentChangedCompName{ get; set; }
        public static string SelectedDocumentName { get; set; }
        public static bool IsLogin { get; set; }
        public static bool IsDeveloperMode { get; set; }
        public static bool IsTaskOpen { get; set; }
        public static string PublisherName
        {
            get { return "AppaDocx AddIn"; }
        }

        public static string ProductName
        {
            get { return "AppaDocx AddIn"; }
        }

        public static string Host
        {
            get { return "http://DocuBuilder.Appatura.com/"; }
        }

        public static string HelpLink
        {
            get { return "http://DocuBuilder.Appatura.com/help"; }
        }

        public static string AboutLink
        {
            get { return "http://DocuBuilder.Appatura.com/about"; }
        }

        public static string UninstallFileName
        {
            get { return "AppaDocx.Uninstall.exe"; }
        }


        public static string CreateXmlNode(string name, string value)
        {
            if (value == "")
                return "<" + name + "/>";

            return "<" + name + ">" + HttpUtility.HtmlEncode(value) + "</" + name + ">";
        }
    }//public class GlobalsData
}