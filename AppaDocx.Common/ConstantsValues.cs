﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppaDocx.Common.Models;

namespace AppaDocx.Common
{
    public static class ConstantsValues
    {
        public static string CurrentWebServiceHomeURL
        {
            get
            {
                string HOMEURL = "";
                HOMEURL = ConfigurationManager.AppSettings["HOMEURL"];
                return (HOMEURL);
            }
        }
        public static string CurrentWebServiceUserName {
            get; set;
        }
        public static string CurrentCompanyName
        {
            get
            {
                string CompanyName = ConfigurationManager.AppSettings["COMPANYNAME"];
                return (CompanyName);
            }
        }
        public static string CurrentWebServicePassword
        {
            get
            {
                string PASSWORD = ConfigurationManager.AppSettings["PASSWORD"];
                return (PASSWORD);
            }
        }
        public static string CurrentClientID {
            get
            {
                string CLIENTID = ConfigurationManager.AppSettings["CLIENTID"];
                return (CLIENTID);
            }
        }

        #region Variable Symbol Constants
        public static string VariableStart
        {
            get
            {
                string strVariableStart = ConfigurationManager.AppSettings["VARIABLE_START"];
                return (strVariableStart);
            }
        }

        public static string FootnoteStart
        {
            get
            {
                string strVariableStart = ConfigurationManager.AppSettings["FOOTNOTE_START"];
                return (strVariableStart);
            }
        }
        public static string FootnoteEnd
        {
            get
            {
                string strVariableStart = ConfigurationManager.AppSettings["FOOTNOTE_END"];
                return (strVariableStart);
            }
        }

        public static string VariableEnd
        {
            get
            {
                string strVariableEnd = ConfigurationManager.AppSettings["VARIABLE_END"];
                return (strVariableEnd);
            }
        }
        public static char VariableSeparator
        {
            get
            {
                char strVariableEnd = ConfigurationManager.AppSettings["VARAIBLE_SEPARATOR"][0];
                return (strVariableEnd);
            }
        }
        public static char FootnoteSeparator
        {
            get
            {
                char strVariableEnd = ConfigurationManager.AppSettings["FOOTNOTE_SEPARATOR"][0];
                return (strVariableEnd);
            }
        }
        public static string BookmarkSeparator
        {
            get
            {
                string strBookmark = ConfigurationManager.AppSettings["BOOKMARK_SEPARATOR"];
                return (strBookmark);
            }
        }
        #endregion

        public static Dictionary<string, Dictionary<string, ComponentModel>> componentMetaDataDictionary { get; set; }
        public static Dictionary<string, string> revisioncomponentData { get; set; }
        public static List<ChangeComponentModel> lstComponentSelected;
        public static List<ChangeComponentModel> lstNoChangeComponentModel;

        #region Saving Path Location
        public static int CurrentDocumentId { get; set; }
        public static string ProofCount { get; set; }
        public static int CurrentChangeComponentId { get; set; }
        public static string CurrentChangeComponentType { get; set; }
        public static string CurrentDocumentStatus { get; set; }

        public static List<ComponentNames> lstComponentAddedtoCart;
        public static bool IsComponentSearchSelectedFlag { get; set; }
        #endregion

        #region RevisionDisplay Settings
        public static int ParentNodesDisplaySize
        {
            get
            {
                string _ParentNodesDisplaySize = ConfigurationManager.AppSettings["PARENT_NODE_DISPLAY_SIZE"];
                int _iParentNodesDisplaySize = Convert.ToInt32(_ParentNodesDisplaySize);
                return (_iParentNodesDisplaySize);
            }
        }
        public static int ChildNodesDisplaySize
        {
            get
            {
                string _ChildNodesDisplaySize = ConfigurationManager.AppSettings["CHILD_NODE_DISPLAY_SIZE"];
                int _iChildNodesDisplaySize = Convert.ToInt32(_ChildNodesDisplaySize);
                return (_iChildNodesDisplaySize);
            }
        }

        public static bool IsUserClickedRevisionTabButton { get; set; }
        public static bool IsSavedDocumentOpen { get; set; }
        #endregion
        public static string documentType
        {
            get
            {
                string documentType = ConfigurationManager.AppSettings["DOCUMENTTYPE"];
                return documentType;
            }
        }

    }
}
